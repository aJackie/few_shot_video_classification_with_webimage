# Google Helps YouTube: Learning Few-Shot Video Classification from Historic Tasks and Cross-Domain Sample Transfer
This repo includes the core code of our ICMR paper "Google Helps YouTube: Learning Few-Shot Video Classification from Historic Tasks and Cross-Domain Sample Transfer"[link](https://dl.acm.org/doi/abs/10.1145/3372278.3390687)

## File sturcture
- /src includes the code to reproduce our method and several competitor methods, for our method specifically, check /src/meta_instance_reweight
- /data includes the scripts for processing the data
- /src_trial is a deprecated folder that includes code for our ealier experiment

