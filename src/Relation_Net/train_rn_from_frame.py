""" 
    training relation model from meta-training video frames
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.utils.model_zoo as model_zoo
import torchvision.models as models
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

from data_generator import FrameMetaTrainDataset, FrameMetaValDataset, FrameListFileTestFolder
from utils import *
from model import RelationNet
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training relation-net model from few-shot training videos",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-em', "--embedding_model", type=str, required=True, help="embedding model architects,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-p', "--pretrain", type=str, default=None, \
                    help="pretrained embeddingmodel path, if 'ImageNet' then use ImageNet pretrained weight")
parser.add_argument('-r', "--resume", type=str, default=None, \
                    help="path to checkpoint file")
parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size in one episode")
parser.add_argument('-m', '--max_steps', default=1000, type=int, help="max batches in one epoch")
parser.add_argument('-s', '--size', type=int, default=224, help="input square image edge length")
parser.add_argument('-w', '--workers', type=int, default=4, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")
parser.add_argument('-lr', '--learning_rate', default=10e-2, type=float, \
                    help="initial learning rate")
parser.add_argument('--start_epoch', default=0, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=100, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=5, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=5, type=int, help="frequency(of epoches) of doing validation")
parser.add_argument("--frame_dir", type=str, required=True, help="root dir of all frames")
parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing training class list")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing validation class list")
parser.add_argument("--train_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                           of training data")
parser.add_argument("-k", "--kshot", type=int, required=True, help="k-shot learning")
parser.add_argument("-qk", "--query_kshot", type=int, required=True, help="query k-shot")
parser.add_argument("-c", "--class_num", type=int, required=True, help="c-way classification")
parser.add_argument("-s1", "--shot1", type=int, required=True, help="repeat time of samples in one class to be pooled in support")
parser.add_argument("-s2", "--shot2", type=int, required=True, help="repeat time of samples in one class to be pooled in query")
parser.add_argument("-vs1", "--val_shot1", type=int, required=True, help="val, repeat time of samples in one class to be pooled in support")
parser.add_argument("-vs2", "--val_shot2", type=int, required=True, help="val, repeat time of samples in one class to be pooled in query")
parser.add_argument("--val_num", type=int, default=100, help="num of val episodes to evaluate")
parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")
parser.add_argument('--torch_model_dir', type=str, required=False, default='/S2/MI/zxz/.torch/models', \
                    help="pytorch model zoo dir of pretrained model")
# test configuration
parser.add_argument('-e', "--evaluate", type=bool, default=False, help="evaluate model on test set")
parser.add_argument("--test_dir", type=str, default=None, help="path to dir of test files")
parser.add_argument("--start_index", type=int, default=0, help="start index of test episodes to evaluate")
parser.add_argument("--end_index", type=int, default=1000, help="end index of test episodes to evaluate")
parser.add_argument("--test_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                          to be used to process testing data")
parser.add_argument("--sample_num", type=int, default=-1, help="sampling num per video, -1 means all")
parser.add_argument("--dup_num", type=int, default=1, help="duplicate sampling num per video")
parser.add_argument("--output", type=str, default=None, help="output dir of file recording evaluation result")

args=parser.parse_args()

def save_tmp():
    save_path = os.path.join(args.cp_dir, "tem.pth.tar")
    torch.save({
        "state_dict": model.state_dict(),
        "optimizer": optimizer.state_dict()},
        save_path)
    print("saved at {}".format(save_path))

# signal handler
# make sure to close writer file before exit
def sigint_handler(sig, frame):
    try:
        writer.close()
    except:
        print("writer not created")
    finally:
        c = raw_input("save state?[y/N]")
        if c == "y":
            save_path = os.path.join(args.cp_dir, "tem.pth.tar")
            torch.save({
                "state_dict": model.state_dict(),
                "optimizer": optimizer.state_dict()},
                save_path)
            print("saved at {}".format(save_path))

        print("exiting...")
        sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)

model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}

model_features = {
    "resnet18": 512,
    "resnet34": 512,
    "resnet50": 2048,
    "resnet101": 2048,
    "resnet152": 2048
}

def train(train_loader, model, criterion, optimizer, epoch):
    data_time = AverageMeter()
    batch_time = AverageMeter()
    losses = AverageMeter()
    acc1 = AverageMeter()

    kshot = train_loader.dataset.kshot
    shot1 = train_loader.dataset.shot1
    shot2 = train_loader.dataset.shot2
    class_num = train_loader.dataset.task_class_num
    batch_size = train_loader.dataset.batch_size

    # switch to training mode
    model.train()
    end = time.time()
    for i, (support_x, query_x, target) in enumerate(tqdm(train_loader)):
        support_x = support_x[0]
        query_x = query_x[0]
        target = target[0]

        if i == 0 :
            print support_x.shape, support_x.dtype
            print query_x.shape, query_x.dtype
            print target.shape, target.dtype

        sx_var = support_x.cuda()
        qx_var = query_x.cuda()
        target_var = target.cuda()

        # measure data loading time
        data_time.update(time.time() - end)

        # forward pass
        output = model(sx_var, qx_var, shot1*kshot, shot2)
        loss = criterion(output, target_var)

        if i == 0 :
            print output.shape, output.dtype

        # measure accuracy and record loss
        ac1 = accuracy_relation(np.array(output.data), np.array(target.data), class_num)
        losses.update(loss.data, target.size(0))
        acc1.update(ac1, target.size(0) / class_num)

        # backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*len(train_loader)+i+1)*batch_size
            writer.add_scalars('train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('train/time/data_time', {'val': data_time.val}, count_samples)
            writer.add_scalars('train/loss', {'val': losses.val}, count_samples)
            writer.add_scalars('train/accuracy/acc1', {'val': acc1.val}, count_samples)
            
    print('Epoch: {} finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Loss: {loss.avg:.4f}\n\
           Acc1: {acc1.avg:.3f}'.format(\
           epoch, batch_time=batch_time, loss=losses, acc1=acc1))

    # epoch record
    count_samples = (epoch+1)*len(train_loader)*batch_size
    writer.add_scalars('train/loss_epoch', {'avg': losses.avg}, count_samples)
    writer.add_scalars('train/accuracy_epoch/acc1', {'avg': acc1.avg}, count_samples)

def validate(val_loader, model, all_classes, val_num):
    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    # switch to evaluate mode
    model.eval()
    end = time.time()

    for val_index in tqdm(range(val_num)):
        val_loader.dataset.reset()
        val_loader.dataset.training = True

        idx_to_cls = val_loader.dataset.idx_to_class
        cls_to_idx = val_loader.dataset.class_to_idx
        class_num = len(idx_to_cls.keys())

        class_proto = [[] for i in range(class_num)]

        with torch.no_grad():
            for i, (input_, target) in enumerate(val_loader):
                if val_index == 0 and i == 0:
                    print input_.shape, input_.dtype
                    print target.shape, target.dtype

                input_var = input_[0].cuda()

                # forward pass
                embed = model.embedding(input_var).mean(dim=0)
                class_proto[int(target[0])].append(embed)

                if val_index == 0 and i == 0:
                    print embed.shape, embed.dtype

            for i in range(class_num):
                class_proto[i] = torch.stack(class_proto[i]).mean(dim=0, keepdim=True)

            val_loader.dataset.training = False

            for i, (input_, target) in enumerate(val_loader):
                #print input_.shape
                #print target.shape

                input_var = input_[0].cuda()

                # forward pass
                embed = model.embedding(input_var).mean(dim=0, keepdim=True)

                concat = torch.cat([torch.cat([c_emd, embed], dim=1) for c_emd in class_proto])
                concat = concat.view(concat.size(0), -1)
                score = model.relation(concat)
                score = score.view(score.size(0))
                pred = np.array(score.data).argmax(axis=0)

                correct = float(pred == int(target[0]))
                avg_acc1.update(correct, 1)
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1))

    return avg_acc1.avg

def evaluate(model, test_dir, start_index, end_index, test_mean_std_file, sample_num, dup_num, output_dir):
    batch_time = AverageMeter()

    print "evaludate on {}".format(args.test_dir)
    mean, std = tuple(np.loadtxt(args.test_mean_std_file, dtype=np.float32))
    mean = [float(a) for a in list(mean)]
    std =[float(b) for b in list(std)]
    print "mean: ", mean
    print "std: ", std

    # switch to evaluate mode
    model.eval()
    end = time.time()

    for test_idx in tqdm(range(start_index, end_index)):
        train_file = os.path.join(test_dir, "train{}.txt".format(test_idx))
        test_file = os.path.join(test_dir, "test{}.txt".format(test_idx))

        train_dataset = FrameListFileTestFolder(
                            train_file,
                            -1,
                            1,
                            frame_transform=transforms.Compose([
                                transforms.Resize(256),
                                transforms.CenterCrop(args.size),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=mean,
                                                     std=std)
                            ]))
        train_loader = torch.utils.data.DataLoader(
                        train_dataset, batch_size=1, shuffle=False,
                        num_workers=args.workers)

        test_dataset = FrameListFileTestFolder(
                            test_file,
                            sample_num,
                            dup_num,
                            frame_transform=transforms.Compose([
                                transforms.Resize(256),
                                transforms.CenterCrop(args.size),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=mean,
                                                     std=std)
                            ]))
        test_loader = torch.utils.data.DataLoader(
                        test_dataset, batch_size=1, shuffle=False,
                        num_workers=args.workers)

        idx_to_cls = train_dataset.idx_to_class
        cls_to_idx = train_dataset.class_to_idx

        classes = [idx_to_cls[idx] for idx in range(len(idx_to_cls.keys()))]
        class_num = len(classes)

        avg_acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in classes}
        class_proto = [[] for i in range(class_num)]

        with torch.no_grad():
            for i, (input_, target) in enumerate(train_loader):
                if test_idx == 0 and i == 0:
                    print input_.shape, input_.dtype
                    print target.shape, target.dtype

                input_var = input_[0].cuda()

                # forward pass
                embed = model.embedding(input_var).mean(dim=0)
                class_proto[int(target[0])].append(embed)

                if test_idx == 0 and i == 0:
                    print embed.shape, embed.dtype

            for i in range(class_num):
                class_proto[i] = torch.stack(class_proto[i]).mean(dim=0, keepdim=True)

            for i, (input_, target) in enumerate(test_loader):
                #print input_.shape
                #print target.shape

                input_var = input_[0].cuda()

                # forward pass
                embed = model.embedding(input_var).mean(dim=0, keepdim=True)

                concat = torch.cat([torch.cat([c_emd, embed], dim=1) for c_emd in class_proto])
                concat = concat.view(concat.size(0), -1)
                score = model.relation(concat)
                score = score.view(score.size(0))
                pred = np.array(score.data).argmax(axis=0)

                correct = float(pred == int(target[0]))
                cls = idx_to_cls[int(target[0])]
                cls_acc1[cls].update(correct, 1)
                avg_acc1.update(correct, 1)
        
        output_file = os.path.join(output_dir, "result{}.txt".format(test_idx))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, duplicate {} times with {} frames once".\
                        format(class_num, dup_num, sample_num)
            print >> f, "average top-1 accuracy: {}".format(avg_acc1.avg)
            print >> f, "------------------------------------"
            for cls in classes:
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}".format(\
           batch_time=batch_time))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    task_class_num = args.class_num

    # load mean and std
    mean, std = tuple(np.loadtxt(args.train_mean_std_file))
    mean = [float(a) for a in list(mean)]
    std = [float(b) for b in list(std)]
    print "mean: ", mean
    print "std: ", std

    train_dataset = FrameMetaTrainDataset(
            train_classes, 
            args.frame_dir,
            task_class_num,
            args.kshot,
            args.query_kshot,
            args.shot1,
            args.shot2,
            args.batch_size,
            args.max_steps,
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=mean, 
                                     std=std)
                ]))
    train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)
    
    val_dataset = FrameMetaValDataset(
            val_classes,
            args.frame_dir,
            task_class_num,
            args.kshot,
            args.query_kshot,
            args.val_shot1,
            args.val_shot2,
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(args.size),
                transforms.ToTensor(),
                transforms.Normalize(mean=mean, 
                                     std=std)
                ]))
    val_loader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # create model
    embedding = models.__dict__[args.embedding_model](pretrained=False)

    # manully load pretrained model since only parts of the network configuration the same
    if args.pretrain:
        print("using pre-trained model {}".format(args.pretrain))
        
        if args.pretrain == "ImageNet":
            model_url = model_urls[args.embedding_model.split('_')[0]]
            model_filename = model_url.split('/')[-1]
            model_filepath = os.path.join(args.torch_model_dir, model_filename)

            if not os.path.exists(model_filepath):
                pretrain_net = model_zoo.load_url(model_url, model_dir=args.torch_model_dir)
            else:
                pretrain_net = torch.load(model_filepath)

            # only load features layer
            pretrain_features = {k:v for k, v in pretrain_net.items() if not ("fc" in k or "classifier" in k or \
                                                                                "num_batches_tracked" in k)}

        else:
            pretrain_net = torch.load(args.pretrain)
            if pretrain_net.has_key("state_dict"):
                pretrain_net = pretrain_net["state_dict"]

            # only load features layer
            pretrain_features = {k[k.find(".")+1:]:v for k, v in pretrain_net.items() if not ("fc" in k or "classifier" in k or \
                                                                                                "num_batches_tracked" in k)}

        print("pretrain_layers:")
        for k,v in pretrain_features.items():
            print("\t{}: {}".format(k, v.size()))

        model_dict = embedding.state_dict()
        model_dict.update(pretrain_features)
        embedding.load_state_dict(model_dict)

        del pretrain_net
        del pretrain_features
        del model_dict
    else:
        print("creating model {}".format(args.embedding_model))

    embedding = nn.Sequential(*(list(embedding.children())[:-1]))

    global model
    model = RelationNet(embedding, model_features[args.embedding_model]) 
    # set data parallel to use multiple gpus
    model = model.cuda()
    
    for name, param in model.named_parameters():
        print name, param.shape, param.dtype

    # loss func and optimizer
    global optimizer
    criterion = nn.MSELoss().cuda()
    optimizer = torch.optim.SGD(model.parameters(), args.learning_rate)
    cudnn.enabled = True
    cudnn.benchmark = True
    
    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            model.load_state_dict(checkpoint['state_dict'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    # just do evaluation if 
    if args.evaluate:
        evaluate(model, args.test_dir, args.start_index, args.end_index,\
                    args.test_mean_std_file, args.sample_num, args.dup_num, args.output)
        return

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch)
        save_tmp()
        
        if (epoch + 1) % args.val_freq == 0:
            # evaluate on validation set
            acc1 = validate(val_loader, model, val_classes, args.val_num)
            
            # record evaluation result
            count_samples = (epoch+1)*len(train_loader)*args.batch_size
            writer.add_scalars('eval/accuracy_epoch/acc1', {'avg': acc1}, count_samples)

        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.embedding_model,
                'state_dict': model.state_dict(),
                'acc1': acc1,
                'optimizer': optimizer.state_dict(),
                }, False, cp_dir)
    writer.close()

if __name__ == "__main__":
    main()
