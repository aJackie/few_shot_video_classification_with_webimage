import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from PIL import Image
from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint
import torchvision.transforms as transforms

class FrameMetaTrainDataset(Dataset):
    # batch_size % task_class_num == 0
    def __init__(self, all_classes, frame_dir, task_class_num, kshot, query_kshot,
                 shot1, shot2, batch_size, max_steps, frm_transform=None):

        super(FrameMetaTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.batch_size = batch_size
        self.max_steps = max_steps
        self.frm_transform = frm_transform

        self.train_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_videos = {}
        self.kshot_test_videos = {}
        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            shuffle(self.train_videos[cls])
            self.kshot_train_videos[cls] = self.train_videos[cls][:self.kshot]

            shuffle(self.test_videos[cls])
            self.kshot_test_videos[cls] = self.test_videos[cls][:self.query_kshot]

    def __len__(self):
        return self.max_steps

    def __getitem__(self, idx):
        self.reset()
        task_train_frames = []
        task_test_frames = []
        target = []

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            for vid in self.kshot_train_videos[cls]:
                frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
                perm = np.random.permutation(max(len(frms), self.shot1))
                for id_ in perm[:self.shot1]:
                    id_ = id_ % len(frms)
                    frame = pil_loader(frms[id_])
                    if self.frm_transform is not None:
                        frame = self.frm_transform(frame)

                    task_train_frames.append(frame)

        task_train_frames = torch.stack(task_train_frames)
        #task_train_frames = task_train_frames.repeat(self.batch_size / self.task_class_num, 1, 1, 1)
        
        """
        for idx in range(self.batch_size):
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            id_ = randint(0, self.kshot-1)
            vid = self.kshot_train_videos[cls][id_]
            frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
            perm = np.random.permutation(max(len(frms), self.shot1))
        """

        for i in range(self.batch_size / self.task_class_num):
            idx = randint(0, self.task_class_num - 1)
            cls = self.idx_to_class[idx]

            t = [0.] * self.task_class_num
            t[idx] = 1.
            target = target + t
            
            id_ = randint(0, self.query_kshot-1)
            vid = self.kshot_test_videos[cls][id_]
            frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
            perm = np.random.permutation(max(len(frms), self.shot2))
            frames = []
            for id_ in perm[:self.shot2]:
                id_ = id_ % len(frms)
                frame = pil_loader(frms[id_])

                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)

                frames.append(frame)

            task_test_frames = task_test_frames + frames# * self.task_class_num
            
        return task_train_frames, torch.stack(task_test_frames), torch.tensor(target, dtype=torch.float32)

class FrameMetaValDataset(Dataset):
    def __init__(self, all_classes, frame_dir, task_class_num, kshot, query_kshot,
                 shot1, shot2, frm_transform=None):

        super(FrameMetaValDataset, self).__init__()
        self.classes = all_classes
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.frm_transform = frm_transform
        self.training = True

        self.train_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_videos = []
        self.kshot_test_videos = []
        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            shuffle(self.train_videos[cls])
            self.kshot_train_videos = self.kshot_train_videos + map(lambda v: (v, idx), self.train_videos[cls][:self.kshot])

            shuffle(self.test_videos[cls])
            self.kshot_test_videos = self.kshot_test_videos + map(lambda v: (v, idx), self.test_videos[cls][:self.query_kshot])

    def __len__(self):
        if self.training:
            return len(self.kshot_train_videos)
        else:
            return len(self.kshot_test_videos)
        
    def __getitem__(self, idx):
        if self.training:
            self.videos = self.kshot_train_videos
            self.shot = self.shot1
        else:
            self.videos = self.kshot_test_videos
            self.shot = self.shot2

        vid_dir, target = self.videos[idx]
        frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])

        if self.shot == -1:
            select = np.concatenate([frames])
        else:
            select = np.array([])
            perms = np.random.permutation(len(frames))
            select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.shot))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.frm_transform is not None:
                sample = self.frm_transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class ImageMetaTrainDataset(Dataset):
    # batch_size % task_class_num == 0
    def __init__(self, all_classes, image_dir, frame_dir, task_class_num, query_kshot,
                 shot1, shot2, batch_size, max_steps, img_transform=None, frm_transform=None):

        super(ImageMetaTrainDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.batch_size = batch_size
        self.max_steps = max_steps
        self.img_transform = img_transform
        self.frm_transform = frm_transform

        self.train_images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, "train", cls)
            self.train_images[cls] = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way".format(self.task_class_num)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose images & videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_images = {}
        self.kshot_test_videos = {}

        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            shuffle(self.train_images[cls])
            self.kshot_train_images[cls] = self.train_images[cls][:self.shot1]

            shuffle(self.test_videos[cls])
            self.kshot_test_videos[cls] = self.test_videos[cls][:self.query_kshot]

    def __len__(self):
        return self.max_steps

    def __getitem__(self, idx):
        self.reset()
        task_train_images = []
        task_test_frames = []
        target = []

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            for img in self.kshot_train_images[cls]:
                image = pil_loader(img)

                if self.img_transform is not None:
                    image = self.img_transform(image)

                task_train_images.append(image)

        task_train_images = torch.stack(task_train_images)
        #task_train_images = task_train_images.repeat(self.batch_size / self.task_class_num, 1, 1, 1)
            
        """
        for idx in range(self.batch_size):
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            imgs = self.kshot_train_images[cls]
            for img in imgs:
                image = pil_loader(img)

                if self.img_transform is not None:
                    image = self.img_transform(image)

                task_train_images.append(image)
        """

        for i in range(self.batch_size / self.task_class_num):
            idx = randint(0, self.task_class_num - 1)
            cls = self.idx_to_class[idx]

            t = [0.] * self.task_class_num
            t[idx] = 1.
            target = target + t
            
            id_ = randint(0, self.query_kshot-1)
            vid = self.kshot_test_videos[cls][id_]
            frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
            perm = np.random.permutation(len(frms))
            frames = []
            for id_ in perm[:self.shot2]:
                frame = pil_loader(frms[id_])

                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)

                frames.append(frame)

            task_test_frames = task_test_frames + frames# * self.task_class_num
            
        return task_train_images, torch.stack(task_test_frames), torch.tensor(target, dtype=torch.float32)

class ImageMetaValDataset(Dataset):
    def __init__(self, all_classes, image_dir, frame_dir, task_class_num, query_kshot,
                 shot1, shot2, img_transform=None, frm_transform=None):

        super(ImageMetaValDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.img_transform = img_transform
        self.frm_transform = frm_transform
        self.training = True

        self.train_images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, "train", cls)
            self.train_images[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way".format(self.task_class_num)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_images = []
        self.kshot_test_videos = []
        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            self.kshot_train_images.append((self.train_images[cls], idx))

            shuffle(self.test_videos[cls])
            self.kshot_test_videos = self.kshot_test_videos + map(lambda v: (v, idx), self.test_videos[cls][:self.query_kshot])

    def __len__(self):
        if self.training:
            return len(self.kshot_train_images)
        else:
            return len(self.kshot_test_videos)
        
    def __getitem__(self, idx):
        if self.training:
            self.images = self.kshot_train_images
            self.shot = self.shot1
            
            imgs, target = self.images[idx]
            imgs = np.array(imgs)

            if self.shot == -1:
                select = np.concatenate([imgs])
            else:
                select = np.array([])
                perms = np.random.permutation(len(imgs))
                select = np.concatenate([select, imgs[perms][map(lambda x: x%len(imgs), range(self.shot))]])

            samples = []
            for path in select:
                sample = pil_loader(path)
                if self.img_transform is not None:
                    sample = self.img_transform(sample)
                samples.append(sample)
            
            sample = torch.stack(samples)

        else:
            self.videos = self.kshot_test_videos
            self.shot = self.shot2

            vid_dir, target = self.videos[idx]
            frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])

            if self.shot == -1:
                select = np.concatenate([frames])
            else:
                select = np.array([])
                perms = np.random.permutation(len(frames))
                select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.shot))]])

            samples = []
            for path in select:
                sample = pil_loader(path)
                if self.frm_transform is not None:
                    sample = self.frm_transform(sample)
                samples.append(sample)
            
            sample = torch.stack(samples)

        return sample, target

class MixtureMetaTrainDataset(Dataset):
    # batch_size % task_class_num == 0
    def __init__(self, all_classes, image_dir, frame_dir, task_class_num, kshot, query_kshot,
                 shot1, shot2, batch_size, max_steps, img_transform=None, frm_transform=None):

        super(MixtureMetaTrainDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.batch_size = batch_size
        self.max_steps = max_steps
        self.img_transform = img_transform
        self.frm_transform = frm_transform

        self.train_images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, "train", cls)
            self.train_images[cls] = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]

        self.train_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_images = {}
        self.kshot_train_frames = {}
        self.kshot_train_samples = {}
        self.kshot_test_videos = {}

        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            self.kshot_train_images[cls] = self.train_images[cls]

            shuffle(self.train_videos[cls])
            self.kshot_train_frames[cls] = []
            for vid in self.train_videos[cls][:self.kshot]:
                self.kshot_train_frames[cls] = self.kshot_train_frames[cls] + [os.path.join(vid, f) for f in os.listdir(vid)]

            self.kshot_train_samples[cls] = [(i, "i") for i in self.kshot_train_images[cls]] + \
                                            [(f, "f") for f in self.kshot_train_frames[cls]]
            shuffle(self.kshot_train_samples[cls])

            shuffle(self.test_videos[cls])
            self.kshot_test_videos[cls] = self.test_videos[cls][:self.query_kshot]

    def __len__(self):
        return self.max_steps

    def __getitem__(self, idx):
        self.reset()
        task_train_samples = []
        task_test_frames = []
        target = []

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            for sample, flag in self.kshot_train_samples[cls][:self.shot1]:
                sample = pil_loader(sample)
                if flag == "i":
                    if self.img_transform is not None:
                        sample = self.img_transform(sample)
                elif flag == "f":
                    if self.frm_transform is not None:
                        sample = self.frm_transform(sample)

                task_train_samples.append(sample)

        task_train_samples = torch.stack(task_train_samples)

        for i in range(self.batch_size / self.task_class_num):
            idx = randint(0, self.task_class_num - 1)
            cls = self.idx_to_class[idx]

            t = [0.] * self.task_class_num
            t[idx] = 1.
            target = target + t
            
            id_ = randint(0, self.query_kshot-1)
            vid = self.kshot_test_videos[cls][id_]
            frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
            perm = np.random.permutation(max(len(frms), self.shot2))
            frames = []
            for id_ in perm[:self.shot2]:
                id_ = id_ % len(frms)
                frame = pil_loader(frms[id_])

                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)

                frames.append(frame)

            task_test_frames = task_test_frames + frames# * self.task_class_num
            
        return task_train_samples, torch.stack(task_test_frames), torch.tensor(target, dtype=torch.float32)

class MixtureMetaValDataset(Dataset):
    def __init__(self, all_classes, image_dir, frame_dir, task_class_num, kshot, query_kshot,
                 shot1, shot2, img_transform=None, frm_transform=None):

        super(MixtureMetaValDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.shot1 = shot1
        self.shot2 = shot2
        self.img_transform = img_transform
        self.frm_transform = frm_transform
        self.training = True

        self.train_images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, "train", cls)
            self.train_images[cls] = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]

        self.train_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.test_videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.kshot_train_images = []
        self.kshot_train_videos = []
        self.kshot_train_samples = []
        self.kshot_test_videos = []

        for idx, jdx in enumerate(perm[:self.task_class_num]):
            cls = self.classes[jdx]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            self.kshot_train_images.append((self.train_images[cls], idx))

            shuffle(self.train_videos[cls])
            self.kshot_train_videos = self.kshot_train_videos + map(lambda v: (v, idx), self.train_videos[cls][:self.kshot])

            shuffle(self.test_videos[cls])
            self.kshot_test_videos = self.kshot_test_videos + map(lambda v: (v, idx), self.test_videos[cls][:self.query_kshot])

        self.kshot_train_samples = self.kshot_train_images + self.kshot_train_videos

    def __len__(self):
        if self.training:
            return len(self.kshot_train_samples)
        else:
            return len(self.kshot_test_videos)
        
    def __getitem__(self, idx):
        if self.training:
            sample, target = self.kshot_train_samples[idx]

            if isinstance(sample, list):
                samples = np.array(sample)
                self.transform = self.img_transform
            else:
                samples = np.array([os.path.join(sample, f) for f in os.listdir(sample)])
                self.transform = self.frm_transform

            self.shot = self.shot1

        else:
            vid_dir, target = self.kshot_test_videos[idx]
            samples = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])
            self.shot = self.shot2
            self.transform = self.frm_transform

        if self.shot == -1:
            select = np.concatenate([samples])
        else:
            select = np.array([])
            perms = np.random.permutation(len(samples))
            select = np.concatenate([select, samples[perms][map(lambda x: x%len(samples), range(self.shot))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.transform is not None:
                sample = self.transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class MixtureMetaTestSupDataset(Dataset):
    def __init__(self, path_file, image_dir, old_frame_dir, new_frame_dir, \
                 shot, img_transform=None, frm_transform=None):

        super(MixtureMetaTestSupDataset, self).__init__()
        self.path_file = path_file
        self.image_dir = image_dir
        self.old_frame_dir = old_frame_dir
        self.new_frame_dir = new_frame_dir
        self.shot = shot
        self.img_transform = img_transform
        self.frm_transform = frm_transform

        with open(self.path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.train_videos = []

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.old_frame_dir, self.new_frame_dir)
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            
            self.train_videos.append((path, idx))

        self.train_images = []
        for idx in range(len(self.idx_to_class.keys())):
            cls = self.idx_to_class[idx]
            cls_dir = os.path.join(self.image_dir, "train", cls)
            self.train_images.append(([os.path.join(cls_dir, i) for i in os.listdir(cls_dir)], idx))

        self.train_samples = self.train_videos + self.train_images

    def __len__(self):
        return len(self.train_samples)
        
    def __getitem__(self, idx):
        sample, target = self.train_samples[idx]

        if isinstance(sample, list):
            samples = np.array(sample)
            self.transform = self.img_transform
        else:
            samples = np.array([os.path.join(sample, f) for f in os.listdir(sample)])
            self.transform = self.frm_transform

        if self.shot == -1:
            select = np.concatenate([samples])
        else:
            select = np.array([])
            perms = np.random.permutation(len(samples))
            select = np.concatenate([select, samples[perms][map(lambda x: x%len(samples), range(self.shot))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.transform is not None:
                sample = self.transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target




class ImageListFolder(Dataset):
    """
        similar to torchvision datasets.ImageFolder
        but use class_lst to indicate valid classes
    """
    def __init__(self, image_dir, class_lst, img_transform=None):
        super(ImageListFolder, self).__init__()
        self.image_dir = image_dir
        self.classes = class_lst
        self.img_transform = img_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), idx), paths)

        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        path, target = self.imgs[idx]
        sample = pil_loader(path)
        if self.img_transform is not None:
            sample = self.img_transform(sample)

        return sample, target

class ImageListTestFolder(Dataset):
    def __init__(self, image_dir, class_lst, sample_num, img_transform=None):
        super(ImageListTestFolder, self).__init__()
        self.image_dir = image_dir
        self.classes = class_lst
        self.sample_num = sample_num
        self.img_transform = img_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]
            self.imgs.append((paths, idx))

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        paths, target = self.imgs[idx]
        if self.sample_num == -1:
            select = np.array(paths) 
        else:
            paths = np.array(paths)
            select = np.array([])
            perms = np.random.permutation(len(paths))
            select = np.concatenate([select, paths[perms][map(lambda x: x%len(paths), range(self.sample_num))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.img_transform is not None:
                sample = self.img_transform(sample)
            samples.append(sample)

        return torch.stack(samples), target


class FrameListFileFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
    """
    def __init__(self, path_file, frame_transform=None):
        super(FrameListFileFolder, self).__init__()
        self.path_file = path_file
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class FrameListFileTestFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
        NOTE: this is used for evaluation
    """
    def __init__(self, path_file, sample_num, dup_num, frame_transform=None):
        super(FrameListFileTestFolder, self).__init__()
        self.path_file = path_file
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
            
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

    def __len__(self):
        return len(self.videos)

    def __getitem__(self, idx):
        vid_dir, target = self.videos[idx]
        frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])
        if self.sample_num == -1:
            select = np.concatenate([frames] * self.dup_num)
        else:
            select = np.array([])
            for _ in range(self.dup_num):
                perms = np.random.permutation(len(frames))
                select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.sample_num))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class ImageFrameMixFolder(Dataset):
    """
        mixture of web images(from image dir) and few shot 
        training video frames(from training file)
    """
    def __init__(self, image_dir, path_file, class_lst, img_transform=None, frame_transform=None):
        super(ImageFrameMixFolder, self).__init__()
        self.image_dir = image_dir
        self.path_file = path_file
        self.classes = class_lst
        self.img_transform = img_transform
        self.frame_transform = frame_transform

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), "i", idx), paths)

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
          
            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), "f", label))

        self.samples = self.imgs + self.frames
        
        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))
        print "total {} samples".format(len(self.samples))

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        path, flag, target = self.samples[idx]
        sample = pil_loader(path)

        if flag == "i":
            if self.img_transform is not None:
                sample = self.img_transform(sample)
        else:
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)

        return sample, target

if __name__ == "__main__":
    import torchvision.transforms as transforms
    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_val/meta_val.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    train_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/train0.txt"
    image_dir = "/local/MI/xz/transfer_learning/data/webimage_actNet"
    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    new_frame_dir = "/local/MI/xz/transfer_learning/data/actNet_frames"
    dataset = MixtureMetaTestSupDataset(train_file, image_dir, frame_dir, new_frame_dir, -1, 
                                      img_transform=transforms.Compose([
                                                    transforms.Resize(256),
                                                    transforms.CenterCrop(224),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                                                         std=[0.22882703, 0.22168043, 0.21761282])
                                                    ]),
                                      frm_transform=transforms.Compose([
                                                    transforms.Resize(256),
                                                    transforms.CenterCrop(224),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                                                         std=[0.22882703, 0.22168043, 0.21761282])
                                                    ]))

    loader = DataLoader(dataset, batch_size=1, num_workers=1, shuffle=False)
    print dataset.idx_to_class
    for i, (x, target) in enumerate(loader):
        print len(loader)
        print x.shape, x.dtype
        print target.shape, target.dtype
        print target

