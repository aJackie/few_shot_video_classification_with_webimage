import torch
import torch.nn as nn


class RelationNet(nn.Module):
    
    def __init__(self, embedding, num_features=2048):
        super(RelationNet, self).__init__()
        self.embedding = embedding
        self.relation = nn.Sequential(
            nn.Linear(num_features*2, num_features),
            nn.ReLU(True),
            nn.Linear(num_features, 1),
            nn.Sigmoid())
        
    def forward(self, sx, qx, shot1, shot2):
        """
            support x: (class_num*shot1, C, H, W)
            query x: (sample_num*shot2, C, H, W)
        """
        sx = self.embedding(sx)
        qx = self.embedding(qx)

        class_num = sx.size(0) / shot1
        sample_num = qx.size(0) / shot2

        def merge_class(x, shot):
            s = []
            for _ in range(x.size(0) / shot):
                s.append(x[_*shot: (_+1)*shot].mean(dim=0))
            s = torch.stack(s)
            return s

        if shot1 > 1:
            sx = merge_class(sx, shot1)
        if shot2 > 1:
            qx = merge_class(qx, shot2)

        # concat every query sample with every support sample
        sx = sx.repeat(sample_num, 1, 1, 1)
        qqx = []
        for i in range(sample_num):
            qqx.append(qx[i:i+1].repeat(class_num, 1,1,1))
        qx = torch.cat(qqx)

        x = torch.cat([sx, qx], dim=1)
        #print x.shape
        x = x.view(x.size(0), -1)
        x = self.relation(x)
        
        return x.view(x.size(0))
        
