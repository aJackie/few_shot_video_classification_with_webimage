"""
    automatically script train_rn_from_image.py for N-way k-shot
"""
import os

N = 5
log_dir = "/S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/relation_net/{}/image/res50"
cp_dir = "/S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/relation_net/{}/image/res50"
cg_dir = "/S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/relation_net/{}/image/res50"
script = "train_rn_from_image_{}way_{}shot.sh"

for k in [1, 5]:
    kscript = script.format(N, k)
    klog_dir = log_dir.format(k)
    kcp_dir = cp_dir.format(k)
    kcg_dir = cg_dir.format(k)
    
    f = open(kscript, "w")
    f.write("python train_rn_from_image.py \\\n" + \
            "\t-em resnet50 \\\n" + \
            "\t-p ImageNet \\\n" + \
            "\t-b 30 \\\n" + \
            "\t-m 1000 \\\n" + \
            "\t-s 224 \\\n" + \
            "\t-w 4 \\\n" + \
            "\t-g 0 \\\n" + \
            "\t-lr 0.001 \\\n" + \
            "\t--start_epoch 0 \\\n" + \
            "\t--epochs 10 \\\n" + \
            "\t--print_freq 2 \\\n" + \
            "\t--save_freq 1 \\\n" + \
            "\t--val_freq 1 \\\n" + \
            "\t--image_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \\\n" + \
            "\t--frame_dir /S2/MI/zxz/transfer_learning/data/actNet_frames \\\n" + \
            "\t--train_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt \\\n" + \
            "\t--val_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_val/meta_val.txt \\\n" + \
            "\t--image_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/image_train_mean_std.out \\\n" + \
            "\t--frame_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/frame_train_mean_std.out \\\n" + \
            "\t-k 1 \\\n" + \
            "\t-qk 1 \\\n" + \
            "\t-c 5 \\\n" + \
            "\t-s1 10 \\\n" + \
            "\t-s2 10 \\\n" + \
            "\t-vs1 100 \\\n" + \
            "\t-vs2 100 \\\n" + \
            "\t--val_num 100 \\\n" + \
            "\t--log_dir {} \\\n".format(klog_dir) + \
            "\t--cp_dir {} \\\n".format(kcp_dir) + \
            "\t--cg_dir {} \\\n".format(kcg_dir))

    f.close()
    os.system("chmod 740 {}".format(kscript))

