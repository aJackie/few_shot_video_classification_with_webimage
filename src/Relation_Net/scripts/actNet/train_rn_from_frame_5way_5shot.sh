python train_rn_from_frame.py \
	-em resnet50 \
	-p ImageNet \
	-b 150 \
	-m 1000 \
	-s 224 \
	-w 4 \
	-g 5 \
	-lr 0.0001 \
	--start_epoch 68 \
	--epochs 108 \
	--print_freq 2 \
	--save_freq 4 \
	--val_freq 4 \
	--frame_dir /local/MI/xz/transfer_learning/data/actNet_frames \
	--train_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt \
	--val_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_val/meta_val.txt \
	--train_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/frame_train_mean_std.out \
	-k 5 \
	-qk 1 \
	-c 5 \
	-s1 1 \
	-s2 1 \
	-vs1 100 \
	-vs2 100 \
	--val_num 100 \
	--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/relation_net/5/frame/res50 \
	--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/relation_net/5/frame/res50 \
	--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/relation_net/5/frame/res50 \
    --resume /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/relation_net/5/frame/res50/checkpoint_epoch68.pth.tar \
