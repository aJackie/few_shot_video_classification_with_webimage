import math
import torch
import torch.nn as nn
import torch.nn.functional as F

class MSE(nn.Module):
    """
        multi-saliency embedding
    """
    def __init__(self, feature_dim, num_features):
        super(MSE, self).__init__()
        self.num_features = num_features
        self.feature_dim = feature_dim
        self.weighing = nn.Linear(feature_dim, num_features, bias=False)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        # x: (frame_num, feature_dim)
        x = self.weighing(x)
        x = self.softmax(x / math.sqrt(self.feature_dim))
        return x

    def embedding(self, frame_fs, weights):
        """
            frame_fs: (frame_num, feature_dim)
            weights: (frame_num, num_features)
        """
        # (num_features, feature_dim)
        param = list(self.weighing.parameters())[0]
        Q = []
        for i in range(self.num_features):
            h = param[i:i+1]
            a = weights[:, i:i+1]
            q = a*(frame_fs - h)
            q = q.sum(dim=0)
            Q.append(q)

        return torch.stack(Q)

class Abstract(nn.Module):
    """
        Abstract memory processing module
    """
    def __init__(self, in_features, out_features):
        """
            in_features == num_features * feature_dim
            out_feaures == abstract_dim
        """
        super(Abstract, self).__init__()
        self.in_featuers = in_features
        self.out_features = out_features
        self.fc = nn.Linear(in_features, out_features)

    def forward(self, x):
        # x: (num_features, feature_dim)
        x = F.normalize(x, p=2, dim=1)
        x = x.view(1, -1)
        x = self.fc(x)
        x = F.normalize(x, p=2, dim=1)

        return x

class CompoundMem(nn.Module):
    """
        Compound Memory Module
    """
    def __init__(self):
        super(CompoundMem, self).__init__()
        self.cmem = []

    def forward(self, A):
        # A: (N, abstract_dim)
        out = [None for _ in range(A.size(0))]
        for i in range(A.size(0)):
            a = A[i:i+1]
            if len(self.cmem) > 0:
                score = [((a*m["abstract"]).sum(), m["label"], idx) for idx, m in enumerate(self.cmem)]
                score.sort(reverse=True, key=lambda x:x[0])
                out[i] = score

        return out

    def insert(self, A, Q, label, abstract):
        """
            insert one once
            A: (1, abstract_dim)
            Q: (num_features, feature_dim)
            idx: int
        """
        score = self.forward(A)
        score = score[0]
        if score is None:
            self.cmem.append({"abstract": A, "constituent": Q, "label": label})
        else:
            score = score[0]
            if score[1] == label:
                idx = score[2]
                self.cmem[idx]["constituent"] = self.cmem[idx]["constituent"] + Q
                self.cmem[idx]["abstract"] = abstract(self.cmem[idx]["constituent"])
            else:
                self.cmem.append({"abstract": A, "constituent": Q, "label": label})

    def clear(self):
        self.cmem = []
                
