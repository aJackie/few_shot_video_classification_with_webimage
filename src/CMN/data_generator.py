import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint

# Frame support & frame query
class FrameFeatureSupFrameFeatureQryTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, task_class_num, 
                 kshot, query_kshot):

        super(FrameFeatureSupFrameFeatureQryTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))
            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)

    def __len__(self):
        if self.training:
            return self.task_class_num * self.kshot
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            vid = self.sup_vid_fs[cls][jdx]
        else:
            vid = self.qry_vid_fs[cls][jdx]

        frame_fs = []
        fs = [os.path.join(vid, f) for f in os.listdir(vid)]
        for f in fs:
            frame_fs.append(np.load(f))
        frame_fs = np.stack(frame_fs)

        return frame_fs, idx

class FrameFeatureSupFrameFeatureQryTestDataset(Dataset):

    def __init__(self, test_file_dir, frame_dir, frame_feature_dir, task_class_num, \
                    kshot, query_kshot):

        super(FrameFeatureSupFrameFeatureQryTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        # training or query
        self.training = True 

        self.file_index = -1

    def next_file(self):
        self.file_index = self.file_index + 1
        self.set()

    def set(self):
        train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_index))
        test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_index))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}
        
        with open(train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.idx_to_class[idx] = cls
            self.class_to_idx[cls] = idx

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]

        with open(test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx

            if self.qry_vid_fs.has_key(cls):
                self.qry_vid_fs[cls].append(path)
            else:
                self.qry_vid_fs[cls] = [path]

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            shuffle(self.sup_vid_fs[cls])
            shuffle(self.qry_vid_fs[cls])

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
        
    def __len__(self):
        if self.training:
            return self.task_class_num * self.kshot
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            vid = self.sup_vid_fs[cls][jdx]
        else:
            vid = self.qry_vid_fs[cls][jdx]

        frame_fs = []
        fs = [os.path.join(vid, f) for f in os.listdir(vid)]
        for f in fs:
            frame_fs.append(np.load(f))
        frame_fs = np.stack(frame_fs)

        return frame_fs, idx

# Image support & frame input
class ImageFeatureSupFrameFeatureQryTrainDataset(Dataset):

    def __init__(self, all_classes, image_feature_dir, frame_feature_dir, task_class_num, \
                    support_num_per_cls, image_num_per_support, query_kshot):

        super(ImageFeatureSupFrameFeatureQryTrainDataset, self).__init__()
        self.classes = all_classes
        self.image_feature_dir = image_feature_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.support_num_per_cls = support_num_per_cls
        self.image_num_per_support = image_num_per_support
        self.query_kshot = query_kshot
        # training or query
        self.training = True 

        self.train_image_fs = {}
        self.query_video_fs = {}

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way".format(self.task_class_num)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_img_fs = {}
        self.qry_vid_fs = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(self.train_image_fs[cls]))
                self.sup_img_fs[cls].append([self.train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

    def __len__(self):
        if self.training:
            return self.task_class_num * self.support_num_per_cls
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            imgs = self.sup_img_fs[cls][jdx]
            image_fs = []
            for img in imgs:
                image_fs.append(np.load(img))
            image_fs = np.stack(image_fs)
            
            return image_fs, idx
        else:
            vid = self.qry_vid_fs[cls][jdx]
            frame_fs = []
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            for f in fs:
                frame_fs.append(np.load(f))
            frame_fs = np.stack(frame_fs)

            return frame_fs, idx

class ImageFeatureSupFrameFeatureQryTestDataset(Dataset):

    def __init__(self, all_classes, image_feature_dir, test_file_dir, frame_dir, frame_feature_dir, task_class_num, \
                    support_num_per_cls, image_num_per_support, query_kshot):

        super(ImageFeatureSupFrameFeatureQryTestDataset, self).__init__()
        self.classes = all_classes
        self.image_feature_dir = image_feature_dir
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.support_num_per_cls = support_num_per_cls
        self.image_num_per_support = image_num_per_support
        self.query_kshot = query_kshot
        # training or query
        self.training = True 

        self.train_image_fs = {}

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50",  cls)
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        self.file_index = -1

    def next_file(self):
        self.file_index = self.file_index + 1
        self.set()

    def set(self):
        test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_index))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_img_fs = {}
        self.qry_vid_fs = {}

        with open(test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            self.idx_to_class[idx] = cls
            self.class_to_idx[cls] = idx

            if self.qry_vid_fs.has_key(cls):
                self.qry_vid_fs[cls].append(path)
            else:
                self.qry_vid_fs[cls] = [path]

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(self.train_image_fs[cls]))
                self.sup_img_fs[cls].append([self.train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

            shuffle(self.qry_vid_fs[cls])
        
    def __len__(self):
        if self.training:
            return self.task_class_num * self.support_num_per_cls
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            imgs = self.sup_img_fs[cls][jdx]
            image_fs = []
            for img in imgs:
                image_fs.append(np.load(img))
            image_fs = np.stack(image_fs)
            
            return image_fs, idx
        else:
            vid = self.qry_vid_fs[cls][jdx]
            frame_fs = []
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            for f in fs:
                frame_fs.append(np.load(f))
            frame_fs = np.stack(frame_fs)

            return frame_fs, idx

# mixture support & frame query
class MixtureFeatureSupFrameFeatureQryTrainDataset(Dataset):

    def __init__(self, all_classes, image_feature_dir, frame_feature_dir, task_class_num, \
                 support_num_per_cls, image_num_per_support, kshot, query_kshot, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(MixtureFeatureSupFrameFeatureQryTrainDataset, self).__init__()
        self.classes = all_classes
        self.image_feature_dir = image_feature_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.support_num_per_cls = support_num_per_cls
        self.image_num_per_support = image_num_per_support
        self.kshot = kshot
        self.query_kshot = query_kshot

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        # training or query
        self.training = True 

        self.train_image_fs = {}
        self.train_video_fs = {}
        self.query_video_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50",  cls)
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_img_fs = {}
        self.sup_vid_fs = {}
        self.sup_sample_fs = {}
        self.qry_vid_fs = {}

        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))
            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in self.train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = self.train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num

        # generate image-volume
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(task_train_image_fs[cls]))
                self.sup_img_fs[cls].append([task_train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

            self.sup_sample_fs[cls] = self.sup_vid_fs[cls] + self.sup_img_fs[cls]
            shuffle(self.sup_sample_fs[cls])

    def __len__(self):
        if self.training:
            return self.task_class_num * (self.kshot + self.support_num_per_cls)
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            sample = self.sup_sample_fs[cls][jdx]
            if isinstance(sample, list):
                fs = sample
                flag = "i"
            else:
                fs = [os.path.join(sample, f) for f in os.listdir(sample)]
                flag = "f"
        else:
            vid = self.qry_vid_fs[cls][jdx]
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            flag = "f"

        sample_fs = []
        for f in fs:
            f = np.load(f)
            if flag == "i" and self.noise_rate > 0. and \
                    np.random.uniform() < self.noise_rate:
                f = f + self.noise_func(size=f.shape).astype(f.dtype)
            sample_fs.append(f)

        sample_fs = np.stack(sample_fs)

        return sample_fs, idx

class MixtureFeatureSupFrameFeatureQryTestDataset(Dataset):

    def __init__(self, all_classes, test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, task_class_num, \
                 support_num_per_cls, image_num_per_support, kshot, query_kshot, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(MixtureFeatureSupFrameFeatureQryTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.support_num_per_cls = support_num_per_cls
        self.image_num_per_support = image_num_per_support
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.classes = all_classes

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        # training or query
        self.training = True 
        self.file_index = -1

        self.train_image_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

    def next_file(self):
        self.file_index = self.file_index + 1
        self.set()

    def set(self):
        train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_index))
        test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_index))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_img_fs = {}
        self.sup_vid_fs = {}
        self.sup_sample_fs = {}
        self.qry_vid_fs = {}
        
        with open(train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.idx_to_class[idx] = cls
            self.class_to_idx[cls] = idx

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
        
        train_image_fs = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            train_image_fs[cls] = self.train_image_fs[cls]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num

        # generate image-volume
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(task_train_image_fs[cls]))
                self.sup_img_fs[cls].append([task_train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

            self.sup_sample_fs[cls] = self.sup_vid_fs[cls] + self.sup_img_fs[cls]
            shuffle(self.sup_sample_fs[cls])

        with open(test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx

            if self.qry_vid_fs.has_key(cls):
                self.qry_vid_fs[cls].append(path)
            else:
                self.qry_vid_fs[cls] = [path]

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            shuffle(self.qry_vid_fs[cls])
        
    def __len__(self):
        if self.training:
            return self.task_class_num * (self.kshot + self.support_num_per_cls)
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]

        if self.training:
            sample = self.sup_sample_fs[cls][jdx]
            if isinstance(sample, list):
                fs = sample
                flag = "i"
            else:
                fs = [os.path.join(sample, f) for f in os.listdir(sample)]
                flag = "f"
        else:
            vid = self.qry_vid_fs[cls][jdx]
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            flag = "f"

        sample_fs = []
        for f in fs:
            f = np.load(f)
            if flag == "i" and self.noise_rate > 0. and \
                    np.random.uniform() < self.noise_rate:
                f = f + self.noise_func(size=f.shape).astype(f.dtype)
            sample_fs.append(f)

        sample_fs = np.stack(sample_fs)

        return sample_fs, idx



if __name__ == "__main__":
    import torchvision.transforms as transforms
    import time

    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    frame_feature_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features"
    image_feature_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features"
    test_file_dir = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1"

    dataset = MixtureFeatureSupFrameFeatureQryTestDataset(test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, \
                                                            5, 1, 100, 1, 1, label_flip_rate=1.0) 
    loader1 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=1)
    loader2 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=1)
    
    loader1.dataset.next_file()
    print loader2.dataset.idx_to_class
    for idx in range(5):
        cls = dataset.idx_to_class[idx]
        print len(dataset.train_image_fs[cls])
        print len(dataset.task_train_image_fs[cls])
        print "-------------"

    for idx, (samples, target) in enumerate(loader1):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target
    
    loader1.dataset.training = False
    print loader2.dataset.training

    for idx, (samples, target) in enumerate(loader2):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target


