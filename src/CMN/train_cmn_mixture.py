""" 
    training Compound Memory Network model with mixture of web images and video frames
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

from model import MSE, Abstract, CompoundMem
from loss import DotMarginLoss
from data_generator import MixtureFeatureSupFrameFeatureQryTrainDataset, MixtureFeatureSupFrameFeatureQryTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training Compound Memory Network model \
                                              with mixture of web images and video frames",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrain model to extract frame feature,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-nf', '--num_features', default=256, type=int, help="multi-saliency embedding hidden variable num")
parser.add_argument('-ad', '--abstract_dim', default=256, type=int, help="abstract key dim")

parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

"""
parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size of samples in one task")
parser.add_argument('-qb', '--query_batch_size', default=256, type=int, \
                        help="mini-batch size of samples in one task query set")
parser.add_argument('-vb', '--val_batch_size', default=256, type=int, help="val mini-batch size of samples in one task")
parser.add_argument('-vqb', '--val_query_batch_size', default=256, type=int, \
                        help="val mini-batch size of samples in one task query set")
"""

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")
parser.add_argument('-mr', '--margin', default=0.1, type=float, help="margin in loss")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")

parser.add_argument("--frame_dir", type=str, required=True, help="path to root dir of video frames")
parser.add_argument("--frame_feature_dir", type=str, required=True, help="path to root dir of video frame features")
parser.add_argument("--image_feature_dir", type=str, required=True, help="path to root dir of web image features")

parser.add_argument("--task_class_num", type=int, default=48, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--support_num_per_cls", type=int, default=1, help="support image sets num in one class, \
                                                                        kind of like k of k-shot")
parser.add_argument("--image_num_per_support", type=int, default=100, help="image num in one support set, \
                                                                            kind of like frame num of one video")

"""
parser.add_argument("--max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--query_max_steps", type=int, default=10, help="max eval steps in query")
parser.add_argument("--val_max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--val_query_max_steps", type=int, default=10, help="max eval steps in query")
"""

parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")
parser.add_argument("--val_max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--noise_rate", type=float, default=0., help="possibility of random adding noise")
parser.add_argument("--normal_std", type=float, default=1., help="standard deviation of normal distribution")
parser.add_argument("--label_flip_rate", type=float, default=0., help="possibility of random fliping labels")

parser.add_argument("--replace_rate", type=float, default=0., help="possibility of random replacing images")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("--evaluate", type=bool, default=False, help="whether to do evaluation")
parser.add_argument("--test_class_file", type=str, default=None, help="path to file containing meta test classes")
parser.add_argument("--test_file_dir", type=str, default=None, help="path to root dir of test episode files")
parser.add_argument("--test_file_num", type=int, default=None, help="num of test files to be evaluated")
parser.add_argument("--test_output", type=str, default=None, help="path to root dir of test results")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(mse, abstract, meta_optimizer, cp_dir):
    state = {
        "state_dict_mse": mse.state_dict(),
        "state_dict_abstract": abstract.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_loader, mse, abstract, criterion, meta_optimizer, epoch, meta_batch_size, max_meta_batches):
    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    # initialze cmem & grad
    cmem = CompoundMem()

    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch :", i

        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
            
            # generate a new task
            train_loader.dataset.reset()
            train_loader.dataset.training = True
            
            # clear mem
            cmem.clear()

            # adding support data into compound mem
            for _, (frame_fs, idx) in enumerate(train_loader):
                #print "\t\tsupport: ", _
                if i == 0 and j == 0 and _ == 0:
                    print frame_fs.shape, frame_fs.dtype
                    print idx.shape, idx.dtype, idx

                frame_fs = frame_fs[0]
                idx = idx[0]
                frame_fs = frame_fs.view(frame_fs.size(0), -1)
                frame_fs = frame_fs.cuda()

                weights = mse(frame_fs)
                Q = mse.embedding(frame_fs, weights)
                A = abstract(Q)
                cmem.insert(A, Q, idx, abstract)

                if i == 0 and j == 0 and _ == 0:
                    print weights.shape, weights.dtype
                    print Q.shape, Q.dtype
                    print A.shape, A.dtype

            # do query
            train_loader.dataset.training = False
            As = []
            labels = []
            for _, (frame_fs, label) in enumerate(train_loader):
                #print "\t\tquery: ", _

                frame_fs = frame_fs[0]
                frame_fs = frame_fs.view(frame_fs.size(0), -1)
                frame_fs = frame_fs.cuda()
                
                weights = mse(frame_fs)
                Q = mse.embedding(frame_fs, weights)
                A = abstract(Q)
                As.append(A)
                labels.append(label)
            
            As = torch.cat(As)
            labels = torch.cat(labels)
            out = cmem(As)
            #print out
           
            ps = []
            ns = []
            ac1 = 0.
            for idx, score in enumerate(out):
                for s in score:
                    if s[1] == labels[idx]:
                        ps.append(cmem.cmem[s[2]]["abstract"])
                        break

                for s in score:
                    if not s[1] == labels[idx]:
                        ns.append(cmem.cmem[s[2]]["abstract"])
                        break

                ac1 = ac1 + float(score[0][1] == labels[idx])
            
            ac1 = ac1 / len(out)

            ps = torch.cat(ps)
            ns = torch.cat(ns)
            #print As.shape, As.dtype
            #print ps.shape, ps.dtype
            #print ns.shape, ns.dtype

            # compute loss
            loss = criterion(As, ps, ns)
            
            # backward
            loss.backward()

            # record loss & accuracy
            losses.update(float(loss.data), len(train_loader))
            acc1.update(ac1, len(train_loader)) 

        # update parameters
        for _, param in enumerate(list(mse.parameters()) + list(abstract.parameters())):
            if i == 0 and  _ == 0:
                print param[:,0]
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
        
        # record results
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': losses.avg}, count_samples)

def validate(val_loader, mse, abstract, criterion, meta_batch_size, max_meta_batches):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    # initialze cmem & grad
    cmem = CompoundMem()

    with torch.no_grad():
        for i in tqdm(range(max_meta_batches)):
            #print "meta_batch :", i

            losses = AverageMeter()
            acc1 = AverageMeter()
            for j in range(meta_batch_size):
                #print "\ttask: ", j
                
                # generate a new task
                val_loader.dataset.reset()
                val_loader.dataset.training = True
                
                # clear mem
                cmem.clear()

                # adding support data into compound mem
                for _, (frame_fs, idx) in enumerate(val_loader):
                    #print "\t\tsupport: "_

                    frame_fs = frame_fs[0]
                    idx = idx[0]
                    frame_fs = frame_fs.view(frame_fs.size(0), -1)
                    frame_fs = frame_fs.cuda()

                    weights = mse(frame_fs)
                    Q = mse.embedding(frame_fs, weights)
                    A = abstract(Q)
                    cmem.insert(A, Q, idx, abstract)

                # do query
                val_loader.dataset.training = False
                As = []
                labels = []
                for _, (frame_fs, label) in enumerate(val_loader):
                    #print "\t\tquery: ", _

                    frame_fs = frame_fs[0]
                    frame_fs = frame_fs.view(frame_fs.size(0), -1)
                    frame_fs = frame_fs.cuda()
                    
                    weights = mse(frame_fs)
                    Q = mse.embedding(frame_fs, weights)
                    A = abstract(Q)
                    As.append(A)
                    labels.append(label)
                
                As = torch.cat(As)
                labels = torch.cat(labels)
                out = cmem(As)
               
                ps = []
                ns = []
                ac1 = 0.
                for idx, score in enumerate(out):
                    for s in score:
                        if s[1] == labels[idx]:
                            ps.append(cmem.cmem[s[2]]["abstract"])
                            break

                    for s in score:
                        if not s[1] == labels[idx]:
                            ns.append(cmem.cmem[s[2]]["abstract"])
                            break

                    ac1 = ac1 + float(score[0][1] == labels[idx])
                
                ac1 = ac1 / len(out)

                ps = torch.cat(ps)
                ns = torch.cat(ns)

                # compute loss
                loss = criterion(As, ps, ns)
                
                # no backward

                # record loss & accuracy
                losses.update(float(loss.data), len(val_loader))
                acc1.update(ac1, len(val_loader)) 

            # record results
            avg_losses.update(losses.avg, meta_batch_size)
            avg_acc1.update(acc1.avg, meta_batch_size)

            # measure one-batch time
            batch_time.update(time.time() - end)
            end = time.time()

    print("Validation End:\n\ttotal_time:{}\n\tavg_loss:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_losses.avg, avg_acc1.avg))

    return avg_losses.avg, avg_acc1.avg

def evaluate(test_loader, mse, abstract, test_file_num, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    # initialze cmem & grad
    cmem = CompoundMem()

    with torch.no_grad():
        for i in tqdm(range(test_file_num)):
            #print "test_file :", i

            # next task 
            test_loader.dataset.next_file()
            test_loader.dataset.training = True
            
            # clear mem
            cmem.clear()

            # adding support data into compound mem
            for _, (frame_fs, idx) in enumerate(test_loader):
                #print "\t\tsupport: "_

                frame_fs = frame_fs[0]
                idx = idx[0]
                frame_fs = frame_fs.view(frame_fs.size(0), -1)
                frame_fs = frame_fs.cuda()

                weights = mse(frame_fs)
                Q = mse.embedding(frame_fs, weights)
                A = abstract(Q)
                cmem.insert(A, Q, idx, abstract)

            idx_to_cls = test_loader.dataset.idx_to_class
            cls_to_idx = test_loader.dataset.class_to_idx
            acc1 = AverageMeter()
            cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

            # do query
            test_loader.dataset.training = False
            As = []
            labels = []
            for _, (frame_fs, label) in enumerate(test_loader):
                #print "\t\tquery: ", _

                frame_fs = frame_fs[0]
                frame_fs = frame_fs.view(frame_fs.size(0), -1)
                frame_fs = frame_fs.cuda()
                
                weights = mse(frame_fs)
                Q = mse.embedding(frame_fs, weights)
                A = abstract(Q)
                As.append(A)
                labels.append(label)
            
            As = torch.cat(As)
            labels = torch.cat(labels)
            out = cmem(As)
           
            for idx, score in enumerate(out):
                label = labels[idx]
                cls = idx_to_cls[int(label)]
                correct = float(score[0][1] == label)
                cls_acc1[cls].update(correct)
                acc1.update(correct)
            
            avg_acc1.update(acc1.avg, len(test_loader)) 

            # measure one-batch time
            batch_time.update(time.time() - end)
            end = time.time()

            output_file = os.path.join(output_dir, "result{}.txt".format(i))
            class_num = len(cls_to_idx.keys())

            with open(output_file, "w") as f:
                print >> f, "inference for {} classes, {} videos".\
                                format(class_num, len(test_loader))
                print >> f, "average top-1 accuracy: {}".format(acc1.avg)
                print >> f, "------------------------------------"
                for idx in range(class_num):
                    cls = idx_to_cls[idx]
                    print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def main():
    print args.replace_rate
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    train_dataset = MixtureFeatureSupFrameFeatureQryTrainDataset(
                        train_classes,
                        args.image_feature_dir,
                        args.frame_feature_dir,
                        args.task_class_num,
                        args.support_num_per_cls,
                        args.image_num_per_support,
                        args.kshot,
                        args.query_kshot,
                        args.noise_rate,
                        random_noise,
                        args.label_flip_rate,
                        args.replace_rate)

    train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_dataset = MixtureFeatureSupFrameFeatureQryTrainDataset(
                        val_classes,
                        args.image_feature_dir,
                        args.frame_feature_dir,
                        args.task_class_num,
                        args.support_num_per_cls,
                        args.image_num_per_support,
                        args.kshot,
                        args.query_kshot,
                        args.noise_rate,
                        random_noise,
                        args.label_flip_rate,
                        args.replace_rate)

    val_loader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # model
    feature_dim = model_features[args.model]
    mse = MSE(feature_dim, args.num_features)
    abstract = Abstract(args.num_features*feature_dim, args.abstract_dim)
    
    # loss & optimizer
    criterion = DotMarginLoss(margin=args.margin).cuda()
    meta_optimizer = torch.optim.SGD(list(mse.parameters())+list(abstract.parameters()), args.meta_learning_rate)

    # TODO: data parallel?
    mse = mse.cuda()
    abstract = abstract.cuda()

    # resume from checkpoint?
    # NOTE: only meta-learner params can be stored and resumed
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            mse.load_state_dict(checkpoint['state_dict_mse'])
            abstract.load_state_dict(checkpoint['state_dict_abstract'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    # loss func and optimizer
    cudnn.enabled = True
    cudnn.benchmark = True

    # evaluate?
    if args.evaluate:
        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        print "Evaluation on {}".format(args.test_file_dir)
        test_dataset = MixtureFeatureSupFrameFeatureQryTestDataset(test_classes, \
                                                args.test_file_dir, args.frame_dir, args.frame_feature_dir, \
                                                args.image_feature_dir, args.task_class_num, args.support_num_per_cls, \
                                                args.image_num_per_support, args.kshot, args.query_kshot,\
                                                args.noise_rate, random_noise, args.label_flip_rate,\
                                                args.replace_rate)

        test_loader = torch.utils.data.DataLoader(
                        test_dataset, batch_size=1, shuffle=False,
                        num_workers=args.workers)
        evaluate(test_loader, mse, abstract, args.test_file_num, args.test_output)
        return 

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        # train for one epoch
        train(train_loader, mse, abstract, criterion, meta_optimizer, epoch, args.meta_batch_size, args.max_meta_batches)
        save_tmp(mse, abstract, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_mse": mse.state_dict(),
                "state_dict_abstract": abstract.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            save_path = os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1))
            torch.save(state, save_path)

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss, acc1 = validate(val_loader, mse, abstract, criterion, \
                                    args.val_meta_batch_size, args.val_max_meta_batches)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)

    writer.close()

if __name__ == "__main__":
    main()
