import torch
import torch.nn as nn

class DotMarginLoss(nn.Module):

    def __init__(self, margin):
        super(DotMarginLoss, self).__init__()
        self.margin = margin

    def forward(self, a, p, n):
        """
            a(nchor): (N, D)
            p(ositive): (N, D)
            n(egative): (N, D)
        """
        p = (a*p).sum(dim=1)
        n = (a*n).sum(dim=1)

        l = self.margin - p + n
        l = l * (l > 0.).float()

        return l.mean()
