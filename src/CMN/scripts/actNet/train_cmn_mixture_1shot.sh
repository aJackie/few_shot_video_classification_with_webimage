python train_cmn_mixture.py \
    -m resnet50 \
    -nf 5 \
    -ad 2048 \
    -mb 32 \
    -vmb 32 \
    -w 0 \
    -g 0 \
    -mt_lr 0.001 \
    -mr 0.5 \
    --start_epoch 0 \
    --epochs 20 \
    --print_freq 1 \
    --save_freq 1 \
    --val_freq 1 \
    --train_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt \
    --val_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_val/meta_val.txt \
    --frame_dir /S2/MI/zxz/transfer_learning/data/actNet_frames \
    --frame_feature_dir /local/MI/xz/transfer_learning/data/actNet_frame_features \
    --image_feature_dir /local/MI/xz/transfer_learning/data/webimage_actNet_features \
    --task_class_num 5 \
    --kshot 1 \
    --query_kshot 1 \
    --support_num_per_cls 1 \
    --image_num_per_support 100 \
    --max_meta_batches 1000 \
    --val_max_meta_batches 10 \
    --log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/CMN/1/res50/mixture \
    --cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/CMN/1/res50/mixture \
    --cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/CMN/1/res50/mixture \
