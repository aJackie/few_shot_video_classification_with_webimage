import os
import sys
import math
import torch
import skvideo.io
import numpy as np
import time

from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint

class FrameFeatureTestDataset(Dataset):

    def __init__(self, test_file_dir, frame_dir, frame_feature_dir, task_class_num, \
                 batch_size, max_updates, \
                 sample_num, dup_num, noise_rate=0., noise_func=None):

        super(FrameFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.batch_size = batch_size
        self.max_updates = max_updates

        self.sample_num = sample_num
        self.dup_num = dup_num

        # training or query
        self.training = True 
        self.video_wise_train = False
        self.file_idx = -1

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))
        # for validation
        #self.test_file = os.path.join(self.test_file_dir, "val{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_frm_fs = []
        self.qry_vid_fs = []

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
            
            self.sup_frm_fs = self.sup_frm_fs + [(os.path.join(path, f), idx) for f in os.listdir(path)]

        self.kshot = len(self.sup_vid_fs[self.idx_to_class[0]])

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            # for validation
            #path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            self.qry_vid_fs.append((path, idx))

    def __len__(self):
        if self.training:
            if self.video_wise_train:
                return self.kshot * self.task_class_num
            else:
                return self.batch_size * self.max_updates
        else:
            return len(self.qry_vid_fs)

    def __getitem__(self, idx):
        if self.training:
            if not self.video_wise_train:
                """
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                jdx = jdx % self.kshot
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                kdx = randint(0, len(fs)-1)
                path = fs[kdx]
                frm_f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    frm_f = frm_f + self.noise_func(size=frm_f.shape).astype(frm_f.dtype)
                return frm_f, idx
                """
                idx = idx % len(self.sup_frm_fs)
                path, idx = self.sup_frm_fs[idx]
                f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                frm_f = [] 
                for f in fs:
                    frm_f.append(np.load(f))
                frm_f = np.stack(frm_f)

                return frm_f, idx

        else:
            vid, idx = self.qry_vid_fs[idx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            if self.sample_num == -1:
                select = np.concatenate([fs] * self.dup_num)
            else:
                select = np.array([])
                for _ in range(self.dup_num):
                    perms = np.random.permutation(len(fs))
                    select = np.concatenate([select, fs[perms][map(lambda x: x%len(fs), range(self.sample_num))]])

            frm_f = [] 
            for f in select:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

class MixtureFeatureTestDataset(Dataset):

    def __init__(self, all_classes, test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, task_class_num, \
                 batch_size, max_updates, \
                 sample_num, dup_num, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(MixtureFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.classes = all_classes

        self.sample_num = sample_num
        self.dup_num = dup_num

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        # training or query
        self.training = True 
        self.file_idx = -1

        self.train_image_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))
        # for validation
        #self.test_file = os.path.join(self.test_file_dir, "val{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}

        self.sup_vid_fs = {}
        self.sup_img_fs = {}
        self.sup_sam_fs = {}
        self.sup_sample_fs = []
        self.qry_vid_fs = []

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
            
            if self.sup_sam_fs.has_key(cls):
                self.sup_sam_fs[cls] = self.sup_sam_fs[cls] + [(os.path.join(path, f), idx, "f") for f in os.listdir(path)]
            else:
                self.sup_sam_fs[cls] = [(os.path.join(path, f), idx, "f") for f in os.listdir(path)]

        train_image_fs = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            train_image_fs[cls] = self.train_image_fs[cls]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num

        self.sup_img_fs = task_train_image_fs

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            self.sup_sam_fs[cls] = self.sup_sam_fs[cls] + [(img, idx, "i") for img in task_train_image_fs[cls]]
            shuffle(self.sup_sam_fs[cls])
            self.sup_sample_fs = self.sup_sample_fs + self.sup_sam_fs[cls]

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            # for validation
            #path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            self.qry_vid_fs.append((path, idx))

    def __len__(self):
        if self.training:
            return self.batch_size * self.max_updates
        else:
            return len(self.qry_vid_fs)

    def __getitem__(self, idx):
        if self.training:
            """
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            jdx = jdx % len(self.sup_sam_fs[cls])
            path, idx, flag = self.sup_sam_fs[cls][jdx]
            """
            idx = idx % len(self.sup_sample_fs)
            path, idx, flag = self.sup_sample_fs[idx]
            f = np.load(path)
            if flag == "i" and self.noise_rate > 0. and \
                    np.random.uniform() < self.noise_rate:
                f = f + self.noise_func(size=f.shape).astype(f.dtype)

            return f, idx
        else:
            vid, idx = self.qry_vid_fs[idx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            if self.sample_num == -1:
                select = np.concatenate([fs] * self.dup_num)
            else:
                select = np.array([])
                for _ in range(self.dup_num):
                    perms = np.random.permutation(len(fs))
                    select = np.concatenate([select, fs[perms][map(lambda x: x%len(fs), range(self.sample_num))]])

            frm_f = [] 
            for f in select:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

class ImageFeatureDataset(Dataset):

    def __init__(self, all_classes, image_feature_dir, batch_size, max_updates, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(ImageFeatureDataset, self).__init__()
        self.image_feature_dir = image_feature_dir
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.classes = all_classes

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        self.train_image_fs = {}
        self.all_image_fs = []

        self.train_image_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

    def set(self, class_to_idx):

        # choose videos
        self.class_to_idx = class_to_idx
        self.idx_to_class = {v:k for k,v in class_to_idx.items()}
        self.task_class_num = len(self.class_to_idx.keys())
        self.sup_img_fs = {}
        self.sup_image_fs = []

        train_image_fs = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            train_image_fs[cls] = self.train_image_fs[cls]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num

        self.sup_img_fs = task_train_image_fs

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            self.sup_image_fs = self.sup_image_fs + [(img, idx) for img in task_train_image_fs[cls]]

    def __len__(self):
        return self.batch_size * self.max_updates

    def __getitem__(self, idx):
        idx = idx % len(self.sup_image_fs)
        path, idx = self.sup_image_fs[idx]
        f = np.load(path)
        if self.noise_rate > 0. and \
            np.random.uniform() < self.noise_rate:
            f = f + self.noise_func(size=f.shape).astype(f.dtype)

        return f, idx

if __name__ == "__main__":
    import torchvision.transforms as transforms
    import time

    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    frame_feature_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features"
    image_feature_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features"
    test_file_dir = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1"
    
    def random_noise(size):
        return np.random.normal(10., 10.0, size)

    dataset = FrameFeatureTestDataset(\
                test_file_dir, frame_dir, frame_feature_dir, 5, -1, 1) 
    dataset2 = ImageFeatureDataset(image_feature_dir)

    #dataset.video_wise_train = True
    loader1 = DataLoader(dataset, batch_size=32, shuffle=True, num_workers=0)
    loader2 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    loader3 = DataLoader(dataset2, batch_size=64, shuffle=True, num_workers=0)
    loader1.dataset.next_file()
    print loader2.dataset.idx_to_class

    loader3.dataset.set(loader2.dataset.class_to_idx)

    for idx, (samples, target) in enumerate(loader1):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target
    
    loader1.dataset.training = False
    print loader2.dataset.training

    for idx, (samples, target) in enumerate(loader2):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target

    for idx, (samples, target) in enumerate(loader3):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target
    

