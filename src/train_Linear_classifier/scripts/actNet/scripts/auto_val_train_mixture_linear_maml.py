import os
import time

output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/mixture_classifier/res50/linear_maml/meta_val/step{}"
output_log_dir = "/S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/5way/1/mixture_classifier/res50/linear_maml/meta_val/step{}"
checkpoint = "/S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/mixture_maml_without_mlit/checkpoint_epoch700.pth.tar"

for steps in range(500, 700, 10):
    dir1 = output_dir.format(steps)
    dir2 = output_log_dir.format(steps)
    if not os.path.isdir(dir1):
        os.mkdir(dir1)
    if not os.path.isdir(dir2):
        os.mkdir(dir2)

    os.system("python train_mixture_maml.py " + \
                "-m resnet50 " + \
                "-r {} ".format(checkpoint) + \
                "-b 32 " + \
                "--max_updates {} ".format(steps) + \
                "--task_class_num 5 " + \
                "-w 0 " + \
                "-g 3 " + \
                "-tr_lr 0.01 " + \
                "--print_freq 1 " + \
                "--frame_dir /S2/MI/zxz/transfer_learning/data/actNet_frames " + \
                "--frame_feature_dir /local/MI/xz/transfer_learning/data/actNet_frame_features " + \
                "--image_feature_dir /local/MI/xz/transfer_learning/data/webimage_actNet_features " + \
                "--test_file_dir /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1 " + \
                "--st_file_idx 0 " + \
                "--end_file_idx 1000 " + \
                "--test_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt " + \
                "--output_dir {} ".format(dir1) + \
                "--output_log_dir {} ".format(dir2))
    time.sleep(1)
