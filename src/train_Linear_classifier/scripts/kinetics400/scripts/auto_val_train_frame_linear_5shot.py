import os
import time

output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/kinetics400/5way/5/frame_classifier/res50/linear/meta_val/step{}"
output_log_dir = "/S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/kinetics400/5way/5/frame_classifier/res50/linear/meta_val/step{}"
for steps in range(500, 700, 10):
    dir1 = output_dir.format(steps)
    dir2 = output_log_dir.format(steps)
    if not os.path.isdir(dir1):
        os.mkdir(dir1)
    if not os.path.isdir(dir2):
        os.mkdir(dir2)

    os.system("python train_frame.py " + \
                "-m resnet50 " + \
                "-b 32 " + \
                "--max_updates {} ".format(steps) + \
                "--task_class_num 5 " + \
                "-w 0 " + \
                "-g 2 " + \
                "-tr_lr 0.1 " + \
                "--print_freq 1 " + \
                "--frame_dir /S2/MI/zxz/transfer_learning/data/kinetics400_frame_features " + \
                "--frame_feature_dir /local/MI/xz/transfer_learning/data/kinetics400_frame_features " + \
                "--test_file_dir /home/zxz/transfer_learning/few_shot/data/kinetics_400/list/meta-learning/meta_test/5way/5 " + \
                "--st_file_idx 0 " + \
                "--end_file_idx 1000 " + \
                "--test_class_file /home/zxz/transfer_learning/few_shot/data/kinetics_400/list/meta-learning/meta_test/meta_test.txt " + \
                "--output_dir {} ".format(dir1) + \
                "--output_log_dir {} ".format(dir2))
    time.sleep(1)
