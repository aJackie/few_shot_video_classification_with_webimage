""" 
    Linear classifier on frames 
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

from data_generator import FrameFeatureTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="Linear classifier on frames ",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")

parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size of samples in one task")
parser.add_argument("--max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-tr_lr', '--train_learning_rate', default=1e-2, type=float, \
                    help="learner learning rate")

parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")

parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")

parser.add_argument("--noise_rate", type=float, default=0., help="possibility of random adding noise")
parser.add_argument("--normal_std", type=float, default=1., help="standard deviation of normal distribution")

parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")
parser.add_argument("--output_log_dir", type=str, help="path of output log dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                num_features, train_learning_rate, criterion, output_dir, output_log_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        if i < 10:
            log_dir = os.path.join(output_log_dir, "episode{}".format(i))
            if not os.path.isdir(log_dir):
                os.mkdir(log_dir)
            writer = SummaryWriter(log_dir=log_dir)

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # initialize a new classifier & optimizer
        classifier = nn.Linear(num_features, task_class_num).cuda()
        optimizer = torch.optim.SGD(classifier.parameters(), train_learning_rate)
            
        # do training
        for idx, (input_, target) in enumerate(test_sloader):
            
            input_var = input_.view(input_.size(0), -1).cuda()
            target_var = target.cuda()

            # do classification using last hidden
            output = classifier(input_var)

            # loss
            loss = criterion(output, target_var)
            
            # update classifier
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # record
            if i < 10:
                count_samples = (idx + 1)*test_sloader.dataset.batch_size
                writer.add_scalars('meta_train/loss', {'val': float(loss)}, count_samples)
                ac1 = accuracy(output, target_var, topk=(1,))
                writer.add_scalars('meta_train/accuracy/acc1', {'val': float(ac1[0][0])}, count_samples)

        if i < 10:
            writer.close()

        # do query
        test_qloader.dataset.training = False

        with torch.no_grad():
            for idx, (input_, target) in enumerate(test_qloader):
                
                input_var = input_[0]
                input_var = input_var.view(input_var.size(0), -1).cuda()
                target_var = target.cuda()

                output = classifier(input_var).sum(dim=0, keepdim=True)
                ac1 = accuracy(output, target_var, topk=(1,))
                acc1.update(float(ac1[0][0]), input_.size(0))
                cls_acc1[idx_to_cls[int(target[0])]].update(float(ac1[0][0]), input_.size(0))

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    num_features = model_features[args.model]

    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    criterion = nn.CrossEntropyLoss().cuda()
    cudnn.enabled = True
    cudnn.benchmark = True

    print "evaluation on {}".format(args.test_file_dir)

    with open(args.test_class_file, "r") as f:
        test_classes = [l.strip() for l in f.readlines()]

    test_dataset = FrameFeatureTestDataset(args.test_file_dir, 
                                           args.frame_dir, 
                                           args.frame_feature_dir, 
                                           args.task_class_num, 
                                           args.batch_size,
                                           args.max_updates,
                                           args.sample_num, args.dup_num,
                                           args.noise_rate, random_noise)

    test_sloader = torch.utils.data.DataLoader(
            test_dataset, batch_size=args.batch_size, shuffle=True,
            num_workers=args.workers)

    test_qloader = torch.utils.data.DataLoader(
            test_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                num_features, args.train_learning_rate, criterion, args.output_dir, args.output_log_dir)

if __name__ == "__main__":
    main()
