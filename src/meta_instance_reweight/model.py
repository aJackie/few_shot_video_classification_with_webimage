import torch
import torch.nn as nn
import torch.nn.functional as F

class Projection(nn.Module):

    def __init__(self, num_features=2048, pjt_features=2048):
        super(Projection, self).__init__()
        self.linear = nn.Linear(num_features, pjt_features)
        
    def forward(self, x):
        # x: (batch_size, num_features)
        x = self.linear(x)
        #x = F.normalize(x)
        return x

class Score(nn.Module):

    def __init__(self):
        super(Score, self).__init__()

    def forward(self, protos, x):
        # protos: (classes, dims)
        # x: (batch_size, dims)
        output = []
        classes = protos.size(0)
        batch_size = x.size(0)
        num_features = x.size(1)

        for i in range(batch_size):
            p = x[i].view(num_features, 1)
            o = torch.mm(protos, p).view(classes)
            output.append(o)

        return torch.stack(output)

################################
# Several Transformer structures
################################

class Transformer0(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer0, self).__init__()
        
    def forward(self, x):
        # identity
        return x

class Transformer1(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer1, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

class Transformer2(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer2, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features // 2),
            nn.Linear(num_features // 2, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

class Transformer3(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer3, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features // 2),
            nn.ReLU(True),
            nn.Linear(num_features // 2, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

################################
# Several Controller structures
################################

class Controller1(nn.Module):

    def __init__(self, num_features=2048):
        super(Controller1, self).__init__()
        self.controller = nn.Sequential(
            nn.Linear(2*num_features, num_features))

    def forward(self, xs):
        # xs.shape: (N, 2*num_features)
        x = self.controller(xs)
        return x

class Controller2(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Controller2, self).__init__()
        self.controller = nn.Sequential(
            nn.Linear(2*num_features, num_features),
            nn.Linear(num_features, num_features))
        
    def forward(self, xs):
        x = self.controller(xs)
        return x

class Controller3(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Controller3, self).__init__()
        self.controller = nn.Sequential(
            nn.Linear(2*num_features, num_features),
            nn.ReLU(True),
            nn.Linear(num_features, num_features))
        
    def forward(self, xs):
        x = self.controller(xs)
        return x

#########################################
# Several Taskmem-Transformer structures
#########################################

class TaskmemTransformer0(nn.Module):
    
    def __init__(self, num_input, num_output):
        super(TaskmemTransformer0, self).__init__()
        self.transformer = nn.Sequential(
                nn.Linear(num_input, num_output),
                nn.Sigmoid())
        
    def forward(self, x):
        x = self.transformer(x)
        return x


################################
# Several Task-reweigher structures
################################

class Reweigher0(nn.Module):
    
    def __init__(self, task_class_num):
        super(Reweigher0, self).__init__()
        dim = task_class_num * (task_class_num - 1) / 2
        self.reweigher = nn.Sequential(
                nn.Linear(dim, dim // 2),
                nn.ReLU(),
                nn.Linear(dim // 2, 1),
                )
        
    def forward(self, x):
        x = self.reweigher(x)
        return x


if __name__ == "__main__":
    transformer = Transformer(2048) 
    print transformer
    for param in transformer.parameters():
        print param.shape, param.dtype
