import torch
import torch.nn.functional as F
import model

class Intergration(object):
    def __call__(self, features, task_features, task_class_num):
        raise NotImplementedError

class Dot(Intergration):
    def __call__(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = F.normalize(expanded_scores, dim=1)

        expanded_features = torch.cat([features, expanded_scores], dim=1)
        expanded_features = F.normalize(expanded_features, dim=1)

        return expanded_features

    def score(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = F.normalize(expanded_scores, dim=1)

        return expanded_scores

class DotMean(Intergration):
    def __call__(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        expanded_features = torch.cat([features, expanded_scores], dim=1)
        expanded_features = F.normalize(expanded_features, dim=1)

        return expanded_features

    def score(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        return expanded_scores

    def score2(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)

        return expanded_scores

class ExpdotMean(Intergration):
    def __init__(self, temperature):
        super(ExpdotMean, self).__init__()
        self.temperature = temperature

    def __call__(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.exp(expanded_scores / self.temperature)
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        expanded_features = torch.cat([features, expanded_scores], dim=1)
        expanded_features = F.normalize(expanded_features, dim=1)

        return expanded_features

    def score(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.exp(expanded_scores / self.temperature)
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        return expanded_scores

    def score2(self, features, task_features, task_class_num):
        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.exp(expanded_scores / self.temperature)
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.stack([torch.mean(s, dim=1) for s in expanded_scores], dim=1)

        return expanded_scores

class DotTrans(Intergration):
    def __init__(self, tmchoice, task_class_num):
        super(DotTrans, self).__init__()
        self.transformer = model.__dict__["TaskmemTransformer{}".format(tmchoice)](task_class_num, 1).cuda()

    def resume(self, state_dict):
        self.transformer.load_state_dict(state_dict)

    def state_dict(self):
        return self.transformer.state_dict()

    def __call__(self, features, task_features, task_class_num):
        num_f = features.size(0)

        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.cat(expanded_scores)
        expanded_scores = self.transformer(expanded_scores)
        expanded_scores = torch.split(expanded_scores, num_f, dim=0)
        expanded_scores = torch.cat(expanded_scores, dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        expanded_features = torch.cat([features, expanded_scores], dim=1)
        expanded_features = F.normalize(expanded_features, dim=1)

        return expanded_features
    
    def score(self, features, task_features, task_class_num):
        num_f = features.size(0)

        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.cat(expanded_scores)
        expanded_scores = self.transformer(expanded_scores)
        expanded_scores = torch.split(expanded_scores, num_f, dim=0)
        expanded_scores = torch.cat(expanded_scores, dim=1)
        expanded_scores = F.normalize(expanded_scores, dim=1)

        return expanded_scores

    def score2(self, features, task_features, task_class_num):
        num_f = features.size(0)

        expanded_scores = torch.mm(features, task_features.transpose(1,0))
        expanded_scores = torch.split(expanded_scores, task_class_num, dim=1)
        expanded_scores = torch.cat(expanded_scores)
        expanded_scores = self.transformer(expanded_scores)
        expanded_scores = torch.split(expanded_scores, num_f, dim=0)
        expanded_scores = torch.cat(expanded_scores, dim=1)

        return expanded_scores
    
        

