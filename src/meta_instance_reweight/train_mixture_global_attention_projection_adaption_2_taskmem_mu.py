""" 
    training meta-instance-reweight model on mixture input with global attention
    features are further processed by projection function and adaption function
    a special task-memory is designed to remember informative historic tasks
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

import model
from data_generator import MixtureFeatureTrainDataset, MixtureFeatureTestDataset
from utils import *
from loss import DotMarginLoss
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

import sort_functions
from intergration import *

parser = argparse.ArgumentParser(description="training meta-instance-reweight model on mixture input with global attention,\
                                                features are further processed by projection function and adaption function, \
                                                a special task-memory is designed to remember informative historic tasks",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('--transformer_choice', default=1, type=int, help="type of transformer model to use")
parser.add_argument('--tmtransformer_choice', default=1, type=int, help="type of task_meme transformer model to use")

parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('--train_batch_size', default=64, type=int, help="train_batch_size")
parser.add_argument('--val_batch_size', default=64, type=int, help="val_batch_size")
parser.add_argument("--train_batches", type=int, default=100, help="train_batches")
parser.add_argument("--val_batches", type=int, default=100, help="val_batches")
parser.add_argument('--replace_batch_size', default=64, type=int, help="replace_batch_size")
parser.add_argument('--replace_tasks', default=10, type=int, help="replace_tasks")
parser.add_argument('--max_rounds', default=100, type=int, help="max_rounds")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")
parser.add_argument('-mr', '--margin', default=0.1, type=float, help="margin in loss")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")
parser.add_argument("--image_feature_dir", type=str, required=True, help="path of image root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--temperature", type=float, default=1.0, help="temperature for softmax")

parser.add_argument("--max_mem_slots", type=int, default=100, help="max slots to fetch for attention in one class")
parser.add_argument("--val_max_mem_slots", type=int, default=100, help="val max slots to fetch for attention in one class")

parser.add_argument("--max_time_steps", type=int, default=1, help="max steps of LSTM processing")
parser.add_argument("--val_max_time_steps", type=int, default=1, help="val max steps of LSTM processing")

parser.add_argument("--max_task_mem_slots", type=int, default=100, help="max slots to store historic tasks")
parser.add_argument('--intergrate_choice', default=0, type=int, help="type of intergration, 0: DotMean 1: ExpdotMean")
parser.add_argument("--inter_temperature", type=float, default=1.0, help="temperature for softmax of intergration")

parser.add_argument("--project_features", type=int, default=2048, help="dim of projection embedding space")

parser.add_argument("--noise_rate", type=float, default=0., help="possibility of random adding noise")
parser.add_argument("--normal_std", type=float, default=1., help="standard deviation of normal distribution")
parser.add_argument("--label_flip_rate", type=float, default=0., help="possibility of random flipping image labels")

parser.add_argument("--replace_rate", type=float, default=0., help="possibility of random replacing web images")

parser.add_argument('--log_dir',type=str, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, help="training config recording dir")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(project, transformer, process, task_mem, tmtransformer, optimizer, cp_dir):
    state = {
        "state_dict_project": project.state_dict(),
        "state_dict_transformer": transformer.state_dict(),
        "state_dict_process": process.state_dict(),
        "state_dict_task_mem": task_mem,
        "state_dict_tmtransformer": tmtransformer.state_dict(),
        "optimizer": optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def warm_up(train_sloader, train_qloader, task_mem, project, transformer, process, temperature,\
        project_features, max_mem_slots, max_time_steps, max_task_mem_slots):

    task_class_num = train_sloader.dataset.task_class_num
    for i in tqdm(range(max_task_mem_slots)):
        print "\ttask: ", i
            
        # generate a new task
        train_sloader.dataset.reset()
        train_sloader.dataset.training = True
        
        idx_to_cls = train_sloader.dataset.idx_to_class

        # get support features
        support_features = [0. for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]
        mem = [0. for _ in range(task_class_num)]
        
        with torch.no_grad():
            for idx, (input_, target) in enumerate(train_sloader):
                
                # initial lstm hidden state
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                # input_var are further projected
                input_var = project(input_var)

                fnum = input_var.size(0)
                target = int(target[0])

                # frames
                if idx < task_class_num:
                    tem_input_var = input_var[:max_mem_slots]
                    mem[target] = input_var
                # images
                else:
                    # further transformed for adaptation
                    input_var = transformer(input_var)
                    tem_input_var = mem[target][:max_mem_slots]

                lstm_hiddens = input_var
                # inital cell state
                lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                for t in range(max_time_steps):
                    # reweight batch-wise
                    scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                    scores = scores / temperature
                    scores = F.softmax(scores, dim=1)
                    lstm_inputs = torch.mm(scores, tem_input_var)

                    lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                # compute weights
                weights = torch.sum(lstm_hiddens * input_var, dim=1)
                all_weights[target].append(weights)

                # images
                if idx >= task_class_num:

                    cls_var = torch.cat((mem[target], input_var))
                    weights = torch.cat(all_weights[target])
                    weights = F.softmax(weights / temperature, dim=0)
                    weights = weights.view(-1, 1)
                    support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))
                   
            # (5, 2048)
            support_features = torch.cat(support_features)

            # write into task_mem
            task_mem[0].append(support_features)

    task_mem[0] = torch.cat(task_mem[0])

# only train task mem
def train(train_sloader, train_qloader, task_mem, project, transformer, process, intergration, \
            optimizer, criterion, temperature,\
            project_features, max_mem_slots, max_time_steps, \
            epoch, train_batch_size, train_batches, replace_batch_size, replace_tasks, max_rounds):

    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()
    avg_replace_num = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    for i in tqdm(range(max_rounds)):
            
        if i == 0:
            print "[{}] before training".format(datetime.now())
            for p in intergration.transformer.parameters():
                print p.data

        # first train the task mem
        for j in range(train_batches):
            if i == 0:
                print "[{}] train batch:".format(datetime.now()), j

            optimizer.zero_grad()
            losses = AverageMeter()
            acc1 = AverageMeter()

            for k in range(train_batch_size):
                
                # generate a new task
                train_sloader.dataset.reset()
                train_sloader.dataset.training = True
                
                idx_to_cls = train_sloader.dataset.idx_to_class

                # get support features
                support_features = [0. for _ in range(task_class_num)]
                all_weights = [[] for _ in range(task_class_num)]
                mem = [0. for _ in range(task_class_num)]

                with torch.no_grad():
                    for idx, (input_, target) in enumerate(train_sloader):
                        #print "\t\ttrain batch: ", idx
                        
                        # initial lstm hidden state
                        input_ = input_[0]
                        input_var = input_.view(input_.size(0), -1).cuda()
                        # input_var are further projected
                        input_var = project(input_var)

                        fnum = input_var.size(0)
                        target = int(target[0])

                        # frames
                        if idx < task_class_num:
                            tem_input_var = input_var[:max_mem_slots]
                            mem[target] = input_var
                        # images
                        else:
                            # further transformed for adaptation
                            input_var = transformer(input_var)
                            tem_input_var = mem[target][:max_mem_slots]

                        lstm_hiddens = input_var
                        # inital cell state
                        lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                        for t in range(max_time_steps):
                            # reweight batch-wise
                            scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                            scores = scores / temperature
                            scores = F.softmax(scores, dim=1)
                            lstm_inputs = torch.mm(scores, tem_input_var)

                            lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                        # compute weights
                        weights = torch.sum(lstm_hiddens * input_var, dim=1)
                        all_weights[target].append(weights)

                        # images
                        if idx >= task_class_num:

                            cls_var = torch.cat((mem[target], input_var))
                            weights = torch.cat(all_weights[target])
                            weights = F.softmax(weights / temperature, dim=0)
                            weights = weights.view(-1, 1)
                            support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))
                       
                # (5, 2048)
                support_features = torch.cat(support_features)
                support_scores = intergration.score(support_features, task_mem[0], task_class_num)

                if i == 0 and j == 0 and k == 0:
                    print "support_scores shape:", support_scores.shape

                # do query and get meta-gradient
                train_qloader.dataset.training = False

                As = []
                ps = []
                ns = []
                ac1 = 0.
                for idx, (input_, target) in enumerate(train_qloader):
                    #print "\t\tquery batch: ", idx
                    
                    input_ = input_[0]
                    input_var = input_.view(input_.size(0), -1).cuda()
                    input_var = project(input_var).mean(dim=0, keepdim=True)
                    input_var = F.normalize(input_var)
                    input_var = intergration.score(input_var, task_mem[0], task_class_num)

                    As.append(input_var)

                    score = torch.sum(input_var * support_scores, dim=1)
                    score = [(score[_], _) for _ in range(score.size(0))]
                    score.sort(reverse=True, key=lambda x: x[0])

                    ps.append(support_scores[int(target[0])])

                    for s in score:
                        if s[1] != int(target[0]):
                            ns.append(support_scores[s[1]])
                            break

                    ac1 = ac1 + float(score[0][1] == int(target[0]))

                ac1 = ac1 / len(train_qloader)
                As = torch.cat(As)
                ps = torch.stack(ps)
                ns = torch.stack(ns)

                loss = criterion(As, ps, ns)
                loss.backward()

                losses.update(float(loss.data), len(train_qloader))
                acc1.update(ac1, len(train_qloader))

            optimizer.step()
            avg_losses.update(losses.avg, train_batch_size)
            avg_acc1.update(acc1.avg, train_batch_size)

        if i == 0:
            print "[{}] after training".format(datetime.now())
            for p in intergration.transformer.parameters():
                print p.data

        if i == 0:
            print "replace"

        replace_num = 0.
        # then replace
        for j in range(replace_tasks):
            if i == 0:
                print "[{}] replace batch:".format(datetime.now()), j

            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            idx_to_cls = train_sloader.dataset.idx_to_class

            # get support features
            support_features = [0. for _ in range(task_class_num)]
            all_weights = [[] for _ in range(task_class_num)]
            mem = [0. for _ in range(task_class_num)]

            with torch.no_grad():
                for idx, (input_, target) in enumerate(train_sloader):
                    #print "\t\ttrain batch: ", idx
                    
                    # initial lstm hidden state
                    input_ = input_[0]
                    input_var = input_.view(input_.size(0), -1).cuda()
                    # input_var are further projected
                    input_var = project(input_var)

                    fnum = input_var.size(0)
                    target = int(target[0])

                    # frames
                    if idx < task_class_num:
                        tem_input_var = input_var[:max_mem_slots]
                        mem[target] = input_var
                    # images
                    else:
                        # further transformed for adaptation
                        input_var = transformer(input_var)
                        tem_input_var = mem[target][:max_mem_slots]

                    lstm_hiddens = input_var
                    # inital cell state
                    lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                    for t in range(max_time_steps):
                        # reweight batch-wise
                        scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                        scores = scores / temperature
                        scores = F.softmax(scores, dim=1)
                        lstm_inputs = torch.mm(scores, tem_input_var)

                        lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                    # compute weights
                    weights = torch.sum(lstm_hiddens * input_var, dim=1)
                    all_weights[target].append(weights)

                    # images
                    if idx >= task_class_num:

                        cls_var = torch.cat((mem[target], input_var))
                        weights = torch.cat(all_weights[target])
                        weights = F.softmax(weights / temperature, dim=0)
                        weights = weights.view(-1, 1)
                        support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))
                   
            # (5, 2048)
            support_features = torch.cat(support_features)
            pend_support_features = torch.cat([task_mem[0], support_features], dim=0)
            sub_losses = [0. for _ in range(pend_support_features.size(0) / task_class_num)]
            if i == 0 and j == 0:
                print "pend_support_features.shape:", pend_support_features.shape
            
            with torch.no_grad():
                for k in range(replace_batch_size):
                    # generate a new task
                    train_sloader.dataset.reset()
                    train_sloader.dataset.training = True
                    
                    idx_to_cls = train_sloader.dataset.idx_to_class

                    # get support features
                    support_features = [0. for _ in range(task_class_num)]
                    all_weights = [[] for _ in range(task_class_num)]
                    mem = [0. for _ in range(task_class_num)]

                    for idx, (input_, target) in enumerate(train_sloader):
                        #print "\t\ttrain batch: ", idx
                        
                        # initial lstm hidden state
                        input_ = input_[0]
                        input_var = input_.view(input_.size(0), -1).cuda()
                        # input_var are further projected
                        input_var = project(input_var)

                        fnum = input_var.size(0)
                        target = int(target[0])

                        # frames
                        if idx < task_class_num:
                            tem_input_var = input_var[:max_mem_slots]
                            mem[target] = input_var
                        # images
                        else:
                            # further transformed for adaptation
                            input_var = transformer(input_var)
                            tem_input_var = mem[target][:max_mem_slots]

                        lstm_hiddens = input_var
                        # inital cell state
                        lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                        for t in range(max_time_steps):
                            # reweight batch-wise
                            scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                            scores = scores / temperature
                            scores = F.softmax(scores, dim=1)
                            lstm_inputs = torch.mm(scores, tem_input_var)

                            lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                        # compute weights
                        weights = torch.sum(lstm_hiddens * input_var, dim=1)
                        all_weights[target].append(weights)

                        # images
                        if idx >= task_class_num:

                            cls_var = torch.cat((mem[target], input_var))
                            weights = torch.cat(all_weights[target])
                            weights = F.softmax(weights / temperature, dim=0)
                            weights = weights.view(-1, 1)
                            support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))
                           
                    # (5, 2048)
                    support_features = torch.cat(support_features)
                    support_scores = intergration.score2(support_features, pend_support_features, task_class_num)

                    if i == 0 and j == 0 and k == 0:
                        print "support_scores.shape:", support_scores.shape

                    # do query
                    train_qloader.dataset.training = False

                    As = [0. for _ in range(len(train_qloader))]
                    for idx, (input_, target) in enumerate(train_qloader):
                        #print "\t\tquery batch: ", idx
                        
                        input_ = input_[0]
                        input_var = input_.view(input_.size(0), -1).cuda()
                        input_var = project(input_var).mean(dim=0, keepdim=True)
                        input_var = F.normalize(input_var)
                        input_var = intergration.score2(input_var, pend_support_features, task_class_num)

                        As[int(target[0])] = input_var

                    As = torch.cat(As)
                    for t in range(pend_support_features.size(0) / task_class_num):
                        sub_support_scores = F.normalize(torch.cat([support_scores[:t], support_scores[t+1:]]))
                        sub_As = F.normalize(torch.cat([As[:t], As[t+1:]]))
                        ps = []
                        ns = []
                        for target in range(sub_As.size(0)):
                            input_var = sub_As[target:target+1]
                                    
                            score = torch.sum(input_var * sub_support_scores, dim=1)
                            score = [(score[_], _) for _ in range(score.size(0))]
                            score.sort(reverse=True, key=lambda x: x[0])

                            ps.append(sub_support_scores[target])

                            for s in score:
                                if s[1] != target:
                                    ns.append(sub_support_scores[s[1]])
                                    break

                        ps = torch.stack(ps)
                        ns = torch.stack(ns)

                        loss = criterion(sub_As, ps, ns)
                        sub_losses[t] = sub_losses[t] + float(loss.data)

                sub_losses = [s / replace_batch_size for s in sub_losses]
                min_indice = np.argmin(sub_losses) 

                # do replace
                task_mem[0] = torch.cat([pend_support_features[:min_indice*5], pend_support_features[(min_indice+1)*5:]])
                if not min_indice == ((pend_support_features.size(0) / task_class_num) - 1):
                    replace_num = replace_num + 1
        
        avg_replace_num.update(replace_num / replace_tasks, replace_tasks)

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = epoch*max_rounds + i + 1
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)
            writer.add_scalars('meta_train/statistics/replace_num', {'val': avg_replace_num.val}, count_samples)

    # epoch record
    count_samples = (epoch+1)*max_rounds
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)
    writer.add_scalars('meta_train/statistics/replace_num_epoch', {'avg': avg_replace_num.avg}, count_samples)

def validate(val_sloader, val_qloader, task_mem, project, transformer, process, intergration, \
                criterion, temperature, \
                project_features, val_max_mem_slots, val_max_time_steps,
                val_batch_size, val_batches):

    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    for i in tqdm(range(max_rounds)):
            
        # first train the task mem
        for j in range(val_batches):
            losses = AverageMeter()
            acc1 = AverageMeter()

            for k in range(val_batch_size):
                
                # generate a new task
                val_sloader.dataset.reset()
                val_sloader.dataset.training = True
                
                idx_to_cls = val_sloader.dataset.idx_to_class

                # get support features
                support_features = [0. for _ in range(task_class_num)]
                all_weights = [[] for _ in range(task_class_num)]
                mem = [0. for _ in range(task_class_num)]

                with torch.no_grad():
                    for idx, (input_, target) in enumerate(val_sloader):
                        #print "\t\ttrain batch: ", idx
                        
                        # initial lstm hidden state
                        input_ = input_[0]
                        input_var = input_.view(input_.size(0), -1).cuda()
                        # input_var are further projected
                        input_var = project(input_var)

                        fnum = input_var.size(0)
                        target = int(target[0])

                        # frames
                        if idx < task_class_num:
                            tem_input_var = input_var[:val_max_mem_slots]
                            mem[target] = input_var
                        # images
                        else:
                            # further transformed for adaptation
                            input_var = transformer(input_var)
                            tem_input_var = mem[target][:val_max_mem_slots]

                        lstm_hiddens = input_var
                        # inital cell state
                        lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                        for t in range(val_max_time_steps):
                            # reweight batch-wise
                            scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                            scores = scores / temperature
                            scores = F.softmax(scores, dim=1)
                            lstm_inputs = torch.mm(scores, tem_input_var)

                            lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                        # compute weights
                        weights = torch.sum(lstm_hiddens * input_var, dim=1)
                        all_weights[target].append(weights)

                        # images
                        if idx >= task_class_num:

                            cls_var = torch.cat((mem[target], input_var))
                            weights = torch.cat(all_weights[target])
                            weights = F.softmax(weights / temperature, dim=0)
                            weights = weights.view(-1, 1)
                            support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))
                       
                    # (5, 2048)
                    support_features = torch.cat(support_features)
                    support_scores = intergration.score(support_features, task_mem[0], task_class_num)

                    # do query
                    val_qloader.dataset.training = False

                    As = []
                    ps = []
                    ns = []
                    ac1 = 0.
                    for idx, (input_, target) in enumerate(val_qloader):
                        #print "\t\tquery batch: ", idx
                        
                        input_ = input_[0]
                        input_var = input_.view(input_.size(0), -1).cuda()
                        input_var = project(input_var).mean(dim=0, keepdim=True)
                        input_var = F.normalize(input_var)
                        input_var = intergration.score(input_var, task_mem[0], task_class_num)

                        As.append(input_var)

                        score = torch.sum(input_var * support_scores, dim=1)
                        score = [(score[_], _) for _ in range(score.size(0))]
                        score.sort(reverse=True, key=lambda x: x[0])

                        ps.append(support_scores[int(target[0])])

                        for s in score:
                            if s[1] != int(target[0]):
                                ns.append(support_scores[s[1]])
                                break

                        ac1 = ac1 + float(score[0][1] == int(target[0]))

                    ac1 = ac1 / len(val_qloader)
                    As = torch.cat(As)
                    ps = torch.stack(ps)
                    ns = torch.stack(ns)

                    loss = criterion(As, ps, ns)

                    losses.update(float(loss.data), len(val_qloader))
                    acc1.update(ac1, len(val_qloader))

            avg_losses.update(losses.avg, val_batch_size)
            avg_acc1.update(acc1.avg, val_batch_size)
    
    return avg_losses.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, task_mem, st_file_idx, end_file_idx, test_classes, \
                project, transformer, process, intergration, temperature, \
                project_features, max_mem_slots, max_time_steps, output_dir):

    avg_acc1 = AverageMeter()
    avg_acc1_a = AverageMeter()
    avg_acc1_b = AverageMeter()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_a = AverageMeter()
        cls_acc1_a = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_b = AverageMeter()
        cls_acc1_b = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get support features
        support_features = [0. for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]
        mem = [0. for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            #print "\t\ttrain batch: ", idx
            
            # initial lstm hidden state
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()

            with torch.no_grad():
                # input_var are further projected
                input_var = project(input_var)

                fnum = input_var.size(0)
                target = int(target[0])

                # frames
                if idx < task_class_num:
                    tem_input_var = input_var[:max_mem_slots]
                    mem[target] = input_var
                # images
                else:
                    # further transformed for adaptation
                    input_var = transformer(input_var)
                    tem_input_var = mem[target][:max_mem_slots]

                lstm_hiddens = input_var
                # inital cell state
                lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                for t in range(max_time_steps):
                    # reweight batch-wise
                    scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                    scores = scores / temperature
                    scores = F.softmax(scores, dim=1)
                    lstm_inputs = torch.mm(scores, tem_input_var)

                    lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                # compute weights
                weights = torch.sum(lstm_hiddens * input_var, dim=1)
                all_weights[target].append(weights)

                # images
                if idx >= task_class_num:

                    cls_var = torch.cat((mem[target], input_var))
                    weights = torch.cat(all_weights[target])
                    weights = F.softmax(weights / temperature, dim=0)
                    weights = weights.view(-1, 1)
                    support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))

        with torch.no_grad():
            # (5, 2048)
            support_features = torch.cat(support_features)
            support_scores = intergration.score(support_features, task_mem[0], task_class_num)
            expanded_features = F.normalize(torch.cat([support_features, support_scores], dim=1))
            if i == st_file_idx:
                print "support_scores:", support_scores.shape
                print "expanded_features:", expanded_features.shape

        # do query
        test_qloader.dataset.training = False

        for idx, (input_, target) in enumerate(test_qloader):
            #print "\t\tquery batch: ", idx
            
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            with torch.no_grad():
                input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)
                input_score = intergration.score(input_var, task_mem[0], task_class_num)
                input_expand = F.normalize(torch.cat([input_var, input_score], dim=1))

            # old feature
            score = torch.sum(input_var * support_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_a.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_a[cls].update(ac1)

            # score feature
            score = torch.sum(input_score * support_scores, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_b.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_b[cls].update(ac1)

            # expanded feature
            score = torch.sum(input_expand * expanded_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1[cls].update(ac1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 
        avg_acc1_a.update(acc1_a.avg, len(test_qloader)) 
        avg_acc1_b.update(acc1_b.avg, len(test_qloader)) 

        # measure one-batch time

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy_a: {}".format(acc1_a.avg)
            print >> f, "average top-1 accuracy_b: {}".format(acc1_b.avg)
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_a {}".format(cls, cls_acc1_a[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_b {}".format(cls, cls_acc1_b[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\tavg_acc1:{}\n\t \
            avg_acc1_a:{}\n\tavg_acc1_b:{}\n".\
            format(avg_acc1.avg, avg_acc1_a.avg, avg_acc1_b.avg))

def tem_evaluate(test_sloader, test_qloader, task_mem, st_file_idx, end_file_idx, test_classes, \
                    project, transformer, process, intergration, temperature, \
                    project_features, max_mem_slots, max_time_steps, output_dir):

    avg_acc1 = AverageMeter()
    avg_acc1_a = AverageMeter()
    avg_acc1_b = AverageMeter()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_a = AverageMeter()
        cls_acc1_a = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_b = AverageMeter()
        cls_acc1_b = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get support features
        support_features = [0. for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]
        mem = [0. for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            #print "\t\ttrain batch: ", idx
            
            # initial lstm hidden state
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()

            with torch.no_grad():
                # input_var are further projected
                input_var = project(input_var)

                fnum = input_var.size(0)
                target = int(target[0])

                # frames
                if idx < task_class_num:
                    tem_input_var = input_var[:max_mem_slots]
                    mem[target] = input_var
                # images
                else:
                    # further transformed for adaptation
                    input_var = transformer(input_var)
                    tem_input_var = mem[target][:max_mem_slots]

                lstm_hiddens = input_var
                # inital cell state
                lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                for t in range(max_time_steps):
                    # reweight batch-wise
                    scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                    scores = scores / temperature
                    scores = F.softmax(scores, dim=1)
                    lstm_inputs = torch.mm(scores, tem_input_var)

                    lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                # compute weights
                weights = torch.sum(lstm_hiddens * input_var, dim=1)
                all_weights[target].append(weights)

                # images
                if idx >= task_class_num:

                    cls_var = torch.cat((mem[target], input_var))
                    weights = torch.cat(all_weights[target])
                    weights = F.softmax(weights / temperature, dim=0)
                    weights = weights.view(-1, 1)
                    support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))

        with torch.no_grad():
            # (5, 2048)
            support_features = torch.cat(support_features)
            support_scores = intergration.score2(support_features, task_mem[0], 1)
            support_scores = torch.split(support_scores, task_class_num, dim=1)
            support_scores = [F.normalize(s) for s in support_scores]
            support_scores = torch.cat(support_scores, dim=1)
            support_scores = F.normalize(support_scores)
            expanded_features = F.normalize(torch.cat([support_features, support_scores], dim=1)) 
                
            if i == st_file_idx:
                print "support_scores:", support_scores.shape
                print "expanded_features:", expanded_features.shape

        # do query
        test_qloader.dataset.training = False

        for idx, (input_, target) in enumerate(test_qloader):
            #print "\t\tquery batch: ", idx
            
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            with torch.no_grad():
                input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)
                input_score = intergration.score2(input_var, task_mem[0], 1)
                input_score = torch.split(input_score, task_class_num, dim=1)
                input_score = [F.normalize(s) for s in input_score]
                input_score = torch.cat(input_score, dim=1)
                input_score = F.normalize(input_score)
                input_expand = F.normalize(torch.cat([input_var, input_score], dim=1))

            # old feature
            score = torch.sum(input_var * support_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_a.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_a[cls].update(ac1)

            # score feature
            score = torch.sum(input_score * support_scores, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_b.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_b[cls].update(ac1)

            # expanded feature
            score = torch.sum(input_expand * expanded_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1[cls].update(ac1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 
        avg_acc1_a.update(acc1_a.avg, len(test_qloader)) 
        avg_acc1_b.update(acc1_b.avg, len(test_qloader)) 

        # measure one-batch time

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy_a: {}".format(acc1_a.avg)
            print >> f, "average top-1 accuracy_b: {}".format(acc1_b.avg)
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_a {}".format(cls, cls_acc1_a[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_b {}".format(cls, cls_acc1_b[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\tavg_acc1:{}\n\t \
            avg_acc1_a:{}\n\tavg_acc1_b:{}\n".\
            format(avg_acc1.avg, avg_acc1_a.avg, avg_acc1_b.avg))

def tem_evaluate2(test_sloader, test_qloader, task_mem, st_file_idx, end_file_idx, test_classes, \
                    project, transformer, process, intergration, temperature, \
                    project_features, max_mem_slots, max_time_steps, output_dir):

    avg_acc1 = AverageMeter()
    avg_acc1_a = AverageMeter()
    avg_acc1_b = AverageMeter()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_a = AverageMeter()
        cls_acc1_a = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_b = AverageMeter()
        cls_acc1_b = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get support features
        support_features = [0. for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]
        mem = [0. for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            #print "\t\ttrain batch: ", idx
            
            # initial lstm hidden state
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()

            with torch.no_grad():
                # input_var are further projected
                input_var = project(input_var)

                fnum = input_var.size(0)
                target = int(target[0])

                # frames
                if idx < task_class_num:
                    tem_input_var = input_var[:max_mem_slots]
                    mem[target] = input_var
                # images
                else:
                    # further transformed for adaptation
                    input_var = transformer(input_var)
                    tem_input_var = mem[target][:max_mem_slots]

                lstm_hiddens = input_var
                # inital cell state
                lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                for t in range(max_time_steps):
                    # reweight batch-wise
                    scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                    scores = scores / temperature
                    scores = F.softmax(scores, dim=1)
                    lstm_inputs = torch.mm(scores, tem_input_var)

                    lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                # compute weights
                weights = torch.sum(lstm_hiddens * input_var, dim=1)
                all_weights[target].append(weights)

                # images
                if idx >= task_class_num:

                    cls_var = torch.cat((mem[target], input_var))
                    weights = torch.cat(all_weights[target])
                    weights = F.softmax(weights / temperature, dim=0)
                    weights = weights.view(-1, 1)
                    support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))

        with torch.no_grad():
            # (5, 2048)
            support_features = torch.cat(support_features)
            support_scores = intergration.score2(support_features, task_mem[0], 1)
            support_scores = torch.split(support_scores, task_class_num, dim=1)
            support_scores = [F.normalize(s) for s in support_scores]
                
        # do query
        test_qloader.dataset.training = False

        for idx, (input_, target) in enumerate(test_qloader):
            #print "\t\tquery batch: ", idx
            
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            with torch.no_grad():
                input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)
                input_score = intergration.score2(input_var, task_mem[0], 1)
                input_score = torch.split(input_score, task_class_num, dim=1)
                input_score = [F.normalize(s) for s in input_score]

            # old feature
            old_score = torch.sum(input_var * support_features, dim=1)
            # new features
            new_score = 0.
            for _ in range(len(input_score)):
                new_score = new_score + torch.sum(input_score[_] * support_scores[_], dim=1)

            new_score = new_score / len(input_score)
            score = old_score + new_score

            # old score
            old_score = [(old_score[_], _) for _ in range(old_score.size(0))]
            old_score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(old_score[0][1] == int(target[0]))
            acc1_a.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_a[cls].update(ac1)

            # new scores 
            new_score = [(new_score[_], _) for _ in range(new_score.size(0))]
            new_score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(new_score[0][1] == int(target[0]))
            acc1_b.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_b[cls].update(ac1)

            # fusion scores
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1[cls].update(ac1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 
        avg_acc1_a.update(acc1_a.avg, len(test_qloader)) 
        avg_acc1_b.update(acc1_b.avg, len(test_qloader)) 

        # measure one-batch time

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy_a: {}".format(acc1_a.avg)
            print >> f, "average top-1 accuracy_b: {}".format(acc1_b.avg)
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_a {}".format(cls, cls_acc1_a[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_b {}".format(cls, cls_acc1_b[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\tavg_acc1:{}\n\t \
            avg_acc1_a:{}\n\tavg_acc1_b:{}\n".\
            format(avg_acc1.avg, avg_acc1_a.avg, avg_acc1_b.avg))

def tem_evaluate3(test_sloader, test_qloader, task_mem, st_file_idx, end_file_idx, test_classes, \
                    project, transformer, process, intergration, temperature, \
                    project_features, max_mem_slots, max_time_steps, output_dir):

    avg_acc1 = AverageMeter()
    avg_acc1_a = AverageMeter()
    avg_acc1_b = AverageMeter()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_a = AverageMeter()
        cls_acc1_a = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        acc1_b = AverageMeter()
        cls_acc1_b = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get support features
        support_features = [0. for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]
        mem = [0. for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            #print "\t\ttrain batch: ", idx
            
            # initial lstm hidden state
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()

            with torch.no_grad():
                # input_var are further projected
                input_var = project(input_var)

                fnum = input_var.size(0)
                target = int(target[0])

                # frames
                if idx < task_class_num:
                    tem_input_var = input_var[:max_mem_slots]
                    mem[target] = input_var
                # images
                else:
                    # further transformed for adaptation
                    input_var = transformer(input_var)
                    tem_input_var = mem[target][:max_mem_slots]

                lstm_hiddens = input_var
                # inital cell state
                lstm_cells = torch.cuda.FloatTensor(fnum, project_features).fill_(0)

                for t in range(max_time_steps):
                    # reweight batch-wise
                    scores = torch.mm(lstm_hiddens, tem_input_var.transpose(1,0))
                    scores = scores / temperature
                    scores = F.softmax(scores, dim=1)
                    lstm_inputs = torch.mm(scores, tem_input_var)

                    lstm_hiddens, lstm_cells = process(lstm_inputs, (lstm_hiddens, lstm_cells))

                # compute weights
                weights = torch.sum(lstm_hiddens * input_var, dim=1)
                all_weights[target].append(weights)

                # images
                if idx >= task_class_num:

                    cls_var = torch.cat((mem[target], input_var))
                    weights = torch.cat(all_weights[target])
                    weights = F.softmax(weights / temperature, dim=0)
                    weights = weights.view(-1, 1)
                    support_features[target] = F.normalize(torch.sum(cls_var * weights, dim=0, keepdim=True))

        with torch.no_grad():
            # (5, 2048)
            support_features = torch.cat(support_features)
            support_scores = intergration.score(support_features, task_mem[0], 1)
            expanded_features = F.normalize(torch.cat([support_features, support_scores], dim=1)) 
                
            if i == st_file_idx:
                print "support_scores:", support_scores.shape
                print "expanded_features:", expanded_features.shape

        # do query
        test_qloader.dataset.training = False

        for idx, (input_, target) in enumerate(test_qloader):
            #print "\t\tquery batch: ", idx
            
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            with torch.no_grad():
                input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)
                input_score = intergration.score(input_var, task_mem[0], 1)
                input_expand = F.normalize(torch.cat([input_var, input_score], dim=1))

            # old feature
            score = torch.sum(input_var * support_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_a.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_a[cls].update(ac1)

            # score feature
            score = torch.sum(input_score * support_scores, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1_b.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1_b[cls].update(ac1)

            # expanded feature
            score = torch.sum(input_expand * expanded_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1[cls].update(ac1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 
        avg_acc1_a.update(acc1_a.avg, len(test_qloader)) 
        avg_acc1_b.update(acc1_b.avg, len(test_qloader)) 

        # measure one-batch time

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy_a: {}".format(acc1_a.avg)
            print >> f, "average top-1 accuracy_b: {}".format(acc1_b.avg)
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_a {}".format(cls, cls_acc1_a[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1_b {}".format(cls, cls_acc1_b[cls].avg)

            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\tavg_acc1:{}\n\t \
            avg_acc1_a:{}\n\tavg_acc1_b:{}\n".\
            format(avg_acc1.avg, avg_acc1_a.avg, avg_acc1_b.avg))



def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    train_dataset = MixtureFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.image_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot,
                        args.noise_rate,
                        random_noise,
                        args.label_flip_rate,
                        args.replace_rate)

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=True,
            num_workers=args.workers)

    val_dataset = MixtureFeatureTrainDataset(
                    val_classes,
                    args.frame_feature_dir,
                    args.image_feature_dir,
                    args.task_class_num,
                    args.val_kshot,
                    args.val_query_kshot,
                    args.noise_rate,
                    random_noise,
                    args.label_flip_rate,
                    args.replace_rate)

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=True,
            num_workers=args.workers)

    # model
    num_features = model_features[args.model]
    project = model.__dict__["Projection"](num_features, args.project_features).cuda()
    transformer = model.__dict__["Transformer{}".format(args.transformer_choice)](args.project_features).cuda()
    process = nn.LSTMCell(input_size=args.project_features, hidden_size=args.project_features, bias=True).cuda()

    if args.intergrate_choice == 0:
        intergration = DotMean()
    elif args.intergrate_choice == 1:
        intergration = ExpdotMean(args.inter_temperature)
    elif args.intergrate_choice == 2:
        intergration = DotTrans(args.tmtransformer_choice, args.task_class_num)
    else:
        raise NotImplementedError

    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            project.load_state_dict(checkpoint['state_dict_project'])
            transformer.load_state_dict(checkpoint['state_dict_transformer'])
            process.load_state_dict(checkpoint['state_dict_process'])

            if checkpoint.has_key("state_dict_task_mem"):
                task_mem = checkpoint["state_dict_task_mem"]
                print "load task_mem from checkpoint"
            else:
                task_mem = []
                print "init task_mem"

            if checkpoint.has_key("state_dict_tmtransformer"):
                intergration.resume(checkpoint["state_dict_tmtransformer"])
                print "load tmtransformer from checkpoint"

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))
    else:
        task_mem = []
        print "init task_mem"

    cudnn.enabled = True
    cudnn.benchmark = True

    # task memory
    if len(task_mem) == 0:
        print "warming up task_mem"
        task_mem = [[]]
        # warm up, store some tasks
        warm_up(train_sloader, train_qloader, task_mem, project, transformer, process, args.temperature, \
                args.project_features, args.max_mem_slots, args.max_time_steps, args.max_task_mem_slots)

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = MixtureFeatureTestDataset(test_classes, args.test_file_dir, args.frame_dir, args.frame_feature_dir, \
                            args.image_feature_dir, args.task_class_num, args.sample_num, args.dup_num, \
                            args.noise_rate, random_noise, args.label_flip_rate, args.replace_rate)

        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        evaluate(test_sloader, test_qloader, task_mem, args.st_file_idx, args.end_file_idx, test_classes,\
                        project, transformer, process, intergration, args.temperature, args.project_features, \
                        args.max_mem_slots, args.max_time_steps, args.output_dir)
        
        return

    # loss & optimizer
    criterion = DotMarginLoss(args.margin).cuda()
    optimizer = torch.optim.SGD(intergration.transformer.parameters(), args.meta_learning_rate)

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        if epoch == args.start_epoch:
            print "before one epoch"
            for p in intergration.transformer.parameters():
                print p.data

        train(train_sloader, train_qloader, task_mem, project, transformer, process, intergration, \
                optimizer, criterion, args.temperature, \
                args.project_features, args.max_mem_slots, args.max_time_steps, \
                epoch, args.train_batch_size, args.train_batches, args.replace_batch_size, args.replace_tasks, args.max_rounds)

        if epoch == args.start_epoch:
            print "after one epoch"
            for p in intergration.transformer.parameters():
                print p.data

        save_tmp(project, transformer, process, task_mem, intergration.transformer, optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_project": project.state_dict(),
                "state_dict_transformer": transformer.state_dict(),
                "state_dict_process": process.state_dict(),
                "state_dict_task_mem": task_mem,
                "state_dict_tmtransformer": tmtransformer.state_dict(),
                "optimizer": optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss, acc1 = validate(val_sloader, val_qloader, task_mem, project, transformer, \
                                process, intergration, criterion, \
                                args.temperature, \
                                args.project_features, \
                                args.val_max_mem_slots,\
                                args.val_max_time_steps,\
                                args.val_batch_size,\
                                args.val_batches)

            count_samples = (epoch + 1) * args.max_rounds
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)

    writer.close()

if __name__ == "__main__":
    main()
