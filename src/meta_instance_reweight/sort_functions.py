"""
    a set of different sort functions
    all functions have only two parameters of paths and weights
    and return indices after sorting
"""
import numpy as np

################################
# SORT FUNCTIONS FOR PURE FRAMES
################################

# sort by 1.video name 2.frame order
def sort0(paths, ws):
    def cmp_func(p1, p2):
        p1 = p1[0]
        p2 = p2[0]
        it1 = p1.split("/")
        it2 = p2.split("/")
        vid1 = it1[-2]
        vid2 = it2[-2]
        fname1 = it1[-1].split(".")[0]
        fname2 = it2[-1].split(".")[0]
        if fname1.find("_") == -1:
            f1 = int(fname1)
        else:
            f1 = int(fname1[fname1.find("_") + 1:])

        if fname2.find("_") == -1:
            f2 = int(fname2)
        else:
            f2 = int(fname2[fname2.find("_") + 1:])

        if vid1 == vid2:
            if f1 > f2:
                return 1
            elif f1 < f2:
                return -1
            else:
                return 0
        else:
            if vid1 > vid2:
                return 1
            else:
                return -1

    new_paths = [(paths[_], _) for _ in range(len(paths))]
    new_paths = sorted(new_paths, cmp=cmp_func)
    idx = [p[1] for p in new_paths]
    return idx

#############################
# SORT FUNCTIONS FOR MIXTURES 
#############################

# sort by 1.frame then image 
#   among frames: 1.video name 2.frame order
#   among images: image order

def sort0_mix(paths, ws):
    def cmp_func(p1, p2):
        p1 = p1[0]
        p2 = p2[0]
        it1 = p1.split("/")
        it2 = p2.split("/")
        l1 = len(it1)
        l2 = len(it2)
        # video vs. image
        if not l1 == l2:
            if l2 > l2:
                return 1
            else:
                return -1

        vid1 = it1[-2]
        vid2 = it2[-2]
        fname1 = it1[-1].split(".")[0]
        fname2 = it2[-1].split(".")[0]
        if fname1.find("_") == -1:
            f1 = int(fname1)
        else:
            f1 = int(fname1[fname1.find("_") + 1:])

        if fname2.find("_") == -1:
            f2 = int(fname2)
        else:
            f2 = int(fname2[fname2.find("_") + 1:])

        if vid1 == vid2:
            if f1 > f2:
                return 1
            elif f1 < f2:
                return -1
            else:
                return 0
        else:
            if vid1 > vid2:
                return 1
            else:
                return -1

    new_paths = [(paths[_], _) for _ in range(len(paths))]
    new_paths = sorted(new_paths, cmp=cmp_func)
    idx = [p[1] for p in new_paths]
    return idx
