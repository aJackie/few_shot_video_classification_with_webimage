import torch
import torch.nn as nn

class DotMarginLoss(nn.Module):

    def __init__(self, margin):
        super(DotMarginLoss, self).__init__()
        self.margin = margin

    def forward(self, a, p, n):
        """
            a(nchor): (N, D)
            p(ositive): (N, D)
            n(egative): (N, D)
        """
        p = (a*p).sum(dim=1)
        n = (a*n).sum(dim=1)

        l = self.margin - p + n
        l = l * (l > 0.).float()

        return l.mean()

class MSEMarginLoss(nn.Module):

    def __init__(self, margin):
        super(MSEMarginLoss, self).__init__()
        self.margin = margin

    def forward(self, s1, s2):
        """
            s1: (N,)
            s2: (N,)
        """
        s = torch.abs(s1 - s2)
        s = s * (s > self.margin).float()
        s = torch.mean(s * s)
        return s

class MarginLoss(nn.Module):

    def __init__(self, margin):
        super(MarginLoss, self).__init__()
        self.margin = margin

    def forward(self, p, n):
        """
            p(ositive): (N, )
            n(egative): (N, )
        """
        l = self.margin - p + n
        l = l * (l > 0.).float()

        return l.mean()



