""" 
    training meta-instance-reweight model on mixture input 
    features are further processed by projection function and adaption function
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

import model
from data_generator import MixtureFeatureTrainDataset, MixtureFeatureTestDataset
from utils import *
from loss import DotMarginLoss
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training meta-instance-reweight model on mixture input, \
                                              features are further processed by projection function and adaption function",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('--transformer_choice', default=1, type=int, help="type of transformer model to use")

parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")
parser.add_argument("--val_max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")
parser.add_argument('-mr', '--margin', default=0.1, type=float, help="margin in loss")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")
parser.add_argument("--image_feature_dir", type=str, required=True, help="path of image root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--project_features", type=int, default=2048, help="dim of projection embedding space")

parser.add_argument("--noise_rate", type=float, default=0., help="possibility of random adding noise")
parser.add_argument("--normal_std", type=float, default=1., help="standard deviation of normal distribution")
parser.add_argument("--label_flip_rate", type=float, default=0., help="possibility of random flipping image labels")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(project, transformer, meta_optimizer, cp_dir):
    state = {
        "state_dict_project": project.state_dict(),
        "state_dict_transformer": transformer.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, project, transformer, meta_optimizer, \
            criterion, epoch, meta_batch_size, max_meta_batches, project_features):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()

        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            idx_to_cls = train_sloader.dataset.idx_to_class

            # get support features
            support_features = [[] for _ in range(task_class_num)]

            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                # input_var are further projected
                input_var = project(input_var)

                # images
                if idx >= task_class_num:
                    # further transformed for adaptation
                    input_var = transformer(input_var)

                target = int(target[0])

                support_features[target].append(input_var)

            for _ in range(task_class_num):
                support_features[_] = F.normalize(torch.sum(torch.cat(support_features[_]), dim=0, keepdim=True))

            # (5, 2048)
            support_features = torch.cat(support_features)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            As = []
            ps = []
            ns = []
            ac1 = 0.
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)

                As.append(input_var)

                score = torch.sum(input_var * support_features, dim=1)
                score = [(score[_], _) for _ in range(score.size(0))]
                score.sort(reverse=True, key=lambda x: x[0])

                ps.append(support_features[int(target[0])])

                for s in score:
                    if s[1] != int(target[0]):
                        ns.append(support_features[s[1]])
                        break

                ac1 = ac1 + float(score[0][1] == int(target[0]))

            ac1 = ac1 / len(train_qloader)
            As = torch.cat(As)
            ps = torch.stack(ps)
            ns = torch.stack(ns)

            loss = criterion(As, ps, ns)
            loss.backward()

            losses.update(float(loss.data), len(train_qloader))
            acc1.update(ac1, len(train_qloader))

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate(val_sloader, val_qloader, project, transformer, \
                criterion, epoch, val_meta_batch_size, val_max_meta_batches, project_features):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses = AverageMeter()
        acc1 = AverageMeter()

        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            idx_to_cls = val_sloader.dataset.idx_to_class

            # get support features
            support_features = [[] for _ in range(task_class_num)]

            for idx, (input_, target) in enumerate(val_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()

                with torch.no_grad():
                    # input_var are further projected
                    input_var = project(input_var)

                    # images
                    if idx >= task_class_num:
                        # further transformed for adaptation
                        input_var = transformer(input_var)

                target = int(target[0])

                support_features[target].append(input_var)

            for _ in range(task_class_num):
                support_features[_] = F.normalize(torch.sum(torch.cat(support_features[_]), dim=0, keepdim=True))

            # (5, 2048)
            support_features = torch.cat(support_features)

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            As = []
            ps = []
            ns = []
            ac1 = 0.
            for idx, (input_, target) in enumerate(val_qloader):
                #print "\t\tquery batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                with torch.no_grad():
                    input_var = project(input_var).mean(dim=0, keepdim=True)
                input_var = F.normalize(input_var)

                As.append(input_var)

                score = torch.sum(input_var * support_features, dim=1)
                score = [(score[_], _) for _ in range(score.size(0))]
                score.sort(reverse=True, key=lambda x: x[0])

                ps.append(support_features[int(target[0])])

                for s in score:
                    if s[1] != int(target[0]):
                        ns.append(support_features[s[1]])
                        break

                ac1 = ac1 + float(score[0][1] == int(target[0]))

            ac1 = ac1 / len(val_qloader)
            As = torch.cat(As)
            ps = torch.stack(ps)
            ns = torch.stack(ns)

            loss = criterion(As, ps, ns)

            losses.update(float(loss.data), len(val_qloader))
            acc1.update(ac1, len(val_qloader))

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)

        end = time.time()
    
    return avg_losses.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                project, transformer, criterion, project_features, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()
    avg_frm_exp_weight = {"mean": AverageMeter(), \
                          "med": AverageMeter(),\
                          "min": AverageMeter(),\
                          "max": AverageMeter(),\
                          "sum": AverageMeter()}

    avg_img_exp_weight = {"mean": AverageMeter(), \
                          "med": AverageMeter(),\
                          "min": AverageMeter(),\
                          "max": AverageMeter(),\
                          "sum": AverageMeter()}

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}
        frm_exp_weight = {"mean": AverageMeter(), \
                          "med": AverageMeter(),\
                          "min": AverageMeter(),\
                          "max": AverageMeter(),\
                          "sum": AverageMeter()}

        img_exp_weight = {"mean": AverageMeter(), \
                          "med": AverageMeter(),\
                          "min": AverageMeter(),\
                          "max": AverageMeter(),\
                          "sum": AverageMeter()}

        # get support features
        support_features = [[] for _ in range(task_class_num)]
        all_weights = [[] for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            #print "\t\ttrain batch: ", idx
            
            # initial lstm hidden state
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            target = int(target[0])

            fnum = input_var.size(0)
            weights = [torch.tensor(1.0, dtype=torch.float32, device="cuda") for _ in range(fnum)]

            with torch.no_grad():
                # input_var are further projected
                input_var = project(input_var)

                all_weights[target] = all_weights[target] + weights

                # images
                if idx >= task_class_num:
                    # further transformed for adaptation
                    input_var = transformer(input_var)

                    weights = torch.stack(all_weights[target])
                    weights = F.softmax(weights / 1.0, dim=0)

                    ws = weights.detach().cpu().numpy()
                    frm_fnum = weights.size(0) - fnum
                    
                    frm_exp_weight["mean"].update(float(np.mean(ws[:frm_fnum])))
                    frm_exp_weight["med"].update(float(np.median(ws[:frm_fnum])))
                    frm_exp_weight["min"].update(float(np.amin(ws[:frm_fnum])))
                    frm_exp_weight["max"].update(float(np.amax(ws[:frm_fnum])))
                    frm_exp_weight["sum"].update(float(np.sum(ws[:frm_fnum])))

                    img_exp_weight["mean"].update(float(np.mean(ws[frm_fnum:])))
                    img_exp_weight["med"].update(float(np.median(ws[frm_fnum:])))
                    img_exp_weight["min"].update(float(np.amin(ws[frm_fnum:])))
                    img_exp_weight["max"].update(float(np.amax(ws[frm_fnum:])))
                    img_exp_weight["sum"].update(float(np.sum(ws[frm_fnum:])))


            support_features[target].append(input_var)

        for _ in range(task_class_num):
            support_features[_] = F.normalize(torch.sum(torch.cat(support_features[_]), dim=0, keepdim=True))

        # (5, 2048)
        support_features = torch.cat(support_features)

        # do query
        test_qloader.dataset.training = False

        for idx, (input_, target) in enumerate(test_qloader):
            #print "\t\tquery batch: ", idx
            
            input_ = input_[0]
            input_var = input_.view(input_.size(0), -1).cuda()
            with torch.no_grad():
                input_var = project(input_var).mean(dim=0, keepdim=True)
            input_var = F.normalize(input_var)

            score = torch.sum(input_var * support_features, dim=1)
            score = [(score[_], _) for _ in range(score.size(0))]
            score.sort(reverse=True, key=lambda x: x[0])

            ac1 = float(score[0][1] == int(target[0]))
            acc1.update(ac1)
            cls = idx_to_cls[int(target[0])]
            cls_acc1[cls].update(ac1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        avg_frm_exp_weight["mean"].update(frm_exp_weight["mean"].avg)
        avg_frm_exp_weight["med"].update(frm_exp_weight["med"].avg)
        avg_frm_exp_weight["min"].update(frm_exp_weight["min"].avg)
        avg_frm_exp_weight["max"].update(frm_exp_weight["max"].avg)
        avg_frm_exp_weight["sum"].update(frm_exp_weight["sum"].avg)

        avg_img_exp_weight["mean"].update(img_exp_weight["mean"].avg)
        avg_img_exp_weight["med"].update(img_exp_weight["med"].avg)
        avg_img_exp_weight["min"].update(img_exp_weight["min"].avg)
        avg_img_exp_weight["max"].update(img_exp_weight["max"].avg)
        avg_img_exp_weight["sum"].update(img_exp_weight["sum"].avg)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)

            print >> f, "f_w_mean: {}".format(frm_exp_weight["mean"].avg)
            print >> f, "f_w_med: {}".format(frm_exp_weight["med"].avg)
            print >> f, "f_w_max: {}".format(frm_exp_weight["max"].avg)
            print >> f, "f_w_min: {}".format(frm_exp_weight["min"].avg)
            print >> f, "f_w_sum: {}".format(frm_exp_weight["sum"].avg)

            print >> f, "i_w_mean: {}".format(img_exp_weight["mean"].avg)
            print >> f, "i_w_med: {}".format(img_exp_weight["med"].avg)
            print >> f, "i_w_max: {}".format(img_exp_weight["max"].avg)
            print >> f, "i_w_min: {}".format(img_exp_weight["min"].avg)
            print >> f, "i_w_sum: {}".format(img_exp_weight["sum"].avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n\t \
            f_w_mean:{}\n\tf_w_med:{}\n\tf_w_max:{}\n\tf_w_min:{}\n\tf_w_sum:{}\n\t \
            i_w_mean:{}\n\ti_w_med:{}\n\ti_w_max:{}\n\ti_w_min:{}\n\ti_w_sum:{}\n\t".\
            format(batch_time.sum, avg_acc1.avg,\
                   avg_frm_exp_weight["mean"].avg, avg_frm_exp_weight["med"].avg, \
                   avg_frm_exp_weight["max"].avg, avg_frm_exp_weight["min"].avg, avg_frm_exp_weight["sum"].avg,\
                   avg_img_exp_weight["mean"].avg, avg_img_exp_weight["med"].avg, \
                   avg_img_exp_weight["max"].avg, avg_img_exp_weight["min"].avg, avg_img_exp_weight["sum"].avg))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    train_dataset = MixtureFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.image_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot,
                        args.noise_rate,
                        random_noise,
                        args.label_flip_rate)

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=True,
            num_workers=args.workers)

    val_dataset = MixtureFeatureTrainDataset(
                    val_classes,
                    args.frame_feature_dir,
                    args.image_feature_dir,
                    args.task_class_num,
                    args.val_kshot,
                    args.val_query_kshot,
                    args.noise_rate,
                    random_noise,
                    args.label_flip_rate)

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=True,
            num_workers=args.workers)

    # model
    num_features = model_features[args.model]
    project = model.__dict__["Projection"](num_features, args.project_features).cuda()
    transformer = model.__dict__["Transformer{}".format(args.transformer_choice)](args.project_features).cuda()

    # loss & optimizer
    criterion = DotMarginLoss(args.margin).cuda()
    meta_optimizer = torch.optim.SGD(list(project.parameters()) + list(transformer.parameters()), \
                                     args.meta_learning_rate)

    # resume from checkpoint?
    # NOTE: only meta-learner params can be stored and resumed
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            project.load_state_dict(checkpoint['state_dict_project'])
            transformer.load_state_dict(checkpoint['state_dict_transformer'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    cudnn.enabled = True
    cudnn.benchmark = True

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = MixtureFeatureTestDataset(args.test_file_dir, args.frame_dir, args.frame_feature_dir, \
                            args.image_feature_dir, args.task_class_num, args.sample_num, args.dup_num, \
                            args.noise_rate, random_noise, args.label_flip_rate)

        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                    project, transformer, criterion, args.project_features, args.output_dir)
        
        return


    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        train(train_sloader, train_qloader, project, transformer, meta_optimizer, \
                criterion, epoch, args.meta_batch_size,\
                args.max_meta_batches, args.project_features)
        save_tmp(project, transformer, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_project": project.state_dict(),
                "state_dict_transformer": transformer.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss, acc1 = validate(val_sloader, val_qloader, project, transformer, \
                                    criterion, epoch, args.val_meta_batch_size, \
                                    args.val_max_meta_batches, args.project_features)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)
        
    writer.close()

if __name__ == "__main__":
    main()
