# batch-sampler used for training & evaluation
import os
import sys
import time
import shutil

import torch
import torch.utils.data

class OneWayBatchSampler(torch.utils.data.Sampler):
    def __init__(self, cls_num, base_num, dup_time, batch_size):
        self.cls_num = cls_num
        self.base_num = base_num
        self.dup_time = dup_time
        self.batch_size = batch_size

    def __iter__(self):
        perms = (torch.randperm(self.cls_num) + self.base_num).tolist()
        max_step = self.cls_num / self.batch_size

        lst = []
        for i in range(max_step):
            tem = perms[i*self.batch_size: (i+1)*self.batch_size]
            tem = [[i]*self.dup_time for i in tem]
            tem = [item for sublist in tem for item in sublist]
            lst.append(tem)
            
        self.lst = lst
        return iter(lst)
    
    def __len__(self):
        return self.cls_num / self.batch_size

class TwoWayBatchSampler(torch.utils.data.Sampler):
    def __init__(self, frame_cls_num, img_cls_num, step1, step2, batch_size1, batch_size2):
        assert img_cls_num >= frame_cls_num
        self.frame_cls_num = frame_cls_num
        self.img_cls_num = img_cls_num
        self.step1 = step1
        self.step2 = step2
        self.batch_size1 = batch_size1
        self.batch_size2 = batch_size2

    def __iter__(self):
        step1 = self.step1
        step2 = self.step2
        batch_size1 = self.batch_size1
        batch_size2 = self.batch_size2
        base1 = step1 * batch_size1
        base2 = step2 * batch_size2

        perms1 = torch.randperm(self.frame_cls_num).tolist()
        perms2 = (torch.randperm(self.img_cls_num - self.frame_cls_num) + self.frame_cls_num).tolist()
        max_step1 = self.frame_cls_num / base1
        max_step2 = (self.img_cls_num - self.frame_cls_num) / base2

        lst = []
        for i in range(min(max_step1, max_step2)):
            for j in range(step1):
                tem = i*step1 + j
                lst.append(perms1[tem*batch_size1: (tem+1)*batch_size1])
            for j in range(step2):
                tem = i*step2 + j
                lst.append(perms2[tem*batch_size2: (tem+1)*batch_size2])
            
        self.lst = lst
        return iter(lst)
    
    def __len__(self):
        base1 = self.step1 * self.batch_size1
        base2 = self.step2 * self.batch_size2
        max_step1 = self.frame_cls_num / base1
        max_step2 = (self.img_cls_num - self.frame_cls_num) / base2
        return min(max_step1, max_step2) * (self.step1 + self.step2)

if __name__ == "__main__":
    sampler = OneWayBatchSampler(50, 150, 3, 4)
    print len(sampler)
    print list(sampler)
    print 

    sampler = TwoWayBatchSampler(150, 200, 3, 1, 4, 8)
    print len(sampler)
    print list(sampler)
    print 
