""" 
    training classification model from mixture of web images and few shot videos
    if evalaute, evaluate on video frames
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.utils.model_zoo as model_zoo
import torchvision.models as models
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

from data_generator import ImageFrameMixFolder, FrameListFileTestFolder
from util import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training classification model from mixture of web images and few shot videos",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="base model architects,\
                     available choice: alexnet, vgg11/13/16/19[_bn], resnet18/34/50/101/152")
parser.add_argument('-p', "--pretrain", type=bool, default=False, \
                    help="load pretrained ImageNet model or not")
parser.add_argument('-r', "--resume", type=str, default=None, \
                    help="path to checkpoint file")
parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size")
parser.add_argument('-s', '--size', type=int, default=224, help="input square image edge length")
parser.add_argument('-w', '--workers', type=int, default=4, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")
parser.add_argument('-lr', '--learning_rate', default=10e-2, type=float, help="initial learning rate")
parser.add_argument('--start_epoch', default=0, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=100, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=5, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=5, type=int, help="frequency(of epoches) of doing validation")
parser.add_argument("--img_data_dir", type=str, required=True, help="root dir of web image dataset, \
                                                                     data dir should be img_data_dir/train")
parser.add_argument("--train_file", type=str, required=True, help="path to file containing training video data list")
parser.add_argument("--val_file", type=str, default=None, help="path to val path list file")
parser.add_argument("--train_img_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                               of training image data")
parser.add_argument("--train_vid_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                               of training video data")
parser.add_argument("-c", "--class_list", type=str, required=True, help="path to file containing valided class names")
parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")
parser.add_argument('--torch_model_dir', type=str, required=False, default='/S2/MI/zxz/.torch/models', \
                    help="pytorch model zoo dir of pretrained model")
# test configuration
parser.add_argument('-e', "--evaluate", type=bool, default=False, help="evaluate model on test set")
parser.add_argument("--test_file", type=str, default=None, help="path to test path list file")
parser.add_argument("--test_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                           used for evaluation")
parser.add_argument("--sample_num", type=int, default=-1, help="sampling num per video, -1 means all")
parser.add_argument("--dup_num", type=int, default=1, help="duplicate sampling num per video")
parser.add_argument("--output", type=str, default=None, help="output file recording evaluation result")

args=parser.parse_args()

def save_tmp():
    save_path = os.path.join(args.cp_dir, "tem.pth.tar")
    torch.save({
        "state_dict": model.state_dict(),
        "optimizer": optimizer.state_dict()},
        save_path)
    print("saved at {}".format(save_path))

# signal handler
# make sure to close writer file before exit
def sigint_handler(sig, frame):
    try:
        writer.close()
    except:
        print("writer not created")
    finally:
        c = raw_input("save state?[y/N]")
        if c == "y":
            save_path = os.path.join(args.cp_dir, "tem.pth.tar")
            torch.save({
                "state_dict": model.state_dict(),
                "optimizer": optimizer.state_dict()},
                save_path)
            print("saved at {}".format(save_path))

        print("exiting...")
        sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)

model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}

def train(train_loader, model, criterion, optimizer, epoch):
    data_time = AverageMeter()
    batch_time = AverageMeter()
    losses = AverageMeter()
    acc1 = AverageMeter()
    acc5 = AverageMeter()

    # switch to training mode
    model.train()

    end = time.time()
    for i, (input_, target) in enumerate(tqdm(train_loader)):
        #print("input_ size: {} target size: {}".format(input_.size(), target.size()))
        # measure data loading time
        data_time.update(time.time() - end)

        target_var = target.cuda()
        input_var = input_.cuda()

        # forward pass
        output = model(input_var)
        loss = criterion(output, target_var)

        # measure accuracy and record loss
        ac1, ac5 = accuracy(output.data, target_var, topk=(1,5))
        losses.update(loss.data, input_.size(0))
        acc1.update(ac1[0], input_.size(0))
        acc5.update(ac5[0], input_.size(0))

        # backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*len(train_loader)+i+1)*args.batch_size
            writer.add_scalars('train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('train/time/data_time', {'val': data_time.val}, count_samples)
            writer.add_scalars('train/loss', {'val': losses.val}, count_samples)
            writer.add_scalars('train/accuracy/acc1', {'val': acc1.val}, count_samples)
            writer.add_scalars('train/accuracy/acc5', {'val': acc5.val}, count_samples)
            

    print('Epoch: {} finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Loss: {loss.avg:.4f}\n\
           Acc1: {acc1.avg:.3f}\n\
           Acc5: {acc5.avg:.3f}'.format(\
           epoch, batch_time=batch_time, loss=losses, acc1=acc1, acc5=acc5
           ))

    # epoch record
    count_samples = (epoch+1)*len(train_loader)*args.batch_size
    writer.add_scalars('train/loss_epoch', {'avg': losses.avg}, count_samples)
    writer.add_scalars('train/accuracy_epoch/acc1', {'avg': acc1.avg}, count_samples)
    writer.add_scalars('train/accuracy_epoch/acc5', {'avg': acc5.avg}, count_samples)


def validate(val_loader, model, criterion):
    data_time = AverageMeter()
    batch_time = AverageMeter()
    losses = AverageMeter()
    acc1 = AverageMeter()
    acc5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (input_, target) in enumerate(tqdm(val_loader)):
            # measure data loading time
            data_time.update(time.time() - end)

            target_var = target.cuda()
            input_var = input_[0].cuda()

            # forward pass
            output = model(input_var)
            #print output.shape
            output = output.sum(dim=0, keepdim=True)
            #print output.shape
            loss = criterion(output, target_var)

            # measure accuracy and record loss
            ac1, ac5 = accuracy(output.data, target_var, topk=(1,5))
            losses.update(loss.data, input_.size(0))
            acc1.update(ac1[0], input_.size(0))
            acc5.update(ac5[0], input_.size(0))

            # no backward

            # measure one-batch time
            batch_time.update(time.time() - end)
            end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Loss: {loss.avg:.3f}\n\
           Acc1: {acc1.avg:.3f}\n\
           Acc5: {acc5.avg:.3f}".format(\
           batch_time=batch_time,loss=losses, acc1=acc1, acc5=acc5))

    return losses.avg, acc1.avg, acc5.avg

def evaluate(test_loader, class_num, model):
    batch_time = AverageMeter()

    cls_ac1 = [AverageMeter() for _ in range(class_num)]
    cls_ac5 = [AverageMeter() for _ in range(class_num)]
    avg_acc1 = AverageMeter()
    avg_acc5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (input_, target) in enumerate(tqdm(test_loader)):
            #print input_.shape
            #print target.shape

            target_var = target.cuda()
            input_var = input_[0].cuda()

            # forward pass
            output = model(input_var)
            #print output.shape
            output = output.sum(dim=0, keepdim=True)
            #print output.shape

            # measure accuracy and record loss
            ac1, ac5 = accuracy(output.data, target_var, topk=(1,5))
            avg_acc1.update(ac1[0], input_.size(0))
            avg_acc5.update(ac5[0], input_.size(0))
            cls_ac1[target.data[0]].update(ac1[0], input_.size(0))
            cls_ac5[target.data[0]].update(ac5[0], input_.size(0))

            # no backward

            # measure one-batch time
            batch_time.update(time.time() - end)
            end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}\n\
           Acc5: {acc5.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1, acc5=avg_acc5))

    return avg_acc1.avg, avg_acc5.avg, \
           [x.avg for x in cls_ac1], [x.avg for x in cls_ac5]

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    # Data loading
    img_traindir = os.path.join(args.img_data_dir, 'train')
    vid_trainfile = args.train_file
    with open(args.class_list, "r") as f:
        classes = [l.strip() for l in f.readlines()]

    # load mean and std
    img_mean, img_std = tuple(np.loadtxt(args.train_img_mean_std_file))
    img_mean = [float(a) for a in list(img_mean)]
    img_std = [float(b) for b in list(img_std)]
    print "image mean: ", img_mean
    print "image std: ", img_std

    vid_mean, vid_std = tuple(np.loadtxt(args.train_vid_mean_std_file))
    vid_mean = [float(a) for a in list(vid_mean)]
    vid_std = [float(b) for b in list(vid_std)]
    print "video mean: ", vid_mean
    print "video std: ", vid_std

    train_dataset = ImageFrameMixFolder(
            img_traindir, 
            vid_trainfile,
            classes, 
            img_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=img_mean, 
                                     std=img_std)
                ]),
            frame_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=vid_mean, 
                                     std=vid_std)
                ]))
    val_dataset = FrameListFileTestFolder(
                    args.val_file,
                    -1,
                    1,
                    frame_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(args.size),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=vid_mean,
                                             std=vid_std)
                    ]))
    
    train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size=args.batch_size, shuffle=True,
            num_workers=args.workers
            )
    val_loader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers
            )

    class_num = len(train_dataset.classes)

    # create model
    global model
    model = models.__dict__[args.model](pretrained=False, num_classes=class_num)
    
    # manully load pretrained model since only parts of the network configuration the same
    if args.pretrain:
        print("using pre-trained model {}".format(args.model))
        model_url = model_urls[args.model.split('_')[0]]
        model_filename = model_url.split('/')[-1]
        model_filepath = os.path.join(args.torch_model_dir, model_filename)

        if not os.path.exists(model_filepath):
            pretrain_net = model_zoo.load_url(model_url, model_dir=args.torch_model_dir)
        else:
            pretrain_net = torch.load(model_filepath)

        # only load features layer
        pretrain_features = {k:v for k, v in pretrain_net.items() if not (k.startswith("fc") or k.startswith("classifier"))}
        print("pretrain_layers:")
        for k,v in pretrain_features.items():
            print("\t{}: {}".format(k, v.size()))

        model_dict = model.state_dict()
        model_dict.update(pretrain_features)
        model.load_state_dict(model_dict)
        del pretrain_net
        del pretrain_features
        del model_dict
    else:
        print("creating model {}".format(args.model))

    # set data parallel to use multiple gpus
    model = torch.nn.DataParallel(model).cuda()

    # loss func and optimizer
    global optimizer
    criterion = nn.CrossEntropyLoss().cuda()
    optimizer = torch.optim.SGD(model.parameters(), args.learning_rate)
    cudnn.enabled = True
    cudnn.benchmark = True
    
    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            model.load_state_dict(checkpoint['state_dict'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    # just do evaluation if 
    if args.evaluate:
        print "evaludate on {}".format(args.test_file)
        mean, std = tuple(np.loadtxt(args.test_mean_std_file, dtype=np.float32))
        mean = [float(a) for a in list(mean)]
        std =[float(b) for b in list(std)]
        print "mean: ", mean
        print "std: ", std

        test_dataset = FrameListFileTestFolder(
                        args.test_file,
                        args.sample_num,
                        args.dup_num,
                        frame_transform=transforms.Compose([
                            transforms.Resize(256),
                            transforms.CenterCrop(args.size),
                            transforms.ToTensor(),
                            transforms.Normalize(mean=vid_mean,
                                                 std=vid_std)
                        ]))
        test_loader = torch.utils.data.DataLoader(
                        test_dataset, batch_size=1, shuffle=False,
                        num_workers=args.workers)
                        
        avg_acc1, avg_acc5, cls_ac1, cls_ac5 = evaluate(test_loader, class_num, model)
        with open(args.output, "w") as f:
            print >> f, "inference for {} classes, {} videos, duplicate {} times with {} frames once".\
                        format(class_num, len(test_dataset), args.dup_num, args.sample_num)
            print >> f, "average top-1 accuracy: {}".format(avg_acc1)
            print >> f, "average top-5 accuracy: {}".format(avg_acc5)
            print >> f, "------------------------------------"
            for idx, cls in enumerate(classes):
                print >> f, "{}: acc1 {} acc5 {}".format(cls, cls_ac1[idx], cls_ac5[idx])

        return

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        f.write("{}".format(datetime.now()))
        f.write("batch_size: {}\n".format(args.batch_size))
        f.write("start_epoch: {}\n".format(args.start_epoch))
        f.write("end_epoch: {}\n".format(args.epochs))
        f.write("learning_rate: {}\n\n".format(args.learning_rate))

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch)
        save_tmp()
        
        if (epoch + 1) % args.val_freq == 0:
            # evaluate on validation set
            loss, acc1, acc5 = validate(val_loader, model, criterion)
            
            # record evaluation result
            count_samples = (epoch+1)*len(train_loader)*args.batch_size
            writer.add_scalars('eval/loss_epoch', {'avg': loss}, count_samples)
            writer.add_scalars('eval/accuracy_epoch/acc1', {'avg': acc1}, count_samples)
            writer.add_scalars('eval/accuracy_epoch/acc5', {'avg': acc5}, count_samples)
        

        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.model,
                'state_dict': model.state_dict(),
                'loss': loss,
                'acc1': acc1,
                'acc5': acc5,
                'optimizer': optimizer.state_dict(),
                }, False, cp_dir)
    writer.close()

if __name__ == "__main__":
    main()
