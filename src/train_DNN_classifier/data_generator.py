import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from PIL import Image
from torch.utils.data import Dataset, DataLoader
from util import *
from random import shuffle
import torchvision.transforms as transforms

class ImageListFolder(Dataset):
    """
        similar to torchvision datasets.ImageFolder
        but use class_lst to indicate valid classes
    """
    def __init__(self, image_dir, class_lst, img_transform=None):
        super(ImageListFolder, self).__init__()
        self.image_dir = image_dir
        self.classes = class_lst
        self.img_transform = img_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), idx), paths)

        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        path, target = self.imgs[idx]
        sample = pil_loader(path)
        if self.img_transform is not None:
            sample = self.img_transform(sample)

        return sample, target

class FrameListFileFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
    """
    def __init__(self, path_file, frame_transform=None):
        super(FrameListFileFolder, self).__init__()
        self.path_file = path_file
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class FrameListFileTestFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
        NOTE: this is used for evaluation
    """
    def __init__(self, path_file, sample_num, dup_num, frame_transform=None):
        super(FrameListFileTestFolder, self).__init__()
        self.path_file = path_file
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
            
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))

    def __len__(self):
        return len(self.videos)

    def __getitem__(self, idx):
        vid_dir, target = self.videos[idx]
        frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])
        if self.sample_num == -1:
            select = np.concatenate([frames] * self.dup_num)
        else:
            select = np.array([])
            for _ in range(self.dup_num):
                perms = np.random.permutation(len(frames))
                select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.sample_num))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class ImageFrameMixFolder(Dataset):
    """
        mixture of web images(from image dir) and few shot 
        training video frames(from training file)
    """
    def __init__(self, image_dir, path_file, class_lst, img_transform=None, frame_transform=None):
        super(ImageFrameMixFolder, self).__init__()
        self.image_dir = image_dir
        self.path_file = path_file
        self.classes = class_lst
        self.img_transform = img_transform
        self.frame_transform = frame_transform

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), "i", idx), paths)

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
          
            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), "f", label))

        self.samples = self.imgs + self.frames
        
        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))
        print "total {} samples".format(len(self.samples))

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        path, flag, target = self.samples[idx]
        sample = pil_loader(path)

        if flag == "i":
            if self.img_transform is not None:
                sample = self.img_transform(sample)
        else:
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)

        return sample, target










