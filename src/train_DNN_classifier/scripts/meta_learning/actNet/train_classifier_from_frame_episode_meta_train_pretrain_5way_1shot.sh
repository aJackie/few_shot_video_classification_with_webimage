python train_classifier_from_frame_episode.py \
    -m resnet50 \
    -p /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/image_classifier/meta_train/res50/checkpoint_epoch40.pth.tar \
    -b 32 \
    -w 4 \
    -g 5 \
    -lr 0.1 \
    --epochs 1 \
    --start_episode_idx 0 \
    --end_episode_idx 20000 \
    --print_freq 2 \
    --data_dir /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1 \
    --train_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/frame_train_mean_std.out \
    -c 5 \
    --log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/5way/1/frame_classifier/res50/meta_train_pretrain \
    --cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/5way/1/frame_classifier/res50/meta_train_pretrain\
    --output_dir /S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/frame_classifier/res50/meta_train_pretrain \

