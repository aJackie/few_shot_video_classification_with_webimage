python train_classifier_from_image_episode.py \
    -m resnet50 \
    -p /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/image_classifier/meta_train/res50/checkpoint_epoch40.pth.tar \
    -b 32 \
    -s 224 \
    -w 4 \
    -g 7 \
    -lr 0.01 \
    --epochs 0 \
    --start_episode_idx 146 \
    --end_episode_idx 1000 \
    --print_freq 2 \
    --img_data_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \
    --test_file_dir /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1 \
    --image_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/image_train_mean_std.out \
    -c 5 \
    --log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/5way/1/image_classifier/res50/meta_train_pretrain \
    --cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/5way/1/image_classifier/res50/meta_train_pretrain \
    --output_dir /S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/image_classifier/res50/meta_train_pretrain
    
