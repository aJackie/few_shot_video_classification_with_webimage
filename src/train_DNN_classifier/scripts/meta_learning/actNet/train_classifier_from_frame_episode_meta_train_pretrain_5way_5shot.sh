python train_classifier_from_frame_episode.py \
    -m resnet50 \
    -p /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/frame_classifier_kshot/meta_train/res50/checkpoint_epoch20.pth.tar \
    -b 32 \
    -w 0 \
    -g 1 \
    -lr 0.01 \
    --epochs 1 \
    --start_episode_idx 980 \
    --end_episode_idx 1000 \
    --print_freq 2 \
    --data_dir /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/5 \
    --train_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/frame_train_mean_std.out \
    -c 5 \
    --log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/5way/5/frame_classifier/res50/meta_train_pretrain0.01 \
    --cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/5way/5/frame_classifier/res50/meta_train_pretrain0.01 \
    --output_dir /S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/5/frame_classifier/res50/meta_train_pretrain0.01 \

