import os

for k in range(1, 11):
    f = open("eval_classifier_from_mixture_meta_test_sep_{}.sh".format(k), "w")
    f.write("python train_classifier_from_mixture.py \\\n" + \
            "\t-m resnet50 \\\n" + \
            "\t-p ImageNet \\\n" + \
            "\t-b 32 \\\n" + \
            "\t-s 224 \\\n" + \
            "\t-w 4 \\\n" + \
            "\t-g 5 \\\n" + \
            "\t-lr 0.001 \\\n" + \
            "\t--img_data_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \\\n" + \
            "\t--train_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt \\\n".format(k) + \
            "\t--val_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/val.txt \\\n".format(k) + \
            "\t--train_img_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/image_train_mean_std.out \\\n" + \
            "\t--train_vid_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/frame_train_mean_std.out \\\n".format(k) + \
            "\t-c /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt \\\n" + \
            "\t--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/mix_classifier_from_kshot/separate_mean_std/{}/res50 \\\n".format(k) + \
            "\t--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/mix_classifier_from_kshot/separate_mean_std/{}/res50 \\\n".format(k) + \
            "\t--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/mix_classifier_from_kshot/separate_mean_std/{}/res50 \\\n".format(k) + \
            "\t--resume /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/mix_classifier_from_kshot/separate_mean_std/{}/res50/checkpoint_epoch20.pth.tar \\\n".format(k) + \
            "\t-e true \\\n" + \
            "\t--test_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/test.txt \\\n".format(k) + \
            "\t--test_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/frame_train_mean_std.out \\\n".format(k) + \
            "\t--output /S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/mix_classifier_from_kshot/separate_mean_std/{}/res50/epoch20.txt \\".format(k))
    f.close()
    os.system("chmod 740 eval_classifier_from_mixture_meta_test_sep_{}.sh".format(k))
