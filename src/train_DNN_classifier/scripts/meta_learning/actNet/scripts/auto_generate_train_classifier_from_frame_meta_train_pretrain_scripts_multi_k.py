"""
    automatically generate training scripts for train_classifier_from_frame.py
    for different k in k-shot settings, pretrained on meta train
"""
import os

for k in range(1, 11):
    with open("train_classifier_from_frame_meta_test_meta_train_pretrain_{}.sh".format(k), "w") as f:
        print >> f, "python train_classifier_from_frame.py \\"
        print >> f, "\t-m resnet50 \\"
        print >> f, "\t-p /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/frame_classifier_kshot/meta_train/res50/checkpoint_epoch20.pth.tar \\"
        print >> f, "\t-b 32 \\"
        print >> f, "\t-s 224 \\"
        print >> f, "\t-w 4 \\"
        print >> f, "\t-g 0 \\"
        print >> f, "\t-lr 0.001 \\"
        print >> f, "\t--start_epoch 0 \\"
        print >> f, "\t--epochs 20 \\"
        print >> f, "\t--print_freq 20 \\"
        print >> f, "\t--save_freq 5 \\"
        print >> f, "\t--val_freq 5 \\"
        print >> f, "\t--train_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt \\".format(k)
        print >> f, "\t--val_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/val.txt \\".format(k)
        print >> f, "\t--train_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/frame_train_mean_std.out \\".format(k)
        print >> f, "\t-c /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt \\"
        print >> f, "\t--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/frame_classifier_kshot/meta_test_meta_train_pretrain/{}/res50 \\".format(k)
        print >> f, "\t--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/frame_classifier_kshot/meta_test_meta_train_pretrain/{}/res50 \\".format(k)
        print >> f, "\t--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/frame_classifier_kshot/meta_test_meta_train_pretrain/{}/res50 \\".format(k)

    os.system("chmod 740 train_classifier_from_frame_meta_test_meta_train_pretrain_{}.sh".format(k))
