import os

for k in range(1, 11):
    f = open("eval_classifier_from_image_meta_test_meta_train_pretrain_{}.sh".format(k), "w")
    f.write("python train_classifier_from_image.py \\\n" + \
            "\t-m resnet50 \\\n" + \
            "\t-p /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/image_classifier/meta_train/res50/checkpoint_epoch40.pth.tar \\\n" + \
            "\t-b 32 \\\n" + \
            "\t-s 224 \\\n" + \
            "\t-w 4 \\\n" + \
            "\t-g 3 \\\n" + \
            "\t--data_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \\\n" + \
            "\t--val_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/10/train.txt \\\n" + \
            "\t--image_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/image_train_mean_std.out \\\n" + \
            "\t-c /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt \\\n" + \
            "\t--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/image_classifier/meta_test_meta_train_pretrain/res50 \\\n" + \
            "\t--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/image_classifier/meta_test_meta_train_pretrain/res50 \\\n" + \
            "\t--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/image_classifier/meta_test_meta_train_pretrain/res50 \\\n" + \
            "\t--resume /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/image_classifier/meta_test_meta_train_pretrain/res50/checkpoint_epoch40.pth.tar \\\n" + \
            "\t-e true \\\n" + \
            "\t--test_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/test.txt \\\n".format(k) + \
            "\t--frame_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/image_train_mean_std.out \\\n" + \
            "\t--output /S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/image_classifier/meta_test_meta_train_pretrain/{}/res50/epoch40.txt \\".format(k))
    f.close()
    os.system("chmod 740 eval_classifier_from_image_meta_test_meta_train_pretrain_{}.sh".format(k))
