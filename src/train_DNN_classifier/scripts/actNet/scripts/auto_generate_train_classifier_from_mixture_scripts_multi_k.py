"""
    automatically generate training scripts for train_classifier_from_mixture.py
    for different k in k-shot settings
"""
import os

for k in range(1, 21):
    """
    with open("train_classifier_from_mixture_sep_{}.sh".format(k), "w") as f:
        print >> f, "python train_classifier_from_mixture.py \\"
        print >> f, "\t-m resnet50 \\"
        print >> f, "\t-p true \\"
        print >> f, "\t-b 32 \\"
        print >> f, "\t-s 224 \\"
        print >> f, "\t-w 4 \\"
        print >> f, "\t-g 0 \\"
        print >> f, "\t-lr 0.001 \\"
        print >> f, "\t--start_epoch 0 \\"
        print >> f, "\t--epochs 20 \\"
        print >> f, "\t--print_freq 20 \\"
        print >> f, "\t--save_freq 2 \\"
        print >> f, "\t--val_freq 2 \\"
        print >> f, "\t--img_data_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \\"
        print >> f, "\t--train_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/train.txt \\".format(k)
        print >> f, "\t--val_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/val.txt \\".format(k)
        print >> f, "\t--train_img_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/webimage/train_mean_std.out \\"
        print >> f, "\t--train_vid_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/train_mean_std.out \\".format(k)
        print >> f, "\t-c /home/zxz/transfer_learning/few_shot/data/actNet/list/all_classes.txt \\"
        print >> f, "\t--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/actNet/mix_classifier_kshot/separate_mean_std/{}/res50 \\".format(k)
        print >> f, "\t--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/actNet/mix_classifier_kshot/separate_mean_std/{}/res50 \\".format(k)
        print >> f, "\t--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/actNet/mix_classifier_kshot/separate_mean_std/{}/res50 \\".format(k)

    os.system("chmod 740 train_classifier_from_mixture_sep_{}.sh".format(k))
    """

    with open("train_classifier_from_mixture_uni_{}.sh".format(k), "w") as f:
        print >> f, "python train_classifier_from_mixture.py \\"
        print >> f, "\t-m resnet50 \\"
        print >> f, "\t-p true \\"
        print >> f, "\t-b 32 \\"
        print >> f, "\t-s 224 \\"
        print >> f, "\t-w 4 \\"
        print >> f, "\t-g 0 \\"
        print >> f, "\t-lr 0.001 \\"
        print >> f, "\t--start_epoch 0 \\"
        print >> f, "\t--epochs 20 \\"
        print >> f, "\t--print_freq 20 \\"
        print >> f, "\t--save_freq 2 \\"
        print >> f, "\t--val_freq 2 \\"
        print >> f, "\t--img_data_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet \\"
        print >> f, "\t--train_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/train.txt \\".format(k)
        print >> f, "\t--val_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/val.txt \\".format(k)
        print >> f, "\t--train_img_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/mixture/{}/train_mean_std.out \\".format(k)
        print >> f, "\t--train_vid_mean_std_file /home/zxz/transfer_learning/few_shot/data/actNet/list/mixture/{}/train_mean_std.out \\".format(k)
        print >> f, "\t-c /home/zxz/transfer_learning/few_shot/data/actNet/list/all_classes.txt \\"
        print >> f, "\t--log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/actNet/mix_classifier_kshot/unified_mean_std/{}/res50 \\".format(k)
        print >> f, "\t--cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/actNet/mix_classifier_kshot/unified_mean_std/{}/res50 \\".format(k)
        print >> f, "\t--cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/actNet/mix_classifier_kshot/unified_mean_std/{}/res50 \\".format(k)

    os.system("chmod 740 train_classifier_from_mixture_uni_{}.sh".format(k))
