""" 
    RBF SVM classifier on frames 
"""
import os
import sys
import time
import argparse
import signal
import shutil

from sklearn import svm
from data_generator import FrameFeatureTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="Linear SVM classifier on frames ",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")

parser.add_argument("--noise_rate", type=float, default=0., help="possibility of random adding noise")
parser.add_argument("--normal_std", type=float, default=1., help="standard deviation of normal distribution")

parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

def evaluate(test_dataset, st_file_idx, end_file_idx, test_classes, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_dataset.next_file()
        test_dataset.training = True

        idx_to_cls = test_dataset.idx_to_class
        cls_to_idx = test_dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # initialize a new classifier & optimizer
        classifier = svm.SVC(gamma="scale")
            
        # do training
        X, Y = test_dataset[0]
        print "start fitting"
        classifier.fit(X, Y)
        print "end fitting"

        # do query
        test_dataset.training = False

        for (X, y) in test_dataset:
            
            ys = np.array(classifier.predict(X))
            y_ = np.bincount(ys).argmax()
            acc1.update(float(y == y_)) 
            cls = idx_to_cls[y]
            cls_acc1[cls].update(float(y == y_))

        avg_acc1.update(acc1.avg, len(test_dataset)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_dataset))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def main():
    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    print "evaluation on {}".format(args.test_file_dir)

    with open(args.test_class_file, "r") as f:
        test_classes = [l.strip() for l in f.readlines()]

    test_dataset = FrameFeatureTestDataset(args.test_file_dir, 
                                           args.frame_dir, 
                                           args.frame_feature_dir, 
                                           args.task_class_num, 
                                           args.noise_rate, random_noise)

    evaluate(test_dataset, args.st_file_idx, args.end_file_idx, test_classes, args.output_dir)

if __name__ == "__main__":
    main()
