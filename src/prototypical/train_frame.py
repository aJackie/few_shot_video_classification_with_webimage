""" 
    training prototypical model on frames
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

from model import Projection, Score
from data_generator import FrameFeatureTrainDataset, \
                            FrameFeatureTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm
import imp

parser = argparse.ArgumentParser(description="training prototypical model on frames",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")

parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")
parser.add_argument("--val_max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--project_features", type=int, default=2048, help="dim of projection embedding space")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(process, meta_optimizer, cp_dir):
    state = {
        "state_dict_process": process.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, process, score, meta_optimizer, criterion, \
            epoch, meta_batch_size, max_meta_batches):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()

        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            idx_to_cls = train_sloader.dataset.idx_to_class

            # extract prototypes
            prototypes = [0. for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                projection = process(input_var).mean(dim=0)
                projection = F.normalize(projection, dim=0)
                prototypes[int(target[0])] = projection

            prototypes = torch.stack(prototypes)
                
            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query = []
            target = []
            for idx, (input_, t) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                target_var = t.cuda()

                projection = process(input_var).mean(dim=0)
                projection = F.normalize(projection, dim=0)
                query.append(projection)
                target.append(target_var)

            query = torch.stack(query)
            target = torch.cat(target)
            
            output = score(prototypes, query)
            loss = criterion(output, target)
            loss.backward()

            losses.update(float(loss.data), query.size(0))
            ac1 = accuracy(output, target, topk=(1,))
            acc1.update(float(ac1[0][0]), query.size(0))

        for _, param in enumerate(process.parameters()):
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate(val_sloader, val_qloader, process, score, criterion, epoch, \
             val_meta_batch_size, val_max_meta_batches):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            idx_to_cls = val_sloader.dataset.idx_to_class

            # extract prototypes
            prototypes = [0. for _ in range(task_class_num)]
            with torch.no_grad():
                for idx, (input_, target) in enumerate(val_sloader):
                    #print "\t\ttrain batch: ", idx
                    
                    input_ = input_[0]
                    input_var = input_.view(input_.size(0), -1).cuda()
                    projection = process(input_var).mean(dim=0)
                    projection = F.normalize(projection, dim=0)
                    prototypes[int(target[0])] = projection

            prototypes = torch.stack(prototypes)

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            query = []
            target = []

            with torch.no_grad():
                for idx, (input_, t) in enumerate(val_qloader):
                    #print "\t\tquery batch: ", idx
                    
                    input_ = input_[0]
                    input_var = input_.view(input_.size(0), -1).cuda()
                    target_var = t.cuda()

                    projection = process(input_var).mean(dim=0)
                    projection = F.normalize(projection, dim=0)
                    query.append(projection)
                    target.append(target_var)

            query = torch.stack(query)
            target = torch.cat(target)
            
            output = score(prototypes, query)
            loss = criterion(output, target)

            losses.update(float(loss.data), query.size(0))
            ac1 = accuracy(output, target, topk=(1,))
            acc1.update(float(ac1[0][0]), query.size(0))

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)
        
        end = time.time()
    
    return avg_losses.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                process, score, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # extract prototypes
        prototypes = [0. for _ in range(task_class_num)]
        with torch.no_grad():
            for idx, (input_, target) in enumerate(test_sloader):
                #print "\t\ttrain batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                projection = process(input_var).mean(dim=0)
                projection = F.normalize(projection, dim=0)
                prototypes[int(target[0])] = projection

        prototypes = torch.stack(prototypes)

        # do query
        test_qloader.dataset.training = False

        query = []
        target = []

        with torch.no_grad():
            for idx, (input_, t) in enumerate(test_qloader):
                #print "\t\tquery batch: ", idx
                
                input_ = input_[0]
                input_var = input_.view(input_.size(0), -1).cuda()
                target_var = t.cuda()

                projection = process(input_var).mean(dim=0)
                projection = F.normalize(projection, dim=0)
                query.append(projection)
                target.append(target_var)

        query = torch.stack(query)
        target = torch.cat(target)
        
        output = score(prototypes, query)

        ac1 = accuracy(output, target, topk=(1,))
        acc1.update(float(ac1[0][0]), len(test_qloader))

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        for _ in range(output.size(0)):
            o = np.array(output[_].cpu())
            t = int(target[_].cpu())
            cls = idx_to_cls[t]
            correct = float(o.max() == o[t])
            cls_acc1[cls].update(correct)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    train_dataset = FrameFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot)

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_dataset = FrameFeatureTrainDataset(
            val_classes,
            args.frame_feature_dir,
            args.task_class_num,
            args.val_kshot,
            args.val_query_kshot)

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # model
    num_features = model_features[args.model]
    process = Projection(num_features, args.project_features).cuda()
    score = Score().cuda()

    # loss & optimizer
    criterion = nn.CrossEntropyLoss().cuda()
    meta_optimizer = torch.optim.SGD(process.parameters(), args.meta_learning_rate)

    # resume from checkpoint?
    # NOTE: only meta-learner params can be stored and resumed
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            process.load_state_dict(checkpoint['state_dict_process'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    cudnn.enabled = True
    cudnn.benchmark = True

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = FrameFeatureTestDataset(args.test_file_dir, 
                                               args.frame_dir, 
                                               args.frame_feature_dir, 
                                               args.task_class_num)

        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                    process, score, args.output_dir)
        
        return


    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        train(train_sloader, train_qloader, process, score, meta_optimizer, criterion, epoch, \
                args.meta_batch_size, args.max_meta_batches)

        save_tmp(process, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_process": process.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss, acc1= validate(val_sloader, val_qloader, process, score, criterion, epoch, \
                                 args.val_meta_batch_size, args.val_max_meta_batches)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)
            #break
        
    writer.close()

if __name__ == "__main__":
    main()
