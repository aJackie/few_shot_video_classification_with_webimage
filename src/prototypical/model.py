import torch
import torch.nn as nn
import torch.nn.functional as F

class Projection(nn.Module):

    def __init__(self, num_features=2048, pjt_features=2048):
        super(Projection, self).__init__()
        self.linear = nn.Linear(num_features, pjt_features)
        
    def forward(self, x):
        # x: (batch_size, num_features)
        x = self.linear(x)
        #x = F.normalize(x)
        return x

class Score(nn.Module):

    def __init__(self):
        super(Score, self).__init__()

    def forward(self, protos, x):
        # protos: (classes, dims)
        # x: (batch_size, dims)
        output = []
        classes = protos.size(0)
        batch_size = x.size(0)
        num_features = x.size(1)

        output = torch.mm(protos, x.transpose_(1,0))
        output.transpose_(1,0)

        return output

################################
# Several Transformer structures
################################

class Transformer0(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer0, self).__init__()
        
    def forward(self, x):
        # identity
        return x

class Transformer1(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer1, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

class Transformer2(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer2, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features // 2),
            nn.Linear(num_features // 2, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

class Transformer3(nn.Module):
    
    def __init__(self, num_features=2048):
        super(Transformer3, self).__init__()
        self.transformer = nn.Sequential(
            nn.Linear(num_features, num_features // 2),
            nn.ReLU(True),
            nn.Linear(num_features // 2, num_features))
        
    def forward(self, x):
        x = self.transformer(x)
        return x

if __name__ == "__main__":
    transformer = Transformer(2048) 
    print transformer
    for param in transformer.parameters():
        print param.shape, param.dtype




