import os
import sys
import math
import torch
import skvideo.io
import numpy as np
import time

from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint

class FrameFeatureTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, task_class_num, 
                 kshot, query_kshot):

        super(FrameFeatureTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot

        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))

            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

    def __len__(self):
        if self.training:
            return self.task_class_num
        else:
            return self.query_kshot * self.task_class_num

    def __getitem__(self, idx):
        if self.training:
            cls = self.idx_to_class[idx]
            vids = self.sup_vid_fs[cls]
            fs = []
            for vid in vids:
                fs = fs + [os.path.join(vid, f) for f in os.listdir(vid)]

            frm_f = [] 
            for f in fs:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            vid = self.qry_vid_fs[cls][jdx]
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]

            frm_f = [] 
            for f in fs:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

class FrameFeatureTestDataset(Dataset):

    def __init__(self, test_file_dir, frame_dir, frame_feature_dir, task_class_num):

        super(FrameFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num

        # training or query
        self.training = True 
        self.file_idx = -1

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
            
        self.kshot = len(self.sup_vid_fs[self.idx_to_class[0]])

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            if self.qry_vid_fs.has_key(cls):
                self.qry_vid_fs[cls].append(path)
            else:
                self.qry_vid_fs[cls] = [path]
        
        self.query_kshot = len(self.qry_vid_fs[self.idx_to_class[0]])

    def __len__(self):
        if self.training:
            return self.task_class_num
        else:
            return self.query_kshot * self.task_class_num

    def __getitem__(self, idx):
        if self.training:
            cls = self.idx_to_class[idx]
            vids = self.sup_vid_fs[cls]
            fs = []
            for vid in vids:
                fs = fs + [os.path.join(vid, f) for f in os.listdir(vid)]

            frm_f = [] 
            for f in fs:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]

            vid = self.qry_vid_fs[cls][jdx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            frm_f = [] 
            for f in fs:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

class MixtureFeatureTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, image_feature_dir, task_class_num, \
                 kshot, query_kshot, noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(MixtureFeatureTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}
        self.train_image_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        # train
        self.sup_vid_fs = {}
        self.sup_img_fs = {}
        # query
        self.qry_vid_fs = {}

        # random select support set vids
        perm = np.random.permutation(len(self.classes))

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))
            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in self.train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = self.train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num
        
        self.sup_img_fs = task_train_image_fs

    def __len__(self):
        if self.training:
            return self.task_class_num
        else:
            return self.query_kshot * self.task_class_num

    def __getitem__(self, idx):
        if self.training:
            cls = self.idx_to_class[idx]
            vids = self.sup_vid_fs[cls]
            imgs = self.sup_img_fs[cls]

            vfs = []
            ifs = imgs
            
            for vid in vids:
                vfs = vfs + [os.path.join(vid, f) for f in os.listdir(vid)]
            
            frm_f = [] 
            for f in vfs:
                frm_f.append(np.load(f))

            img_f = []
            for f in ifs:
                f = np.load(f)
                if self.noise_rate > 0. and np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                img_f.append(f)
        
            sample_f = np.stack(frm_f + img_f)

            return sample_f, idx
        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]

            vid = self.qry_vid_fs[cls][jdx]
            fs = [os.path.join(vid, f) for f in os.listdir(vid)]

            frm_fs = []
            for f in fs:
                frm_fs.append(np.load(f))

            return np.stack(frm_fs), idx

class MixtureFeatureTestDataset(Dataset):

    def __init__(self, all_classes, test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, task_class_num, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 replace_rate=0.):

        super(MixtureFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.classes = all_classes

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate
        self.replace_rate = replace_rate

        # training or query
        self.training = True 
        self.file_idx = -1

        self.train_image_fs = {}
        self.all_image_fs = []

        for cls in self.classes:
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]
            self.all_image_fs = self.all_image_fs + [(cls, f) for f in self.train_image_fs[cls]]

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_img_fs = {}
        self.qry_vid_fs = {}

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]

        self.kshot = len(self.sup_vid_fs[self.idx_to_class[0]])
            
        train_image_fs = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            train_image_fs[cls] = self.train_image_fs[cls]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = train_image_fs[cls]

        # replace images with those of other classes
        if self.replace_rate > 0.:
            total_num = len(self.all_image_fs)

            for idx in range(self.task_class_num):
                cls = self.idx_to_class[idx]
                shuffle(self.all_image_fs)

                num = len(task_train_image_fs[cls])
                replace_num = int(self.replace_rate * num)
                
                j = 0
                for i in range(num-replace_num, num):
                    while self.all_image_fs[j][0] == cls:
                        j = (j + 1) % total_num

                    task_train_image_fs[cls][i] = self.all_image_fs[j][1]
                    j = (j + 1) % total_num

        self.sup_img_fs = task_train_image_fs

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            if self.qry_vid_fs.has_key(cls):
                self.qry_vid_fs[cls].append(path)
            else:
                self.qry_vid_fs[cls] = [path]

        self.query_kshot = len(self.qry_vid_fs[self.idx_to_class[0]])

    def __len__(self):
        if self.training:
            return self.task_class_num
        else:
            return self.task_class_num * self.query_kshot

    def __getitem__(self, idx):
        if self.training:
            cls = self.idx_to_class[idx]
            vids = self.sup_vid_fs[cls]
            imgs = self.sup_img_fs[cls]

            vfs = []
            ifs = imgs
            
            for vid in vids:
                vfs = vfs + [os.path.join(vid, f) for f in os.listdir(vid)]
            
            frm_f = [] 
            for f in vfs:
                frm_f.append(np.load(f))

            img_f = []
            for f in ifs:
                f = np.load(f)
                if self.noise_rate > 0. and np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                img_f.append(f)
        
            sample_f = np.stack(frm_f + img_f)

            return sample_f, idx

        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            vid = self.qry_vid_fs[cls][jdx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            frm_f = [] 
            for f in fs:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

if __name__ == "__main__":
    import torchvision.transforms as transforms
    import time

    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    frame_feature_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features"
    image_feature_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features"
    test_file_dir = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1"
    
    def random_noise(size):
        return np.random.normal(10., 10.0, size)

    dataset = MixtureFeatureTrainDataset(\
                classes, frame_feature_dir, image_feature_dir, \
                5, 1, 1, replace_rate=0.1) 
    '''
                    frm_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ]),
                    img_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ])) 
    '''
    loader1 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    loader2 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    loader1.dataset.reset()
    print loader2.dataset.idx_to_class
    idx_to_cls = loader2.dataset.idx_to_class
    for idx in range(5):
        cls = idx_to_cls[idx]
        print loader2.dataset.sup_img_fs[cls]

    exit()
    

    for idx, (samples, target) in enumerate(loader1):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print flag.shape, flag.dtype
        print target
        print flag
    
    loader1.dataset.training = False
    print loader2.dataset.training

    for idx, (samples, target) in enumerate(loader2):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target


