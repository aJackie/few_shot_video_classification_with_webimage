import os
import time

all_steps = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 350, 400, \
              450, 500]

for steps in all_steps:
    output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/tem_{}".format(steps)
    output_log_dir = "/S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/tem_{}".format(steps)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    if not os.path.isdir(output_log_dir):
        os.mkdir(output_log_dir)

    os.system("python train_frame.py \
                    -m resnet50 \
                    -mb 32 \
                    -vmb 32 \
                    -b 30 \
                    -qb 30 \
                    -vb 30 \
                    -w 0 \
                    -g 0 \
                    -mt_lr 0.0001 \
                    -tr_lr 0.1 \
                    --start_epoch 60 \
                    --epochs 80 \
                    --print_freq 1 \
                    --save_freq 2 \
                    --val_freq 1 \
                    --frame_feature_dir /local/MI/xz/transfer_learning/data/actNet_frame_features \
                    --train_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt \
                    --val_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_val/meta_val.txt \
                    --task_class_num 5 \
                    --kshot 1 \
                    --query_kshot 1 \
                    --val_kshot 1 \
                    --val_query_kshot 1 \
                    --max_updates 20 \
                    --query_max_steps 5 \
                    --max_meta_batches 100 \
                    --val_max_updates {} ".format(steps) + \
                    "--val_query_max_steps 5 \
                    --val_max_meta_batches 10 \
                    --max_mem_slots 100 \
                    --val_max_mem_slots 100 \
                    --max_time_steps 1 \
                    --val_max_time_steps 1 \
                    --log_dir /S2/MI/zxz/transfer_learning/tensorboard_log/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/frame_mem_frame \
                    --cp_dir /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/frame_mem_frame \
                    --cg_dir /S2/MI/zxz/transfer_learning/training_config/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/frame_mem_frame \
                    --resume /S2/MI/zxz/transfer_learning/checkpoint/few_shot/meta_learning/actNet/meta_input/1/res50/meta_train/frame_mem_frame/checkpoint_epoch50.pth.tar \
                    -e true \
                    --frame_dir /S2/MI/zxz/transfer_learning/data/actNet_frames \
                    --test_file_dir /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1 \
                    --st_file_idx 0 \
                    --end_file_idx 100 \
                    --test_class_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt \
                    --tr_epoches 1 \
                    --output_dir {} ".format(output_dir) + \
                    "--output_log_dir {} ".format(output_log_dir))
    time.sleep(5)
