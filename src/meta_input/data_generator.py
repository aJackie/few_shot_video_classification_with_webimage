import os
import sys
import math
import torch
import skvideo.io
import numpy as np
import time

from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint

"""
class FrameMemImageTrainDataset(Dataset):

    def __init__(self, all_classes, frame_dir, image_dir, task_class_num, kshot,
                 batch_size, max_updates, frm_transform=None, img_transform=None):

        super(FrameMemImageTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_dir = frame_dir
        self.image_dir = image_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.frm_transform = frm_transform
        self.img_transform = img_transform

        self.videos = {}
        self.images = {}
        for cls in self.classes:
            frm_cls_dir = os.path.join(self.frame_dir, cls)
            img_cls_dir = os.path.join(self.image_dir, cls)
            self.videos[cls] = [os.path.join(frm_cls_dir, v) for v in os.listdir(frm_cls_dir)]
            self.images[cls] = [os.path.join(img_cls_dir, i) for i in os.listdir(img_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids[cls] = [self.videos[cls][jdx] for jdx in vid_perm]
            else:
                self.task_vids[cls] = [self.videos[cls][jdx] for jdx in vid_perm[:self.kshot]]

        # the Mem component
        self.frame_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frames = []

            for vid in self.task_vids[cls]:
                frms = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frms:
                    frame = pil_loader(f)
                    if self.frm_transform is not None:
                        frame = self.frm_transform(frame)
                    frames.append(frame)
            
            self.frame_mem[cls] = np.stack(frames)
            
            shuffle(self.images[cls])

    def __len__(self):
        return self.batch_size * self.max_updates

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]
        
        path = self.images[cls][jdx]
        image = pil_loader(path)
        if self.img_transform is not None:
            image = self.img_transform(image)

        return image, idx
"""

class FrameFeatureMemImageFeatureTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, image_feature_dir, task_class_num, 
                 kshot, query_kshot, batch_size, query_batch_size, max_updates, query_max_updates):

        super(FrameFeatureMemImageFeatureTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.query_batch_size = query_batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates
        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}
        self.train_image_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}
        self.qry_frm_fs = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))

            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))

            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

            for vid in self.qry_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                self.qry_frm_fs = self.qry_frm_fs + [(f, idx) for f in frm_fs]

        shuffle(self.qry_frm_fs)

        self.min_cls_image_num = 1000
        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
            
            shuffle(self.train_image_fs[cls])
            self.min_cls_image_num = min(self.min_cls_image_num, len(self.train_image_fs[cls]))

    def __len__(self):
        if self.training:
            return min(self.batch_size * self.max_updates, self.min_cls_image_num*self.task_class_num)
        else:
            return min(self.query_batch_size * self.query_max_updates, len(self.qry_frm_fs))

    def __getitem__(self, idx):
        if self.training:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            path = self.train_image_fs[cls][jdx]
            image_f = np.load(path)

            return image_f, idx
        else:
            path, idx = self.qry_frm_fs[idx]
            frm_f = np.load(path)

            return frm_f, idx

class FrameFeatureMemImageFeatureValDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, image_feature_dir, task_class_num, 
                 kshot, query_kshot, batch_size, max_updates, query_max_updates):

        super(FrameFeatureMemImageFeatureValDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates
        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}
        self.train_image_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.qry_vid_fs = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))

            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))

            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

        self.min_cls_image_num = 1000
        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
            
            shuffle(self.train_image_fs[cls])
            self.min_cls_image_num = min(self.min_cls_image_num, len(self.train_image_fs[cls]))

    def __len__(self):
        if self.training:
            return min(self.batch_size * self.max_updates, self.min_cls_image_num*self.task_class_num)
        else:
            return min(self.query_max_updates, self.query_kshot*self.task_class_num)

    def __getitem__(self, idx):
        jdx = idx // self.task_class_num
        idx = idx % self.task_class_num
        cls = self.idx_to_class[idx]
            
        if self.training:
            path = self.train_image_fs[cls][jdx]
            image_f = np.load(path)

            return image_f, idx
        else:
            vid = self.qry_vid_fs[cls][jdx]
            frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            frame_fs = []
            for f in frm_fs:
                frame_fs.append(np.load(f))

            return np.stack(frame_fs), idx

class FrameFeatureMemFrameFeatureTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, task_class_num, 
                 kshot, query_kshot, batch_size, query_batch_size, max_updates, query_max_updates,\
                 noise_rate=0., noise_func=None):

        super(FrameFeatureMemFrameFeatureTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.query_batch_size = query_batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates

        # training or query
        self.training = True 
        self.video_wise_train = False

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func

        self.train_video_fs = {}
        self.query_video_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_frm_fs = []
        self.qry_vid_fs = {}
        self.qry_frm_fs = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))

            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]
            shuffle(self.sup_vid_fs[cls])
            for vid in self.sup_vid_fs[cls]:
                self.sup_frm_fs = self.sup_frm_fs + [(os.path.join(vid, f), idx) for f in os.listdir(vid)]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

            for vid in self.qry_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                self.qry_frm_fs = self.qry_frm_fs + [(f, idx) for f in frm_fs]

        shuffle(self.sup_frm_fs)
        shuffle(self.qry_frm_fs)

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
            
    def __len__(self):
        if self.training:
            if self.video_wise_train:
                return self.kshot * self.task_class_num
            else:
                return min(self.batch_size * self.max_updates, len(self.sup_frm_fs))
        else:
            if self.video_wise_train:
                return self.query_kshot * self.task_class_num
            else:
                return min(self.query_batch_size * self.query_max_updates, len(self.qry_frm_fs))

    def __getitem__(self, idx):
        if self.training:
            if not self.video_wise_train:
                """
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                jdx = jdx % self.kshot 
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                kdx = randint(0, len(fs)-1)
                path = fs[kdx]

                frm_f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    frm_f = frm_f + self.noise_func(size=frm_f.shape).astype(frm_f.dtype)
                return frm_f, idx
                """

                path, idx = self.sup_frm_fs[idx]
                f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                frm_f = [] 
                for f in fs:
                    frm_f.append(np.load(f))
                frm_f = np.stack(frm_f)

                return frm_f, idx

        else:
            if not self.video_wise_train:
                path, idx = self.qry_frm_fs[idx]
                frm_f = np.load(path)
                return frm_f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                vid = self.qry_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                frm_f = [] 
                for f in fs:
                    frm_f.append(np.load(f))
                frm_f = np.stack(frm_f)

                return frm_f, idx


class FrameFeatureMemFrameFeatureValDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, task_class_num, 
                 kshot, query_kshot, batch_size, max_updates, query_max_updates,\
                 noise_rate=0., noise_func=None):

        super(FrameFeatureMemFrameFeatureValDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates

        # training or query
        self.training = True 
        self.video_wise_train = False

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func

        self.train_video_fs = {}
        self.query_video_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_frm_fs = []
        self.qry_vid_fs = {}

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))

            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]
            shuffle(self.sup_vid_fs[cls])
            for vid in self.sup_vid_fs[cls]:
                self.sup_frm_fs = self.sup_frm_fs + [(os.path.join(vid, f), idx) for f in os.listdir(vid)]

            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))

            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]
        
        shuffle(self.sup_frm_fs)

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
            
    def __len__(self):
        if self.training:
            if self.video_wise_train:
                return self.kshot * self.task_class_num
            else:
                return min(self.batch_size * self.max_updates, len(self.sup_frm_fs))
        else:
            if self.video_wise_train:
                return self.query_kshot * self.task_class_num
            else:
                return min(self.query_max_updates, self.query_kshot*self.task_class_num)

    def __getitem__(self, idx):
        if self.training:
            if not self.video_wise_train:
                """
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                jdx = jdx % self.kshot
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                kdx = randint(0, len(fs)-1)
                path = fs[kdx]
                frm_f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    frm_f = frm_f + self.noise_func(size=frm_f.shape).astype(frm_f.dtype)
                return frm_f, idx
                """
                path, idx = self.sup_frm_fs[idx]
                f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                frm_f = [] 
                for f in fs:
                    frm_f.append(np.load(f))
                frm_f = np.stack(frm_f)

                return frm_f, idx
        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            vid = self.qry_vid_fs[cls][jdx]
            frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            frame_fs = []
            for f in frm_fs:
                frame_fs.append(np.load(f))

            return np.stack(frame_fs), idx

class FrameFeatureMemFrameFeatureTestDataset(Dataset):

    def __init__(self, test_file_dir, frame_dir, frame_feature_dir, task_class_num, 
                 sample_num, dup_num, batch_size, max_updates, \
                 noise_rate=0., noise_func=None):

        super(FrameFeatureMemFrameFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.task_class_num = task_class_num
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.batch_size = batch_size
        self.max_updates = max_updates

        # training or query
        self.training = True 
        self.video_wise_train = False
        self.file_idx = -1

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_frm_fs = []
        self.qry_vid_fs = []

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
            
            self.sup_frm_fs = self.sup_frm_fs + [(os.path.join(path, f), idx) for f in os.listdir(path)]

        self.kshot = len(self.sup_vid_fs[self.idx_to_class[0]])

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            self.qry_vid_fs.append((path, idx))

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)
            
    def __len__(self):
        if self.training:
            if self.video_wise_train:
                return self.kshot * self.task_class_num
            else:
                return min(self.batch_size * self.max_updates, len(self.sup_frm_fs))
        else:
            return len(self.qry_vid_fs)

    def __getitem__(self, idx):
        if self.training:
            if not self.video_wise_train:
                """
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                jdx = jdx % self.kshot
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                kdx = randint(0, len(fs)-1)
                path = fs[kdx]
                frm_f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    frm_f = frm_f + self.noise_func(size=frm_f.shape).astype(frm_f.dtype)
                return frm_f, idx
                """
                path, idx = self.sup_frm_fs[idx]
                f = np.load(path)
                if np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)
                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                vid = self.sup_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                frm_f = [] 
                for f in fs:
                    frm_f.append(np.load(f))
                frm_f = np.stack(frm_f)

                return frm_f, idx


        else:
            vid, idx = self.qry_vid_fs[idx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            if self.sample_num == -1:
                select = np.concatenate([fs] * self.dup_num)
            else:
                select = np.array([])
                for _ in range(self.dup_num):
                    perms = np.random.permutation(len(fs))
                    select = np.concatenate([select, fs[perms][map(lambda x: x%len(fs), range(self.sample_num))]])

            frm_f = [] 
            for f in select:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx

class FrameFeatureMemMixtureFeatureTrainDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, image_feature_dir, task_class_num, \
                 kshot, query_kshot, batch_size, query_batch_size, max_updates, query_max_updates, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 image_num_per_support=100, support_num_per_cls=1):

        super(FrameFeatureMemMixtureFeatureTrainDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.query_batch_size = query_batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates
        self.image_num_per_support = image_num_per_support
        self.support_num_per_cls = support_num_per_cls

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate

        # training or query
        self.training = True 
        self.video_wise_train = False

        self.train_video_fs = {}
        self.query_video_fs = {}
        self.train_image_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        # mem
        self.sup_vid_fs = {}
        # train
        self.sup_img_fs = {}
        self.tr_img_fs = []
        self.tr_frm_fs = []
        self.tr_sample_fs = []
        # query
        self.qry_vid_fs = {}
        self.qry_frm_fs = []

        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))
            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            for vid in self.sup_vid_fs[cls]:
                self.tr_frm_fs = self.tr_frm_fs + [(os.path.join(vid, f), idx, "f") for f in os.listdir(vid)]
            
            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

            for vid in self.qry_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                self.qry_frm_fs = self.qry_frm_fs + [(f, idx) for f in frm_fs]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in self.train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = self.train_image_fs[cls]
        
        # generate image-volume
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(task_train_image_fs[cls]))
                self.sup_img_fs[cls].append([task_train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            self.tr_img_fs = self.tr_img_fs + map(lambda x:(x, idx, "i"), task_train_image_fs[cls])

        self.tr_sample_fs = self.tr_img_fs + self.tr_frm_fs
        shuffle(self.tr_sample_fs)
        shuffle(self.qry_frm_fs)

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)

    def __len__(self):
        if self.training:
            if not self.video_wise_train:
                return min(self.batch_size * self.max_updates, len(self.tr_sample_fs))
            else:
                return self.task_class_num * (self.kshot + self.support_num_per_cls)
        else:
            if not self.video_wise_train:
                return min(self.query_batch_size * self.query_max_updates, len(self.qry_frm_fs))
            else:
                return self.query_kshot * self.task_class_num

    def __getitem__(self, idx):
        if self.training:
            if not self.video_wise_train:
                path, idx, flag = self.tr_sample_fs[idx]
                f = np.load(path)
                if flag == "i" and self.noise_rate > 0. and \
                        np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)

                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                if jdx < self.kshot:
                    vid = self.sup_vid_fs[cls][jdx]
                    fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                    flag = "f"
                else:
                    fs = self.sup_img_fs[cls][jdx - self.kshot]
                    flag = "i"

                sample_fs = []
                for f in fs:
                    f = np.load(f)
                    if flag == "i" and self.noise_rate > 0. and \
                            np.random.uniform() < self.noise_rate:
                        f = f + self.noise_func(size=f.shape).astype(f.dtype)
                    sample_fs.append(f)

                sample_fs = np.stack(sample_fs)

                return sample_fs, idx
        else:
            if not self.video_wise_train:
                path, idx = self.qry_frm_fs[idx]
                frm_f = np.load(path)

                return frm_f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]

                vid = self.qry_vid_fs[cls][jdx]
                fs = [os.path.join(vid, f) for f in os.listdir(vid)]

                frm_fs = []
                for f in fs:
                    frm_fs.append(np.load(f))

                return np.stack(frm_fs), idx


class FrameFeatureMemMixtureFeatureValDataset(Dataset):

    def __init__(self, all_classes, frame_feature_dir, image_feature_dir, task_class_num, 
                 kshot, query_kshot, batch_size, max_updates, query_max_updates, \
                 noise_rate=0., noise_func=None, label_flip_rate=0.):

        super(FrameFeatureMemMixtureFeatureValDataset, self).__init__()
        self.classes = all_classes
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.query_max_updates = query_max_updates
        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate

        # training or query
        self.training = True 

        self.train_video_fs = {}
        self.query_video_fs = {}
        self.train_image_fs = {}

        for cls in self.classes:
            frm_tr_cls_dir = os.path.join(self.frame_feature_dir, "train_res50",  cls)
            frm_val_cls_dir = os.path.join(self.frame_feature_dir, "val_res50", cls)
            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50", cls)

            self.train_video_fs[cls] = [os.path.join(frm_tr_cls_dir, v) for v in os.listdir(frm_tr_cls_dir)]
            self.query_video_fs[cls] = [os.path.join(frm_val_cls_dir, v) for v in os.listdir(frm_val_cls_dir)]
            self.train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)

    def reset(self):
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        # mem
        self.sup_vid_fs = {}
        # train
        self.tr_img_fs = []
        self.tr_frm_fs = []
        self.tr_sample_fs = []
        # query
        self.qry_vid_fs = {}

        # random select support set vids
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.train_video_fs[cls]))
            self.sup_vid_fs[cls] = [self.train_video_fs[cls][jdx] for jdx in vid_perm[:self.kshot]]

            for vid in self.sup_vid_fs[cls]:
                self.tr_frm_fs = self.tr_frm_fs + [(os.path.join(vid, f), idx, "f") for f in os.listdir(vid)]
            
            vid_perm = np.random.permutation(len(self.query_video_fs[cls]))
            self.qry_vid_fs[cls] = [self.query_video_fs[cls][jdx] for jdx in vid_perm[:self.query_kshot]]

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in self.train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = self.train_image_fs[cls]
        
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            self.tr_img_fs = self.tr_img_fs + map(lambda x:(x, idx, "i"), task_train_image_fs[cls])

        self.tr_sample_fs = self.tr_img_fs + self.tr_frm_fs
        shuffle(self.tr_sample_fs)

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)

    def __len__(self):
        if self.training:
            return min(self.batch_size * self.max_updates, len(self.tr_sample_fs))
        else:
            return min(self.query_max_updates, self.query_kshot*self.task_class_num)

    def __getitem__(self, idx):
        if self.training:
            path, idx, flag = self.tr_sample_fs[idx]
            f = np.load(path)
            if flag == "i" and self.noise_rate > 0. and \
                    np.random.uniform() < self.noise_rate:
                f = f + self.noise_func(size=f.shape).astype(f.dtype)

            return f, idx
        else:
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            
            vid = self.qry_vid_fs[cls][jdx]
            frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
            frame_fs = []
            for f in frm_fs:
                frame_fs.append(np.load(f))

            return np.stack(frame_fs), idx

class FrameFeatureMemMixtureFeatureTestDataset(Dataset):

    def __init__(self, test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, task_class_num, \
                 sample_num, dup_num, batch_size, max_updates, \
                 noise_rate=0., noise_func=None, label_flip_rate=0., \
                 image_num_per_support=100, support_num_per_cls=1,\
                 kshot=1, query_kshot=1):

        super(FrameFeatureMemMixtureFeatureTestDataset, self).__init__()
        self.test_file_dir = test_file_dir
        self.frame_dir = frame_dir
        self.frame_feature_dir = frame_feature_dir
        self.image_feature_dir = image_feature_dir
        self.task_class_num = task_class_num
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.image_num_per_support = image_num_per_support
        self.support_num_per_cls = support_num_per_cls
        self.kshot = 1
        self.query_kshot = 1

        # for adding noise
        self.noise_rate = noise_rate
        self.noise_func = noise_func
        self.label_flip_rate = label_flip_rate

        # training or query
        self.training = True 
        self.video_wise_train = False
        self.file_idx = -1

    def next_file(self):
        self.file_idx = self.file_idx + 1
        self.train_file = os.path.join(self.test_file_dir, "train{}.txt".format(self.file_idx))
        self.test_file = os.path.join(self.test_file_dir, "test{}.txt".format(self.file_idx))

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.sup_vid_fs = {}
        self.sup_img_fs = {}
        self.sup_sam_fs = {}
        self.sup_sample_fs = []
        self.qry_vid_fs = []

        with open(self.train_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/train", self.frame_feature_dir+"/train_res50")
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            if self.sup_vid_fs.has_key(cls):
                self.sup_vid_fs[cls].append(path)
            else:
                self.sup_vid_fs[cls] = [path]
            
            if self.sup_sam_fs.has_key(cls):
                self.sup_sam_fs[cls] = self.sup_sam_fs[cls] + [(os.path.join(path, f), idx, "f") for f in os.listdir(path)]
            else:
                self.sup_sam_fs[cls] = [(os.path.join(path, f), idx, "f") for f in os.listdir(path)]

        train_image_fs = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            img_tr_cls_dir = os.path.join(self.image_feature_dir, "train_res50",  cls)
            train_image_fs[cls] = [os.path.join(img_tr_cls_dir, i) for i in os.listdir(img_tr_cls_dir)]

        #self.train_image_fs = train_image_fs

        task_train_image_fs = {cls:[] for cls in self.class_to_idx.keys()}
        # flip the label of images at random
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            if self.label_flip_rate > 0.:
                for img in train_image_fs[cls]:

                    if np.random.uniform() < self.label_flip_rate:
                        new_idx = randint(0, self.task_class_num - 1)
                        while new_idx == idx:
                            new_idx = randint(0, self.task_class_num - 1)

                        new_cls = self.idx_to_class[new_idx]
                        task_train_image_fs[new_cls].append(img)
                    else:
                        task_train_image_fs[cls].append(img)
            else:
                task_train_image_fs[cls] = train_image_fs[cls]

        #self.task_train_image_fs = task_train_image_fs

        # generate image-volume
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]

            self.sup_img_fs[cls] = []
            for i in range(self.support_num_per_cls):
                img_perm = np.random.permutation(len(task_train_image_fs[cls]))
                self.sup_img_fs[cls].append([task_train_image_fs[cls][jdx] for jdx in img_perm[:self.image_num_per_support]])

        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            self.sup_sam_fs[cls] = self.sup_sam_fs[cls] + [(img, idx, "i") for img in task_train_image_fs[cls]]
            shuffle(self.sup_sam_fs[cls])
            self.sup_sample_fs = self.sup_sample_fs + self.sup_sam_fs[cls]

        with open(self.test_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            items = line.split("|")
            path = items[0].replace(self.frame_dir+"/val", self.frame_feature_dir+"/val_res50")
            idx = int(items[1])
            cls = items[2]

            assert self.class_to_idx[cls] == idx
            assert self.idx_to_class[idx] == cls

            self.qry_vid_fs.append((path, idx))

        # the Mem component
        self.frame_f_mem = {}
        for idx in range(self.task_class_num):
            cls = self.idx_to_class[idx]
            frame_fs = []

            for vid in self.sup_vid_fs[cls]:
                frm_fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                for f in frm_fs:
                    frame_f = np.load(f)
                    frame_fs.append(frame_f)
            
            self.frame_f_mem[cls] = np.stack(frame_fs)

    def __len__(self):
        if self.training:
            if not self.video_wise_train:
                #return self.batch_size * self.max_updates
                return min(self.batch_size * self.max_updates, len(self.sup_sample_fs))
            else:
                return self.task_class_num * (self.kshot + self.support_num_per_cls)

        else:
            return len(self.qry_vid_fs)

    def __getitem__(self, idx):
        if self.training:
            """
            jdx = idx // self.task_class_num
            idx = idx % self.task_class_num
            cls = self.idx_to_class[idx]
            jdx = jdx % len(self.sup_sam_fs[cls])
            path, idx, flag = self.sup_sam_fs[cls][jdx]
            """
            if not self.video_wise_train:
                path, idx, flag = self.sup_sample_fs[idx]
                f = np.load(path)
                if flag == "i" and self.noise_rate > 0. and \
                        np.random.uniform() < self.noise_rate:
                    f = f + self.noise_func(size=f.shape).astype(f.dtype)

                return f, idx
            else:
                jdx = idx // self.task_class_num
                idx = idx % self.task_class_num
                cls = self.idx_to_class[idx]
                
                if jdx < self.kshot:
                    vid = self.sup_vid_fs[cls][jdx]
                    fs = [os.path.join(vid, f) for f in os.listdir(vid)]
                    flag = "f"
                else:
                    fs = self.sup_img_fs[cls][jdx - self.kshot]
                    flag = "i"

                sample_fs = []
                for f in fs:
                    f = np.load(f)
                    if flag == "i" and self.noise_rate > 0. and \
                            np.random.uniform() < self.noise_rate:
                        f = f + self.noise_func(size=f.shape).astype(f.dtype)
                    sample_fs.append(f)

                sample_fs = np.stack(sample_fs)

                return sample_fs, idx

        else:
            vid, idx = self.qry_vid_fs[idx]
            fs = np.array([os.path.join(vid, f) for f in os.listdir(vid)])

            if self.sample_num == -1:
                select = np.concatenate([fs] * self.dup_num)
            else:
                select = np.array([])
                for _ in range(self.dup_num):
                    perms = np.random.permutation(len(fs))
                    select = np.concatenate([select, fs[perms][map(lambda x: x%len(fs), range(self.sample_num))]])

            frm_f = [] 
            for f in select:
                frm_f.append(np.load(f))
            frm_f = np.stack(frm_f)

            return frm_f, idx


class FrameMetaTrainMBDataset(Dataset):
    # advice: batch_size % task_class_num == 0
    def __init__(self, all_classes, frame_dir, task_class_num, kshot,
                 batch_size, max_updates, frm_transform=None):

        super(FrameMetaTrainMBDataset, self).__init__()
        self.classes = all_classes
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.frm_transform = frm_transform

        self.videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, cls)
            self.videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
        self.reset()

    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label) , frame_paths)
        shuffle(self.frames)

        """
        print "task {} classes".format(len(self.class_to_idx.keys()))
        print "task {} videos".format(len(self.task_vids))
        print "task {} frames".format(len(self.frames))
        """

    def set(self, class_to_idx):

        self.task_vids = []
        for cls, idx in class_to_idx.items():
            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label) , frame_paths)
        shuffle(self.frames)

        """
        print "task {} classes".format(len(self.class_to_idx.keys()))
        print "task {} videos".format(len(self.task_vids))
        print "task {} frames".format(len(self.frames))
        """
        
    def __len__(self):
        return min(len(self.frames), self.max_updates * self.batch_size)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        try:
            sample = pil_loader(path)
        except Exception as e:
            print >> sys.stderr, e, path
            return self.__getitem__((idx + randint(1, len(self.frames) - 1)) % len(self.frames))

        if self.frm_transform is not None:
            sample = self.frm_transform(sample)

        return sample, target

class FrameMetaTrainDataset(Dataset):
    def __init__(self, meta_tr_classes, frame_dir, meta_test_class_num, kshot, query_kshot,
                 frm_per_cls=10, query_frm_per_cls=10,  max_tasks=1000, frm_transform=None):

        super(FrameMetaTrainDataset, self).__init__()
        self.classes = meta_tr_classes
        self.frame_dir = frame_dir
        self.test_class_num = meta_test_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.frm_per_cls = frm_per_cls
        self.query_frm_per_cls = query_frm_per_cls
        self.max_tasks = max_tasks
        self.frm_transform = frm_transform

        self.train_vids = {}
        self.test_vids = {}

        for cls in self.classes:
            tr_cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_vids[cls] = [os.path.join(tr_cls_dir, v) for v in os.listdir(tr_cls_dir)]

            test_cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_vids[cls] = [os.path.join(test_cls_dir, v) for v in os.listdir(test_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.test_class_num, self.kshot)

    def __len__(self):
        return self.max_tasks

    def __getitem__(self, idx):
        support_vids = []
        query_vids = []

        # random select test_class_num classes
        shuffle(self.classes)
        
        for idx in range(self.test_class_num):

            cls = self.classes[idx]
            tr_cls_vids = self.train_vids[cls]
            test_cls_vids = self.test_vids[cls]
            
            # random select k-shot videos
            shuffle(tr_cls_vids)
            support_vids.append(tr_cls_vids[:self.kshot])

            shuffle(test_cls_vids)
            query_vids.append(test_cls_vids[:self.query_kshot])
            
        support_frms = []
        query_frms = []
    
        # random select frames
        for idx in range(self.test_class_num):
            
            support_cls_vids = support_vids[idx]
            query_cls_vids = query_vids[idx]

            support_cls_frms = []
            for vid in support_cls_vids:
                frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
                support_cls_frms = support_cls_frms + frms
            
            shuffle(support_cls_frms)
            support_frms.append(support_cls_frms[:self.frm_per_cls])

            max_len = len(support_cls_frms)
            if max_len < self.frm_per_cls:
                for _ in range(self.frm_per_cls - max_len):
                    support_frms[-1].append(support_cls_frms[randint(0, max_len-1)])

            query_cls_frms = []
            for vid in query_cls_vids:
                frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
                query_cls_frms = query_cls_frms + frms
            
            shuffle(query_cls_frms)
            query_frms.append(query_cls_frms[:self.query_frm_per_cls])

            max_len = len(query_cls_frms)
            if max_len < self.query_frm_per_cls:
                for _ in range(self.query_frm_per_cls - max_len):
                    query_frms[-1].append(query_cls_frms[randint(0, max_len-1)])

        # form tensor
        support_x = []
        support_y = []
        query_x = []
        query_y = []

        for idx in range(self.test_class_num):

            support_cls_frms = support_frms[idx]
            for frm in support_cls_frms:
                frame = pil_loader(frm)
                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)
                support_x.append(frame)
                support_y.append(idx)
            
            query_cls_frms = query_frms[idx]
            for frm in query_cls_frms:
                frame = pil_loader(frm)
                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)
                query_x.append(frame)
                query_y.append(idx)

        return torch.stack(support_x), np.array(support_y, dtype=np.long), \
                torch.stack(query_x), np.array(query_y, dtype=np.long)

class FrameMetaValFolder(Dataset):
    # classes can be subset of shuffled outside
    def __init__(self, meta_val_classes, frame_dir, kshot, frm_transform=None):
        super(FrameMetaValFolder, self).__init__()
        self.meta_val_classes = meta_val_classes
        self.frame_dir = self.frame_dir
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.frame_transform = frm_transform

        self.all_videos = []

        for cls in self.meta_val_classes:
            cls_dir = os.path.join(frame_dir, cls)
            self.all_videos.append([os.path.join(cls_dir, v) for v in os.listdir(cls_dir)])
        
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for idx, cls in enumerate(self.meta_val_classes): 
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            shuffle(self.all_videos[idx])
            if self.kshot == -1:
                self.videos = self.videos + map(lambda v: (v, idx), self.all_videos[idx])
            else:
                self.videos = self.videos + map(lambda v: (v, idx), self.all_videos[idx][:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class ImageListFolder(Dataset):
    """
        similar to torchvision datasets.ImageFolder
        but use class_lst to indicate valid classes
    """
    def __init__(self, image_dir, class_lst, img_transform=None):
        super(ImageListFolder, self).__init__()
        self.image_dir = image_dir
        self.classes = class_lst
        self.img_transform = img_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), idx), paths)

        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        path, target = self.imgs[idx]
        sample = pil_loader(path)
        if self.img_transform is not None:
            sample = self.img_transform(sample)

        return sample, target

class FrameListFileFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
    """
    def __init__(self, path_file, frame_transform=None):
        super(FrameListFileFolder, self).__init__()
        self.path_file = path_file
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class FrameListFileTestFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
        NOTE: this is used for evaluation
    """
    def __init__(self, path_file, sample_num, dup_num, frame_transform=None):
        super(FrameListFileTestFolder, self).__init__()
        self.path_file = path_file
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
            
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))

    def __len__(self):
        return len(self.videos)

    def __getitem__(self, idx):
        vid_dir, target = self.videos[idx]
        frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])
        if self.sample_num == -1:
            select = np.concatenate([frames] * self.dup_num)
        else:
            select = np.array([])
            for _ in range(self.dup_num):
                perms = np.random.permutation(len(frames))
                select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.sample_num))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class ImageFrameMixFolder(Dataset):
    """
        mixture of web images(from image dir) and few shot 
        training video frames(from training file)
    """
    def __init__(self, image_dir, path_file, class_lst, img_transform=None, frame_transform=None):
        super(ImageFrameMixFolder, self).__init__()
        self.image_dir = image_dir
        self.path_file = path_file
        self.classes = class_lst
        self.img_transform = img_transform
        self.frame_transform = frame_transform

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), "i", idx), paths)

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
          
            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), "f", label))

        self.samples = self.imgs + self.frames
        
        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))
        print "total {} samples".format(len(self.samples))

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        path, flag, target = self.samples[idx]
        sample = pil_loader(path)

        if flag == "i":
            if self.img_transform is not None:
                sample = self.img_transform(sample)
        else:
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)

        return sample, target

if __name__ == "__main__":
    import torchvision.transforms as transforms
    import time

    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    frame_feature_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features"
    image_feature_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features"
    test_file_dir = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1"
    
    def random_noise(size):
        return np.random.normal(10., 10.0, size)

    dataset = FrameFeatureMemMixtureFeatureTestDataset(\
                test_file_dir, frame_dir, frame_feature_dir, image_feature_dir, \
                5, -1, 1, 0, 0, 0., random_noise, label_flip_rate=1.) 
    dataset.video_wise_train = True
    '''
                    frm_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ]),
                    img_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ])) 
    '''
    loader1 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    loader2 = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    loader1.dataset.next_file()
    print loader2.dataset.idx_to_class

    for idx, (samples, target) in enumerate(loader1):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target
    
    loader1.dataset.training = False
    print loader2.dataset.training

    for idx, (samples, target) in enumerate(loader2):
        print samples.shape, samples.dtype
        print target.shape, target.dtype
        print target


