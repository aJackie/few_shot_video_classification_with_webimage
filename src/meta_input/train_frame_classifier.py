""" 
    training meta-input model,
    pure frame input, frame mem
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

from model import RelationNet, Classifier
from data_generator import FrameFeatureMemFrameFeatureTrainDataset, FrameFeatureMemFrameFeatureValDataset, \
                            FrameFeatureMemFrameFeatureTestDataset

from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training meta-input model, pure frame input, frame mem",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size of samples in one task")
parser.add_argument('-qb', '--query_batch_size', default=256, type=int, \
                        help="mini-batch size of samples in one task query set")
parser.add_argument('-vb', '--val_batch_size', default=256, type=int, help="val mini-batch size of samples in one task")

#parser.add_argument('-vqb', '--val_query_batch_size', default=256, type=int, \
#                        help="val mini-batch size of samples in one task query set")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")
parser.add_argument('-tr_lr', '--train_learning_rate', default=1e-2, type=float, \
                    help="learner learning rate")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--query_max_steps", type=int, default=10, help="max eval steps in query")
parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--val_max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--val_query_max_steps", type=int, default=10, help="max eval steps in query")
parser.add_argument("--val_max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--max_mem_slots", type=int, default=100, help="max slots to fetch for attention in one class")
parser.add_argument("--val_max_mem_slots", type=int, default=100, help="val max slots to fetch for attention in one class")

parser.add_argument("--max_time_steps", type=int, default=1, help="max steps of LSTM processing")
parser.add_argument("--val_max_time_steps", type=int, default=1, help="val max steps of LSTM processing")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--tr_epoches', default=1, type=int, help="num of training epoches of one episode")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")
parser.add_argument("--output_log_dir", type=str, help="path of output log dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(relation, process, meta_optimizer, cp_dir):
    state = {
        "state_dict_relation": relation.state_dict(),
        "state_dict_process": process.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, relation, process, meta_optimizer, criterion, \
            epoch, meta_batch_size, max_meta_batches, num_features, train_learning_rate, max_mem_slots, max_time_steps):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_grads = []
        for j in range(meta_batch_size):
            print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = train_sloader.dataset.frame_f_mem
            idx_to_cls = train_sloader.dataset.idx_to_class

            # initialize a new classifier & optimizer
            classifier = Classifier(num_features, task_class_num).cuda()
            classifier_params = list(classifier.parameters())
            
            if i == 0 and j == 0:
                print classifier

            # do training
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                lstm_hidden = input_.view(input_.size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                for t in range(max_time_steps):
                    
                    lstm_input = []
                    # for every hidden sample, compute the soft attention scores
                    for jdx in range(lstm_hidden.size(0)):
                        x = lstm_hidden[jdx:jdx+1]
                        cls = idx_to_cls[int(target[jdx])]
                        cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                        cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                        cls_mem = np.squeeze(cls_mem, axis=(2,3))
                        cls_mem = torch.from_numpy(cls_mem).cuda()
                        x = x.repeat(cls_mem.size(0), 1)
                        
                        scores = relation(cls_mem, x)
                        scores = F.softmax(scores, dim=0).view(-1,1)

                        # LSTM input
                        mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                        lstm_input.append(mem_output)

                    lstm_input = torch.cat(lstm_input, dim=0) 
                    
                    # do LSTM 
                    lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))
               
                # do classification using last hidden
                output = classifier(lstm_hidden, classifier_params)

                # loss
                loss = criterion(output, target_var)
                
                grad = torch.autograd.grad(loss, classifier_params, create_graph=True)
                for _, param in enumerate(classifier_params):
                    classifier_params[_] = param - train_learning_rate * grad[_]
                
            # do query and get meta-gradient
            train_qloader.dataset.training = False

            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_.view(input_.size(0), -1).cuda()
                target_var = target.cuda()

                output = classifier(input_var, classifier_params)
                loss = criterion(output, target_var)
                if len(meta_grads) == 0:
                    meta_grads = torch.autograd.grad(loss, list(relation.parameters()) + list(process.parameters()),\
                                                        retain_graph=True)
                    #print meta_grads[0]
                else:
                    tem_grads = torch.autograd.grad(loss, list(relation.parameters()) + list(process.parameters()), \
                                                        retain_graph=(not idx == len(train_qloader)-1))
                    meta_grads = [meta_grads[_]+g for _, g in enumerate(tem_grads)]
                    #print meta_grads[0]

                losses.update(float(loss.data), input_.size(0))
                ac1 = accuracy(output, target_var, topk=(1,))
                acc1.update(float(ac1[0][0]), input_.size(0))

        meta_optimizer.zero_grad()
        meta_grads = [g/(len(train_qloader)*meta_batch_size) for g in meta_grads]
        for _, param in enumerate(list(relation.parameters()) + list(process.parameters())):
            param.grad = meta_grads[_]

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            max_updates = train_sloader.dataset.max_updates
            writer.add_scalars('meta_train/accuracy/acc1/step_{}'.format(max_updates), {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate(val_sloader, val_qloader, relation, process, criterion, epoch, val_meta_batch_size, \
                val_max_meta_batches, num_features, train_learning_rate, val_max_mem_slots, val_max_time_steps):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = val_sloader.dataset.frame_f_mem
            idx_to_cls = val_sloader.dataset.idx_to_class

            # initialize a new classifier & optimizer
            classifier = Classifier(num_features, task_class_num).cuda()
            optimizer = torch.optim.SGD(classifier.parameters(), train_learning_rate)

            # do training
            for idx, (input_, target) in enumerate(val_sloader):
                
                # initial lstm hidden state
                lstm_hidden = input_.view(input_.size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                with torch.no_grad():
                    for t in range(val_max_time_steps):
                        
                        lstm_input = []
                        # for every hidden sample, compute the soft attention scores
                        for jdx in range(lstm_hidden.size(0)):
                            x = lstm_hidden[jdx:jdx+1]
                            cls = idx_to_cls[int(target[jdx])]
                            cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                            cls_mem = cls_mem[:min(cls_mem.shape[0], val_max_mem_slots)]
                            cls_mem = np.squeeze(cls_mem, axis=(2,3))
                            cls_mem = torch.from_numpy(cls_mem).cuda()
                            x = x.repeat(cls_mem.size(0), 1)
                            
                            scores = relation(cls_mem, x)
                            scores = F.softmax(scores, dim=0).view(-1,1)

                            # LSTM input
                            mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                            lstm_input.append(mem_output)

                        lstm_input = torch.cat(lstm_input, dim=0) 
                        
                        # do LSTM 
                        lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))
               
                # do classification using last hidden
                output = classifier(lstm_hidden)

                # loss
                loss = criterion(output, target_var)
                
                # update classifier
                grad = torch.autograd.grad(loss, list(classifier.parameters()))
                for _, param in enumerate(classifier.parameters()):
                    param.grad = grad[_]
                
                optimizer.step()

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            with torch.no_grad():
                for idx, (input_, target) in enumerate(val_qloader):
                    
                    input_var = input_[0]
                    input_var = input_var.view(input_var.size(0), -1).cuda()
                    target_var = target.cuda()

                    output = classifier(input_var).sum(dim=0, keepdim=True)
                    loss = criterion(output, target_var)

                    losses.update(float(loss.data), input_.size(0))
                    ac1 = accuracy(output, target_var, topk=(1,))
                    acc1.update(float(ac1[0][0]), input_.size(0))

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)
        
        end = time.time()
    
    return avg_losses.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                relation, process, criterion, tr_epoches, num_features, \
                train_learning_rate, max_mem_slots, max_time_steps, output_dir, output_log_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        log_dir = os.path.join(output_log_dir, "episode{}".format(i))
        if not os.path.isdir(log_dir):
            os.mkdir(log_dir)
        writer = SummaryWriter(log_dir=log_dir)

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get the Mem & classes info
        mem = test_sloader.dataset.frame_f_mem

        # initialize a new classifier & optimizer
        classifier = Classifier(num_features, task_class_num, 0.5).cuda()
        optimizer = torch.optim.SGD(classifier.parameters(), train_learning_rate)
            
        # do training
        for epoch in range(tr_epoches):
            for idx, (input_, target) in enumerate(test_sloader):
                
                # initial lstm hidden state
                lstm_hidden = input_.view(input_.size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                with torch.no_grad():
                    for t in range(max_time_steps):
                        
                        lstm_input = []
                        # for every hidden sample, compute the soft attention scores
                        for jdx in range(lstm_hidden.size(0)):
                            x = lstm_hidden[jdx:jdx+1]
                            cls = idx_to_cls[int(target[jdx])]
                            cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                            cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                            cls_mem = np.squeeze(cls_mem, axis=(2,3))
                            cls_mem = torch.from_numpy(cls_mem).cuda()
                            x = x.repeat(cls_mem.size(0), 1)
                            
                            scores = relation(cls_mem, x)
                            scores = F.softmax(scores, dim=0).view(-1,1)

                            # LSTM input
                            mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                            lstm_input.append(mem_output)

                        lstm_input = torch.cat(lstm_input, dim=0) 
                        
                        # do LSTM 
                        lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))
               
                # normalize
                lstm_hidden = F.normalize(lstm_hidden)

                # do classification using last hidden
                output = classifier(lstm_hidden)

                # loss
                loss = criterion(output, target_var)
                
                # update classifier
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                # record
                count_samples = (epoch * len(test_sloader) + idx + 1)*test_sloader.dataset.batch_size
                writer.add_scalars('meta_train/loss', {'val': float(loss)}, count_samples)
                ac1 = accuracy(output, target_var, topk=(1,))
                writer.add_scalars('meta_train/accuracy/acc1', {'val': float(ac1[0][0])}, count_samples)

        writer.close()

        # do query
        test_qloader.dataset.training = False

        with torch.no_grad():
            for idx, (input_, target) in enumerate(test_qloader):
                
                input_var = input_[0]
                input_var = input_var.view(input_var.size(0), -1).cuda()
                target_var = target.cuda()

                # normalize
                input_var = F.normalize(input_var)

                output = classifier(input_var).sum(dim=0, keepdim=True)
                ac1 = accuracy(output, target_var, topk=(1,))
                acc1.update(float(ac1[0][0]), input_.size(0))
                cls_acc1[idx_to_cls[int(target[0])]].update(float(ac1[0][0]), input_.size(0))

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    train_dataset = FrameFeatureMemFrameFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot,
                        args.batch_size,
                        args.query_batch_size,
                        args.max_updates,
                        args.query_max_steps)

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=args.batch_size, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=args.query_batch_size, shuffle=True,
            num_workers=args.workers)

    val_dataset = FrameFeatureMemFrameFeatureValDataset(
            val_classes,
            args.frame_feature_dir,
            args.task_class_num,
            args.val_kshot,
            args.val_query_kshot,
            args.val_batch_size,
            args.val_max_updates,
            args.val_query_max_steps)

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=args.val_batch_size, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # model
    num_features = model_features[args.model]
    relation = RelationNet(num_features).cuda()
    process = nn.LSTMCell(input_size=num_features, hidden_size=num_features, bias=True).cuda()

    # loss & optimizer
    criterion = nn.CrossEntropyLoss().cuda()
    meta_optimizer = torch.optim.SGD(list(relation.parameters()) +list(process.parameters()), args.meta_learning_rate)

    # resume from checkpoint?
    # NOTE: only meta-learner params can be stored and resumed
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            relation.load_state_dict(checkpoint['state_dict_relation'])
            process.load_state_dict(checkpoint['state_dict_process'])

            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    cudnn.enabled = True
    cudnn.benchmark = True

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = FrameFeatureMemFrameFeatureTestDataset(args.test_file_dir, args.frame_dir, args.frame_feature_dir, \
                            args.task_class_num, args.sample_num, args.dup_num, args.val_batch_size, args.val_max_updates)
        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=args.batch_size, shuffle=False,
                num_workers=args.workers)

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                    relation, process, criterion, args.tr_epoches, num_features, args.train_learning_rate, \
                    args.max_mem_slots, args.max_time_steps, args.output_dir, args.output_log_dir)
        
        return


    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        train(train_sloader, train_qloader, relation, process, meta_optimizer, criterion, epoch, args.meta_batch_size,\
                args.max_meta_batches, num_features, args.train_learning_rate, args.max_mem_slots, args.max_time_steps)
        save_tmp(relation, process, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_relation": relation.state_dict(),
                "state_dict_process": process.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss, acc1= validate(val_sloader, val_qloader, relation, process, criterion, epoch, args.val_meta_batch_size, \
                                    args.val_max_meta_batches, num_features, args.train_learning_rate, args.val_max_mem_slots,\
                                    args.val_max_time_steps)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1/step_{}'.format(args.val_max_updates), {'val': acc1}, count_samples)
        
    writer.close()

if __name__ == "__main__":
    main()
