import os
import sys
import torch
import shutil
from PIL import Image
import numpy as np
from tqdm import tqdm

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        img = img.convert('RGB')
    return img

class AverageMeter(object):
    """recording the average and current value of a summation var"""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += self.val * n
        self.count += n
        self.avg = self.sum * 1./ self.count

def save_checkpoint(state, is_best, cp_dir):
    epoch_num = state['epoch']
    filename = os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch_num))
    torch.save(state, filename)
    if is_best:
        shutil.copy(filename, os.path.join(cp_dir, "model_best.pth.tar"))

def accuracy(output, target, topk=(1,)):
    """computes the topk precision"""

    maxk = max(topk)
    batch_size = target.size(0)
    '''
    print("in accuracy function:")
    print("\touput: {}".format(output.size()))
    print("\ttarget: {}".format(target.size()))
    '''

    _,pred = output.topk(maxk, 1, True, True)
    #print("\tpred: {}".format(pred.shape))
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(1./batch_size))

    return res

def accuracy_relation(output, target, class_num):
    sample_num = target.shape[0] / class_num
    correct_num = 0.
    for i in range(sample_num):
        correct_num = correct_num + float(output[i*class_num: (i+1)*class_num].argmax(axis=0) ==\
                                            target[i*class_num: (i+1)*class_num].argmax(axis=0))

    return float(correct_num) / sample_num

def accuracy_svm(output, target):
    (values, counts) = np.unique(output, return_counts=True)
    idx = np.argmax(counts)
    maxcnt = counts[idx]
    maxnum = (counts == maxcnt).astype(np.float32).sum()
    ismax = (counts[values == target] == maxcnt).astype(np.float32).sum()
    return float(ismax / maxnum)

def my_split(line, sign, last_num):
    split = []
    ed = len(line)
    st = ed - 1
    while st >= 0:
        if line[st] == sign:
            split.append(line[st+1:ed])
            ed = st
        st -= 1
        if len(split) >= last_num:
            break
    split.append(line[0:ed])
    split.reverse()
    return split

def compute_frames_mean_std(list_file):
    with open(list_file, "r") as f:
        lines = [l.strip() for l in f.readlines()]

    paths = []
    for line in lines:
        ed = line.rfind("/") + 12
        path = line[:ed]
        paths.append(path)
    
    means = []
    stds = []
    for path in tqdm(paths):
        for frame in os.listdir(path):
            frame = os.path.join(path, frame)
            img = pil_loader(frame)
            img = np.array(img, dtype=np.float32) / 255.
            means.append(list(np.mean(img, axis=(0, 1))))
            stds.append(list(np.std(img, axis=(0, 1))))
    
    means = np.array(means, dtype=np.float32)
    stds = np.array(stds, dtype=np.float32)

    return list(np.mean(means, axis=0)), list(np.mean(stds, axis=0))

def compute_images_mean_std(root_dir):
    subdirs = [os.path.join(root_dir, _) for _ in os.listdir(root_dir)]
    means = []
    stds = []
    for subdir in tqdm(subdirs):
        for image in os.listdir(subdir):
            image = os.path.join(subdir, image)
            img = pil_loader(image)
            img = np.array(img, dtype=np.float32) / 255.
            means.append(list(np.mean(img, axis=(0, 1))))
            stds.append(list(np.std(img, axis=(0, 1))))
    
    means = np.array(means, dtype=np.float32)
    stds = np.array(stds, dtype=np.float32)

    return list(np.mean(means, axis=0)), list(np.mean(stds, axis=0))

if __name__ == "__main__":
    assert len(sys.argv) >= 2
    root_dir = sys.argv[1]

    if len(sys.argv) == 2:
        # for frame
        subdirs = [os.path.join(root_dir, _) for _ in os.listdir(root_dir)]
        for subdir in subdirs:
            mean, std = compute_frames_mean_std(os.path.join(subdir, "train.txt"))
            np.savetxt(os.path.join(subdir, "train_mean_std.out"), np.array([mean, std], dtype=np.float32),\
                        fmt="%.8f")
    elif len(sys.argv) == 3: 
        save_file = sys.argv[2]
        # for web image
        mean, std = compute_images_mean_std(root_dir)
        np.savetxt(save_file, np.array([mean, std], dtype=np.float32), fmt="%.8f")


