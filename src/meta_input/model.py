import torch
import torch.nn as nn
import torch.nn.functional as F


class RelationNet(nn.Module):
    
    def __init__(self, num_features=2048):
        super(RelationNet, self).__init__()
        self.relation = nn.Sequential(
            nn.Linear(num_features*2, num_features),
            nn.ReLU(True),
            nn.Linear(num_features, 1),
            nn.Sigmoid())
        
    def forward(self, sx, qx):
        x = torch.cat([sx, qx], dim=1)
        #print x.shape
        x = x.view(x.size(0), -1)
        x = self.relation(x)
        
        return x.view(x.size(0))

class DDRelationNet(nn.Module):
    
    def __init__(self, num_features=2048):
        super(DDRelationNet, self).__init__()
        self.relation = nn.Sequential(
            nn.Linear(num_features*2, num_features),
            nn.ReLU(True),
            nn.Linear(num_features, 4))
        
    def forward(self, sx, qx):
        x = torch.cat([sx, qx], dim=1)
        #print x.shape
        x = x.view(x.size(0), -1)
        x = self.relation(x)
        
        return x

class DotProduct(nn.Module):

    def __init__(self, num_features=2048):
        self.num_features = num_features
        super(DotProduct, self).__init__()

    def forward(self, sx, qx):
        batch_size = sx.size(0)
        sx = sx.view(batch_size, 1, self.num_features)
        qx = qx.view(batch_size, self.num_features, 1)
        out = torch.bmm(sx, qx)
        
        return out.view(batch_size)

        
class Classifier(nn.Module):

    def __init__(self, num_features, num_classes, dropout_rate=0.):
        super(Classifier, self).__init__()
        self.linear1 = nn.Linear(num_features, num_features // 2)
        self.relu = nn.ReLU(True)
        self.dropout = nn.Dropout(p=dropout_rate)
        self.linear2 = nn.Linear(num_features // 2, num_features // 2)
        self.linear3 = nn.Linear(num_features // 2, num_classes)

    def forward(self, x, params=None):
        if params is None:
            x = self.linear1(x)
            x = self.relu(x)
            x = self.dropout(x)
            x = self.linear2(x)
            x = self.relu(x)
            x = self.dropout(x)
            x = self.linear3(x)
        else:
            x = F.linear(x, params[0], params[1])
            x = self.relu(x)
            x = self.dropout(x)
            x = F.linear(x, params[2], params[3])
            x = self.relu(x)
            x = self.dropout(x)
            x = F.linear(x, params[4], params[5])

        return x

if __name__ == "__main__":
    classifier = Classifier(2048, 5)
    print classifier
    for param in classifier.parameters():
        print param.shape, param.dtype




