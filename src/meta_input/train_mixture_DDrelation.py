""" 
    Domain Discriminative relation model on mixture
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

from model import DDRelationNet
from data_generator import FrameFeatureMemMixtureFeatureTrainDataset, \
                            FrameFeatureMemMixtureFeatureTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="relation model on mixture",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")
parser.add_argument("--image_feature_dir", type=str, required=True, help="path of image root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--support_num_per_cls", type=int, default=1, help="support image sets num in one class, \
                                                                        kind of like k of k-shot")
parser.add_argument("--image_num_per_support", type=int, default=100, help="image num in one support set, \
                                                                            kind of like frame num of one video")

parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")
parser.add_argument("--val_max_meta_batches", type=int, default=10, help="max val meta-batches per epoch")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(relation_model, meta_optimizer, cp_dir):
    state = {
        "state_dict_ddrelation": relation_model.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, relation_model, meta_optimizer, criterion, \
            epoch, meta_batch_size, max_meta_batches, num_features):

    batch_time = AverageMeter()
    avg_losses1 = AverageMeter()
    avg_losses2 = AverageMeter()
    avg_losses3 = AverageMeter()
    avg_losses4 = AverageMeter()
    avg_losses = AverageMeter()

    avg_acc1_frm = AverageMeter()
    avg_acc1_img = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num
    kshot = train_sloader.dataset.kshot

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch: ", i
            
        losses1 = AverageMeter()
        losses2 = AverageMeter()
        losses3 = AverageMeter()
        losses4 = AverageMeter()
        losses = AverageMeter()

        acc1_frm = AverageMeter()
        acc1_img = AverageMeter()
        acc1 = AverageMeter()

        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            idx_to_cls = train_sloader.dataset.idx_to_class

            # feature transformation
            support_features_frm = [[] for _ in range(task_class_num)]
            support_features_img = [[] for _ in range(task_class_num)]

            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                jdx = idx // task_class_num
                idx = idx % task_class_num
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()

                if jdx < kshot:
                    support_features_frm[int(target[0])].append(input_var.mean(dim=0)) 
                else:
                    support_features_img[int(target[0])].append(input_var.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features_frm[idx] = torch.stack(support_features_frm[idx]).mean(dim=0)
                support_features_img[idx] = torch.stack(support_features_img[idx]).mean(dim=0)

            support_features_frm = torch.stack(support_features_frm)
            support_features_img = torch.stack(support_features_img)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features_frm = support_features_frm.repeat(len(query_features), 1)
            support_features_img = support_features_img.repeat(len(query_features), 1)

            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            output_frm = relation_model(support_features_frm, query_features)
            output_img = relation_model(support_features_img, query_features)

            output_frm = torch.exp(output_frm)
            output_img = torch.exp(output_img)

            confidence_frm = output_frm.sum(dim=1, keepdim=True) 
            confidence_img = output_img.sum(dim=1, keepdim=True)

            output_frm = output_frm / confidence_frm
            output_img = output_img / confidence_img

            output_frm = output_frm.transpose(0,1)
            output_img = output_img.transpose(0,1)

            # Marginal probability
            # for output_frm
            pd1_frm = output_frm[1] + output_frm[3]
            pc1_frm = output_frm[2] + output_frm[3]

            # for output_img
            pd0_img = output_img[0] + output_img[2]
            pc1_img = output_img[2] + output_img[3]

            # Loss
            loss1 = criterion(pd1_frm, torch.ones(pd1_frm.size(0), dtype=torch.float32).cuda())
            loss2 = criterion(pc1_frm, expanded_targets)
            loss3 = criterion(pd0_img, torch.ones(pd0_img.size(0), dtype=torch.float32).cuda())
            loss4 = criterion(pc1_img, expanded_targets)
            
            loss = (loss1 + loss2 + loss3 + loss4) / 4.
            loss.backward()

            losses1.update(float(loss1), 1)
            losses2.update(float(loss2), 1)
            losses3.update(float(loss3), 1)
            losses4.update(float(loss4), 1)
            losses.update(float(loss), 1)

            predict_frm = (output_frm[3] / pd1_frm) * confidence_frm.view(confidence_frm.size(0))
            predict_img = (output_img[2] / pd0_img) * confidence_img.view(confidence_img.size(0))
            predict = predict_frm + predict_img

            ac1_frm = accuracy_relation(predict_frm.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            ac1_img = accuracy_relation(predict_img.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            ac1 = accuracy_relation(predict.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)

            acc1_frm.update(float(ac1_frm), 1)
            acc1_img.update(float(ac1_img), 1)
            acc1.update(float(ac1), 1)

        for param in relation_model.parameters():
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses1.update(losses1.avg, meta_batch_size)
        avg_losses2.update(losses2.avg, meta_batch_size)
        avg_losses3.update(losses3.avg, meta_batch_size)
        avg_losses4.update(losses4.avg, meta_batch_size)
        avg_losses.update(losses.avg, meta_batch_size)

        avg_acc1_frm.update(acc1_frm.avg, meta_batch_size)
        avg_acc1_img.update(acc1_img.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss1_batch', {'val': avg_losses1.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss2_batch', {'val': avg_losses2.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss3_batch', {'val': avg_losses3.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss4_batch', {'val': avg_losses4.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)

            writer.add_scalars('meta_train/accuracy/acc1_frm', {'val': avg_acc1_frm.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1_img', {'val': avg_acc1_img.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate(val_sloader, val_qloader, relation_model, criterion, epoch, val_meta_batch_size, \
                val_max_meta_batches, num_features):

    batch_time = AverageMeter()
    avg_losses1 = AverageMeter()
    avg_losses2 = AverageMeter()
    avg_losses3 = AverageMeter()
    avg_losses4 = AverageMeter()
    avg_losses = AverageMeter()

    avg_acc1_frm = AverageMeter()
    avg_acc1_img = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num
    kshot = val_sloader.dataset.kshot

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses1 = AverageMeter()
        losses2 = AverageMeter()
        losses3 = AverageMeter()
        losses4 = AverageMeter()
        losses = AverageMeter()

        acc1_frm = AverageMeter()
        acc1_img = AverageMeter()
        acc1 = AverageMeter()

        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            idx_to_cls = val_sloader.dataset.idx_to_class

            # feature transformation
            support_features_frm = [[] for _ in range(task_class_num)]
            support_features_img = [[] for _ in range(task_class_num)]

            for idx, (input_, target) in enumerate(val_sloader):
                jdx = idx // task_class_num
                idx = idx % task_class_num
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()

                if jdx < kshot:
                    support_features_frm[int(target[0])].append(input_var.mean(dim=0)) 
                else:
                    support_features_img[int(target[0])].append(input_var.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features_frm[idx] = torch.stack(support_features_frm[idx]).mean(dim=0)
                support_features_img[idx] = torch.stack(support_features_img[idx]).mean(dim=0)

            support_features_frm = torch.stack(support_features_frm)
            support_features_img = torch.stack(support_features_img)

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(val_qloader):
                
                input_var = input_[0]
                input_var = input_var.view(input_var.size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features_frm = support_features_frm.repeat(len(query_features), 1)
            support_features_img = support_features_img.repeat(len(query_features), 1)

            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            with torch.no_grad():
                output_frm = relation_model(support_features_frm, query_features)
                output_img = relation_model(support_features_img, query_features)

            output_frm = torch.exp(output_frm)
            output_img = torch.exp(output_img)

            confidence_frm = output_frm.sum(dim=1, keepdim=True) 
            confidence_img = output_img.sum(dim=1, keepdim=True)

            output_frm = output_frm / confidence_frm
            output_img = output_img / confidence_img

            output_frm = output_frm.transpose(0,1)
            output_img = output_img.transpose(0,1)

            # Marginal probability
            # for output_frm
            pd1_frm = output_frm[1] + output_frm[3]
            pc1_frm = output_frm[2] + output_frm[3]

            # for output_img
            pd0_img = output_img[0] + output_img[2]
            pc1_img = output_img[2] + output_img[3]

            # Loss
            loss1 = criterion(pd1_frm, torch.ones(pd1_frm.size(0), dtype=torch.float32).cuda())
            loss2 = criterion(pc1_frm, expanded_targets)
            loss3 = criterion(pd0_img, torch.ones(pd0_img.size(0), dtype=torch.float32).cuda())
            loss4 = criterion(pc1_img, expanded_targets)
            
            loss = (loss1 + loss2 + loss3 + loss4) / 4.

            losses1.update(float(loss1), 1)
            losses2.update(float(loss2), 1)
            losses3.update(float(loss3), 1)
            losses4.update(float(loss4), 1)
            losses.update(float(loss), 1)

            predict_frm = (output_frm[3] / pd1_frm) * confidence_frm.view(confidence_frm.size(0))
            predict_img = (output_img[2] / pd0_img) * confidence_img.view(confidence_img.size(0))
            predict = predict_frm + predict_img

            ac1_frm = accuracy_relation(predict_frm.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            ac1_img = accuracy_relation(predict_img.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            ac1 = accuracy_relation(predict.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)

            acc1_frm.update(float(ac1_frm), 1)
            acc1_img.update(float(ac1_img), 1)
            acc1.update(float(ac1), 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses1.update(losses1.avg, val_meta_batch_size)
        avg_losses2.update(losses2.avg, val_meta_batch_size)
        avg_losses3.update(losses3.avg, val_meta_batch_size)
        avg_losses4.update(losses4.avg, val_meta_batch_size)
        avg_losses.update(losses.avg, val_meta_batch_size)

        avg_acc1_frm.update(acc1_frm.avg, val_meta_batch_size)
        avg_acc1_img.update(acc1_img.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)
        
        end = time.time()
    
    return avg_losses1.avg, avg_losses2.avg, avg_losses3.avg, avg_losses4.avg, avg_losses.avg,\
                avg_acc1_frm.avg, avg_acc1_img.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                relation_model, criterion, num_features, output_dir):

    batch_time = AverageMeter()
    avg_acc1_frm = AverageMeter()
    avg_acc1_img = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    kshot = test_sloader.dataset.kshot

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1_frm = AverageMeter()
        acc1_img = AverageMeter()
        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # feature transformation
        support_features_frm = [[] for _ in range(task_class_num)]
        support_features_img = [[] for _ in range(task_class_num)]

        for idx, (input_, target) in enumerate(test_sloader):
            
            jdx = idx // task_class_num
            idx = idx % task_class_num
                
            input_var = input_[0].view(input_[0].size(0), -1).cuda()

            if jdx < kshot:
                support_features_frm[int(target[0])].append(input_var.mean(dim=0)) 
            else:
                support_features_img[int(target[0])].append(input_var.mean(dim=0)) 

        for idx in range(task_class_num):
            support_features_frm[idx] = torch.stack(support_features_frm[idx]).mean(dim=0)
            support_features_img[idx] = torch.stack(support_features_img[idx]).mean(dim=0)

        support_features_frm = torch.stack(support_features_frm)
        support_features_img = torch.stack(support_features_img)

        # do query
        test_qloader.dataset.training = False

        query_features = []
        query_targets = []
        for idx, (input_, target) in enumerate(test_qloader):
            
            input_var = input_[0]
            input_var = input_var.view(input_var.size(0), -1).cuda()
            query_features.append(input_var.mean(dim=0, keepdim=True))
            query_targets.append(int(target[0]))

        # expand 
        support_features_frm = support_features_frm.repeat(len(query_features), 1)
        support_features_img = support_features_img.repeat(len(query_features), 1)

        for _, f in enumerate(query_features):
            query_features[_] = f.repeat(task_class_num, 1)

        query_features = torch.cat(query_features)
        
        expanded_targets = []
        for idx in query_targets:
            t = [0.] * task_class_num
            t[idx] = 1.
            expanded_targets = expanded_targets + t

        expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

        with torch.no_grad():
            output_frm = relation_model(support_features_frm, query_features)
            output_img = relation_model(support_features_img, query_features)

        output_frm = torch.exp(output_frm)
        output_img = torch.exp(output_img)

        confidence_frm = output_frm.sum(dim=1, keepdim=True) 
        confidence_img = output_img.sum(dim=1, keepdim=True)

        output_frm = output_frm / confidence_frm
        output_img = output_img / confidence_img

        output_frm = output_frm.transpose(0,1)
        output_img = output_img.transpose(0,1)

        # Marginal probability
        # for output_frm
        pd1_frm = output_frm[1] + output_frm[3]
        pc1_frm = output_frm[2] + output_frm[3]

        # for output_img
        pd0_img = output_img[0] + output_img[2]
        pc1_img = output_img[2] + output_img[3]

        predict_frm = (output_frm[3] / pd1_frm) * confidence_frm.view(confidence_frm.size(0))
        predict_img = (output_img[2] / pd0_img) * confidence_img.view(confidence_img.size(0))
        predict = predict_frm + predict_img

        ac1_frm = accuracy_relation(predict_frm.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
        ac1_img = accuracy_relation(predict_img.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
        ac1 = accuracy_relation(predict.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)

        acc1_frm.update(float(ac1_frm), 1)
        acc1_img.update(float(ac1_img), 1)
        acc1.update(float(ac1), 1)

        predict = np.array(predict.cpu())
        for _, idx in enumerate(query_targets):
            ac1 = float(predict[_*task_class_num: (_+1)*task_class_num].argmax(axis=0) == idx)
            cls_acc1[idx_to_cls[idx]].update(ac1, 1)

        avg_acc1_frm.update(acc1_frm.avg, len(test_qloader)) 
        avg_acc1_img.update(acc1_img.avg, len(test_qloader)) 
        avg_acc1.update(acc1.avg, len(test_qloader)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1_frm:{}\n\tavg_acc1_img:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1_frm.avg, avg_acc1_img.avg, avg_acc1.avg))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    train_dataset = FrameFeatureMemMixtureFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.image_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot,
                        1,
                        1,
                        0,
                        0,
                        image_num_per_support=args.image_num_per_support,
                        support_num_per_cls=args.support_num_per_cls)

    train_dataset.video_wise_train = True

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_dataset = FrameFeatureMemMixtureFeatureTrainDataset(
            val_classes,
            args.frame_feature_dir,
            args.image_feature_dir,
            args.task_class_num,
            args.val_kshot,
            args.val_query_kshot,
            1,
            1,
            0,
            0,
            image_num_per_support=args.image_num_per_support,
            support_num_per_cls=args.support_num_per_cls)
    val_dataset.video_wise_train = True

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # Relation module
    num_features = model_features[args.model]
    relation_model = DDRelationNet(num_features).cuda()

    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            relation_model.load_state_dict(checkpoint['state_dict_ddrelation'])
            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    # loss & optimizer
    criterion = nn.MSELoss().cuda()
    meta_optimizer = torch.optim.SGD(relation_model.parameters(), args.meta_learning_rate)

    cudnn.enabled = True
    cudnn.benchmark = True

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = FrameFeatureMemMixtureFeatureTestDataset(args.test_file_dir,
                                                                args.frame_dir,
                                                                args.frame_feature_dir,
                                                                args.image_feature_dir,
                                                                args.task_class_num,
                                                                args.sample_num,
                                                                args.dup_num,
                                                                1,
                                                                0,
                                                                image_num_per_support=args.image_num_per_support,
                                                                support_num_per_cls=args.support_num_per_cls,
                                                                kshot=args.val_kshot,
                                                                query_kshot=args.val_query_shot)
        test_dataset.video_wise_train = True

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                    relation_model, criterion, num_features, args.output_dir)

        return

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        train(train_sloader, train_qloader, relation_model, meta_optimizer, criterion, \
                epoch, args.meta_batch_size, args.max_meta_batches, num_features)

        save_tmp(relation_model, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_ddrelation": relation_model.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            loss1, loss2, loss3, loss4, loss, acc1_frm, acc1_img, acc1 = \
                    validate(val_sloader, val_qloader, relation_model, criterion, epoch, args.val_meta_batch_size, \
                             args.val_max_meta_batches, num_features)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss1_epoch', {'val': loss1}, count_samples)
            writer.add_scalars('meta_val/loss/loss2_epoch', {'val': loss2}, count_samples)
            writer.add_scalars('meta_val/loss/loss3_epoch', {'val': loss3}, count_samples)
            writer.add_scalars('meta_val/loss/loss4_epoch', {'val': loss4}, count_samples)
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)

            writer.add_scalars('meta_val/accuracy/acc1_frm', {'val': acc1_frm}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1_img', {'val': acc1_img}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)
            #break
        
    writer.close()

if __name__ == "__main__":
    main()
