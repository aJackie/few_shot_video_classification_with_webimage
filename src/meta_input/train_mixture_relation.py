""" 
    relation model on mixture
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
import torch.nn.functional as F

from model import RelationNet
from data_generator import FrameFeatureMemMixtureFeatureTrainDataset, \
                            FrameFeatureMemMixtureFeatureTestDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="relation model on mixture",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="(ImageNet)pretrained model to extract features,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")
parser.add_argument("--resume_mlit", type=str, default=None, help="path to MLIT checkpoint file")

parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")

parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")

parser.add_argument('--start_epoch', default=None, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--frame_feature_dir", type=str, required=True, help="path of frame root dir")
parser.add_argument("--image_feature_dir", type=str, required=True, help="path of image root dir")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")
parser.add_argument("--task_class_num", type=int, default=5, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")
parser.add_argument("--val_kshot", type=int, default=1, help="val k-shot(of vids) of support set")
parser.add_argument("--val_query_kshot", type=int, default=1, help="val k-shot(of vids) of query set")

parser.add_argument("--support_num_per_cls", type=int, default=1, help="support image sets num in one class, \
                                                                        kind of like k of k-shot")
parser.add_argument("--image_num_per_support", type=int, default=100, help="image num in one support set, \
                                                                            kind of like frame num of one video")

parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")
parser.add_argument("--val_max_meta_batches", type=int, default=10, help="max val meta-batches per epoch")
parser.add_argument("--max_mem_slots", type=int, default=100, help="max slots to fetch for attention in one class")
parser.add_argument("--max_time_steps", type=int, default=1, help="max steps of LSTM processing")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

parser.add_argument("--MLIT", type=bool, default=False, help="whether with MLIT")
parser.add_argument("--train_protocol", type=int, default=0, help="training protocol with MLIT \
                                                                    0: only train relation module \
                                                                    1: train with MLIT jointly \
                                                                    2: train with MLIT interchanged")

parser.add_argument("-e", "--evaluate", type=bool, default=False, help="whether do evaluation only")
parser.add_argument("--frame_dir", type=str, help="path of frame root dir")
parser.add_argument("--test_file_dir", type=str, help="path of testing file root dir")
parser.add_argument('--st_file_idx', default=0, type=int, help="start index of testing files")
parser.add_argument('--end_file_idx', default=1000, type=int, help="end index of testing files")
parser.add_argument("--test_class_file", type=str, help="path to file containing meta testing classes")
parser.add_argument('--sample_num', default=-1, type=int, help="sampling num, -1 means all")
parser.add_argument('--dup_num', default=1, type=int, help="duplicate num")
parser.add_argument("--output_dir", type=str, help="path of output file root dir")

args=parser.parse_args()

model_features = {
        "resnet18": 512,
        "resnet34": 512,
        "resnet50": 2048,
        "resnet101": 2048,
        "resnet152": 2048
        }

def save_tmp(process, relation, relation_model, meta_optimizer, cp_dir):
    state = {
        "state_dict_relation": relation.state_dict(),
        "state_dict_process": process.state_dict(),
        "state_dict": relation_model.state_dict(),
        "meta_optimizer": meta_optimizer.state_dict()}

    save_path = os.path.join(cp_dir, "tem.pth.tar")
    torch.save(state, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, relation_model, meta_optimizer, criterion, \
            epoch, meta_batch_size, max_meta_batches, num_features):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = train_sloader.dataset.frame_f_mem
            idx_to_cls = train_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()
                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            output = relation_model(support_features, query_features)
            loss = criterion(output, expanded_targets)
            loss.backward()

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)

        for param in relation_model.parameters():
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate(val_sloader, val_qloader, relation_model, criterion, epoch, val_meta_batch_size, \
                val_max_meta_batches, num_features):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses = AverageMeter()
        acc1 = AverageMeter()

        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = val_sloader.dataset.frame_f_mem
            idx_to_cls = val_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(val_sloader):
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()
                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(val_qloader):
                
                input_var = input_[0]
                input_var = input_var.view(input_var.size(0), -1).cuda()
                target_var = target.cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            with torch.no_grad():
                output = relation_model(support_features, query_features)
                loss = criterion(output, expanded_targets)

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)
        
        end = time.time()
    
    return avg_losses.avg, avg_acc1.avg

def evaluate(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                relation_model, criterion, num_features, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get the Mem & classes info
        mem = test_sloader.dataset.frame_f_mem

        # feature transformation
        support_features = [[] for _ in range(task_class_num)]
        for idx, (input_, target) in enumerate(test_sloader):
            
            # initial lstm hidden state
            lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
            target_var = target.cuda()
            support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

        for idx in range(task_class_num):
            support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

        support_features = torch.stack(support_features)

        # do query
        test_qloader.dataset.training = False

        query_features = []
        query_targets = []
        for idx, (input_, target) in enumerate(test_qloader):
            
            input_var = input_[0]
            input_var = input_var.view(input_var.size(0), -1).cuda()
            target_var = target.cuda()
            query_features.append(input_var.mean(dim=0, keepdim=True))
            query_targets.append(int(target[0]))

        # expand 
        support_features = support_features.repeat(len(query_features), 1)
        for _, f in enumerate(query_features):
            query_features[_] = f.repeat(task_class_num, 1)

        query_features = torch.cat(query_features)
        
        expanded_targets = []
        for idx in query_targets:
            t = [0.] * task_class_num
            t[idx] = 1.
            expanded_targets = expanded_targets + t

        expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

        with torch.no_grad():
            output = relation_model(support_features, query_features)

        ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
        acc1.update(float(ac1), 1)
        output = np.array(output.cpu())
        for _, idx in enumerate(query_targets):
            ac1 = float(output[_*task_class_num: (_+1)*task_class_num].argmax(axis=0) == idx)
            cls_acc1[idx_to_cls[idx]].update(ac1, 1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))


def train_MLIT(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                epoch, meta_batch_size, max_meta_batches, num_features, max_mem_slots, max_time_steps):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = train_sloader.dataset.frame_f_mem
            idx_to_cls = train_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                with torch.no_grad():
                    for t in range(max_time_steps):
                        
                        lstm_input = []
                        # for every hidden sample, compute the soft attention scores
                        for jdx in range(lstm_hidden.size(0)):
                            x = lstm_hidden[jdx:jdx+1]
                            cls = idx_to_cls[int(target[0])]
                            cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                            cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                            cls_mem = np.squeeze(cls_mem, axis=(2,3))
                            cls_mem = torch.from_numpy(cls_mem).cuda()
                            x = x.repeat(cls_mem.size(0), 1)
                            
                            scores = relation(cls_mem, x)
                            scores = F.softmax(scores, dim=0).view(-1,1)

                            # LSTM input
                            mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                            lstm_input.append(mem_output)

                        lstm_input = torch.cat(lstm_input, dim=0) 
                        
                        # do LSTM 
                        lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))

                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            output = relation_model(support_features, query_features)
            loss = criterion(output, expanded_targets)
            loss.backward()

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)

        for param in relation_model.parameters():
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def validate_MLIT(val_sloader, val_qloader, relation, process, relation_model, criterion, epoch, val_meta_batch_size, \
                    val_max_meta_batches, num_features, val_max_mem_slots, val_max_time_steps):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = val_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(val_max_meta_batches)):
            
        losses = AverageMeter()
        acc1 = AverageMeter()

        for j in range(val_meta_batch_size):
                
            # generate a new task
            val_sloader.dataset.reset()
            val_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = val_sloader.dataset.frame_f_mem
            idx_to_cls = val_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(val_sloader):
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                with torch.no_grad():
                    for t in range(val_max_time_steps):
                        
                        lstm_input = []
                        # for every hidden sample, compute the soft attention scores
                        for jdx in range(lstm_hidden.size(0)):
                            x = lstm_hidden[jdx:jdx+1]
                            cls = idx_to_cls[int(target[0])]
                            cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                            cls_mem = cls_mem[:min(cls_mem.shape[0], val_max_mem_slots)]
                            cls_mem = np.squeeze(cls_mem, axis=(2,3))
                            cls_mem = torch.from_numpy(cls_mem).cuda()
                            x = x.repeat(cls_mem.size(0), 1)
                            
                            scores = relation(cls_mem, x)
                            scores = F.softmax(scores, dim=0).view(-1,1)

                            # LSTM input
                            mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                            lstm_input.append(mem_output)

                        lstm_input = torch.cat(lstm_input, dim=0) 
                        
                        # do LSTM 
                        lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))
               
                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            val_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(val_qloader):
                
                input_var = input_[0]
                input_var = input_var.view(input_var.size(0), -1).cuda()
                target_var = target.cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            with torch.no_grad():
                output = relation_model(support_features, query_features)
                loss = criterion(output, expanded_targets)

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, val_meta_batch_size)
        avg_acc1.update(acc1.avg, val_meta_batch_size)
        
        end = time.time()
    
    return avg_losses.avg, avg_acc1.avg

def evaluate_MLIT(test_sloader, test_qloader, st_file_idx, end_file_idx, test_classes, \
                    relation, process, relation_model, criterion, num_features, \
                    max_mem_slots, max_time_steps, output_dir):

    batch_time = AverageMeter()
    avg_acc1 = AverageMeter()

    end = time.time()

    test_sloader.dataset.file_idx = st_file_idx - 1
    for i in tqdm(range(st_file_idx, end_file_idx)):
        #print "test_file :", i

        # next task 
        test_sloader.dataset.next_file()
        test_sloader.dataset.training = True

        idx_to_cls = test_sloader.dataset.idx_to_class
        cls_to_idx = test_sloader.dataset.class_to_idx
        task_class_num = len(cls_to_idx.keys())

        acc1 = AverageMeter()
        cls_acc1 = {cls:AverageMeter() for cls in cls_to_idx.keys()}

        # get the Mem & classes info
        mem = test_sloader.dataset.frame_f_mem

        # feature transformation
        support_features = [[] for _ in range(task_class_num)]
        for idx, (input_, target) in enumerate(test_sloader):
            
            # initial lstm hidden state
            lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
            target_var = target.cuda()

            # inital cell state
            lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

            with torch.no_grad():
                for t in range(max_time_steps):
                    
                    lstm_input = []
                    # for every hidden sample, compute the soft attention scores
                    for jdx in range(lstm_hidden.size(0)):
                        x = lstm_hidden[jdx:jdx+1]
                        cls = idx_to_cls[int(target[0])]
                        cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                        cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                        cls_mem = np.squeeze(cls_mem, axis=(2,3))
                        cls_mem = torch.from_numpy(cls_mem).cuda()
                        x = x.repeat(cls_mem.size(0), 1)
                        
                        scores = relation(cls_mem, x)
                        scores = F.softmax(scores, dim=0).view(-1,1)

                        # LSTM input
                        mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                        lstm_input.append(mem_output)

                    lstm_input = torch.cat(lstm_input, dim=0) 
                    
                    # do LSTM 
                    lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))
           
                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

        for idx in range(task_class_num):
            support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

        support_features = torch.stack(support_features)

        # do query
        test_qloader.dataset.training = False

        query_features = []
        query_targets = []
        for idx, (input_, target) in enumerate(test_qloader):
            
            input_var = input_[0]
            input_var = input_var.view(input_var.size(0), -1).cuda()
            target_var = target.cuda()
            query_features.append(input_var.mean(dim=0, keepdim=True))
            query_targets.append(int(target[0]))

        # expand 
        support_features = support_features.repeat(len(query_features), 1)
        for _, f in enumerate(query_features):
            query_features[_] = f.repeat(task_class_num, 1)

        query_features = torch.cat(query_features)
        
        expanded_targets = []
        for idx in query_targets:
            t = [0.] * task_class_num
            t[idx] = 1.
            expanded_targets = expanded_targets + t

        expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

        with torch.no_grad():
            output = relation_model(support_features, query_features)

        ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
        acc1.update(float(ac1), 1)
        output = np.array(output.cpu())
        for _, idx in enumerate(query_targets):
            ac1 = float(output[_*task_class_num: (_+1)*task_class_num].argmax(axis=0) == idx)
            cls_acc1[idx_to_cls[idx]].update(ac1, 1)

        avg_acc1.update(acc1.avg, len(test_qloader)) 

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        output_file = os.path.join(output_dir, "result{}.txt".format(i))

        with open(output_file, "w") as f:
            print >> f, "inference for {} classes, {} videos".\
                            format(task_class_num, len(test_qloader))
            print >> f, "average top-1 accuracy: {}".format(acc1.avg)
            print >> f, "------------------------------------"
            for idx in range(task_class_num):
                cls = idx_to_cls[idx]
                print >> f, "{}: acc1 {}".format(cls, cls_acc1[cls].avg)

    print("Evaluation End:\n\ttotal_time:{}\n\tavg_acc1:{}\n".\
            format(batch_time.sum, avg_acc1.avg))

def train_MLIT_joint(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                        epoch, meta_batch_size, max_meta_batches, num_features, max_mem_slots, max_time_steps, max_sup_vids=5):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = train_sloader.dataset.frame_f_mem
            idx_to_cls = train_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                for t in range(max_time_steps):
                    
                    lstm_input = []
                    # for every hidden sample, compute the soft attention scores
                    for jdx in range(lstm_hidden.size(0)):
                        x = lstm_hidden[jdx:jdx+1]
                        cls = idx_to_cls[int(target[0])]
                        cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                        cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                        cls_mem = np.squeeze(cls_mem, axis=(2,3))
                        cls_mem = torch.from_numpy(cls_mem).cuda()
                        x = x.repeat(cls_mem.size(0), 1)
                        
                        scores = relation(cls_mem, x)
                        scores = F.softmax(scores, dim=0).view(-1,1)

                        # LSTM input
                        mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                        lstm_input.append(mem_output)

                    lstm_input = torch.cat(lstm_input, dim=0) 
                    
                    # do LSTM 
                    lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))

                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

                if (idx+1) == max_sup_vids:
                    break

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            output = relation_model(support_features, query_features)
            loss = criterion(output, expanded_targets)
            loss.backward()

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)

        for param in relation.parameters():
            param.grad = param.grad / meta_batch_size

        for param in process.parameters():
            param.grad = param.grad / meta_batch_size

        for param in relation_model.parameters():
            param.grad = param.grad / meta_batch_size

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)

def train_MLIT_interchanged(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                            epoch, meta_batch_size, max_meta_batches, num_features, max_mem_slots, max_time_steps, max_sup_vids=5):

    batch_time = AverageMeter()
    avg_losses = AverageMeter()
    avg_acc1 = AverageMeter()

    task_class_num = train_sloader.dataset.task_class_num

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        #print "meta_batch: ", i
            
        losses = AverageMeter()
        acc1 = AverageMeter()
        meta_optimizer.zero_grad()

        for j in range(meta_batch_size):
            #print "\ttask: ", j
                
            # generate a new task
            train_sloader.dataset.reset()
            train_sloader.dataset.training = True
            
            # get the Mem & classes info
            mem = train_sloader.dataset.frame_f_mem
            idx_to_cls = train_sloader.dataset.idx_to_class

            # feature transformation
            support_features = [[] for _ in range(task_class_num)]
            for idx, (input_, target) in enumerate(train_sloader):
                #print "\t\ttrain batch: ", idx
                
                # initial lstm hidden state
                lstm_hidden = input_[0].view(input_[0].size(0), -1).cuda()
                target_var = target.cuda()

                # inital cell state
                lstm_cell = torch.cuda.FloatTensor(lstm_hidden.shape).fill_(0)

                for t in range(max_time_steps):
                    
                    lstm_input = []
                    # for every hidden sample, compute the soft attention scores
                    for jdx in range(lstm_hidden.size(0)):
                        x = lstm_hidden[jdx:jdx+1]
                        cls = idx_to_cls[int(target[0])]
                        cls_mem = mem[cls][np.random.permutation(mem[cls].shape[0])]
                        cls_mem = cls_mem[:min(cls_mem.shape[0], max_mem_slots)]
                        cls_mem = np.squeeze(cls_mem, axis=(2,3))
                        cls_mem = torch.from_numpy(cls_mem).cuda()
                        x = x.repeat(cls_mem.size(0), 1)
                        
                        scores = relation(cls_mem, x)
                        scores = F.softmax(scores, dim=0).view(-1,1)

                        # LSTM input
                        mem_output = (scores * cls_mem).sum(dim=0, keepdim=True)

                        lstm_input.append(mem_output)

                    lstm_input = torch.cat(lstm_input, dim=0) 
                    
                    # do LSTM 
                    lstm_hidden, lstm_cell = process(lstm_input, (lstm_hidden, lstm_cell))

                if (idx+1) == max_sup_vids:
                    break

                support_features[int(target[0])].append(lstm_hidden.mean(dim=0)) 

            for idx in range(task_class_num):
                support_features[idx] = torch.stack(support_features[idx]).mean(dim=0)

            support_features = torch.stack(support_features)

            # do query and get meta-gradient
            train_qloader.dataset.training = False

            query_features = []
            query_targets = []
            for idx, (input_, target) in enumerate(train_qloader):
                #print "\t\tquery batch: ", idx
                
                input_var = input_[0].view(input_[0].size(0), -1).cuda()
                query_features.append(input_var.mean(dim=0, keepdim=True))
                query_targets.append(int(target[0]))

            # expand 
            support_features = support_features.repeat(len(query_features), 1)
            for _, f in enumerate(query_features):
                query_features[_] = f.repeat(task_class_num, 1)

            query_features = torch.cat(query_features)
            
            expanded_targets = []
            for idx in query_targets:
                t = [0.] * task_class_num
                t[idx] = 1.
                expanded_targets = expanded_targets + t

            expanded_targets = torch.tensor(expanded_targets, dtype=torch.float32).cuda()

            output = relation_model(support_features, query_features)
            loss = criterion(output, expanded_targets)
            loss.backward()

            losses.update(float(loss), 1)
            ac1 = accuracy_relation(output.detach().cpu().numpy(), np.array(expanded_targets.cpu()), task_class_num)
            acc1.update(float(ac1), 1)
        
        # when even, update only relation module
        if i % 2 == 0:
            for param in relation.parameters():
                param.grad = None

            for param in process.parameters():
                param.grad = None

            for param in relation_model.parameters():
                param.grad = param.grad / meta_batch_size
        # when odd, update only MLIT module
        else:
            for param in relation.parameters():
                param.grad = param.grad / meta_batch_size

            for param in process.parameters():
                param.grad = param.grad / meta_batch_size

            for param in relation_model.parameters():
                param.grad = None

        meta_optimizer.step()
                
        # measure one-batch time
        batch_time.update(time.time() - end)
        avg_losses.update(losses.avg, meta_batch_size)
        avg_acc1.update(acc1.avg, meta_batch_size)
        
        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_losses.val}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc1', {'val': avg_acc1.val}, count_samples)

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': avg_losses.avg}, count_samples)
    writer.add_scalars('meta_train/accuracy/acc1_epoch', {'avg': avg_acc1.avg}, count_samples)


def main():
    if args.MLIT:
        print "with MLIT"
    else:
        print "without MLIT"

    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    train_dataset = FrameFeatureMemMixtureFeatureTrainDataset(
                        train_classes,
                        args.frame_feature_dir,
                        args.image_feature_dir,
                        args.task_class_num,
                        args.kshot,
                        args.query_kshot,
                        1,
                        1,
                        0,
                        0,
                        image_num_per_support=args.image_num_per_support,
                        support_num_per_cls=args.support_num_per_cls)

    train_dataset.video_wise_train = True

    train_sloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    train_qloader = torch.utils.data.DataLoader(
            train_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_dataset = FrameFeatureMemMixtureFeatureTrainDataset(
            val_classes,
            args.frame_feature_dir,
            args.image_feature_dir,
            args.task_class_num,
            args.val_kshot,
            args.val_query_kshot,
            1,
            1,
            0,
            0,
            image_num_per_support=args.image_num_per_support,
            support_num_per_cls=args.support_num_per_cls)
    val_dataset.video_wise_train = True

    val_sloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    val_qloader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False,
            num_workers=args.workers)

    # MLIT model
    num_features = model_features[args.model]
    relation = RelationNet(num_features).cuda()
    process = nn.LSTMCell(input_size=num_features, hidden_size=num_features, bias=True).cuda()

    # resume from MLIT checkpoint?
    if args.resume_mlit:
        if os.path.isfile(args.resume_mlit):
            print("loading MLIT checkpoint {}".format(args.resume_mlit))
            checkpoint = torch.load(args.resume_mlit)
            relation.load_state_dict(checkpoint['state_dict_relation'])
            process.load_state_dict(checkpoint['state_dict_process'])
            print("loaded MLIT checkpoint {}".format(args.resume_mlit))
            del checkpoint
        else:
            print("no MLIT checkpoint found at {}".format(args.resume_mlit))

    # Relation module
    relation_model = RelationNet(num_features).cuda()

    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)
            relation_model.load_state_dict(checkpoint['state_dict'])
            if checkpoint.has_key("epoch"):
                if args.start_epoch is None:
                    args.start_epoch = checkpoint['epoch']

            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    # loss & optimizer
    criterion = nn.MSELoss().cuda()
    if args.MLIT and args.train_protocol > 0:
        meta_optimizer = torch.optim.SGD(list(relation.parameters()) + list(process.parameters()) + \
                                            list(relation_model.parameters()), args.meta_learning_rate)
    else:
        meta_optimizer = torch.optim.SGD(relation_model.parameters(), args.meta_learning_rate)

    cudnn.enabled = True
    cudnn.benchmark = True

    if args.evaluate:
        print "evaluation on {}".format(args.test_file_dir)

        with open(args.test_class_file, "r") as f:
            test_classes = [l.strip() for l in f.readlines()]

        test_dataset = FrameFeatureMemMixtureFeatureTestDataset(args.test_file_dir,
                                                                args.frame_dir,
                                                                args.frame_feature_dir,
                                                                args.image_feature_dir,
                                                                args.task_class_num,
                                                                args.sample_num,
                                                                args.dup_num,
                                                                1,
                                                                0,
                                                                image_num_per_support=args.image_num_per_support,
                                                                support_num_per_cls=args.support_num_per_cls,
                                                                kshot=args.val_kshot,
                                                                query_kshot=args.val_query_kshot)
        test_dataset.video_wise_train = True

        test_qloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        test_sloader = torch.utils.data.DataLoader(
                test_dataset, batch_size=1, shuffle=False,
                num_workers=args.workers)

        if not args.MLIT:
            print "without MLIT"
            evaluate(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                        relation_model, criterion, num_features, args.output_dir)
        else:
            print "with MLIT"
            evaluate_MLIT(test_sloader, test_qloader, args.st_file_idx, args.end_file_idx, test_classes,\
                            relation, process, relation_model, criterion, num_features, \
                            args.max_mem_slots, args.max_time_steps, args.output_dir)
        
        return


    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        if not args.MLIT:
            train(train_sloader, train_qloader, relation_model, meta_optimizer, criterion, \
                    epoch, args.meta_batch_size, args.max_meta_batches, num_features)
        else:
            if args.train_protocol == 0:
                train_MLIT(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                            epoch, args.meta_batch_size, args.max_meta_batches, num_features, args.max_mem_slots, args.max_time_steps)
            elif args.train_protocol == 1:
                train_MLIT_joint(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                                    epoch, args.meta_batch_size, args.max_meta_batches, num_features, args.max_mem_slots, args.max_time_steps, 5)
            elif args.train_protocol == 2:
                train_MLIT_interchanged(train_sloader, train_qloader, relation, process, relation_model, meta_optimizer, criterion, \
                                        epoch, args.meta_batch_size, args.max_meta_batches, num_features, args.max_mem_slots, args.max_time_steps, 5)
            else:
                raise NotImplementedError


        save_tmp(process, relation, relation_model, meta_optimizer, cp_dir)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            state = {
                "feature": args.model,
                "state_dict_relation": relation.state_dict(),
                "state_dict_process": process.state_dict(),
                "state_dict": relation_model.state_dict(),
                "meta_optimizer": meta_optimizer.state_dict(),
                "epoch": epoch}

            torch.save(state, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            # evaluate on meta validation set
            if not args.MLIT:
                loss, acc1= validate(val_sloader, val_qloader, relation_model, criterion, epoch, args.val_meta_batch_size, \
                                        args.val_max_meta_batches, num_features)
            else:
                loss, acc1= validate_MLIT(val_sloader, val_qloader, relation, process, relation_model, criterion, epoch, \
                                            args.val_meta_batch_size, \
                                            args.val_max_meta_batches, num_features, args.max_mem_slots, args.max_time_steps)


            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1', {'val': acc1}, count_samples)
            #break
        
    writer.close()

if __name__ == "__main__":
    main()
