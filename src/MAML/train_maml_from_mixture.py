""" 
    training maml model from meta-training frame using mixture of web images & video frames
"""
import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

import deparam_models as dpmodels
from maml import MAML

from data_generator import MixtureMetaTrainMBDataset, FrameMetaTrainMBDataset
from utils import *
from tensorboardX import SummaryWriter
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="training maml model from meta-training frame set, \
                                               using mixture of web images & video frames",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="base model architects,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-p', "--pretrain", type=str, default=None, \
                    help="pretrained model path, if 'ImageNet' then use ImageNet pretrained weight")
parser.add_argument('-r', "--resume", type=str, default=None, help="path to checkpoint file")
parser.add_argument('-mb', '--meta_batch_size', default=256, type=int, help="mini-batch size of tasks")
parser.add_argument('-vmb', '--val_meta_batch_size', default=256, type=int, help="val mini-batch size of tasks")
parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size of samples in one task")
parser.add_argument('-qb', '--query_batch_size', default=256, type=int, \
                        help="mini-batch size of samples in one task query set")
parser.add_argument('-vb', '--val_batch_size', default=256, type=int, help="val mini-batch size of samples in one task")
parser.add_argument('-vqb', '--val_query_batch_size', default=256, type=int, \
                        help="val mini-batch size of samples in one task query set")
parser.add_argument('-s', '--size', type=int, default=224, help="input square image edge length")
parser.add_argument('-w', '--workers', type=int, default=1, help="num of data loading workers")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")

parser.add_argument('-mt_lr', '--meta_learning_rate', default=1e-2, type=float, \
                    help="meta learning rate")
parser.add_argument('-tr_lr', '--train_learning_rate', default=1e-2, type=float, \
                    help="learner learning rate")
parser.add_argument('-val_tr_lr', '--val_train_learning_rate', default=1e-2, type=float, \
                    help="val learner learning rate")

parser.add_argument('--start_epoch', default=0, type=int, help="starting epoch number")
parser.add_argument('--epochs', default=10, type=int, help="number of total epochs")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument('--save_freq', default=1, type=int, help="frequency(of epoches) of saving checkpoint info")
parser.add_argument('--val_freq', default=1, type=int, help="frequency(of epoches) of doing validation")

parser.add_argument("--image_dir", type=str, required=True, help="path to root dir of web image data")
parser.add_argument("--frame_dir", type=str, required=True, help="path to root dir of video frame data")

parser.add_argument("--train_class_file", type=str, required=True, help="path to file containing meta training classes")
parser.add_argument("--val_class_file", type=str, default=None, help="path to file containing meta validation classes")

parser.add_argument("--train_image_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                                 of training web image data")
parser.add_argument("--train_frame_mean_std_file", type=str, default=None, help="path to file containing mean & std statistics \
                                                                                 of training video frame data")

parser.add_argument("--task_class_num", type=int, default=48, help="class num of every task")

parser.add_argument("--kshot", type=int, default=1, help="k-shot(of vids) of support set")
parser.add_argument("--query_kshot", type=int, default=1, help="k-shot(of vids) of query set")

parser.add_argument("--max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--query_max_steps", type=int, default=10, help="max eval steps in query")
parser.add_argument("--max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--val_max_updates", type=int, default=10, help="max gradient descent step in one task")
parser.add_argument("--val_query_max_steps", type=int, default=10, help="max eval steps in query")
parser.add_argument("--val_max_meta_batches", type=int, default=100, help="max meta-batches per epoch")

parser.add_argument("--first_order_approx", type=bool, default=False, help="whether use first order approximation")

parser.add_argument('--log_dir', type=str, required=True, help="log dir of tensorboardX")
parser.add_argument("--cp_dir", type=str, required=True, help="checkpoint dir")
parser.add_argument("--cg_dir", type=str, required=True, help="training config recording dir")

args=parser.parse_args()

def save_tmp(param):
    save_path = os.path.join(args.cp_dir, "tem.pth.tar")
    torch.save(param, save_path)
    print("saved at {}".format(save_path))

def train(train_sloader, train_qloader, model, epoch, meta_batch_size, max_meta_batches, first_order_approx=True):
    batch_time = AverageMeter()
    losses = AverageMeter()

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        avg_loss, avg_acc1, avg_acc5 = model(train_sloader, train_qloader, meta_batch_size, first_order_approx)

        # measure one-batch time
        batch_time.update(time.time() - end)
        
        losses.update(avg_loss, meta_batch_size)

        if (i+1) % args.print_freq == 0:
            # batches record
            count_samples = (epoch*max_meta_batches+i+1)*meta_batch_size
            writer.add_scalars('meta_train/time/batch_time', {'val': batch_time.val}, count_samples)
            writer.add_scalars('meta_train/loss/loss_batch', {'val': avg_loss}, count_samples)
            max_updates = train_sloader.dataset.max_updates
            writer.add_scalars('meta_train/accuracy/acc1/step_{}'.format(max_updates), {'val': float(avg_acc1)}, count_samples)
            writer.add_scalars('meta_train/accuracy/acc5/step_{}'.format(max_updates), {'val': float(avg_acc5)}, count_samples)
            """
            for _ in range(train_sloader.dataset.max_updates + 1):
                writer.add_scalars('meta_train/accuracy/acc1/step_{}'.format(_), {'val': float(avg_acc1[_])}, count_samples)
                writer.add_scalars('meta_train/accuracy/acc5/step_{}'.format(_), {'val': float(avg_acc5[_])}, count_samples)
            """

        end = time.time()

    # epoch record
    count_samples = (epoch+1)*max_meta_batches*meta_batch_size
    writer.add_scalars('meta_train/loss/loss_epoch', {'avg': losses.avg}, count_samples)

def validate(val_sloader, val_qloader, model, epoch, meta_batch_size, max_meta_batches):
    losses = AverageMeter()
    acc1 = 0.
    acc5 = 0.

    end = time.time()
    for i in tqdm(range(max_meta_batches)):
        avg_loss, avg_acc1, avg_acc5 = model(val_sloader, val_qloader, meta_batch_size, True, False)
        losses.update(avg_loss, meta_batch_size)
        #acc1 = acc1 + np.array(avg_acc1, dtype=np.float32)
        #acc5 = acc5 + np.array(avg_acc5, dtype=np.float32)
        acc1 = acc1 + avg_acc1
        acc5 = acc5 + avg_acc5

    """
    acc1 = acc1 / float(max_meta_batches)
    acc5 = acc5 / float(max_meta_batches)
    """

    #return losses.avg, [float(_) for _ in acc1], [float(_) for _ in acc5]
    return losses.avg, acc1 / float(max_meta_batches), acc5 / float(max_meta_batches)

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    with open(args.train_class_file, "r") as f:
        train_classes = [l.strip() for l in f.readlines()]

    with open(args.val_class_file, "r") as f:
        val_classes = [l.strip() for l in f.readlines()]

    tr_class_num = len(train_classes)
    val_class_num = len(val_classes)

    print "train class num: ", tr_class_num
    print "val class num: ", val_class_num

    # load mean and std
    img_mean, img_std = tuple(np.loadtxt(args.train_image_mean_std_file))
    img_mean = [float(a) for a in list(img_mean)]
    img_std = [float(b) for b in list(img_std)]
    print "img_mean: ", img_mean
    print "img_std: ", img_std

    frm_mean, frm_std = tuple(np.loadtxt(args.train_frame_mean_std_file))
    frm_mean = [float(a) for a in list(frm_mean)]
    frm_std = [float(b) for b in list(frm_std)]
    print "frm_mean: ", frm_mean
    print "frm_std: ", frm_std

    train_sdataset = MixtureMetaTrainMBDataset(
            train_classes,
            os.path.join(args.image_dir, "train"),
            os.path.join(args.frame_dir, "train"),
            args.task_class_num,
            args.kshot,
            args.batch_size,
            args.max_updates,
            img_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=img_mean, 
                                     std=img_std)
                ]),
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=frm_mean, 
                                     std=frm_std)
                ]))
    train_sloader = torch.utils.data.DataLoader(
            train_sdataset, batch_size=args.batch_size, shuffle=True,
            num_workers=args.workers)

    train_qdataset = FrameMetaTrainMBDataset(
            train_classes,
            os.path.join(args.frame_dir, "val"),
            args.task_class_num,
            args.query_kshot,
            args.query_batch_size,
            args.query_max_steps,
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(args.size),
                transforms.ToTensor(),
                transforms.Normalize(mean=frm_mean, 
                                     std=frm_std)
                ]))
    train_qloader = torch.utils.data.DataLoader(
            train_qdataset, batch_size=args.query_batch_size, shuffle=True,
            num_workers=args.workers)

    val_sdataset = MixtureMetaTrainMBDataset(
            val_classes,
            os.path.join(args.image_dir, "train"),
            os.path.join(args.frame_dir, "train"),
            args.task_class_num,
            args.kshot,
            args.val_batch_size,
            args.val_max_updates,
            img_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=img_mean, 
                                     std=img_std)
                ]),
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(args.size),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=frm_mean, 
                                     std=frm_std)
                ]))
    val_sloader = torch.utils.data.DataLoader(
            val_sdataset, batch_size=args.val_batch_size, shuffle=True,
            num_workers=args.workers)

    val_qdataset = FrameMetaTrainMBDataset(
            val_classes,
            os.path.join(args.frame_dir, "val"),
            args.task_class_num,
            args.query_kshot,
            args.val_query_batch_size,
            args.val_query_max_steps,
            frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(args.size),
                transforms.ToTensor(),
                transforms.Normalize(mean=frm_mean, 
                                     std=frm_std)
                ]))
    val_qloader = torch.utils.data.DataLoader(
            val_qdataset, batch_size=args.val_query_batch_size, shuffle=True,
            num_workers=args.workers)

    # create meta-param
    if args.resume is not None:
        print "load from checkpoint ", args.resume
        meta_params = torch.load(args.resume)
    else:
        meta_params = dpmodels.construct_parameters(args.model, num_classes=args.task_class_num,\
                        pretrain=args.pretrain, cuda=True)
    
    # create MAML meta-learner
    criterion = nn.CrossEntropyLoss().cuda()
    model = MAML(dpmodels.__dict__[args.model], meta_params, criterion, args.max_updates, \
                    args.meta_learning_rate, args.train_learning_rate, num_classes=args.task_class_num)

    # TODO: data parallel?
    model = model.cuda()

    # loss func and optimizer
    cudnn.enabled = True
    cudnn.benchmark = True

    # tensorboard log dir
    log_dir = args.log_dir
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    global writer
    writer = SummaryWriter(log_dir=log_dir)

    # checkpoint save dir
    cp_dir = args.cp_dir
    if not os.path.isdir(cp_dir):
        os.mkdir(cp_dir)

    # training config dir
    cg_dir = args.cg_dir
    if not os.path.isdir(cg_dir):
        os.mkdir(cg_dir)

    with open(os.path.join(cg_dir, "config.txt"), "a") as f:
        print >> f, datetime.now()
        print >> f, args
        print >> f
        print >> f

    # do training
    for epoch in range(args.start_epoch, args.epochs):
        #if epoch > args.start_epoch:
        # train for one epoch
        train(train_sloader, train_qloader, model, epoch, args.meta_batch_size, args.max_meta_batches, args.first_order_approx)
        save_tmp(meta_params)
        
        # save checkpoint
        if (epoch + 1) % args.save_freq == 0:
            torch.save(meta_params, os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch+1)))

        if (epoch + 1) % args.val_freq == 0:
            print "Validation..."
            val_model = MAML(dpmodels.__dict__[args.model], meta_params, criterion, args.val_max_updates, \
                             0., args.val_train_learning_rate, num_classes=args.task_class_num)

            val_model = val_model.cuda()
            # evaluate on meta validation set
            loss, acc1, acc5 = validate(val_sloader, val_qloader, model, epoch, args.val_meta_batch_size, args.val_max_meta_batches)

            count_samples = (epoch + 1) * args.meta_batch_size * args.max_meta_batches
            writer.add_scalars('meta_val/loss/loss_epoch', {'val': loss}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc1/step_{}'.format(args.val_max_updates), {'val': float(acc1)}, count_samples)
            writer.add_scalars('meta_val/accuracy/acc5/step_{}'.format(args.val_max_updates), {'val': float(acc5)}, count_samples)
            """
            for _ in range(args.val_max_updates + 1):
                writer.add_scalars('meta_val/accuracy/acc1/step_{}'.format(_), {'val': float(acc1[_])}, count_samples)
                writer.add_scalars('meta_val/accuracy/acc5/step_{}'.format(_), {'val': float(acc5[_])}, count_samples)
            """

            val_model = 0
        
    writer.close()

if __name__ == "__main__":
    main()
