import torch
from torch import nn
from torch import optim
from torch.nn import functional as F
import numpy as np
from utils import *

class MAML(nn.Module):
    # learner_net: class prototype, not an instance
    # in order to create a new instance before every new task
    # this is used to reinitialize all runnning buffer cross different tasks
    def __init__(self, learner_net, meta_parameters, criterion, K, meta_lr, train_lr, **kwargs):
        super(MAML, self).__init__()

        self.train_lr = train_lr
        self.meta_lr = meta_lr
        self.K = K
        self.kwargs = kwargs

        self.net = learner_net
        self.meta_param = meta_parameters
        self.criterion = criterion

        # remove all None otherwise error
        tem = []
        for p in self.meta_param:
            if p is not None:
                tem.append(p)

        self.meta_optim = optim.SGD(tem, lr= self.meta_lr)

    def zero_grad(self, params):
        for p in params:
            if p is not None:
                if p.grad is not None:
                    p.grad.zero_()

    # SGD for fast weights update
    def sgd(self, param, grad, lr):
        new_param = []
        idx = 0
        for i in range(len(param)):
            if param[i] is None:
                new_param.append(None)
            else:
                new_param.append(param[i] - lr * grad[idx])
                idx += 1
        return new_param

    def forward(self, support_loader, query_loader, meta_batch_size, first_order_approx=True, Training=True):
        # record loss(final step) for different tasks
        losses_q = [] 

        # record gradients of meta_params
        # use this to prevent high memory consumption
        meta_grads = 0.

        # record average accuracy of different update steps,
        # average over all batchsz tasks
        """
        avg_acc1 = [AverageMeter() for _ in range(self.K + 1)]
        avg_acc5 = [AverageMeter() for _ in range(self.K + 1)]
        """
        last_acc1 = AverageMeter()
        last_acc5 = AverageMeter()

        for i in range(meta_batch_size): 
            print "task ", i

            # 0. create a new net instance
            net = self.net(**self.kwargs).cuda()
            #print "net prepared"

            # 1. generate a new task
            support_loader.dataset.reset()
            query_loader.dataset.set(support_loader.dataset.class_to_idx)

            #print "task generated"

            """
            # accuracy before first update
            net.eval()
            with torch.no_grad():
                for idx, (input_, target) in enumerate(query_loader):
                    print "query ", idx
                    input_var = input_.cuda()
                    target_var = target.cuda()
                    pred = net(input_var, self.meta_param)
                    ac1, ac5 = accuracy(pred.data, target_var, topk=(1,5))
                    avg_acc1[0].update(float(ac1[0].data), input_.size(0))
                    avg_acc5[0].update(float(ac5[0].data), input_.size(0))
            """

            self.zero_grad(self.meta_param)
            #print "zero graded"
            # 2. train the i-th task
            for idx, (input_, target) in enumerate(support_loader):
                print "\t step ", idx

                net.train()
                input_var = input_.cuda()
                target_var = target.cuda()

                if idx == 0:
                    pred = net(input_var, self.meta_param)
                    loss = self.criterion(pred, target_var)
                    tem = []
                    for p in self.meta_param:
                        if p is not None:
                            tem.append(p)

                    grad = list(torch.autograd.grad(loss, tem, create_graph=((not first_order_approx) and Training)))
                    fast_weights = self.sgd(self.meta_param, grad, self.train_lr)
                else:

                    pred = net(input_var, fast_weights)
                    loss = self.criterion(pred, target_var)
                    tem = []
                    for p in fast_weights:
                        if p is not None:
                            tem.append(p)

                    grad = list(torch.autograd.grad(loss, tem, create_graph=((not first_order_approx) and Training)))
                    fast_weights = self.sgd(fast_weights, grad, self.train_lr)

                """
                # accuracy after the update
                net.eval()
                with torch.no_grad():
                    for jdx, (input_, target) in enumerate(query_loader):
                        input_var = input_.cuda()
                        target_var = target.cuda()
                        pred = net(input_var, fast_weights)
                        ac1, ac5 = accuracy(pred.data, target_var, topk=(1,5))
                        avg_acc1[idx+1].update(float(ac1[0].data), input_.size(0))
                        avg_acc5[idx+1].update(float(ac5[0].data), input_.size(0))
                """

            # 4. compute loss and record
            tem = []
            for p in self.meta_param:
                if p is not None:
                    tem.append(p)

            net.eval()
            losses = []
            if Training:
                for jdx, (input_, target) in enumerate(query_loader):
                    print "\t\t query ", jdx
                    input_var = input_.cuda()
                    target_var = target.cuda()
                    pred = net(input_var, fast_weights)
                    loss = self.criterion(pred, target_var)

                    if meta_grads == 0:
                        meta_grads = list(torch.autograd.grad(loss, tem, retain_graph=(not first_order_approx) and \
                                                                                     (not jdx == len(query_loader)-1)))
                    else:
                        tem_grads = list(torch.autograd.grad(loss, tem, retain_graph=(not first_order_approx) and \
                                                                                     (not jdx == len(query_loader)-1)))
                        for _ in range(len(meta_grads)):
                            meta_grads[_] = meta_grads[_] + tem_grads[_]

                    losses.append(float(loss.data))

                    ac1, ac5 = accuracy(pred.data, target_var, topk=(1,5))
                    last_acc1.update(float(ac1[0].data), input_.size(0))
                    last_acc5.update(float(ac5[0].data), input_.size(0))
            else:
                for p in fast_weights:
                    if p is not None:
                        p.detach_()

                with torch.no_grad():
                    for jdx, (input_, target) in enumerate(query_loader):
                        #print "\t\t query ", jdx
                        input_var = input_.cuda()
                        target_var = target.cuda()
                        pred = net(input_var, fast_weights)
                        loss = self.criterion(pred, target_var)
                        losses.append(float(loss.data))

                        ac1, ac5 = accuracy(pred.data, target_var, topk=(1,5))
                        last_acc1.update(float(ac1[0].data), input_.size(0))
                        last_acc5.update(float(ac5[0].data), input_.size(0))

            loss = np.array(losses, dtype=np.float32).mean()
            losses_q.append(loss)

        # end of all tasks
        # sum over all losses across all tasks
        loss_q = np.array(losses_q, dtype=np.float32).mean()
        
        if Training:
            # optimize theta parameters
            self.meta_optim.zero_grad()
            meta_grads = [g/(len(query_loader) * meta_batch_size) for g in meta_grads]
            kdx = 0
            for param in self.meta_param:
                if param is None:
                    pass
                else:
                    param.grad = meta_grads[kdx]
                    kdx += 1

            #print self.meta_param[-1].grad
            self.meta_optim.step()

        """
        acc1 = [ac1.avg for ac1 in avg_acc1]
        acc5 = [ac5.avg for ac5 in avg_acc5]
        """

        #return float(loss_q), acc1, acc5
        return float(loss_q), last_acc1.avg, last_acc5.avg

    # TODO: mini-batch?
    def old_forward(self, support_x, support_y, query_x, query_y, first_order_approx=True):
        """
        :param support_x:   [b, setsz, c_, h, w]
        :param support_y:   [b, setsz]
        :param query_x:     [b, querysz, c_, h, w]
        :param query_y:     [b, querysz]
        """
        batchsz, setsz, c_, h, w = support_x.size()
        querysz = query_x.size(1)

        # record loss(final step) for different tasks
        losses_q = [] 

        # record average accuracy of different update steps,
        # average over all batchsz tasks
        avg_acc1 = [0.] * (self.K + 1)
        avg_acc5 = [0.] * (self.K + 1)


        for i in range(batchsz): 

            # 0. create a new net instance
            net = self.net(**self.kwargs).cuda()

            # accuracy before first update
            net.eval()
            pred_q = net(query_x[i], self.meta_param)
            # scalar
            ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
            avg_acc1[0] = avg_acc1[0] + ac1
            avg_acc5[0] = avg_acc5[0] + ac5

            # 1. run the i-th task and compute loss for k=0
            net.train()
            pred = net(support_x[i], self.meta_param)
            loss = self.criterion(pred, support_y[i])

            # 2. grad on theta
            # clear theta grad info
            self.zero_grad(self.meta_param)

            # remove all None otherwise error
            tem = []
            for p in self.meta_param:
                if p is not None:
                    tem.append(p)

            grad = list(torch.autograd.grad(loss, tem, create_graph=not first_order_approx))

            # 3. theta_pi = theta - train_lr * grad
            fast_weights = self.sgd(self.meta_param, grad, self.train_lr)

            # accuracy after the first update
            net.eval()
            pred_q = net(query_x[i], fast_weights)
            # scalar
            ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
            avg_acc1[1] = avg_acc1[1] + ac1
            avg_acc5[1] = avg_acc5[1] + ac5

            # repeat for remaining K times
            for k in range(1, self.K):

                net.train()
                # 1. run the i-th task and compute loss for k=1~K-1
                pred = net(support_x[i], fast_weights)
                loss = self.criterion(pred, support_y[i])

                # clear fast_weights grad info
                self.zero_grad(fast_weights)

                # 2. compute grad on theta_pi
                tem = []
                for p in fast_weights:
                    if p is not None:
                        tem.append(p)

                grad = list(torch.autograd.grad(loss, tem, create_graph=not first_order_approx))

                # 3. theta_pi = theta - train_lr * grad
                fast_weights = self.sgd(fast_weights, grad, self.train_lr)

                net.eval()
                pred_q = net(query_x[i], fast_weights)
                # scalar
                ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
                avg_acc1[k+1] = avg_acc1[k+1] + ac1
                avg_acc5[k+1] = avg_acc5[k+1] + ac5

            # 4. record last step's loss for task i
            loss_q = self.criterion(pred_q, query_y[i])
            losses_q.append(loss_q)

        # end of all tasks
        # sum over all losses across all tasks
        loss_q = torch.stack(losses_q).mean(0)
        
        # optimize theta parameters
        self.meta_optim.zero_grad()
        loss_q.backward()
        self.meta_optim.step()

        avg_acc1 = np.array(avg_acc1) / batchsz
        avg_acc5 = np.array(avg_acc5) / batchsz

        return loss_q.data, avg_acc1, avg_acc5

    def old_evaluate(self, support_x, support_y, query_x, query_y):
        """
        :param support_x:   [b, setsz, c_, h, w]
        :param support_y:   [b, setsz]
        :param query_x:     [b, querysz, c_, h, w]
        :param query_y:     [b, querysz]
        """
        batchsz, setsz, c_, h, w = support_x.size()
        querysz = query_x.size(1)

        # record average accuracy of different update steps,
        # average over all batchsz tasks
        avg_acc1 = [0.] * (self.K + 1)
        avg_acc5 = [0.] * (self.K + 1)

        # record loss(final step) for different tasks
        losses_q = [] 

        for i in range(batchsz): 

            # 0. create a new net instance
            net = self.net(**self.kwargs).cuda()

            # accuracy before first update
            net.eval()
            pred_q = net(query_x[i], self.meta_param)
            # scalar
            ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
            avg_acc1[0] = avg_acc1[0] + ac1
            avg_acc5[0] = avg_acc5[0] + ac5

            # 1. run the i-th task and compute loss for k=0
            net.train()
            pred = net(support_x[i], self.meta_param)
            loss = self.criterion(pred, support_y[i])

            # 2. grad on theta
            # clear theta grad info
            self.zero_grad(self.meta_param)

            # remove all None otherwise error
            tem = []
            for p in self.meta_param:
                if p is not None:
                    tem.append(p)

            grad = list(torch.autograd.grad(loss, tem))

            # 3. theta_pi = theta - train_lr * grad
            fast_weights = self.sgd(self.meta_param, grad, self.train_lr)

            # accuracy after the first update
            net.eval()
            pred_q = net(query_x[i], fast_weights)
            # scalar
            ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
            avg_acc1[1] = avg_acc1[1] + ac1
            avg_acc5[1] = avg_acc5[1] + ac5

            # repeat for remaining K times
            for k in range(1, self.K):

                net.train()
                # 1. run the i-th task and compute loss for k=1~K-1
                pred = net(support_x[i], fast_weights)
                loss = self.criterion(pred, support_y[i])

                # clear fast_weights grad info
                self.zero_grad(fast_weights)

                # 2. compute grad on theta_pi
                tem = []
                for p in fast_weights:
                    if p is not None:
                        tem.append(p)

                grad = list(torch.autograd.grad(loss, tem))

                # 3. theta_pi = theta - train_lr * grad
                fast_weights = self.sgd(fast_weights, grad, self.train_lr)

                net.eval()
                pred_q = net(query_x[i], fast_weights)
                # scalar
                ac1, ac5 = accuracy(pred_q.data, query_y[i], topk=(1,5))
                avg_acc1[k+1] = avg_acc1[k+1] + ac1
                avg_acc5[k+1] = avg_acc5[k+1] + ac5

            # 4. record last step's loss for task i
            loss_q = self.criterion(pred_q, query_y[i])
            losses_q.append(loss_q)

        # end of all tasks
        # sum over all losses across all tasks
        loss_q = torch.stack(losses_q).mean(0)

        avg_acc1 = np.array(avg_acc1) / batchsz
        avg_acc5 = np.array(avg_acc5) / batchsz

        return loss_q.data, avg_acc1, avg_acc5


if __name__ == '__main__':
    import os
    import deparam_models as dpmodels
    from data_generator import FrameMetaTrainMBDataset
    import torchvision.transforms as transforms
    from torch.utils.data import DataLoader
    
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

    net = dpmodels.resnet50
    kwargs = {"num_classes": 48}
    meta_params = dpmodels.construct_parameters("resnet50", num_classes=48, pretrain="ImageNet", cuda=True)

    maml = MAML(net, meta_params, nn.CrossEntropyLoss().cuda(), 10, 0.01, 0.001, **kwargs)
    maml.cuda()

    with open("/scratch/xz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/k/xz/transfer_learning/data/actNet_frames"
    sdataset = FrameMetaTrainMBDataset(classes, os.path.join(frame_dir, "train"), 48, 1, 48, 10, 
                    frm_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ])) 
    sdataloader = DataLoader(sdataset, batch_size=48, shuffle=False, num_workers=8)

    qdataset = FrameMetaTrainMBDataset(classes, os.path.join(frame_dir, "val"), 48, 1, 48, 10, 
                    frm_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ])) 
    qdataloader = DataLoader(qdataset, batch_size=48, shuffle=False, num_workers=8)

    loss, acc1, acc5 = maml(sdataloader, qdataloader, 2)
    print loss
    print acc1
    print acc5

