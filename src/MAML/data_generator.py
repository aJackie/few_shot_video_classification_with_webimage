import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from torch.utils.data import Dataset, DataLoader
from utils import *
from random import shuffle, randint

class FrameMetaTrainMBDataset(Dataset):
    def __init__(self, all_classes, frame_dir, task_class_num, kshot,
                 batch_size, max_updates, frm_transform=None):

        super(FrameMetaTrainMBDataset, self).__init__()
        self.classes = all_classes
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.frm_transform = frm_transform

        self.videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, cls)
            self.videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
        self.reset()

    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label) , frame_paths)
        shuffle(self.frames)

        """
        print "task {} classes".format(len(self.class_to_idx.keys()))
        print "task {} videos".format(len(self.task_vids))
        print "task {} frames".format(len(self.frames))
        """

    def set(self, class_to_idx):

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = []
        for cls, idx in class_to_idx.items():
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label) , frame_paths)
        shuffle(self.frames)

        """
        print "task {} classes".format(len(self.class_to_idx.keys()))
        print "task {} videos".format(len(self.task_vids))
        print "task {} frames".format(len(self.frames))
        """
        
    def __len__(self):
        return min(len(self.frames), self.max_updates * self.batch_size)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        try:
            sample = pil_loader(path)
        except Exception as e:
            print >> sys.stderr, e, path
            return self.__getitem__((idx + randint(1, len(self.frames) - 1)) % len(self.frames))

        if self.frm_transform is not None:
            sample = self.frm_transform(sample)

        return sample, target

class ImageMetaTrainMBDataset(Dataset):
    def __init__(self, all_classes, image_dir, task_class_num,
                 batch_size, max_updates, img_transform=None):

        super(ImageMetaTrainMBDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.task_class_num = task_class_num
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.img_transform = img_transform

        self.images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, cls)
            self.images[cls] = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way".format(self.task_class_num)
        
        self.reset()

    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_imgs = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            self.task_imgs = self.task_imgs + map(lambda i: (i, idx) ,self.images[cls])

        shuffle(self.task_imgs)

    def set(self, class_to_idx):

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_imgs = []

        for cls, idx in class_to_idx.items():

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.task_imgs = self.task_imgs + map(lambda i: (i, idx) ,self.images[cls])

        shuffle(self.task_imgs)

    def __len__(self):
        return min(len(self.task_imgs), self.max_updates * self.batch_size)

    def __getitem__(self, idx):
        path, target = self.task_imgs[idx]
        try:
            sample = pil_loader(path)
        except Exception as e:
            print >> sys.stderr, e, path
            return self.__getitem__((idx + randint(1, len(self.task_imgs) - 1)) % len(self.task_imgs))

        if self.img_transform is not None:
            sample = self.img_transform(sample)

        return sample, target

class MixtureMetaTrainMBDataset(Dataset):
    def __init__(self, all_classes, image_dir, frame_dir, task_class_num, kshot,
                 batch_size, max_updates, img_transform=None, frm_transform=None):

        super(MixtureMetaTrainMBDataset, self).__init__()
        self.classes = all_classes
        self.image_dir = image_dir
        self.frame_dir = frame_dir
        self.task_class_num = task_class_num
        self.kshot = kshot
        self.batch_size = batch_size
        self.max_updates = max_updates
        self.img_transform = img_transform
        self.frm_transform = frm_transform

        self.videos = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.frame_dir, cls)
            self.videos[cls] = [os.path.join(cls_dir, v) for v in os.listdir(cls_dir)]

        self.images = {}
        for cls in self.classes:
            cls_dir = os.path.join(self.image_dir, cls)
            self.images[cls] = [os.path.join(cls_dir, i) for i in os.listdir(cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.task_class_num, self.kshot)
        
        self.reset()

    def reset(self):
        # random select support set
        perm = np.random.permutation(len(self.classes))
        #shuffle(self.classes)

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = []
        self.task_imgs = []
        self.task_samples = []

        for idx, id_ in enumerate(perm[:self.task_class_num]): 
            cls = self.classes[id_]
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

            self.task_imgs = self.task_imgs + map(lambda i: (i, idx, "i") ,self.images[cls])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label, "f") , frame_paths)

        self.task_samples = self.task_imgs + self.frames
        shuffle(self.task_samples)

    def set(self, class_to_idx):

        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.task_vids = []
        self.task_imgs = []
        self.task_samples = []

        for cls, idx in class_to_idx.items():
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            vid_perm = np.random.permutation(len(self.videos[cls]))
            #shuffle(self.videos[cls])

            if self.kshot == -1:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm)
            else:
                self.task_vids = self.task_vids + map(lambda jdx: (self.videos[cls][jdx], idx), vid_perm[:self.kshot])

            self.task_imgs = self.task_imgs + map(lambda i: (i, idx, "i") ,self.images[cls])

        self.frames = []
        for (vid_dir, label) in self.task_vids:
            frame_paths = os.listdir(vid_dir)
            self.frames = self.frames + map(lambda f: (os.path.join(vid_dir, f), label, "f") , frame_paths)

        self.task_samples = self.task_imgs + self.frames
        shuffle(self.task_samples)

    def __len__(self):
        return min(len(self.task_samples), self.max_updates * self.batch_size)

    def __getitem__(self, idx):
        path, target, flag = self.task_samples[idx]
        try:
            sample = pil_loader(path)
        except Exception as e:
            print >> sys.stderr, e, path
            return self.__getitem__((idx + randint(1, len(self.task_samples) - 1)) % len(self.task_samples))

        if flag == "f":
            if self.frm_transform is not None:
                sample = self.frm_transform(sample)
        elif flag == "i":
            if self.img_transform is not None:
                sample = self.img_transform(sample)

        return sample, target




class FrameMetaTrainDataset(Dataset):
    def __init__(self, meta_tr_classes, frame_dir, meta_test_class_num, kshot, query_kshot,
                 frm_per_cls=10, query_frm_per_cls=10,  max_tasks=1000, frm_transform=None):

        super(FrameMetaTrainDataset, self).__init__()
        self.classes = meta_tr_classes
        self.frame_dir = frame_dir
        self.test_class_num = meta_test_class_num
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.frm_per_cls = frm_per_cls
        self.query_frm_per_cls = query_frm_per_cls
        self.max_tasks = max_tasks
        self.frm_transform = frm_transform

        self.train_vids = {}
        self.test_vids = {}

        for cls in self.classes:
            tr_cls_dir = os.path.join(self.frame_dir, "train", cls)
            self.train_vids[cls] = [os.path.join(tr_cls_dir, v) for v in os.listdir(tr_cls_dir)]

            test_cls_dir = os.path.join(self.frame_dir, "val", cls)
            self.test_vids[cls] = [os.path.join(test_cls_dir, v) for v in os.listdir(test_cls_dir)]

        print "total {} classes".format(len(self.classes))
        print "{}-way {}-shot".format(self.test_class_num, self.kshot)

    def __len__(self):
        return self.max_tasks

    def __getitem__(self, idx):
        support_vids = []
        query_vids = []

        # random select test_class_num classes
        shuffle(self.classes)
        
        for idx in range(self.test_class_num):

            cls = self.classes[idx]
            tr_cls_vids = self.train_vids[cls]
            test_cls_vids = self.test_vids[cls]
            
            # random select k-shot videos
            shuffle(tr_cls_vids)
            support_vids.append(tr_cls_vids[:self.kshot])

            shuffle(test_cls_vids)
            query_vids.append(test_cls_vids[:self.query_kshot])
            
        support_frms = []
        query_frms = []
    
        # random select frames
        for idx in range(self.test_class_num):
            
            support_cls_vids = support_vids[idx]
            query_cls_vids = query_vids[idx]

            support_cls_frms = []
            for vid in support_cls_vids:
                frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
                support_cls_frms = support_cls_frms + frms
            
            shuffle(support_cls_frms)
            support_frms.append(support_cls_frms[:self.frm_per_cls])

            max_len = len(support_cls_frms)
            if max_len < self.frm_per_cls:
                for _ in range(self.frm_per_cls - max_len):
                    support_frms[-1].append(support_cls_frms[randint(0, max_len-1)])

            query_cls_frms = []
            for vid in query_cls_vids:
                frms = [os.path.join(vid, _) for _ in os.listdir(vid)]
                query_cls_frms = query_cls_frms + frms
            
            shuffle(query_cls_frms)
            query_frms.append(query_cls_frms[:self.query_frm_per_cls])

            max_len = len(query_cls_frms)
            if max_len < self.query_frm_per_cls:
                for _ in range(self.query_frm_per_cls - max_len):
                    query_frms[-1].append(query_cls_frms[randint(0, max_len-1)])

        # form tensor
        support_x = []
        support_y = []
        query_x = []
        query_y = []

        for idx in range(self.test_class_num):

            support_cls_frms = support_frms[idx]
            for frm in support_cls_frms:
                frame = pil_loader(frm)
                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)
                support_x.append(frame)
                support_y.append(idx)
            
            query_cls_frms = query_frms[idx]
            for frm in query_cls_frms:
                frame = pil_loader(frm)
                if self.frm_transform is not None:
                    frame = self.frm_transform(frame)
                query_x.append(frame)
                query_y.append(idx)

        return torch.stack(support_x), np.array(support_y, dtype=np.long), \
                torch.stack(query_x), np.array(query_y, dtype=np.long)

class FrameMetaValFolder(Dataset):
    # classes can be subset of shuffled outside
    def __init__(self, meta_val_classes, frame_dir, kshot, frm_transform=None):
        super(FrameMetaValFolder, self).__init__()
        self.meta_val_classes = meta_val_classes
        self.frame_dir = self.frame_dir
        self.kshot = kshot
        self.query_kshot = query_kshot
        self.frame_transform = frm_transform

        self.all_videos = []

        for cls in self.meta_val_classes:
            cls_dir = os.path.join(frame_dir, cls)
            self.all_videos.append([os.path.join(cls_dir, v) for v in os.listdir(cls_dir)])
        
        # choose videos
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for idx, cls in enumerate(self.meta_val_classes): 
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            shuffle(self.all_videos[idx])
            if self.kshot == -1:
                self.videos = self.videos + map(lambda v: (v, idx), self.all_videos[idx])
            else:
                self.videos = self.videos + map(lambda v: (v, idx), self.all_videos[idx][:self.kshot])

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class ImageListFolder(Dataset):
    """
        similar to torchvision datasets.ImageFolder
        but use class_lst to indicate valid classes
    """
    def __init__(self, image_dir, class_lst, img_transform=None):
        super(ImageListFolder, self).__init__()
        self.image_dir = image_dir
        self.classes = class_lst
        self.img_transform = img_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), idx), paths)

        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        path, target = self.imgs[idx]
        sample = pil_loader(path)
        if self.img_transform is not None:
            sample = self.img_transform(sample)

        return sample, target

class FrameListFileFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
    """
    def __init__(self, path_file, frame_transform=None):
        super(FrameListFileFolder, self).__init__()
        self.path_file = path_file
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]

            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), label))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        frame = pil_loader(path)
        if self.frame_transform is not None:
            frame = self.frame_transform(frame)

        return frame, target

class FrameListFileTestFolder(Dataset):
    """
        take path list file as input
        the file contains frame dir paths and labels
        NOTE: this is used for evaluation
    """
    def __init__(self, path_file, sample_num, dup_num, frame_transform=None):
        super(FrameListFileTestFolder, self).__init__()
        self.path_file = path_file
        self.sample_num = sample_num
        self.dup_num = dup_num
        self.frame_transform = frame_transform

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
            
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls
            self.videos.append((path, idx))

        print "total {} classes".format(len(self.class_to_idx.keys()))
        print "total {} videos".format(len(self.videos))

    def __len__(self):
        return len(self.videos)

    def __getitem__(self, idx):
        vid_dir, target = self.videos[idx]
        frames = np.array([os.path.join(vid_dir, f) for f in os.listdir(vid_dir)])
        if self.sample_num == -1:
            select = np.concatenate([frames] * self.dup_num)
        else:
            select = np.array([])
            for _ in range(self.dup_num):
                perms = np.random.permutation(len(frames))
                select = np.concatenate([select, frames[perms][map(lambda x: x%len(frames), range(self.sample_num))]])

        samples = []
        for path in select:
            sample = pil_loader(path)
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)
            samples.append(sample)
        
        sample = torch.stack(samples)
        return sample, target

class ImageFrameMixFolder(Dataset):
    """
        mixture of web images(from image dir) and few shot 
        training video frames(from training file)
    """
    def __init__(self, image_dir, path_file, class_lst, img_transform=None, frame_transform=None):
        super(ImageFrameMixFolder, self).__init__()
        self.image_dir = image_dir
        self.path_file = path_file
        self.classes = class_lst
        self.img_transform = img_transform
        self.frame_transform = frame_transform

        self.class_to_idx = {}
        self.idx_to_class = {}
        self.imgs = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(image_dir, cls)
            paths = os.listdir(cls_dir)
            self.imgs = self.imgs + map(lambda x:(os.path.join(cls_dir, x), "i", idx), paths)

        with open(path_file, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        self.videos = []
        for line in lines:
            items = line.split("|")
            path = items[0]
            idx = int(items[1])
            cls = items[2]
          
            assert self.idx_to_class[idx] == cls
            assert self.class_to_idx[cls] == idx
            self.videos.append((path, idx))

        self.frames = []
        for (vid_dir, label) in self.videos:
            frame_paths = os.listdir(vid_dir)

            for f in frame_paths:
                self.frames.append((os.path.join(vid_dir, f), "f", label))

        self.samples = self.imgs + self.frames
        
        print "total {} classes".format(len(self.classes))
        print "total {} images".format(len(self.imgs))
        print "total {} videos".format(len(self.videos))
        print "total {} frames".format(len(self.frames))
        print "total {} samples".format(len(self.samples))

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        path, flag, target = self.samples[idx]
        sample = pil_loader(path)

        if flag == "i":
            if self.img_transform is not None:
                sample = self.img_transform(sample)
        else:
            if self.frame_transform is not None:
                sample = self.frame_transform(sample)

        return sample, target

if __name__ == "__main__":
    import torchvision.transforms as transforms
    with open("/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_train/meta_train.txt", "r") as f:
        classes = [c.strip() for c in f.readlines()]

    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames/train"
    image_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet/train"
    dataset = MixtureMetaTrainMBDataset(classes, image_dir, frame_dir, 5, 1, 32, 2, 
                    img_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ]),
                    frm_transform=transforms.Compose([
                        transforms.Resize(256),
                        transforms.RandomCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                             std=[0.22882703, 0.22168043, 0.21761282])
                        ])) 

    dataloader = DataLoader(dataset, batch_size=32, shuffle=False, num_workers=1)
    for i in range(32):
        print dataset.task_samples[i]

    for i, (input, target) in enumerate(dataloader):
        print dataset.class_to_idx
        print dataset.idx_to_class
        print input.shape, input.dtype
        print target.shape, target.dtype


    

    """
    frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
    dataset = FrameMetaTrainDataset(classes, frame_dir, 3, 1, 1, 100, 100, 1000, 
              frm_transform=transforms.Compose([
                transforms.Resize(256),
                transforms.RandomCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.42715070, 0.40111491, 0.37107715], 
                                     std=[0.22882703, 0.22168043, 0.21761282])
                ])) 
    
    dataloader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=1)
    for i, (support_x, support_y, query_x, query_y) in enumerate(dataloader):
        print support_x.shape, support_x.dtype
        print support_y.shape, support_y.dtype
        print query_x.shape, query_x.dtype
        print query_y.shape, query_y.dtype
        break 
    """
