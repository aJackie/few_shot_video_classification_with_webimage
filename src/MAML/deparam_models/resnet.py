import os
import torch
import torch.nn as nn
from torch.nn import functional as F
import torchvision.models as models
import math
import numpy as np
from layers import *

def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

class BasicBlock(Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, momentum=0.1):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = BatchNorm2d(planes, momentum=momentum)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = BatchNorm2d(planes, momentum=momentum)
        self.downsample = downsample
        self.stride = stride

        self.get_params()

    # parameters: list of parameters, the order is strictly consistent with 
    # modules
    def forward(self, x, parameters):
        residual = x

        out = self.conv1(x, parameters)
        out = self.bn1(out, parameters[2:])
        out = self.relu(out)

        out = self.conv2(out, parameters[4:])
        out = self.bn2(out, parameters[6:])

        if self.downsample is not None:
            residual = self.downsample(x, parameters[8:])

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, momentum=0.1):
        super(Bottleneck, self).__init__()
        self.conv1 = Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = BatchNorm2d(planes, momentum=momentum)
        self.conv2 = Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = BatchNorm2d(planes, momentum=momentum)
        self.conv3 = Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = BatchNorm2d(planes * 4, momentum=momentum)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

        self.get_params()

    def forward(self, x, parameters):
        residual = x

        out = self.conv1(x, parameters)
        out = self.bn1(out, parameters[2:])
        out = self.relu(out)

        out = self.conv2(out, parameters[4:])
        out = self.bn2(out, parameters[6:])
        out = self.relu(out)

        out = self.conv3(out, parameters[8:])
        out = self.bn3(out, parameters[10:])

        # TODO: downsample parameters?
        if self.downsample is not None:
            residual = self.downsample(x, parameters[12:])

        out += residual
        out = self.relu(out)

        return out


class ResNet(Module):

    def __init__(self, block, layers, num_classes=1000, momentum=0.1):
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = BatchNorm2d(64, momentum=momentum)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0], momentum=momentum)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2, momentum=momentum)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2, momentum=momentum)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2, momentum=momentum)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc = Linear(512 * block.expansion, num_classes)

        self.get_params()

    def _make_layer(self, block, planes, blocks, stride=1, momentum=0.1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = Sequential(
                Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                BatchNorm2d(planes * block.expansion, momentum=momentum),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, momentum=momentum))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, momentum=momentum))

        return Sequential(*layers)

    def forward(self, x, parameters):

        x = self.conv1(x, parameters)
        x = self.bn1(x, parameters[2:])
        x = self.relu(x)
        x = self.maxpool(x)

        idx = 4
        x = self.layer1(x, parameters[idx:])
        idx += self.layer1.param_cnt
        x = self.layer2(x, parameters[idx:])
        idx += self.layer2.param_cnt
        x = self.layer3(x, parameters[idx:])
        idx += self.layer3.param_cnt
        x = self.layer4(x, parameters[idx:])
        idx += self.layer4.param_cnt

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x, parameters[idx:])

        return x


def resnet18(**kwargs):
    """Constructs a ResNet-18 model.
    """
    model = ResNet(BasicBlock, [2, 2, 2, 2], **kwargs)
    return model


def resnet34(**kwargs):
    """Constructs a ResNet-34 model.
    """
    model = ResNet(BasicBlock, [3, 4, 6, 3], **kwargs)
    return model


def resnet50(**kwargs):
    """Constructs a ResNet-50 model.
    """
    model = ResNet(Bottleneck, [3, 4, 6, 3], **kwargs)
    return model


def resnet101(**kwargs):
    """Constructs a ResNet-101 model.
    """
    model = ResNet(Bottleneck, [3, 4, 23, 3], **kwargs)
    return model


def resnet152(**kwargs):
    """Constructs a ResNet-152 model.
    """
    model = ResNet(Bottleneck, [3, 8, 36, 3], **kwargs)
    return model

if __name__ == "__main__":
    model = resnet50(num_classes=200)
    print model.param_cnt
    print 

    parameters = construct_parameters("resnet50", num_classes=200, pretrain="ImageNet")

    print len(parameters)

    x = torch.rand([32, 3, 224, 224])
    y = model(x, parameters)
    label = torch.rand([32, 200])

    mse = nn.MSELoss()
    loss = mse(y, label)
    
    grad = torch.autograd.grad(loss, parameters)

