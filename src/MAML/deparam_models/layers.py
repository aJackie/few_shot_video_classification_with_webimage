import numpy as np

import torch
import torch.nn as nn
from torch.nn import functional as F

'''
Functional definitions of common layers
Useful for when weights are exposed rather 
than being contained in modules
'''

def linear(input, weight, bias=None):
    if bias is None:
        return F.linear(input, weight.cuda())
    else:
        return F.linear(input, weight.cuda(), bias.cuda())


def conv2d(input, weight, bias=None, stride=1, padding=0, dilation=1, groups=1):
    return F.conv2d(input, weight.cuda(), bias.cuda(), stride, padding, dilation, groups)

def relu(input):
    return F.threshold(input, 0, 0, inplace=True)

def maxpool(input, kernel_size, stride=None):
    return F.max_pool2d(input, kernel_size, stride)

def batchnorm(input, weight=None, bias=None, running_mean=None, running_var=None, training=True, eps=1e-5, momentum=0.1):
    ''' momentum = 1 restricts stats to the current mini-batch '''
    # This hack only works when momentum is 1 and avoids needing to track running stats
    # by substuting dummy variables
    running_mean = torch.zeros(np.prod(np.array(input.data.size()[1]))).cuda()
    running_var = torch.ones(np.prod(np.array(input.data.size()[1]))).cuda()
    return F.batch_norm(input, running_mean, running_var, weight, bias, training, momentum, eps)


# Modules with parameter input
class Module(nn.Module):
    
    def __init__(self):
        super(Module, self).__init__()

    def get_params(self):
        if hasattr(self, "params"):
            pass
        else:
            self.params = []
            self.param_cnt = 0
            for name, m in self.named_children():
                if isinstance(m, Module):
                    self.params.append((name, m.get_params()))
                    self.param_cnt += m.param_cnt
                else:
                    self.params.append((name, None))

        return self.params

class Sequential(Module):

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

        self.get_params()

    def _get_item_by_idx(self, iterator, idx):
        """Get the idx-th item of the iterator"""
        size = len(self)
        idx = operator.index(idx)
        if not -size <= idx < size:
            raise IndexError('index {} is out of range'.format(idx))
        idx %= size
        return next(islice(iterator, idx, None))

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            return Sequential(OrderedDict(list(self._modules.items())[idx]))
        else:
            return self._get_item_by_idx(self._modules.values(), idx)

    def __setitem__(self, idx, module):
        key = self._get_item_by_idx(self._modules.keys(), idx)
        return setattr(self, key, module)

    def __delitem__(self, idx):
        if isinstance(idx, slice):
            for key in list(self._modules.keys())[idx]:
                delattr(self, key)
        else:
            key = self._get_item_by_idx(self._modules.keys(), idx)
            delattr(self, key)

    def __len__(self):
        return len(self._modules)

    def __dir__(self):
        keys = super(Sequential, self).__dir__()
        keys = [key for key in keys if not key.isdigit()]
        return keys

    def forward(self, input, parameters):
        idx = 0
        for module in self._modules.values():
            if isinstance(module, Module):
                input = module(input, parameters[idx:])
                idx += module.param_cnt
            else:
                input = module(input)
        return input


class Linear(Module):

    param_cnt = 2
    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features

        if bias:
            self.params = [("weight", (self.out_features, self.in_features)),
                           ("bias", (self.out_features,))]
        else:

            self.params = [("weight", (self.out_features, self.in_features)),
                           ("bias", None)]
    
    def forward(self, input, parameters):
        weight, bias = tuple(parameters[:2])
        assert weight.shape == (self.out_features, self.in_features)
        if bias is not None:
            assert bias.shape == (self.out_features,)
        return F.linear(input, weight, bias)

class Conv2d(Module):

    param_cnt = 2
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
        super(Conv2d, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        if isinstance(kernel_size, int):
            self.kernel_size = (kernel_size, kernel_size)
        else:
            self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.groups = groups

        if bias:
            self.params = [("weight", (self.out_channels, self.in_channels // self.groups, self.kernel_size[0], self.kernel_size[1])),
                           ("bias", (self.out_channels,))]
        else:
            self.params = [("weight", (self.out_channels, self.in_channels // self.groups, self.kernel_size[0], self.kernel_size[1])),
                           ("bias", None)]

    def forward(self, input, parameters):
        weight, bias = tuple(parameters[:2])
        assert weight.shape == (self.out_channels, self.in_channels // self.groups, self.kernel_size[0], self.kernel_size[1])
        if bias is not None:
            assert bias.shape == (self.out_channels, )

        return F.conv2d(input, weight, bias, self.stride,
                        self.padding, self.dilation, self.groups)

class BatchNorm2d(Module):
    
    param_cnt = 2
    def __init__(self, num_features, eps=1e-5, momentum=0.1, affine=True,
                 track_running_stats=True):
        super(BatchNorm2d, self).__init__()
        self.num_features = num_features
        self.eps = eps
        self.momentum = momentum
        self.affine = affine
        self.track_running_stats = track_running_stats

        if self.track_running_stats:
            self.register_buffer('running_mean', torch.zeros(num_features))
            self.register_buffer('running_var', torch.ones(num_features))
            self.register_buffer('num_batches_tracked', torch.tensor(0, dtype=torch.long))
        else:
            self.register_parameter('running_mean', None)
            self.register_parameter('running_var', None)
            self.register_parameter('num_batches_tracked', None)
        self.reset_running_stats()

        if affine:
            self.params = [("weight", (self.num_features,)),
                           ("bias", (self.num_features,))]
        else:
            self.params = [("weight", None),
                           ("bias", None)]

    def reset_running_stats(self):
        if self.track_running_stats:
            self.running_mean.zero_()
            self.running_var.fill_(1)
            self.num_batches_tracked.zero_()

    def forward(self, input, parameters):
        weight, bias = tuple(parameters[:2])
        if self.affine:
            assert weight is not None
            assert bias is not None
            assert weight.shape == (self.num_features,)
            assert bias.shape == (self.num_features,)
        else:
            assert weight is None
            assert bias is None

        exponential_average_factor = 0.0

        if self.training and self.track_running_stats:
            self.num_batches_tracked += 1
            if self.momentum is None:  # use cumulative moving average
                exponential_average_factor = 1.0 / self.num_batches_tracked.item()
            else:  # use exponential moving average
                exponential_average_factor = self.momentum

        return F.batch_norm(
            input, self.running_mean, self.running_var, weight, bias,
            self.training or not self.track_running_stats,
            exponential_average_factor, self.eps)

if __name__ == "__main__":
    import torch.optim as optim

    linear = Linear(10, 10)
    print linear.params

    x = torch.rand([10,])
    w = torch.rand([10,10], requires_grad=True)
    b = torch.rand([10,], requires_grad=True)
    p = [w, b]

    new_w = w.clone()
    new_b = b.clone()
    new_p = [new_w, new_b]
    sgd = optim.SGD(p, 1.)

    mse = nn.MSELoss()
    label = torch.rand([10,])

    #########################
    y = linear(x, new_p)
    loss = mse(y, label)
    sgd.zero_grad()
    loss.backward(create_graph=True)
    print b.grad
    print new_b.grad

    print
    y = linear(x, [new_w - w.grad, new_b - b.grad])
    loss = mse(y, label)
    sgd.zero_grad()
    loss.backward()
    print b.grad
    print new_b.grad


    #########################

    y = linear(x, new_p)
    loss = mse(y, label)
    sgd.zero_grad()
    loss.backward()
    print b.grad
    print new_b.grad

    print
    y = linear(x, [new_w - w.grad, new_b - b.grad])

    loss = mse(y, label)
    sgd.zero_grad()
    loss.backward()
    sgd.step()
    print b.grad
    print new_b.grad

    
    conv = Conv2d(3, 10, 3)
    print conv.params
    x = torch.rand([2, 3, 224, 224])
    w = torch.rand([10, 3, 3, 3], requires_grad=True)
    b = None
    p = [w, b]
    y = conv(x, p)
    print y.shape

    bn = BatchNorm2d(10)
    print bn.params
    w = torch.rand([10,], requires_grad=True)
    b = torch.rand([10,], requires_grad=True)
    y = bn(y, [w, b])
    print y.shape

    seq = Sequential(conv, bn)
    print seq.params
    print seq.param_cnt
    y = seq(x, p + [w, b])
    print y.shape
    
