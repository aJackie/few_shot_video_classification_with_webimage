import os
import torch
import torch.nn as nn
import torchvision.models as models
import math
import numpy as np

model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}
torch_model_dir = "/S2/MI/zxz/.torch/models"

# construct parameters list for all models
def construct_parameters(model_name, num_classes=1000, pretrain=None, cuda=False):
    model = models.__dict__[model_name](num_classes=num_classes)

    # manully load pretrained model since only parts of the network configuration the same
    if pretrain:
        print("using pretrained model {}".format(pretrain))
        if pretrain == "ImageNet":
            model_url = model_urls[model_name.split('_')[0]]
            model_filename = model_url.split('/')[-1]
            model_filepath = os.path.join(torch_model_dir, model_filename)
            pretrain_net = torch.load(model_filepath)

            # only load features layer
            pretrain_features = {k:v for k, v in pretrain_net.items() if not ("fc" in k or "classifier" in k)}

        else:
            pretrain_net = torch.load(args.pretrain)
            if pretrain_net.has_key("state_dict"):
                pretrain_net = pretrain_net["state_dict"]

            # only load features layer
            pretrain_features = {k[k.find(".")+1:]:v for k, v in pretrain_net.items() if not ("fc" in k or "classifier" in k)}

        model_dict = model.state_dict()
        model_dict.update(pretrain_features)
        model.load_state_dict(model_dict)

        del pretrain_net
        del pretrain_features
        del model_dict
    else:
        print("creating model {}".format(model_name))

    if cuda:
        model = model.cuda(0)

    parameters = []
    for name, param in model.named_parameters():
        parameters.append(param)
        if "conv" in name or "downsample.0" in name:
            parameters.append(None)

    return parameters

def transfer_parameters(params, new_class_num, feature_dim=2048):
    fc = nn.Linear(feature_dim, new_class_num)
    new_params = params[:-2]
    for name, param in fc.named_parameters():
        new_params.append(param)
    return new_params

# restore parameters into models
def restore_model(model_name, params, num_classes=1000, restore_fc=False):
    model = models.__dict__[model_name](num_classes=num_classes)

    param_dict = {}
    idx = 0
    for name, param in model.named_parameters():
        while params[idx] is None:
            idx += 1
        param_dict[name] = params[idx]
        idx += 1
        
    if not restore_fc:
        features = {k:v for k, v in param_dict.items() if not ("fc" in k or "classifier" in k)}

    model_dict = model.state_dict()
    model_dict.update(features)
    model.load_state_dict(model_dict)

    return model

if __name__ == "__main__":
    param = construct_parameters("resnet50")
    print param[-2].shape
    param = transfer_parameters(param, 200)
    print param[-2].shape


