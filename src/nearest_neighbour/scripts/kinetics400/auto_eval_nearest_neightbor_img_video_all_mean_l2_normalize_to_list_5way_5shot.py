"""
    automatic running eval_nearest_neighbor_img_video_all_mean_l2_normalize_to_list 5-way 5-shot
"""
import os
import numpy as np
from time import sleep
from tqdm import tqdm

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

old_suffix = "/S2/MI/zxz/transfer_learning/data/kinetics400_frame_features"
new_suffix = "/local/MI/xz/transfer_learning/data/kinetics400_frame_features"
input_dir1 = "/local/MI/xz/transfer_learning/data/webimage_kinetics400_features/train_res50"
input_file2 = "/home/zxz/transfer_learning/few_shot/data/kinetics_400/list/meta-learning/meta_test/5way/5/train{}.txt"
input_file3 = "/home/zxz/transfer_learning/few_shot/data/kinetics_400/list/meta-learning/meta_test/5way/5/test{}.txt"
class_num = 5
output_file = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/kinetics400/5way/5/nearest_neighbor/img_video_all_mean/average_l2_normalize/result{}.txt"

for k in tqdm(range(1000)):
    k_input_file2 = input_file2.format(k)
    k_input_file3 = input_file3.format(k)
    k_output_file = output_file.format(k)

    os.system("python eval_nearest_neighbor_img_video_all_mean_l2_normalize_to_list.py" + \
              " -os {}".format(old_suffix) + \
              " -ns {}".format(new_suffix) + \
              " --input_dir1 {}".format(input_dir1) + \
              " --input_file2 {}".format(k_input_file2) + \
              " --input_file3 {}".format(k_input_file3) + \
              " --class_num {}".format(class_num) + \
              " --output {}".format(k_output_file))
    sleep(1)

