"""
    automatic running eval_nearest_neighbor_img_cls_mean_video_mean_average_l2_normalize_to_list_noise 5-way 1-shot
"""
import os
import numpy as np
from time import sleep
from tqdm import tqdm

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

old_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
new_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/video_wise_mean"
input_dir1 = "/local/MI/xz/transfer_learning/data/webimage_actNet_features/train_res50"
input_file2 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/train{}.txt"
input_file3 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/test{}.txt"
class_num = 5
output_file = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/nearest_neighbor/img_cls_mean_video_mean/average_l2_normalize_noise_2/{}/result{}.txt"
noise_rates = [0.8]#[0.1, 0.3, 0.5, 1.0]
rates = [80]#[10, 30, 50, 100]
normal_std = 1.0

for idx, noise_rate in enumerate(noise_rates):
    rate = rates[idx]
    print noise_rate, rate

    for k in tqdm(range(1000)):
        k_input_file2 = input_file2.format(k)
        k_input_file3 = input_file3.format(k)
        k_output_file = output_file.format(rate, k)

        os.system("python eval_nearest_neighbor_img_cls_mean_video_mean_average_l2_normalize_to_list_noise.py" + \
                  " -os {}".format(old_suffix) + \
                  " -ns {}".format(new_suffix) + \
                  " --input_dir1 {}".format(input_dir1) + \
                  " --input_file2 {}".format(k_input_file2) + \
                  " --input_file3 {}".format(k_input_file3) + \
                  " --class_num {}".format(class_num) + \
                  " --noise_rate {}".format(noise_rate) + \
                  " --normal_std {}".format(normal_std) + \
                  " --output {}".format(k_output_file))
        sleep(1)

