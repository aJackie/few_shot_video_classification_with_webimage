"""
    automatic running eval_test_nearest_neighbor_video_mean_from_list_to_list.py 5-way 5-shot
"""
import os
import numpy as np
from time import sleep
from tqdm import tqdm

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

old_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
new_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/video_wise_mean"
input_file1 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/5/train{}.txt"
input_file2 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/5/test{}.txt"
class_num = 5
output_file = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/5/nearest_neighbor/video_mean/result{}.txt"

for k in tqdm(range(20000)):
    k_input_file1 = input_file1.format(k)
    k_input_file2 = input_file2.format(k)
    k_output_file = output_file.format(k)

    os.system("python eval_nearest_neighbor_video_mean_from_list_to_list.py" + \
              " -os {}".format(old_suffix) + \
              " -ns {}".format(new_suffix) + \
              " --input_file1 {}".format(k_input_file1) + \
              " --input_file2 {}".format(k_input_file2) + \
              " --class_num {}".format(class_num) + \
              " --output {}".format(k_output_file))
    sleep(1)

