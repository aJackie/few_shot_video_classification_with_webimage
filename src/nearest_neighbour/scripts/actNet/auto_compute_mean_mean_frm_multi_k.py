"""
    automatic running compute_mean_mean_frm_kshot.py for different ks 
"""
import os

st = 1
ed = 10

for k in range(st, ed+1):
    os.system("python compute_mean_mean_frm_kshot.py" + \
              " -i /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt".format(k) + \
              " -os /S2/MI/zxz/transfer_learning/data/actNet_frames/train" + \
              " -ns /S2/MI/zxz/transfer_learning/data/actNet_frame_features/train_res50" + \
              " -k {}".format(k) + \
              " -c 48" + \
              " --out_mean_dir /S2/MI/zxz/transfer_learning/data/actNet_frame_features/mean/few_shot/meta_test_training" + \
              " --out_mean_mean_dir /S2/MI/zxz/transfer_learning/data/actNet_frame_features/mean_mean/few_shot/meta_test_training")

