"""
    automatic running eval_nearest_neighbour_video_cls_mean_from_list_to_list.py using video class-mean features, test on file
"""
import os
import numpy as np
from tqdm import tqdm
from time import sleep

old_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
new_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/video_wise_mean"
train_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/train{}.txt"
test_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/test{}.txt"
class_num = 5
img_output_file = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/nearest_neighbor/cls_mean/frame/result{}.txt"

for k in tqdm(range(1000)):
    k_train_file = train_file.format(k)
    k_test_file = test_file.format(k)
    k_output_file = img_output_file.format(k)
    os.system("python eval_nearest_neighbor_video_cls_mean_from_list_to_list.py" + \
              " -os {}".format(old_suffix) + \
              " -ns {}".format(new_suffix) + \
              " --input_file1 {}".format(k_train_file) + \
              " --input_file2 {}".format(k_test_file) + \
              " --class_num {}".format(class_num) + \
              " --output {}".format(k_output_file))

    sleep(1)

