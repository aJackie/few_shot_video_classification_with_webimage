"""
    automatic running eval_nearest_neighbor_img_cls_mean_video_mean_concat_to_list 5-way 1-shot
"""
import os
import numpy as np
from time import sleep
from tqdm import tqdm

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

old_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
new_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/video_wise_mean"
input_dir1 = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features/mean/train_res50"
input_file2 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/train{}.txt"
input_file3 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/5way/1/test{}.txt"
class_num = 5
output_file = "/S2/MI/zxz/transfer_learning/results/few_shot/meta_learning/actNet/5way/1/nearest_neighbor/img_cls_mean_video_mean/concat/result{}.txt"

for k in tqdm(range(20000)):
    k_input_file2 = input_file2.format(k)
    k_input_file3 = input_file3.format(k)
    k_output_file = output_file.format(k)

    os.system("python eval_nearest_neighbor_img_cls_mean_video_mean_concat_to_list.py" + \
              " -os {}".format(old_suffix) + \
              " -ns {}".format(new_suffix) + \
              " --input_dir1 {}".format(input_dir1) + \
              " --input_file2 {}".format(k_input_file2) + \
              " --input_file3 {}".format(k_input_file3) + \
              " --class_num {}".format(class_num) + \
              " --output {}".format(k_output_file))
    sleep(1)

