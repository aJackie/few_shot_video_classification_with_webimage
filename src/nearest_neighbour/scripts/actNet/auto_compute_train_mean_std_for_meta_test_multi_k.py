"""
    automatically run utils.py to compute mean & std for multi k in k-shot learning of meta-test training list
"""
import os
from time import sleep

for k in range(1, 11):
    os.system("python util.py" + \
              " --choice 1" + \
              " --list_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt".format(k) + \
              " --output_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/frame_train_mean_std.out".format(k))
    sleep(5)
