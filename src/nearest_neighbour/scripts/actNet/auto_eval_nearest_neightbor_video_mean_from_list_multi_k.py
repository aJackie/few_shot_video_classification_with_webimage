"""
    automatic running eval_test_nearest_neighbor_video_mean_from_list.py using different k in kshot
"""
import os
import numpy as np
from time import sleep

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

old_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frames/train"
new_suffix = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/video_wise_mean/train_res50"
input_file1 = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt"
input_dir2 = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/val_res50"
class_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt"
output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/actNet/meta_learning/nearest_neighbor/video_mean/frame_kshot"

for k in range(1, 11):
    k_input_file1 = input_file1.format(k)
    k_output_dir = os.path.join(output_dir, str(k))
    test_mkdir(k_output_dir)

    os.system("python eval_nearest_neighbor_video_mean_from_list.py" + \
              " -os {}".format(old_suffix) + \
              " -ns {}".format(new_suffix) + \
              " --input_file1 {}".format(k_input_file1) + \
              " --input_dir2 {}".format(input_dir2) + \
              " -c {}".format(class_file) + \
              " --sample_num -1" + \
              " --dup_num 1" + \
              " --output {}".format(os.path.join(k_output_dir, "results.txt")))
    sleep(5)

