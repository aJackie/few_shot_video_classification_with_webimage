"""
    automatic running eval_test_nearest_neighbor_cls_mean_2src_aver.py using different k in kshot
"""
import os
import numpy as np
from time import sleep

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

input_dir1 = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features/mean/train_res50"
input_dir2 = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/mean/few_shot/meta_test_training/{}"
input_dir3 = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/val_res50"
class_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt"
output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/actNet/meta_learning/nearest_neighbor/cls_mean/aver_kshot"

for k in range(1, 11):
    k_input_dir2 = input_dir2.format(k)
    k_output_dir = os.path.join(output_dir, str(k))
    test_mkdir(k_output_dir)

    os.system("python eval_nearest_neighbor_cls_mean_2src_aver.py" + \
              " --input_dir1 {}".format(input_dir1) + \
              " --input_dir2 {}".format(k_input_dir2) + \
              " --input_dir3 {}".format(input_dir3) + \
              " -c {}".format(class_file) + \
              " --sample_num -1" + \
              " --dup_num 1" + \
              " --output {}".format(os.path.join(k_output_dir, "results.txt")))
    sleep(5)

