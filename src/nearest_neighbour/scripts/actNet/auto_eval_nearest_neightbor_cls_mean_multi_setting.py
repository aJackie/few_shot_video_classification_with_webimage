"""
    automatic running eval_test_nearest_neighbour.py using different settings:
    1.with pure web images
    2.with pure frames
    - both without centering
"""
import os
import numpy as np
from time import sleep

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

img_mean_dir = "/S2/MI/zxz/transfer_learning/data/webimage_actNet_features/mean/train_res50"
kshot_mean_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/mean/few_shot/meta_test_training/{}"
test_frame_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/val_res50"

class_file = "/home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt"

zero_mean_mean_file = "/S2/MI/zxz/transfer_learning/data/actNet_frame_features/mean_mean/zero_mean_mean.npy"


# on pure web images
mean_mean_files = [(zero_mean_mean_file, zero_mean_mean_file)]

img_output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/actNet/meta_learning/nearest_neighbor/cls_mean/image"
img_output_files = [os.path.join(img_output_dir, "results_no_centering.txt")]

for idx, (img_mean_mean_file, frm_mean_mean_file) in enumerate(mean_mean_files):
    img_output_file = img_output_files[idx]
    os.system("python eval_nearest_neighbor_cls_mean.py" + \
              " --input_dir1 {}".format(img_mean_dir) + \
              " --input_dir2 {}".format(test_frame_dir) + \
              " -m1 {}".format(img_mean_mean_file) + \
              " -m2 {}".format(frm_mean_mean_file) + \
              " -c {}".format(class_file) + \
              " --sample_num -1" + \
              " --dup_num 1" + \
              " --output {}".format(img_output_file))

    sleep(5)

# on pure frames
mean_mean_files = [(zero_mean_mean_file, zero_mean_mean_file)]

frm_output_dir = "/S2/MI/zxz/transfer_learning/results/few_shot/actNet/meta_learning/nearest_neighbor/cls_mean/frame_kshot"
frm_output_files = ["results_no_centering.txt"]

for k in range(1, 11):
    k_frm_output_dir = os.path.join(frm_output_dir, str(k))
    test_mkdir(k_frm_output_dir)

    for idx, (train_mean_mean_file, test_mean_mean_file) in enumerate(mean_mean_files):
        k_frm_output_file = os.path.join(k_frm_output_dir, frm_output_files[idx])

        os.system("python eval_nearest_neighbor_cls_mean.py" + \
                  " --input_dir1 {}".format(kshot_mean_dir.format(k)) + \
                  " --input_dir2 {}".format(test_frame_dir) + \
                  " -m1 {}".format(train_mean_mean_file) + \
                  " -m2 {}".format(test_mean_mean_file) + \
                  " -c {}".format(class_file) + \
                  " --sample_num -1" + \
                  " --dup_num 1" + \
                  " --output {}".format(k_frm_output_file))
        
        sleep(5)

