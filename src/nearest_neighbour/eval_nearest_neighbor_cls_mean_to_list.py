"""
    using nearest neighbor classifier using class-wise mean features on test file
"""
import os
import sys
import time
import argparse
import signal
import shutil
import numpy as np
from util import *
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="using nearest neighbor classifier using class-wise mean features on test file",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-os", "--old_suffix", type=str, required=True, help="old suffix in the path file")
parser.add_argument("-ns", "--new_suffix", type=str, required=True, help="new suffix to be replaced on")
parser.add_argument("--input_dir1", type=str, required=True, help="root dir of reference mean features")
parser.add_argument("--input_file2", type=str, required=True, help="path to evaluation video path")
parser.add_argument("--class_num", type=int, required=True, help="num of classes")
parser.add_argument("--output", type=str, default=None, help="output file recording evaluation result")

def compute_projected_distance(refer_features, test_feature):

    dis = []
    test_feature = test_feature.reshape((-1, 1))
    for f in refer_features:
        f = f.reshape((-1, 1))
        dis.append(np.linalg.norm(test_feature - f))

    return dis

def evaluate(refer_dir, test_file, old_suffix, new_suffix, class_num):

    cls_to_idx = {}
    idx_to_cls = {}

    # loading test features
    test_features = [[] for _ in range(class_num)]
    with open(test_file, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        cls_to_idx[cls] = idx
        idx_to_cls[idx] = cls

        path = path.replace(os.path.join(old_suffix, "val"), os.path.join(new_suffix, "val_res50"))
        test_features[idx].append(np.load(os.path.join(path, "mean.npy")))

    # loading refer features
    refer_features = []
    for idx in range(class_num):
        cls = idx_to_cls[idx]
        refer_features.append(np.load(os.path.join(refer_dir, cls, "mean.npy")))

    batch_time = AverageMeter()
    cls_ac1 = [AverageMeter() for _ in range(class_num)]
    avg_acc1 = AverageMeter()

    total_video_num = 0
    end = time.time()
    for idx in tqdm(range(class_num)):
        cls = idx_to_cls[idx]
        #print "inference for No.{} class {}".format(idx, cls)
        #print "\ttotal {} videos".format(len(test_features[idx]))
        total_video_num += len(test_features[idx])

        for test_feature in test_features[idx]:
            distance = 0.
            distance = distance + np.array(compute_projected_distance(refer_features, test_feature))
            is_top1 = float((distance < distance[idx]).sum() < 1)
            
            cls_ac1[idx].update(is_top1, 1)
            avg_acc1.update(is_top1, 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1))

    return avg_acc1.avg, [x.avg for x in cls_ac1], total_video_num, idx_to_cls

def main():
    args = parser.parse_args()
    print "evaludate on {}".format(args.input_file2)
                    
    avg_acc1, cls_ac1, total_video_num, idx_to_cls = evaluate(args.input_dir1, args.input_file2, \
                                                        args.old_suffix, args.new_suffix, args.class_num)

    with open(args.output, "w") as f:
        print >> f, "inference for {} classes, {} videos".\
                    format(args.class_num, total_video_num)
        print >> f, "average top-1 accuracy: {}".format(avg_acc1)
        print >> f, "------------------------------------"
        for idx in range(args.class_num):
            cls = idx_to_cls[idx]
            print >> f, "{}: acc1 {}".format(cls, cls_ac1[idx])

if __name__ == "__main__":
    main()
