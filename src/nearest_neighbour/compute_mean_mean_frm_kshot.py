"""
    compute mean features of all classes of video frames using few-shot training data
    as well as compute the mean of all mean features
"""
import os
import numpy as np
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_file", required=True, type=str, help="input file, containing raw paths of frame dirs")
parser.add_argument("-os", "--old_suffix", required=True, type=str, help="old suffix of the raw path")
parser.add_argument("-ns", "--new_suffix", required=True, type=str, help="new suffix of the replaced path, \
                                                                          used to generate path for features")
parser.add_argument("-k", "--kshot", required=True, type=int, help="k shot learning")
parser.add_argument("-c", "--class_num", required=True, type=int, help="class num")
parser.add_argument("--out_mean_dir", required=True, type=str, help="output root dir of class-wise mean features")
parser.add_argument("--out_mean_mean_dir", required=True, type=str, help="output root dir of mean of mean features")

args = parser.parse_args()

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

with open(args.input_file, "r") as f:
    lines = [_.strip() for _ in f.readlines()]

paths = []
idx_to_cls = {}
for line in tqdm(lines):
    items = line.split("|")
    path = items[0]
    idx = int(items[1])
    cls = items[2]

    paths.append((path.replace(args.old_suffix, args.new_suffix), idx, cls))
    idx_to_cls[idx] = cls

class_num = args.class_num
cls_means = [0.0 for _ in range(class_num)]
cls_fnums = [0 for _ in range(class_num)]
for (path, idx, cls) in tqdm(paths):
    features = [os.path.join(path, _) for _ in os.listdir(path)]
    
    tem = cls_means[idx]
    tem_num = cls_fnums[idx]
    for f in features:
        tem = tem + np.load(f)
    
    tem_num = tem_num + len(features)

    cls_means[idx] = tem
    cls_fnums[idx] = tem_num

mean_mean = 0.
for idx in tqdm(range(len(cls_means))):
    cls_means[idx] = cls_means[idx] / float(cls_fnums[idx])
    mean_mean = mean_mean + cls_means[idx]

mean_mean = mean_mean / float(class_num) 

test_mkdir(os.path.join(args.out_mean_dir, str(args.kshot)))
test_mkdir(os.path.join(args.out_mean_mean_dir, str(args.kshot)))

for idx in tqdm(range(len(cls_means))):
    cls = idx_to_cls[idx]
    test_mkdir(os.path.join(args.out_mean_dir, str(args.kshot), cls))
    np.save(os.path.join(args.out_mean_dir, str(args.kshot), cls, "mean.npy"), cls_means[idx])

np.save(os.path.join(args.out_mean_mean_dir, str(args.kshot), "mean_mean.npy"), mean_mean)

