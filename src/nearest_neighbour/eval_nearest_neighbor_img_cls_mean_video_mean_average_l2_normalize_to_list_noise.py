"""
    using nearest neighbor classifier on image class-wise mean features and video cls-wise mean average and l2 normalize.
    use both web images and video frames to eval on test file
    web image features are randomly noisized with probability p
"""
import os
import sys
import time
import argparse
import signal
import shutil
import numpy as np
from util import *
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="using nearest neighbor classifier on class-wise mean features, \
                                              use both web images and video frames to eval on test file",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-os", "--old_suffix", type=str, required=True, help="old suffix in the path file")
parser.add_argument("-ns", "--new_suffix", type=str, required=True, help="new suffix to be replaced on")
parser.add_argument("--input_dir1", type=str, required=True, help="root dir of web image features")
parser.add_argument("--input_file2", type=str, required=True, help="path to kshot training videos")
parser.add_argument("--input_file3", type=str, required=True, help="path to query videos")
parser.add_argument("--class_num", type=int, required=True, help="class num")
parser.add_argument("--noise_rate", type=float, default=0.0, help="probability of adding noise")
parser.add_argument("--normal_std", type=float, default=1.0, help="std of Gaussian noise")
parser.add_argument("--output", type=str, default=None, help="output file recording evaluation result")

def compute_projected_distance(refer_features, test_feature):

    dis = []
    test_feature = test_feature.reshape((-1, 1))
    
    for fs in refer_features:
        cls_dis = []

        for f in fs:
            f = f.reshape((-1, 1))
            cls_dis.append(np.linalg.norm(test_feature - f))
        
        dis.append(cls_dis)

    return dis

def evaluate(refer_dir1, refer_file2, test_file, old_suffix, new_suffix, class_num, noise_rate, noise_func):

    cls_to_idx = {}
    idx_to_cls = {}

    # loading refer features
    refer_features = [[] for _ in range(class_num)]
    with open(refer_file2, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        cls_to_idx[cls] = idx
        idx_to_cls[idx] = cls

        path = path.replace(os.path.join(old_suffix, "train"), os.path.join(new_suffix, "train_res50"))
        refer_features[idx].append(np.load(os.path.join(path, "mean.npy")))

    img_features = {}
    for idx in range(class_num):
        cls = idx_to_cls[idx]
        cls_dir = os.path.join(refer_dir1, cls)
        fs = [os.path.join(cls_dir, f) for f in os.listdir(cls_dir)]

        sample_fs = []
        for f in fs:
            f = np.load(f)
            if noise_rate > 0. and np.random.uniform() < noise_rate:
                #f = f + noise_func(size=f.shape).astype(f.dtype)
                #f = f * (noise_func(size=f.shape).astype(f.dtype) + 1.0)  + noise_func(size=f.shape).astype(f.dtype)
                f = noise_func(size=f.shape).astype(f.dtype)
            sample_fs.append(f)
        sample_fs = np.stack(sample_fs).mean(axis=0)
        
        img_features[cls] = sample_fs

    for idx in range(class_num):
        cls = idx_to_cls[idx]
        refer_features[idx] = [np.stack(refer_features[idx]).mean(axis=0)]
        refer_features[idx].append(img_features[cls])
        refer_features[idx] = [np.stack(refer_features[idx]).mean(axis=0)]
        # l2 normalize
        refer_features[idx] = [refer_features[idx][0] / np.linalg.norm(refer_features[idx][0])]

    # loading test features
    test_features = [[] for _ in range(class_num)]
    with open(test_file, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        path = path.replace(os.path.join(old_suffix, "val"), os.path.join(new_suffix, "val_res50"))
        # L2 normalize
        f = np.load(os.path.join(path, "mean.npy"))
        f = f / np.linalg.norm(f)
        test_features[idx].append(f)

    batch_time = AverageMeter()
    cls_ac1 = [AverageMeter() for _ in range(class_num)]
    avg_acc1 = AverageMeter()

    total_video_num = 0
    end = time.time()
    for idx in tqdm(range(class_num)):
        cls = idx_to_cls[idx]
        #print "inference for No.{} class {}".format(idx, cls)
        #print "\ttotal {} videos".format(len(test_features[idx]))
        total_video_num += len(test_features[idx])

        for test_feature in test_features[idx]:
            distance = 0.
            distance = distance + np.array(compute_projected_distance(refer_features, test_feature))
            distance = distance.min(axis=1)
            is_top1 = float((distance < distance[idx]).sum() < 1)
            
            cls_ac1[idx].update(is_top1, 1)
            avg_acc1.update(is_top1, 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1))

    return avg_acc1.avg, [x.avg for x in cls_ac1], total_video_num, idx_to_cls

def main():
    args = parser.parse_args()
    print "evaludate on {}".format(args.input_file3)
                    
    def random_noise(size):
        return np.random.normal(0., args.normal_std, size)

    avg_acc1, cls_ac1, total_video_num, idx_to_cls = evaluate(args.input_dir1, args.input_file2, args.input_file3, \
                                                        args.old_suffix, args.new_suffix, args.class_num, \
                                                        args.noise_rate, random_noise)

    with open(args.output, "w") as f:
        print >> f, "inference for {} classes, {} videos".\
                    format(args.class_num, total_video_num)
        print >> f, "average top-1 accuracy: {}".format(avg_acc1)
        print >> f, "------------------------------------"
        for idx in range(args.class_num):
            cls = idx_to_cls[idx]
            print >> f, "{}: acc1 {}".format(cls, cls_ac1[idx])

if __name__ == "__main__":
    main()
