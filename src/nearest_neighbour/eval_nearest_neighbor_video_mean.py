"""
    using nearest neighbor classifier with video-wise mean features
"""
import os
import sys
import time
import argparse
import signal
import shutil
import numpy as np
from util import *
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="using nearest neighbor classifier with video-wise mean features",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--input_dir1", type=str, required=True, help="root dir of reference video-wise mean features")
parser.add_argument("--input_dir2", type=str, required=True, help="root dir of evaluation frame-wise mean features")
parser.add_argument("-c", "--class_list", type=str, required=True, help="path to file containing valided class names")
# test configuration
parser.add_argument("--sample_num", type=int, default=-1, help="sampling num per video, -1 means all")
parser.add_argument("--dup_num", type=int, default=1, help="duplicate sampling num per video")
parser.add_argument("--output", type=str, default=None, help="output file recording evaluation result")

def compute_projected_distance(refer_features, test_feature):
    dis = []
    test_feature = test_feature.reshape((-1, 1))

    for cls_fs in refer_features:
        cls_dis = []

        for f in cls_fs:
            f = f.reshape((-1, 1))
            cls_dis.append(np.linalg.norm(test_feature - f))

        dis.append(cls_dis)

    return dis

def evaluate(refer_dir, test_dir, classes, num, dup_num):
    batch_time = AverageMeter()

    class_num = len(classes)
    cls_ac1 = [AverageMeter() for _ in range(class_num)]
    avg_acc1 = AverageMeter()

    # loading refer features
    refer_features = []
    for cls in classes:
        cls_features = []
        
        for vid in os.listdir(os.path.join(refer_dir, cls)):
            cls_features.append(np.load(os.path.join(refer_dir, cls, vid, "mean.npy")))

        refer_features.append(cls_features)

    total_video_num = 0
    end = time.time()
    for idx, cls in enumerate(tqdm(classes)):
        print "inference for No.{} class {}".format(idx, cls)

        cls_dir = os.path.join(test_dir, cls)
        vids = [os.path.join(cls_dir, vid) for vid in os.listdir(cls_dir)]
        print "\ttotal {} videos".format(len(vids))
        total_video_num += len(vids)

        for vid in vids:
            files = np.array([os.path.join(vid, f) for f in os.listdir(vid)])
            distance = 0.
            for _ in range(dup_num):
                if num == -1:
                    select = files
                else:
                    perms = np.random.permutation(len(files))
                    select = files[perms][map(lambda x: x%len(files), range(num))]

                test_feature = 0.
                for f in select:
                    test_feature = test_feature + np.load(f)

                # centering
                test_feature = test_feature / len(select)
                distance = distance + np.array(compute_projected_distance(refer_features, test_feature))
        
            distance = distance.min(axis=1)
            is_top1 = float((distance < distance[idx]).sum() < 1)
            
            cls_ac1[idx].update(is_top1, 1)
            avg_acc1.update(is_top1, 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}\n\
           Acc5: {acc5.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1, acc5=avg_acc5))

    return avg_acc1.avg, [x.avg for x in cls_ac1], total_video_num

def main():
    args = parser.parse_args()
    with open(args.class_list, "r") as f:
        classes = [l.strip() for l in f.readlines()]

    testdir = args.input_dir2
    print "evaludate on {}".format(testdir)
                    
    avg_acc1, cls_ac1, total_video_num = evaluate(args.input_dir1, testdir, classes, args.sample_num, args.dup_num)

    with open(args.output, "w") as f:
        print >> f, "inference for {} classes, {} videos, duplicate {} times with {} frames once".\
                    format(len(classes), total_video_num, args.dup_num, args.sample_num)
        print >> f, "average top-1 accuracy: {}".format(avg_acc1)
        print >> f, "------------------------------------"
        for idx, cls in enumerate(classes):
            print >> f, "{}: acc1 {}".format(cls, cls_ac1[idx])

if __name__ == "__main__":
    main()
