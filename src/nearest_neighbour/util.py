import os
import sys
import torch
import shutil
from PIL import Image
import numpy as np
from tqdm import tqdm
from random import shuffle
import argparse

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

class AverageMeter(object):
    """recording the average and current value of a summation var"""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += self.val * n
        self.count += n
        self.avg = self.sum * 1./ self.count

def save_checkpoint(state, is_best, cp_dir):
    epoch_num = state['epoch']
    filename = os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch_num))
    torch.save(state, filename)
    if is_best:
        shutil.copy(filename, os.path.join(cp_dir, "model_best.pth.tar"))

def accuracy(output, target, topk=(1,)):
    """computes the topk precision"""

    maxk = max(topk)
    batch_size = target.size(0)
    '''
    print("in accuracy function:")
    print("\touput: {}".format(output.size()))
    print("\ttarget: {}".format(target.size()))
    '''

    _,pred = output.topk(maxk, 1, True, True)
    #print("\tpred: {}".format(pred.shape))
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100./batch_size))

    return res

def my_split(line, sign, last_num):
    split = []
    ed = len(line)
    st = ed - 1
    while st >= 0:
        if line[st] == sign:
            split.append(line[st+1:ed])
            ed = st
        st -= 1
        if len(split) >= last_num:
            break
    split.append(line[0:ed])
    split.reverse()
    return split

def compute_frames_mean_std(root_dir, class_file=None, num=-1):
    if class_file is None:
        subdirs = [os.path.join(root_dir, _) for _ in os.listdir(root_dir)]
    else:
        with open(class_file, "r") as f:
            classes = [c.strip() for c in f.readlines()]
        subdirs = [os.path.join(root_dir, c) for c in classes]
    
    paths = []
    for subdir in subdirs:
        vids = [os.path.join(subdir, vid) for vid in os.listdir(subdir)]
        paths = paths + vids

    means = []
    stds = []
    for path in tqdm(paths):
        frames = os.listdir(path)
        shuffle(frames)
        if not num == -1:
            frames = frames[:min(num, len(frames))]
        
        for frame in os.listdir(path):
            frame = os.path.join(path, frame)
            img = pil_loader(frame)
            img = np.array(img, dtype=np.float32) / 255.
            means.append(list(np.mean(img, axis=(0, 1))))
            stds.append(list(np.std(img, axis=(0, 1))))
    
    means = np.array(means, dtype=np.float32)
    stds = np.array(stds, dtype=np.float32)

    return list(np.mean(means, axis=0)), list(np.mean(stds, axis=0))

def compute_frames_mean_std_list(list_file, num=-1):
    with open(list_file, "r") as f:
        lines = [l.strip() for l in f.readlines()]

    paths = []
    for line in lines:
        items = line.split("|")
        path = items[0]
        paths.append(path)
    
    means = []
    stds = []
    for path in tqdm(paths):
        frames = os.listdir(path)
        shuffle(frames)
        if not num == -1:
            frames = frames[:min(num, len(frames))]
        
        for frame in os.listdir(path):
            frame = os.path.join(path, frame)
            img = pil_loader(frame)
            img = np.array(img, dtype=np.float32) / 255.
            means.append(list(np.mean(img, axis=(0, 1))))
            stds.append(list(np.std(img, axis=(0, 1))))
    
    means = np.array(means, dtype=np.float32)
    stds = np.array(stds, dtype=np.float32)

    return list(np.mean(means, axis=0)), list(np.mean(stds, axis=0))

def compute_images_mean_std(root_dir, class_file=None):
    if class_file is None:
        subdirs = [os.path.join(root_dir, _) for _ in os.listdir(root_dir)]
    else:
        with open(class_file, "r") as f:
            classes = [c.strip() for c in f.readlines()]
        subdirs = [os.path.join(root_dir, c) for c in classes]

    means = []
    stds = []
    for subdir in tqdm(subdirs):
        for image in os.listdir(subdir):
            image = os.path.join(subdir, image)
            img = pil_loader(image)
            img = np.array(img, dtype=np.float32) / 255.
            means.append(list(np.mean(img, axis=(0, 1))))
            stds.append(list(np.std(img, axis=(0, 1))))
    
    means = np.array(means, dtype=np.float32)
    stds = np.array(stds, dtype=np.float32)

    return list(np.mean(means, axis=0)), list(np.mean(stds, axis=0))

parser = argparse.ArgumentParser()
parser.add_argument("--choice", type=int, required=True, help="choice of functions 0/1/2")
parser.add_argument("--root_dir", type=str, help="root_dir parameter")
parser.add_argument("--class_file", type=str, default=None, help="class_file parameter")
parser.add_argument("--list_file", type=str, help="list_file parameter")
parser.add_argument("--num", type=int, default=-1, help="num parameter")
parser.add_argument("--output_file", type=str, required=True, help="output file to save results")

if __name__ == "__main__":
    args = parser.parse_args()

    if args.choice == 0:
        mean, std = compute_frames_mean_std(args.root_dir, args.class_file, args.num)
    elif args.choice == 1:
        mean, std = compute_frames_mean_std_list(args.list_file, args.num)
    else:
        mean, std = compute_images_mean_std(args.root_dir, args.class_file)

    np.savetxt(args.output_file, np.array([mean, std], dtype=np.float32), fmt="%.8f")

