"""
    using nearest neighbor classifier on mean of all images and video frames of one class and do l2 normalize 
    random replace web images with probability p
"""
import os
import sys
import time
import argparse
import signal
import shutil
import numpy as np
from util import *
from datetime import datetime
from tqdm import tqdm

parser = argparse.ArgumentParser(description="using nearest neighbor classifier on mean of all images and \
                                              video frames of one class and do l2 normalize, \
                                              random replace web images with probability p",\
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-os", "--old_suffix", type=str, required=True, help="old suffix in the path file")
parser.add_argument("-ns", "--new_suffix", type=str, required=True, help="new suffix to be replaced on")
parser.add_argument("--input_dir1", type=str, required=True, help="root dir of web image features")
parser.add_argument("--input_file2", type=str, required=True, help="path to kshot training videos")
parser.add_argument("--input_file3", type=str, required=True, help="path to query videos")
parser.add_argument("--class_num", type=int, required=True, help="class num")
parser.add_argument("--test_class_file", type=str, required=True, help="path to test classes")
parser.add_argument("--replace_rate", type=float, default=0.0, help="probability of replacing")
parser.add_argument("--output", type=str, default=None, help="output file recording evaluation result")

def compute_projected_distance(refer_features, test_feature):

    dis = []
    test_feature = test_feature.reshape((-1, 1))
    
    for fs in refer_features:
        cls_dis = []

        for f in fs:
            f = f.reshape((-1, 1))
            cls_dis.append(np.linalg.norm(test_feature - f))
        
        dis.append(cls_dis)

    return dis

def evaluate(refer_dir1, refer_file2, test_file, test_classes, old_suffix, new_suffix, class_num, replace_rate):

    cls_to_idx = {}
    idx_to_cls = {}

    # loading refer features
    refer_features = [[] for _ in range(class_num)]
    with open(refer_file2, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        cls_to_idx[cls] = idx
        idx_to_cls[idx] = cls

        path = path.replace(os.path.join(old_suffix, "train"), os.path.join(new_suffix, "train_res50"))

        # append all features of one video
        fs = [os.path.join(path, f) for f in os.listdir(path)]
        for f in fs:
            refer_features[idx].append(np.load(f))

    all_image_fs = []
    for cls in test_classes:
        cls_dir = os.path.join(refer_dir1, cls)
        fs = [os.path.join(cls_dir, f) for f in os.listdir(cls_dir)]
        all_image_fs = all_image_fs + [(cls, f) for f in fs]

    img_features = {}
    for idx in range(class_num):
        cls = idx_to_cls[idx]
        cls_dir = os.path.join(refer_dir1, cls)
        fs = [os.path.join(cls_dir, f) for f in os.listdir(cls_dir)]
        img_features[cls] = fs

    # replace
    if replace_rate > 0.:
        total_num = len(all_image_fs)

        for idx in range(class_num):
            cls = idx_to_cls[idx]
            shuffle(all_image_fs)

            num = len(img_features[cls])
            replace_num = int(replace_rate * num)

            j = 0
            for i in range(num-replace_num, num):
                while all_image_fs[j][0] == cls:
                    j = (j + 1) % total_num

                img_features[cls][i] = all_image_fs[j][1]
                j = (j + 1) % total_num

    for idx in range(class_num):
        cls = idx_to_cls[idx]
        fs = img_features[cls]

        for f in fs:
            refer_features[idx].append(np.load(f))
        refer_features[idx] = np.stack(refer_features[idx])
        refer_features[idx] = refer_features[idx].mean(axis=0)

        # l2 normalize
        refer_features[idx] = [refer_features[idx] / np.linalg.norm(refer_features[idx])]

    # loading test features
    test_features = [[] for _ in range(class_num)]
    with open(test_file, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        path = path.replace(os.path.join(old_suffix, "val"), os.path.join(new_suffix, "val_res50"))
        fs = [os.path.join(path, f) for f in os.listdir(path)]
        frms = []
        for f in fs:
            frms.append(np.load(f))
        frms = np.stack(frms)
        f = frms.mean(axis=0)
        # L2 normalize
        f = f / np.linalg.norm(f)
        test_features[idx].append(f)

    batch_time = AverageMeter()
    cls_ac1 = [AverageMeter() for _ in range(class_num)]
    avg_acc1 = AverageMeter()

    total_video_num = 0
    end = time.time()
    for idx in tqdm(range(class_num)):
        cls = idx_to_cls[idx]
        #print "inference for No.{} class {}".format(idx, cls)
        #print "\ttotal {} videos".format(len(test_features[idx]))
        total_video_num += len(test_features[idx])

        for test_feature in test_features[idx]:
            distance = 0.
            distance = distance + np.array(compute_projected_distance(refer_features, test_feature))
            distance = distance.min(axis=1)
            is_top1 = float((distance < distance[idx]).sum() < 1)
            
            cls_ac1[idx].update(is_top1, 1)
            avg_acc1.update(is_top1, 1)

        # measure one-batch time
        batch_time.update(time.time() - end)
        end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}\n\
           Acc1: {acc1.avg:.3f}".format(\
           batch_time=batch_time, acc1=avg_acc1))

    return avg_acc1.avg, [x.avg for x in cls_ac1], total_video_num, idx_to_cls

def main():
    args = parser.parse_args()
    print "evaludate on {}".format(args.input_file3)
    print args.replace_rate

    with open(args.test_class_file, "r") as f:
        test_classes = [l.strip() for l in f.readlines()]
                    
    avg_acc1, cls_ac1, total_video_num, idx_to_cls = evaluate(args.input_dir1, args.input_file2, args.input_file3, test_classes, \
                                                        args.old_suffix, args.new_suffix, args.class_num, args.replace_rate)

    with open(args.output, "w") as f:
        print >> f, "inference for {} classes, {} videos".\
                    format(args.class_num, total_video_num)
        print >> f, "average top-1 accuracy: {}".format(avg_acc1)
        print >> f, "------------------------------------"
        for idx in range(args.class_num):
            cls = idx_to_cls[idx]
            print >> f, "{}: acc1 {}".format(cls, cls_ac1[idx])

if __name__ == "__main__":
    main()
