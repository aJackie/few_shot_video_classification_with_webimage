"""
    compute mean features video-wise
"""
import os
import numpy as np
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_dir", type=str, required=True, help="root dir of frame-wise features")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="root dir of saving video-wise features")

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

def main():
    args = parser.parse_args()
    classes = os.listdir(args.input_dir)
    
    for cls in tqdm(classes):
        cls_dir = os.path.join(args.input_dir, cls)
        out_cls_dir = os.path.join(args.output_dir, cls)
        test_mkdir(out_cls_dir)
        vids = os.listdir(cls_dir)

        for vid in vids:
            vid_dir = os.path.join(cls_dir, vid)
            out_vid_dir = os.path.join(out_cls_dir, vid)
            test_mkdir(out_vid_dir)

            cls_mean = 0.
            features = os.listdir(vid_dir)
            for f in features:
                cls_mean = cls_mean + np.load(os.path.join(vid_dir, f))

            cls_mean = cls_mean / float(len(features))
            
            np.save(os.path.join(out_vid_dir, "mean.npy"), cls_mean)

if __name__ == "__main__":
    main()
