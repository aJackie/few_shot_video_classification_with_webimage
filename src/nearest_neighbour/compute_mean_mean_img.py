"""
    compute mean of mean features of all classes for web images
"""
import os
import numpy as np
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_dir", required=True, type=str, help="input root dir of mean features of all classes")
parser.add_argument("-o", "--output_file", required=True, type=str, help="output file to save mean of all means")
args = parser.parse_args()

cls_dirs = [os.path.join(args.input_dir, _) for _ in os.listdir(args.input_dir)]

mean_mean = 0.
for cls_dir in tqdm(cls_dirs):
    mean = np.load(os.path.join(cls_dir, "mean.npy"))
    mean_mean = mean_mean + mean

mean_mean = mean_mean / float(len(cls_dirs))
np.save(args.output_file, mean_mean)
