import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from PIL import Image
from torch.utils.data import Dataset, DataLoader
from util import my_split
from random import shuffle
import torchvision.transforms as transforms

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

class FrameListFolder(Dataset):
    """
    similar to ImageListFolder
    but has one more level of folders as video directory
    """
    def __init__(self, frame_dir, class_lst, frame_transform=None):
        super(FrameListFolder, self).__init__()
        self.frame_dir = frame_dir
        self.classes = class_lst
        self.frame_transform = frame_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        self.frames = []
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

            cls_dir = os.path.join(frame_dir, cls)
            vid_dirs = [os.path.join(cls_dir, p) for p in os.listdir(cls_dir)]
            for vid_dir in vid_dirs:
                paths = os.listdir(vid_dir)
                self.frames = self.frames + map(lambda x:(os.path.join(vid_dir, x), idx), paths)

        print "total {} classes".format(len(self.classes))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path, target = self.frames[idx]
        sample = pil_loader(path)
        if self.frame_transform is not None:
            sample = self.frame_transform(sample)

        return sample, target
