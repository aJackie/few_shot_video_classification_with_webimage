import os
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_dir", type=str, required=True, help="input reference root dir")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="output result root dir")
args = parser.parse_args()

subdirs = os.listdir(args.input_dir)
for subdir in tqdm(subdirs):
    os.mkdir(os.path.join(args.output_dir, subdir))

