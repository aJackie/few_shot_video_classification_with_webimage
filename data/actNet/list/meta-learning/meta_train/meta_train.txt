Archery
Arm wrestling
Assembling bicycle
BMX
Baking cookies
Ballet
Bathing dog
Baton twirling
Beach soccer
Beer pong
Belly dance
Blow-drying hair
Blowing leaves
Braiding hair
Breakdancing
Brushing teeth
Bullfighting
Calf roping
Camel ride
Capoeira
Changing car wheel
Chopping wood
Clean and jerk
Cleaning shoes
Cleaning windows
Clipping cat claws
Cricket
Croquet
Cumbia
Cutting the grass
Decorating the Christmas tree
Disc dog
Doing crunches
Doing fencing
Doing karate
Doing kickboxing
Drinking beer
Drinking coffee
Elliptical trainer
Futsal
Gargling mouthwash
Getting a piercing
Grooming horse
Hammer throw
Hand car wash
Hand washing clothes
Having an ice cream
Hitting a pinata
Hopscotch
Horseback riding
Hula hoop
Hurling
Installing carpet
Ironing clothes
Kayaking
Knitting
Laying tile
Layup drill in basketball
Long jump
Making a lemonade
Making an omelette
Mixing drinks
Mooping floor
Mowing the lawn
Paintball
Painting fence
Ping-pong
Plastering
Plataform diving
Playing badminton
Playing beach volleyball
Playing blackjack
Playing congas
Playing drums
Playing field hockey
Playing flauta
Playing guitarra
Playing harmonica
Playing ice hockey
Playing kickball
Playing lacrosse
Playing piano
Playing pool
Playing racquetball
Playing rubik cube
Playing saxophone
Playing squash
Playing ten pins
Playing violin
Pole vault
Powerbocking
Preparing pasta
Putting in contact lenses
Putting on makeup
Removing curlers
Removing ice from car
River tubing
Rock-paper-scissors
Roof shingle removal
Running a marathon
Sharpening knives
Shaving
Slacklining
Smoking hookah
Snatch
Snow tubing
Springboard diving
Surfing
Swimming
Swinging at the playground
Tai chi
Tennis serve with ball bouncing
Throwing darts
Trimming branches or hedges
Triple jump
Tumbling
Using parallel bars
Using the balance beam
Using the rowing machine
Using uneven bars
Vacuuming floor
Volleyball
Washing hands
Waterskiing
Waxing skis
Welding
Windsurfing
Zumba
