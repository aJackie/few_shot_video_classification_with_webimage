"""
    automatically do mix_mean_std.py for different k in k-shot learning 
"""
import os

for k in range(1, 11):
    os.system("python mix_mean_std.py" + \
              " -m1 /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/image_train_mean_std.out" + \
              " -m2 /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/frame_train_mean_std.out".format(k) + \
              " --img_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet/train" + \
              " --vid_file /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/train.txt".format(k) + \
              " -c /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt" + \
              " -o /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/mixture_meta_test/{}/train_mean_std.out".format(k))
