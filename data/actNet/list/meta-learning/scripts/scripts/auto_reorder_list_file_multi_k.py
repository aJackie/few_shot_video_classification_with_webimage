"""
    automatically run reorder_list_file.py for multi-k train/val/test.txt
"""
import os

files = ["train.txt", "val.txt", "test.txt"]
for k in range(1, 11):
    for f in files:
        os.system("python reorder_list_file.py" + \
                  " -l /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/{}".format(k, f) + \
                  " -c /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/meta_test.txt" + \
                  " -o /home/zxz/transfer_learning/few_shot/data/actNet/list/meta-learning/meta_test/{}/{}".format(k, f))
