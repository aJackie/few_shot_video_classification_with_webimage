"""
    generate N-way k-shot meta-test episodes
"""
import os
import argparse
from tqdm import tqdm
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--data_dir", type=str)
parser.add_argument("-c", "--class_file", type=str)
parser.add_argument("-N", "--Nway", type=int)
parser.add_argument("-k", "--kshot", type=int)
parser.add_argument("-qk", "--query_kshot", type=int)
parser.add_argument("-n", "--episode_num", type=int)
parser.add_argument("-o", "--out_dir", type=str)
args = parser.parse_args()

with open(args.class_file, "r") as f:
    classes = [l.strip() for l in f.readlines()]

train_dir = os.path.join(args.data_dir, "train")
val_dir = os.path.join(args.data_dir, "val")
for n in tqdm(range(args.episode_num)):
    f1 = open(os.path.join(args.out_dir, "train{}.txt".format(n)), "w")
    f2 = open(os.path.join(args.out_dir, "val{}.txt".format(n)), "w")
    f3 = open(os.path.join(args.out_dir, "test{}.txt".format(n)), "w")

    shuffle(classes)
    for idx, cls in enumerate(classes[:args.Nway]):
        train_vids = [os.path.join(train_dir, cls, v) for v in os.listdir(os.path.join(train_dir, cls))]
        val_vids = [os.path.join(val_dir, cls, v) for v in os.listdir(os.path.join(val_dir, cls))]

        shuffle(train_vids)
        shuffle(val_vids)

        for v in train_vids[:args.kshot]:
            f1.write("{}|{}|{}\n".format(v, idx, cls))

        for v in train_vids[args.kshot: args.kshot+args.query_kshot]:
            f2.write("{}|{}|{}\n".format(v, idx, cls))

        for v in val_vids[:args.query_kshot]:
            f3.write("{}|{}|{}\n".format(v, idx, cls))

    f1.close()
    f2.close()
    f3.close()
