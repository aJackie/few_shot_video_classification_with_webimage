"""
    calculate the (weighted)average mean and std value of mixture of web images and few shot video frames
"""
import os
import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-m1", "--mean_file1", type=str, required=True, help="path to mean_std file of web images")
parser.add_argument("-m2", "--mean_file2", type=str, required=True, help="path to mean_std file of few shot frames")
parser.add_argument("--img_dir", type=str, required=True, help="web images root dir, used to count image num")
parser.add_argument("--vid_file", type=str, required=True, help="video frames list file, used to count frame num")
parser.add_argument("-c", "--class_file", type=str, required=True, help="path to class file")
parser.add_argument("-o", "--output_file", type=str, required=True, help="output file to save average mean and std")

def main():
    args = parser.parse_args()
    
    img_mean, img_std = tuple(np.loadtxt(args.mean_file1))
    vid_mean, vid_std = tuple(np.loadtxt(args.mean_file2))

    with open(args.class_file, "r") as f:
        classes = [c.strip() for c in f.readlines()]

    img_count = 0
    for subdir in classes:
        img_count = img_count + len(os.listdir(os.path.join(args.img_dir, subdir)))
    print "total {} images".format(img_count)

    vid_count = 0
    with open(args.vid_file, "r") as f:
        lines = [_.strip() for _ in f.readlines()]

    for line in lines:
        items = line.split("|")
        path = items[0]
        vid_count = vid_count + len(os.listdir(path))

    print "total {} frames".format(vid_count)

    mix_mean = (img_mean * img_count + vid_mean * vid_count) / float(img_count + vid_count)
    mix_std = (img_std * img_count + vid_std * vid_count) / float(img_count + vid_count)

    np.savetxt(args.output_file, np.array([mix_mean, mix_std], dtype=np.float32), fmt="%.8f")

if __name__ == "__main__":
    main()
