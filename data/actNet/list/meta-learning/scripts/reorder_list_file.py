"""
    reorder list file, to make the class-index mapping consistent with class_file
"""
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--list_file", type=str, required=True, help="list file")
parser.add_argument("-c", "--class_file", type=str, required=True, help="class file")
parser.add_argument("-o", "--output_file", type=str, required=True, help="output list file")

def main():
    args = parser.parse_args()
    with open(args.class_file, "r") as f:
        classes = [c.strip() for c in f.readlines()]

    cls_to_idx = {}
    idx_to_cls = {}
    for idx, cls in enumerate(classes):
        cls_to_idx[cls] = idx
        idx_to_cls[idx] = cls

    with open(args.list_file, "r") as f:
        lines = [l.strip() for l in f.readlines()]
    
    paths = [[] for _ in classes]
    for line in lines:
        items = line.split("|") 
        path = items[0]
        cls = items[2]

        idx = cls_to_idx[cls]
        paths[idx].append(path)

    f = open(args.output_file, "w")
    for idx, cls in enumerate(classes):
        ps = paths[idx]
        for p in ps:
            f.write("{}|{}|{}\n".format(p, idx, cls))
    
    f.close()

if __name__ == "__main__":
    main()
        


