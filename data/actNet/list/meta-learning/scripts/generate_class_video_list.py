"""
    generate list of all videos belonging to classes indicated by class_file
"""
import os
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--class_file", type=str, required=True, help="path to class file")
parser.add_argument("-d", "--data_dir", type=str, required=True, help="data dir")
parser.add_argument("-o", "--output_file", type=str, required=True, help="output file")

def main():
    args = parser.parse_args()
    with open(args.class_file, "r") as f:
        classes = [c.strip() for c in f.readlines()]

    f = open(args.output_file, "w")
    for idx, cls in enumerate(tqdm(classes)):
        vids = os.listdir(os.path.join(args.data_dir, cls))

        for v in vids:
            f.write("{}|{}|{}\n".format(os.path.join(args.data_dir, cls, v), idx, cls))
    
    f.close()

if __name__ == "__main__":
    main()

