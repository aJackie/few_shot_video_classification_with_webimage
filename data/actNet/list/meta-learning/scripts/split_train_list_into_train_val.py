"""
    split list of all train data into train list & val list
"""
import os
from tqdm import tqdm
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_file", type=str, required=True, help="input list file path")
parser.add_argument("-o1", "--output_file1", type=str, required=True, help="output list file 1 path(train)")
parser.add_argument("-o2", "--output_file2", type=str, required=True, help="output list file 2 path(val)")
parser.add_argument("-c", "--class_num", type=int, required=True, help="all class num")
parser.add_argument("-r", "--rate", type=str, required=True, help="division rate of train:val, e.g. 2:1")

def main():
    args = parser.parse_args()
    with open(args.input_file, "r") as f:
        lines = [l.strip() for l in f.readlines()]
    
    paths = [[] for _ in range(args.class_num)]
    idx_to_cls = {}
    for line in lines:
        items = line.split("|")
        path = items[0]
        idx = int(items[1])
        cls = items[2]

        paths[idx].append(path)
        idx_to_cls[idx] = cls

    rates = map(lambda x:float(x), args.rate.split(":"))
    rsum = sum(rates)
    rates = map(lambda x:x/rsum, rates)
    print rates

    f1 = open(args.output_file1, "w")
    f2 = open(args.output_file2, "w")

    min_train_num = -1
    for idx, path in enumerate(tqdm(paths)):
        cls = idx_to_cls[idx]
        num = len(path)
        train_num = int(rates[0]*num)

        if min_train_num == -1:
            min_train_num = train_num
        else:
            min_train_num = min(min_train_num, train_num)

        for p in path[:train_num]:
            f1.write("{}|{}|{}\n".format(p, idx, cls))

        for p in path[train_num:]:
            f2.write("{}|{}|{}\n".format(p, idx, cls))

    f1.close()
    f2.close()

    print "min_train_num:", min_train_num

if __name__ == "__main__":
    main()
