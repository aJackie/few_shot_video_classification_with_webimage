"""
    automatic the multi-execution of genetate_few_shot_list.py
"""
import os
root_dir = "/S2/MI/zxz/transfer_learning/data/actNet_frames"
root_output_dir = "/home/zxz/transfer_learning/few_shot/data/actNet/list/frames"
st_train_num = 1
ed_train_num = 20

for train_num in range(st_train_num, ed_train_num+1):
    output_dir = root_output_dir + "/{}".format(train_num)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    os.system("python generate_few_shot_list.py " + \
                " -r {}".format(root_dir) + \
                " -n {}".format(train_num) + \
                " -o {}".format(output_dir))

