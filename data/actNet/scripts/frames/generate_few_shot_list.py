"""
    generate training, validation & testing set list for few shot classification
    NOTE: this only fits activityNet
"""
import os
import sys
import shutil
import argparse
from tqdm import tqdm
from random import shuffle

parser = argparse.ArgumentParser(description="generate training, validation & testing set list for few shot classification",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-r", "--root_dir", type=str, required=True, help="root dir of all video frames, \
                                                                      subdirs should be root_dir/train(val)")
parser.add_argument("-n", "--train_num", type=int, required=True, help="training num of every class of few shot learning")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="output root dir, will output 3 new files named \
                                                                        output_dir/train(val/test).txt")
args = parser.parse_args()

root_dir = args.root_dir
output_dir = args.output_dir
train_num = args.train_num

# keep class-index consistent
all_classes = os.listdir(os.path.join(root_dir, "train"))
all_classes.sort()

min_train_num = 100
f1 = open(os.path.join(output_dir, "train.txt"), "w")
f2 = open(os.path.join(output_dir, "test.txt"), "w")

for idx, cls in enumerate(tqdm(all_classes)):
    cls_videos = os.listdir(os.path.join(root_dir, "train", cls))
    shuffle(cls_videos)
    min_train_num = min(min_train_num, len(cls_videos))
    assert len(cls_videos) > train_num
    
    for cnt, v in enumerate(cls_videos):
        if cnt < train_num:
            f1.write(os.path.join(root_dir, "train", cls, v) + " {} {}\n".format(idx, cls))
        else:
            f2.write(os.path.join(root_dir, "train", cls, v) + " {} {}\n".format(idx, cls))

f1.close()
f2.close()

min_val_num = 100
with open(os.path.join(output_dir, "val.txt"), "w") as f:
    for idx, cls in enumerate(tqdm(all_classes)):
        cls_videos = os.listdir(os.path.join(root_dir, "val", cls))
        min_val_num = min(min_val_num, len(cls_videos))
        for cnt, v in enumerate(cls_videos):
            f.write(os.path.join(root_dir, "val", cls, v) + " {} {}\n".format(idx, cls))

print "min training class num: {}".format(min_train_num)
print "min validation class num: {}".format(min_val_num)
