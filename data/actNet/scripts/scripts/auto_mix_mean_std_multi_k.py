"""
    automatically do mix_mean_std.py for different k in k-shot learning 
"""
import os

for k in range(1, 11):
    os.system("python mix_mean_std.py" + \
              " -m1 /home/zxz/transfer_learning/few_shot/data/actNet/list/webimage/train_mean_std.out" + \
              " -m2 /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/train_mean_std.out".format(k) + \
              " --img_dir /S2/MI/zxz/transfer_learning/data/webimage_actNet/train" + \
              " --vid_file /home/zxz/transfer_learning/few_shot/data/actNet/list/frames/{}/train.txt".format(k) + \
              " -o /home/zxz/transfer_learning/few_shot/data/actNet/list/mixture/{}/train_mean_std.out".format(k))
