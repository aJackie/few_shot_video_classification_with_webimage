"""
    form k-shot training & testing data lists for meta-test set,
    since for fair comparsion, meta-test should be fixed beforehand
"""
import os
from random import shuffle
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--class_file", type=str, required=True, help="path to file containing all classes in meta-test")
parser.add_argument("-d", "--data_dir", type=str, required=True, help="root dir of video data, should have data_dir/train(val)")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="root dir of output kshot data, \
                                                                         for k-shot it would mkdir output_dir/k")
parser.add_argument("-k", "--kshot", type=int, required=True, help="k for k-shot dir")

def test_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

def main():
    args = parser.parse_args()
    with open(args.class_file, "r") as f:
        classes = [c.strip() for c in f.readlines()]

    # random order in all k
    # but should garauntee consistent orders in train & test list 
    # of same k
    shuffle(classes)
    
    k_output_dir = os.path.join(args.output_dir, str(args.kshot))
    test_mkdir(k_output_dir)

    # generate train & val list
    f1 = open(os.path.join(k_output_dir, "train.txt"), "w")
    f2 = open(os.path.join(k_output_dir, "val.txt"), "w")

    train_dir = os.path.join(args.data_dir, "train")
    for idx, c in enumerate(classes):
        vids = os.listdir(os.path.join(train_dir, c))
        shuffle(vids)
        
        for cnt, v in enumerate(vids):
            if cnt < args.kshot:
                f1.write("{}|{}|{}\n".format(os.path.join(train_dir, c, v), idx, c))
            else:
                f2.write("{}|{}|{}\n".format(os.path.join(train_dir, c, v), idx, c))

    f1.close()
    f2.close()

    # generate test list
    f3 = open(os.path.join(k_output_dir, "test.txt"), "w")

    test_dir = os.path.join(args.data_dir, "val")
    for idx, c in enumerate(classes):
        vids = os.listdir(os.path.join(test_dir, c))
        
        for cnt, v in enumerate(vids):
            f3.write("{}|{}|{}\n".format(os.path.join(test_dir, c, v), idx, c))
    
    f3.close()

if __name__ == "__main__":
    main()
    


        


