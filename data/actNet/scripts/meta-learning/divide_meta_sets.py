"""
    divide raw dataset into meta-training, meta-validation & meta-testing sets
"""
import os
from random import shuffle
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-c', "--class_file", type=str, required=True, help="path to file containing all classes")
parser.add_argument('-r', "--rate", type=str, required=True, help="rate of divided train:val:test, \
                                                                   input format should be like 2:1:1")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="path to root dir of output files")

def main():
    args = parser.parse_args()
    with open(args.class_file, "r") as f:
        classes = [c.strip() for c in f.readlines()]

    print classes[:10]

    shuffle(classes)
    print classes[:10]

    rates = map(lambda x: float(x), args.rate.split(":"))
    s = sum(rates)
    rates = map(lambda x: x / s, rates)
    c_num = len(classes)
    nums = map(lambda x: int(x * c_num), rates)
    print nums

    with open(os.path.join(args.output_dir, "meta_train.txt"), "w") as f:
        train_list = classes[:nums[0]]
        train_list.sort()
        for c in train_list:
            f.write(c + "\n")

    with open(os.path.join(args.output_dir, "meta_val.txt"), "w") as f:
        val_list = classes[nums[0]: nums[0]+nums[1]]
        val_list.sort()
        for c in val_list:
            f.write(c + "\n")

    with open(os.path.join(args.output_dir, "meta_test.txt"), "w") as f:
        test_list = classes[nums[0]+nums[1]:]
        test_list.sort()
        for c in test_list:
            f.write(c + "\n")

if __name__ == "__main__":
    main()
