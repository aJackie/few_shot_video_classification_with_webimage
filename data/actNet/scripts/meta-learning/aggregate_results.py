"""
    aggregate results of all testing files to give a overall result
"""
import os
import shutil
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--result_dir", type=str, required=True, help="root dir of result files")
parser.add_argument("-c", "--class_file", type=str, required=True, help="file containing all testing classes")
parser.add_argument("-s", "--start_idx", type=int, required=True, help="start index of result files to count")
parser.add_argument("-e", "--end_idx", type=int, required=True, help="end index of result files to count")
parser.add_argument("-o", "--output_file", type=str, required=True, help="output file")
parser.add_argument("--percent", type=bool, default=False, help="whether the figures are in percentage, x100 if not")
args = parser.parse_args()

with open(args.class_file, "r") as f:
    classes = [l.strip() for l in f.readlines()]

avg_acc1 = 0.
cls_acc1 = {cls:0. for cls in classes}
cls_cnt = {cls:0 for cls in classes}

for idx in tqdm(range(args.start_idx, args.end_idx)):
    result_file = os.path.join(args.result_dir, "result{}.txt".format(idx))

    with open(result_file, "r") as f:
        lines = [l.strip() for l in f.readlines()]

    line = lines[1]
    items = line.split(":")
    assert items[0] == "average top-1 accuracy"
    acc1 = float(items[1])
    avg_acc1 = avg_acc1 + acc1

    for _ in range(-1, -6, -1):
        line = lines[_]
        items = line.split(":")
        cls = items[0]
        assert cls_acc1.has_key(cls)

        items = line.split()
        if items[-2] == "acc1":
            acc1 = float(items[-1])
        elif items[-4] == "acc1":
            acc1 = float(items[-3])
        else:
            raise Exception

        cls_acc1[cls] = cls_acc1[cls] + acc1
        cls_cnt[cls] = cls_cnt[cls] + 1

if not args.percent:
    avg_acc1 = avg_acc1 * 100.
    cls_acc1 = {cls:acc1*100 for cls,acc1 in cls_acc1.items()}

avg_acc1 = avg_acc1 / (args.end_idx - args.start_idx)
cls_acc1 = {cls:acc1/cls_cnt[cls] for cls,acc1 in cls_acc1.items()}

with open(args.output_file, "w") as f:
    print >> f, "avg_acc1: {}".format(avg_acc1)
    print >> f, "-----------------------"
    for cls in classes:
        print >> f, "{}: {}".format(cls, cls_acc1[cls])

print "end"




