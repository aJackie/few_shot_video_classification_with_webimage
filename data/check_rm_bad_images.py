"""
    try to load all images and remove those would cause error
"""
import os
from PIL import Image
import argparse
from tqdm import tqdm
import warnings
warnings.filterwarnings("error")

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dir", type=str, required=True, help="root dir of all classes")
args = parser.parse_args()

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert("RGB")

root_dir = args.dir
cls_dirs = [os.path.join(root_dir, _) for _ in os.listdir(root_dir)]

rm_cnt = 0
for cls_dir in tqdm(cls_dirs):
    images = [os.path.join(cls_dir, _) for _ in os.listdir(cls_dir)]

    for img in images:
        try:
            pil_loader(img)
        except Exception as e:
            print e, " ", img
            os.remove(img)
            rm_cnt += 1

print "{} images removed".format(rm_cnt)

            

    
