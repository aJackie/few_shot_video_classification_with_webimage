"""
    select a subset of all classes
"""
import os
import argparse
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=str, help="path to file containing all classes")
parser.add_argument("-n", "--num", type=int, default=100, help="num of classes in selected subset")
parser.add_argument("-o", "--output", type=str, help="path to output file")
args = parser.parse_args()

with open(args.input, "r") as f:
    lines = f.readlines()
    all_classes = [l.strip() for l in lines]

shuffle(all_classes)
sub_classes = all_classes[:args.num]
sub_classes.sort()

with open(args.output, "w") as f:
    for c in sub_classes:
        f.write(c + "\n")

print "end"

