import os
import sys
import shutil
import argparse
from tqdm import tqdm
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_dir", type=str, required=True, help="raw all file root dir")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="output set root dir, \
                    further divide into output_dir/train/val/test")
parser.add_argument("-r", "--rate", type=str, required=True, help="division rate, e.g. 4:1:1")
args = parser.parse_args()

def test_mkdir(new_dir):
    if not os.path.isdir(new_dir):
        os.mkdir(new_dir)

def mv_lst(file_lst, raw_dir, new_dir):
    test_mkdir(new_dir)
    for f in file_lst:
        shutil.move(os.path.join(raw_dir, f), os.path.join(new_dir, f))

rates = map(int, args.rate.split(":"))
sum_ = sum(rates)
assert sum_ > 0
rates = map(lambda n: n*1./sum_, rates)
print rates

subdirs = os.listdir(args.input_dir)
for subdir in tqdm(subdirs):
    subdir_path = os.path.join(args.input_dir, subdir)
    files = os.listdir(subdir_path)
    shuffle(files)
    
    train_num = int(rates[0] * len(files))
    val_num = int(rates[1] * len(files))
    test_num = int(rates[2] * len(files))

    mv_lst(files[0:train_num], subdir_path, os.path.join(args.output_dir, "train", subdir))
    mv_lst(files[train_num:train_num+val_num], subdir_path, os.path.join(args.output_dir, "val", subdir))
    mv_lst(files[train_num+val_num:len(files)], subdir_path, os.path.join(args.output_dir, "test", subdir))







