"""
    select subset of class-video json file, both classes and videos per class can be downsampled
"""
import os
import json
import argparse
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=str, help="path to raw class-video json file")
parser.add_argument("-c", "--class_file", type=str, help="path to file containing subset of classes")
parser.add_argument("-n", "--num", type=int, help="selected video num in one class")
parser.add_argument("-o", "--output", type=str, help="path to downsampled class-video json file")
args = parser.parse_args()

with open(args.input, "r") as f:
    raw_json = json.loads(f.read())

with open(args.class_file, "r") as f:
    sub_classes = [l.strip() for l in f.readlines()]

new_json = {}
for label in raw_json.keys():
    if label in sub_classes:
        vids = raw_json[label]
        shuffle(vids)
        new_json[label] = vids[:args.num]

with open(args.output, "w") as f:
    f.write(json.dumps(new_json))

print "end"


