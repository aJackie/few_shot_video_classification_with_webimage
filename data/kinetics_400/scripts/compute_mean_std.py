"""
    compute mean and std of training frames
"""
import os
import sys
import argparse
import numpy as np
from tqdm import tqdm
from PIL import Image
from random import shuffle
import warnings

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--root_dir", type=str, required=True, help="root dir of video folders")
parser.add_argument("-n1", "--vid_num", type=int, default=-1, help="num of videos to compute, -1 means all")
parser.add_argument("-n2", "--frm_num", type=int, default=-1, help="num of frames per video to compute, -1 means all")
parser.add_argument("-o", "--output", type=str, required=True, help="output file of record")
args = parser.parse_args()

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

means = []
stds = []

warnings.filterwarnings('error')

folders = [os.path.join(args.root_dir, s) for s in os.listdir(args.root_dir)]
shuffle(folders)
if args.vid_num > -1:
    folders = folders[:args.vid_num]

print "compute mean and std for {}".format(args.root_dir)
print "{} videos, {} frames per video, output in {}".format(args.vid_num, args.frm_num, args.output)

for folder in tqdm(folders):
    frm_paths = [os.path.join(folder, n) for n in os.listdir(folder)]
    shuffle(frm_paths)
    if args.frm_num > -1:
        frm_paths = frm_paths[:args.frm_num]

    sub_means = []
    sub_stds = []
    for path in frm_paths:
        try:
            frm = np.array(pil_loader(path)).astype(np.float32) / 255.
        except Exception as e:
            print >> sys.stderr, path, " ", e 
            #os.remove(path)
        else:
            sub_means.append(np.mean(frm, axis=(0,1)))
            sub_stds.append(np.std(frm, axis=(0,1)))

    if len(sub_means) > 0:
        sub_means = np.array(sub_means, dtype=np.float32)
        sub_stds = np.array(sub_stds, dtype=np.float32)
        avg_submean = np.mean(sub_means, axis=0)
        avg_substd = np.mean(sub_stds, axis=0)

        means.append(avg_submean)
        stds.append(avg_substd)

means = np.array(means, dtype=np.float32)
stds = np.array(stds, dtype=np.float32)

avg_mean = np.mean(means, axis=0)
avg_std = np.mean(stds, axis=0)

with open(args.output, "w") as f:
    print >> f, args.root_dir
    print >> f, "mean: {}".format(avg_mean)
    print >> f, "std: {}".format(avg_std)
