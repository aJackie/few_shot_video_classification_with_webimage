import os
import sys
import time
import argparse
import signal
import shutil

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.utils.model_zoo as model_zoo
import torchvision.models as models
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

from util import *
from datetime import datetime
import multiprocessing as mp
from tqdm import tqdm
import numpy as np

parser = argparse.ArgumentParser(description="extract web image features using pretrained model", 
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-m', "--model", type=str, required=True, help="base model architects,\
                     available choice: resnet18/34/50/101/152")
parser.add_argument('-p', "--pretrain", type=bool, default=False, \
                    help="load pretrained ImageNet model or not")
parser.add_argument("-r", "--resume", type=str, default=None, help="resume path of weight")
parser.add_argument('-b', '--batch_size', default=256, type=int, help="mini-batch size")
parser.add_argument('-c', '--class_num', default=1000, type=int, help="class num of last fc, not used actually but for compatibility")
parser.add_argument('-s', '--size', type=int, default=224, help="input square image edge length")
parser.add_argument('-w', '--workers', type=int, default=8, help="num of data loading workers")
parser.add_argument('-sw', '--save_workers', type=int, default=8, help="num of data saving workers(not used now)")
parser.add_argument('-g', '--gpu', type=str, default="0", help="cuda visible devices")
parser.add_argument('--print_freq', default=10, type=int, help="frequency(of batches) of printing training info")
parser.add_argument("--data_dir", type=str, required=True, help="root dir of data")
parser.add_argument("--save_dir", type=str, required=True, help="root dir of features to save")
parser.add_argument('--torch_model_dir', type=str, required=False, default='/S2/MI/zxz/.torch/models', \
                    help="pytorch model zoo dir of pretrained model(default: /S2/MI/zxz/.torch/models)")

args=parser.parse_args()

# signal handler
# make sure to close writer file before exit
def sigint_handler(sig, frame):
    try:
        writer.close()
    except:
        print("writer not created")
    finally:
        print("exiting...")
        sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)

model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}

def extract(dataset, model):
    imgs = dataset.imgs

    loader = torch.utils.data.DataLoader(
            dataset, batch_size=args.batch_size, shuffle=False,
            num_workers=args.workers
            )

    batch_time = AverageMeter()
    # switch to evaluate mode
    model.eval()

    #pool = mp.Pool(args.save_workers)
    end = time.time()
    with torch.no_grad():
        for i, (input_, target) in enumerate(tqdm(loader)):
            save_paths = [path.replace(args.data_dir, args.save_dir).split(".")[0]+".npy" for path, idx in imgs[i*args.batch_size: (i+1)*args.batch_size]]
            if i == 0:
                print imgs[0]
                print save_paths[0]


            # forward pass
            features = model(input_)
            #print(features.shape)

            # no backward

            # save features async
            for j in range(len(save_paths)):
                np.save(save_paths[j], features.data[j])
            #[pool.apply(np.save, (save_paths[j], features.data[j])) for j in range(args.batch_size)]

            # measure one-batch time
            batch_time.update(time.time() - end)
            end = time.time()

    print("Test finished\n\
           Total_Time: {batch_time.sum:.3f}".format(\
           batch_time=batch_time))

def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    dataset = datasets.ImageFolder(
            args.data_dir, 
            transform=transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(args.size),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.5408135, 0.50624526, 0.4717917],\
                                     std=[0.24714236, 0.24275628, 0.24627835])
                ]))

    print("total classes: {}".format(len(dataset.classes)))

    # create model
    global model
    model = models.__dict__[args.model](pretrained=False, num_classes=args.class_num)

    # manully load pretrained model since only parts of the network configuration the same
    if args.pretrain:
        print("using pre-trained model {}".format(args.model))
        model_url = model_urls[args.model.split('_')[0]]
        model_filename = model_url.split('/')[-1]
        model_filepath = os.path.join(args.torch_model_dir, model_filename)
        if not os.path.exists(model_filepath):
            pretrain_net = model_zoo.load_url(model_url, model_dir=args.torch_model_dir)
        else:
            pretrain_net = torch.load(model_filepath)
        # only use features layer
        pretrain_features = {k:v for k, v in pretrain_net.items() if not (k.startswith('fc') or k.startswith('classifier'))}
        print("pretrain_layers:")
        for k,v in pretrain_features.items():
            print("\t{}: {}".format(k, v.size()))

        model_dict = model.state_dict()
        print("model layers")
        for k,v in model_dict.items():
            print("\t{}: {}".format(k, v.size()))
        
        model_dict.update(pretrain_features)
        model.load_state_dict(model_dict)
        del pretrain_net
        del pretrain_features
        del model_dict
    else:
        print("creating model {}".format(args.model))

    # resume from checkpoint?
    if args.resume:
        if os.path.isfile(args.resume):
            print("loading checkpoint {}".format(args.resume))
            checkpoint = torch.load(args.resume)['state_dict']
            checkpoint = {k[7:]:v for k, v in checkpoint.items()}
            model.load_state_dict(checkpoint)
            print("loaded checkpoint {}".format(args.resume))
            del checkpoint
        else:
            print("no checkpoint found at {}".format(args.resume))

    model = nn.Sequential(*(list(model.children())[:-1]))
    for param in model.parameters():
        param.requires_grad = False

    # set data parallel to use multiple gpus
    #model = model.cuda(0)
    model = torch.nn.DataParallel(model).cuda()

    cudnn.enabled = True
    cudnn.benchmark = True
    
    extract(dataset, model)

if __name__ == "__main__":
    main()
