import os
import sys
import math
import torch
import skvideo.io
import numpy as np

from PIL import Image
from torch.utils.data import Dataset, DataLoader
from util import my_split
from random import shuffle
import torchvision.transforms as transforms

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

class FrameListFolder(Dataset):
    """
        for kinetics400 frame data hierarchy
    """
    def __init__(self, frame_dir, classes, vid_cls, frame_transform=None):
        super(FrameListFolder, self).__init__()
        self.frame_dir = frame_dir
        self.classes = classes
        self.vid_cls = vid_cls
        self.frame_transform = frame_transform
        
        self.class_to_idx = {}
        self.idx_to_class = {}
        for idx, cls in enumerate(self.classes):
            self.class_to_idx[cls] = idx
            self.idx_to_class[idx] = cls

        vid_dirs = []
        for vid in os.listdir(frame_dir):
            id = vid[:11]
            if self.vid_cls.has_key(id):
                vid_dirs.append(os.path.join(frame_dir, vid))

        self.frames = []
        for vid_dir in vid_dirs:
            paths = os.listdir(vid_dir)
            self.frames = self.frames + [os.path.join(vid_dir, p) for p in paths]

        print "total {} classes".format(len(self.classes))
        print "total {} frames".format(len(self.frames))

    def __len__(self):
        return len(self.frames)

    def __getitem__(self, idx):
        path = self.frames[idx]
        sample = pil_loader(path)

        if self.frame_transform is not None:
            sample = self.frame_transform(sample)

        return sample
