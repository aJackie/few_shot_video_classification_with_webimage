"""
    compute mean and std of training web images
"""
import os
import sys
import argparse
import numpy as np
from tqdm import tqdm
from PIL import Image
from random import shuffle
import warnings

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--root_dir", type=str, required=True, help="root dir of training web image folders")
parser.add_argument("-c", "--class_file", type=str, required=True, help="path to file containing all considered classes")
parser.add_argument("-n", "--img_num", type=int, default=-1, help="num of images per class to compute, -1 means all")
parser.add_argument("-o", "--output", type=str, required=True, help="output file of record")
args = parser.parse_args()

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

means = []
stds = []

warnings.filterwarnings('error')

with open(args.class_file, "r") as f:
    classes = [l.strip() for l in f.readlines()]

folders = [os.path.join(args.root_dir, c) for c in classes]
shuffle(folders)

print "compute mean and std for {}".format(args.root_dir)
print "{} classes, output in {}".format(len(classes), args.output)

for folder in tqdm(folders):
    img_paths = [os.path.join(folder, n) for n in os.listdir(folder)]
    shuffle(img_paths)
    if args.img_num > -1:
        img_paths = img_paths[:args.img_num]

    sub_means = []
    sub_stds = []
    for path in img_paths:
        try:
            img = np.array(pil_loader(path)).astype(np.float32) / 255.
        except Exception as e:
            print >> sys.stderr, path, " ", e 
            #os.remove(path)
        else:
            sub_means.append(np.mean(img, axis=(0,1)))
            sub_stds.append(np.std(img, axis=(0,1)))

    if len(sub_means) > 0:
        sub_means = np.array(sub_means, dtype=np.float32)
        sub_stds = np.array(sub_stds, dtype=np.float32)
        avg_submean = np.mean(sub_means, axis=0)
        avg_substd = np.mean(sub_stds, axis=0)

        means.append(avg_submean)
        stds.append(avg_substd)

means = np.array(means, dtype=np.float32)
stds = np.array(stds, dtype=np.float32)

avg_mean = np.mean(means, axis=0)
avg_std = np.mean(stds, axis=0)

with open(args.output, "w") as f:
    print >> f, args.root_dir
    print >> f, "mean: {}".format(avg_mean)
    print >> f, "std: {}".format(avg_std)
