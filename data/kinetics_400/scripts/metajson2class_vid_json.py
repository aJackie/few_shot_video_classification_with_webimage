"""
    convert from raw video-indexed json meta-file to class-indexed json file
"""
import os
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=str, help="path to raw video-indexed json meta-file")
parser.add_argument("-o", "--output", type=str, help="path to output new json file")

args = parser.parse_args()

with open(args.input, "r") as f:
    raw_json = json.loads(f.read())

new_json = {}
for id in raw_json.keys():
    label = raw_json[id]["annotations"]["label"]
    if new_json.has_key(label):
        new_json[label].append(id)
    else:
        new_json[label] = [id]

with open(args.output, "w") as f:
    f.write(json.dumps(new_json))

print "end"
