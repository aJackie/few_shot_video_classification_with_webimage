import os
import torch
import shutil
import numpy as np
from PIL import Image

def pil_loader(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('RGB')

def pil_loader_gs(path):
    with open(path, "rb") as f:
        img = Image.open(f)
        return img.convert('L')

class AverageMeter(object):
    """recording the average and current value of a summation var"""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += self.val * n
        self.count += n
        self.avg = self.sum * 1./ self.count

def save_checkpoint(state, is_best, cp_dir):
    epoch_num = state['epoch']
    filename = os.path.join(cp_dir, "checkpoint_epoch{}.pth.tar".format(epoch_num))
    torch.save(state, filename)
    if is_best:
        shutil.copy(filename, os.path.join(cp_dir, "model_best.pth.tar"))

def accuracy(output, target, topk=(1,)):
    """computes the topk precision"""

    maxk = max(topk)
    batch_size = target.size(0)
    '''
    print("in accuracy function:")
    print("\touput: {}".format(output.size()))
    print("\ttarget: {}".format(target.size()))
    '''

    _,pred = output.topk(maxk, 1, True, True)
    #print("\tpred: {}".format(pred.shape))
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100./batch_size))

    return res

def my_split(line, sign, last_num):
    split = []
    ed = len(line)
    st = ed - 1
    while st >= 0:
        if line[st] == sign:
            split.append(line[st+1:ed])
            ed = st
        st -= 1
        if len(split) >= last_num:
            break
    split.append(line[0:ed])
    split.reverse()
    return split

# for mnist
def sample_mean_std(image_dir, k):
    imgs = []

    for cls in range(10):
        cls_dir = os.path.join(image_dir, str(cls))
        for _ in range(k):
            img = pil_loader_gs(os.path.join(cls_dir, "{}.png".format(_)))
            img = np.array(img, dtype=np.float32) / 255.
            imgs.append(img)

    imgs = np.stack(imgs)
    mean = np.mean(imgs)
    std = np.std(imgs)
    
    return float(mean), float(std)

