"""customized loss for k-hot vector"""
import torch
import torch.nn as nn
import torch.nn.functional as F

class KHotCrossEntropyLoss(nn.Module):
    def __init__(self):
        super(KHotCrossEntropyLoss, self).__init__()
        self.logsoftmax = nn.LogSoftmax(dim=1)

    def forward(self, output, target):
        loss = -1. * self.logsoftmax(output) * target
        loss = torch.sum(loss, dim=1)
        loss = torch.mean(loss)

        return loss
