import os
import sys
import math
import torch
import skvideo.io
import numpy as np

import cv2
from PIL import Image
from torch.utils.data import Dataset, DataLoader
from util import *
from random import shuffle
import torchvision.transforms as transforms

class MnistKShotImageFolder(Dataset):
    """
        used for mnist dataset k-shot learning setting
    """
    def __init__(self, image_dir, k, size, img_transform=None):
        super(MnistKShotImageFolder, self).__init__()
        self.image_dir = image_dir
        self.k = k
        self.size = size
        self.img_transform = img_transform

        self.cls_images = [[] for _ in range(10)]
        for cls in range(10):
            for _ in range(k):
                self.cls_images[cls].append(os.path.join(self.image_dir, str(cls), "{}.png".format(_)))

    def __len__(self):
        return (self.k+1)**10
    
    def __getitem__(self, idx):
        sample = []
        target = np.zeros((10,), dtype=np.float32)
        coins = np.random.rand(10)
        
        for cls in range(10):
            if coins[cls] < 0.5:
                pass
            else:
                cls_imgs = self.cls_images[cls] 
                select = int(np.random.randint(0, len(cls_imgs)))
                img = pil_loader_gs(cls_imgs[select])  
                if self.img_transform is not None:
                    img = self.img_transform(img)
                sample.append(img)
                target[cls] = 1.

        # fill sample with black images(all 0)
        for _ in range(len(sample), 10):
            sample.append(torch.zeros(1, *self.size, dtype=torch.float32))

        # shuffle images
        shuffle(sample)

        sample = torch.cat(sample)

        return sample, target

class MnistKShotImageTestFolder(Dataset):
    """
        used for mnist dataset k-shot learning setting
    """
    def __init__(self, image_dir, k, num, size, img_transform=None):
        super(MnistKShotImageTestFolder, self).__init__()
        self.image_dir = image_dir
        self.k = k
        self.num = num
        self.size = size
        self.img_transform = img_transform

        self.images = []
        for cls in range(10):
            if num is not None:
                for _ in range(k, k+num):
                    self.images.append((os.path.join(self.image_dir, str(cls), "{}.png".format(_)), cls))
            else:
                for _ in range(k, len(os.listdir(os.path.join(self.image_dir, str(cls))))):
                    self.images.append((os.path.join(self.image_dir, str(cls), "{}.png".format(_)), cls))


    def __len__(self):
        return len(self.images)
    
    def __getitem__(self, idx):
        path, cls = self.images[idx]
        sample = []
        target = np.zeros((10,), dtype=np.float32)

        img = pil_loader_gs(path)  
        if self.img_transform is not None:
            img = self.img_transform(img)

        sample.append(img)
        target[cls] = 1.

        # fill sample with black images(all 0)
        for _ in range(len(sample), 10):
            sample.append(torch.zeros(1, *self.size, dtype=torch.float32))

        sample = torch.cat(sample)

        return sample, target, cls

if __name__ == "__main__":
    dataset = MnistKShotImageTestFolder("/S2/MI/zxz/transfer_learning/data/mnist/train", 1, 10, (20, 20), \
                                    img_transform=transforms.Compose([
                                        transforms.CenterCrop(20),
                                        transforms.ToTensor(),
                                        transforms.Normalize(mean=[0.5], std=[0.2])
                                        ]))

    loader = torch.utils.data.DataLoader(
            dataset, batch_size=32, shuffle=False,
            num_workers=1
            )

    for idx, (sample, target, cls) in enumerate(loader):
        print sample.shape, sample.dtype
        print target.shape, target.dtype
        print cls.shape, cls.dtype
        print cls
        print target[0]

