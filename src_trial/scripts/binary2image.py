"""
    convert the data stored in binary file format IDX
    to images origanized by classes
"""
import os
import cv2
import struct as st
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-if", "--image_file", type=str, required=True, help="path to image data binary file stored in IDX format")
parser.add_argument("-lf", "--label_file", type=str, required=True, help="path to label binary file stored in IDX format")
parser.add_argument("-o", "--output_dir", type=str, required=True, help="path to output root dir")
args = parser.parse_args()

filename = {"images": args.image_file, "labels": args.label_file}

# for image file
train_imagesfile = open(filename["images"], "rb")
train_imagesfile.seek(0)
print "reading image file"

magic = st.unpack('>I', train_imagesfile.read(4))
print "magic number: ", magic

img_num = st.unpack(">I", train_imagesfile.read(4))[0]
row_num = st.unpack(">I", train_imagesfile.read(4))[0]
col_num = st.unpack(">I", train_imagesfile.read(4))[0]
print "images num: ", img_num
print "rows num: ", row_num
print "cols num: ", col_num

nBytesTotal = img_num * row_num * col_num * 1
imgs = 255 - np.asarray(st.unpack(">"+"B"*nBytesTotal, train_imagesfile.read(nBytesTotal)), dtype=np.uint8).\
            reshape(img_num, row_num, col_num)

train_imagesfile.close()

# for label file
train_labelsfile = open(filename["labels"], "rb")
train_labelsfile.seek(0)
print "reading label file"

magic = st.unpack(">I", train_labelsfile.read(4))
print "magic number: ", magic

label_num = st.unpack(">I", train_labelsfile.read(4))[0]
print "label num: ", label_num

nBytesTotal = label_num * 1
labels = np.asarray(st.unpack(">"+"B"*nBytesTotal, train_labelsfile.read(nBytesTotal)), dtype=np.int8).reshape(label_num, )

train_labelsfile.close()

# save image 
cls_img_num = [0 for _ in range(10)]
assert label_num == img_num

for _ in range(img_num):
    label = labels[_]
    img = imgs[_]
    cls_dir = os.path.join(args.output_dir, str(label))
    if not os.path.isdir(cls_dir):
        os.mkdir(cls_dir)

    idx = cls_img_num[label]
    cls_img_num[label] = idx + 1
    cv2.imwrite(os.path.join(cls_dir, "{}.png".format(idx)), img)

print "cls_img_num: ", cls_img_num
